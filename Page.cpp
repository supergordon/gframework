#include "Page.h"
#include "Process.h"
#include "NT.h"
#include "PE.h"

namespace GF {

	Page::Page(DWORD64 BaseAddress, DWORD64 AllocationBase, DWORD AllocationProtect,
		DWORD64 RegionSize, DWORD State, DWORD Protect, DWORD Type)
	{
		base_address = BaseAddress;
		allocation_base = AllocationBase;
		allocation_protect = AllocationProtect;
		size = RegionSize;
		state = State;
		protect = Protect;
		type = Type;
	}

	DWORD64 Page::Size()
	{
		return size;
	}

	DWORD64 Page::Address()
	{
		return base_address;
	}

	DWORD64 Page::AllocationBase()
	{
		return allocation_base;
	}

	DWORD Page::Type()
	{
		return type;
	}

	DWORD Page::State()
	{
		return state;
	}

	DWORD Page::Protect()
	{
		return protect;
	}

	DWORD Page::AllocationProtect()
	{
		return allocation_protect;
	}

	bool Page::MemCommit()
	{
		return (state & MEM_COMMIT) != 0;
	}

	bool Page::MemFree()
	{
		return (state & MEM_FREE) != 0;
	}

	bool Page::MemReserve()
	{
		return (state & MEM_RESERVE) != 0;
	}

	bool Page::MemImage()
	{
		return (type & MEM_IMAGE) != 0;
	}

	bool Page::MemMapped()
	{
		return (type & MEM_MAPPED) != 0;
	}

	bool Page::MemPrivate()
	{
		return (type & MEM_PRIVATE) != 0;
	}

	bool Page::PageExecute()
	{
		return (allocation_protect & PAGE_EXECUTE) != 0;
	}

	bool Page::PageExecuteRead()
	{
		return (allocation_protect & PAGE_EXECUTE_READ) != 0;
	}

	bool Page::PageExecuteReadWrite()
	{
		return (allocation_protect & PAGE_EXECUTE_READWRITE) != 0;
	}

	bool Page::PageExecuteWriteCopy()
	{
		return (allocation_protect & PAGE_EXECUTE_WRITECOPY) != 0;
	}

	bool Page::PageNoAccess()
	{
		return (allocation_protect & PAGE_NOACCESS) != 0;
	}

	bool Page::PageReadOnly()
	{
		return (allocation_protect & PAGE_READONLY) != 0;
	}

	bool Page::PageReadWrite()
	{
		return (allocation_protect & PAGE_READWRITE) != 0;
	}

	bool Page::PageWriteCopy()
	{
		return (allocation_protect & PAGE_WRITECOPY) != 0;
	}

	bool Page::PageGuard()
	{
		return (allocation_protect & PAGE_GUARD) != 0;
	}

	bool Page::PageNoCache()
	{
		return (allocation_protect & PAGE_NOCACHE) != 0;
	}

	bool Page::PageWriteCombine()
	{
		return (allocation_protect & PAGE_WRITECOMBINE) != 0;
	}

	PageList PageManager::Enumerate(DWORD process_id)
	{
		PageList buf;

		HANDLE handle = OpenProcess(PROCESS_QUERY_INFORMATION, FALSE, process_id);
		if (!handle)
			return buf;

#ifdef _AMD64_
		MEMORY_BASIC_INFORMATION64 mbi = { 0 };
		SIZE_T dwLength = 0;
		while (NT::ZwQueryVirtualMemory(handle, PVOID(DWORD_PTR(mbi.BaseAddress) + mbi.RegionSize), NT::MemoryBasicInformation, &mbi, sizeof(mbi), &dwLength) == 0) {
			buf.push_back(new Page(mbi.BaseAddress, mbi.AllocationBase, mbi.AllocationProtect, mbi.RegionSize, mbi.State, mbi.Protect, mbi.Type));
		}
#else
		MEMORY_BASIC_INFORMATION64 mbi = { 0 };
		DWORD64 dwLength = 0;
		while (NT::ZwWow64QueryVirtualMemory64(handle, PVOID64(DWORD64(mbi.BaseAddress) + mbi.RegionSize), NT::MemoryBasicInformation, &mbi, sizeof(mbi), &dwLength) == 0) {
			buf.push_back(new Page(mbi.BaseAddress, mbi.AllocationBase, mbi.AllocationProtect, mbi.RegionSize, mbi.State, mbi.Protect, mbi.Type));
		}
#endif

		CloseHandle(handle);

		return buf;
	}

	int PageManager::FreeList(PageList &pl)
	{
		int num_destroyed = 0;

		for (PageListIterator it = pl.begin(); it != pl.end(); it++, num_destroyed++)
			delete *it;

		pl.clear();

		return num_destroyed;
	}

	Page *PageManager::GetByAddress(DWORD process_id, DWORD64 address)
	{
		UNREFERENCED_PARAMETER(process_id);
		UNREFERENCED_PARAMETER(address);

		return NULL;
	}

	bool PageManager::IsValid(DWORD process_id, DWORD64 address)
	{
		UNREFERENCED_PARAMETER(process_id);
		UNREFERENCED_PARAMETER(address);

		return true;
	}

}