#include "Service.h"

namespace GF {

	SC_HANDLE scmgr = NULL;

	Service::Service(const GString &service_name, const GString &display_name, SERVICE_STATUS_PROCESS *p_ssp)
	{
		name.clear();
		display.clear();

		memset(&ssp, 0, sizeof(ssp));

		name = service_name;
		if (display_name.length())
			display = display_name;

		if (p_ssp)
			memcpy(&ssp, p_ssp, sizeof(ssp));

		SC_HANDLE handle = OpenService(ServiceManager::GetSCManager(), service_name.c_str(), SERVICE_QUERY_CONFIG); //40ms
		if (!handle)
			return;

#ifdef _UNICODE
		QUERY_SERVICE_CONFIGW *qsc = (QUERY_SERVICE_CONFIGW*)malloc(8192);
#else
		QUERY_SERVICE_CONFIGA *qsc = (QUERY_SERVICE_CONFIGA*)malloc(8192);
#endif
		DWORD dwLen = 0;

		BOOL ret = QueryServiceConfig(handle, qsc, 8192, &dwLen); //60ms
		if (!ret) {
			CloseServiceHandle(handle);
			return;
		}

		path = qsc->lpBinaryPathName;
		loadordergroup = qsc->lpLoadOrderGroup;
		dependencies = qsc->lpDependencies;
		start_name = qsc->lpServiceStartName;

		start_type = qsc->dwStartType;
		error_control = qsc->dwErrorControl;
		tag_id = qsc->dwTagId;

		GChar windows_dir[MAX_PATH] = { 0 };
		GetWindowsDirectory(windows_dir, MAX_PATH);

		if (path[0] == '\"')
			path.erase(0, 1);

		if (path[path.length() - 1] == '\"')
			path.erase(path.length() - 1, 1);

		if (path[path.length() - 1] == ' ')
			path.erase(path.length() - 1, 1);

		size_t res = path.find(_G("\\\\"));
		if (res != GString::npos) {
			path.erase(res, 1);
		}

		res = path.find(_G(".exe"));
		if (res != GString::npos)
			path.erase(res + 4);

		res = path.find(_G("\\SystemRoot\\SystemRoot\\"));
		if (res != GString::npos) {
			path.erase(res, 22);
			path.insert(0, windows_dir);
		}

		res = path.find(_G("\\SystemRoot\\"));
		if (res != GString::npos) {
			path.erase(res, 11);
			path.insert(0, windows_dir);
		}

		res = path.find(_G("system32\\"), 0, 9);
		if (res != GString::npos && res == 0) {
			GSprintf(windows_dir, _G("%s\\"), windows_dir);
			path.insert(0, windows_dir);
		}

		res = path.find(_G("System32\\"), 0, 9);
		if (res != GString::npos && res == 0) {
			GSprintf(windows_dir, _G("%s\\"), windows_dir);
			path.insert(0, windows_dir);
		}

		res = path.find(_G("\\??\\"));
		if (res != GString::npos) {
			path.erase(res, 4);
		}

		free(qsc);

		CloseServiceHandle(handle); //17ms
	}


	const GString &Service::Name()
	{
		return name;
	}

	const GString &Service::DisplayName()
	{
		return display;
	}

	const GString &Service::Path()
	{
		return path;
	}

	const GString &Service::LoadOrderGroup()
	{
		return loadordergroup;
	}

	const GString &Service::Dependencies()
	{
		return dependencies;
	}

	const GString &Service::StartName()
	{
		return start_name;
	}

	GString Service::TypeName()
	{
		if (Type() & SERVICE_KERNEL_DRIVER)
			return _G("Kernel");
		else if (Type() & SERVICE_FILE_SYSTEM_DRIVER)
			return _G("File system");
		else if (Type() & SERVICE_WIN32_OWN_PROCESS)
			return _G("Own Process");
		else if (Type() & SERVICE_WIN32_SHARE_PROCESS)
			return _G("Share Process");
		else
			return GEmptyString;
	}

	GString Service::StartTypeName()
	{
		GChar *type[] = { _G("Boot Start"), _G("System Start"), _G("Auto Start"), _G("Demand Start"), _G("Disabled") };

		return type[start_type];
	}

	GString Service::ErrorControlName()
	{
		GChar *error[] = { _G("Ignore"), _G("Normal"), _G("Serve"), _G("Critical") };

		return error[error_control];
	}

	GString Service::CurrentStateName()
	{
		GChar *state[] = { _G("Unknown"), _G("Stopped"), _G("Starting"), _G("Stopping"), _G("Running"),
			_G("Continuing"), _G("Pausing"), _G("Paused") };

		return state[CurrentState()];
	}

	SERVICE_STATUS_PROCESS *Service::Status()
	{
		return &ssp;
	}

	DWORD Service::Type()
	{
		return ssp.dwServiceType;
	}

	DWORD Service::StartType()
	{
		return start_type;
	}

	DWORD Service::ErrorControl()
	{
		return error_control;
	}

	DWORD Service::CurrentState()
	{
		return ssp.dwCurrentState;
	}

	DWORD Service::TagId()
	{
		return tag_id;
	}

	DWORD Service::ProcessId()
	{
		return ssp.dwProcessId;
	}

	DWORD Service::Exitcode()
	{
		DWORD ret = 0;

		if (ssp.dwWin32ExitCode != ERROR_SERVICE_SPECIFIC_ERROR)
			ret = ssp.dwWin32ExitCode;
		else
			ret = ssp.dwServiceSpecificExitCode;

		return ret;
	}

	DWORD Service::ControlsAccepted()
	{
		return ssp.dwControlsAccepted;
	}

	bool Service::RunsInSystemProcess()
	{
		return ssp.dwServiceFlags == SERVICE_RUNS_IN_SYSTEM_PROCESS;
	}

	bool Service::Start()
	{
		return ServiceManager::StartService_(name);
	}

	bool Service::Stop()
	{
		return ServiceManager::StopService_(name);
	}

	bool Service::Unregister()
	{
		return ServiceManager::UnregisterService_(name);
	}

	ServiceList ServiceManager::Enumerate()
	{
		ServiceList buf;

		SC_HANDLE handle;
		ENUM_SERVICE_STATUS_PROCESS estp, *pServices = NULL, *tmp;
		DWORD dwBytesNeeded = 0, dwServicesReturned = 0, dwResumedHandle = 0, dwBytes = 0;

		handle = GetSCManager();

		if (handle == NULL)
			return buf;

		BOOL ret = EnumServicesStatusEx(handle, SC_ENUM_PROCESS_INFO, SERVICE_DRIVER | SERVICE_WIN32, SERVICE_STATE_ALL,
			(PBYTE)&estp, sizeof(ENUM_SERVICE_STATUS_PROCESS), &dwBytesNeeded, &dwServicesReturned, &dwResumedHandle, 0);

		if (ret != 0 && GetLastError() != ERROR_MORE_DATA) {
			return buf;
		}

		dwBytes = sizeof(ENUM_SERVICE_STATUS_PROCESS)+dwBytesNeeded;
		pServices = new ENUM_SERVICE_STATUS_PROCESS[dwBytes];

		EnumServicesStatusEx(handle, SC_ENUM_PROCESS_INFO, SERVICE_DRIVER | SERVICE_WIN32, SERVICE_STATE_ALL,
			(PBYTE)pServices, dwBytes, &dwBytesNeeded, &dwServicesReturned, &dwResumedHandle, 0);

		for (DWORD i = 0; i < dwServicesReturned; i++) {
			tmp = pServices + i;

			buf.push_back(new Service((GChar*)tmp->lpServiceName, (GChar*)tmp->lpDisplayName, &tmp->ServiceStatusProcess));
		}

		delete[] pServices;

		return buf;
	}

	int ServiceManager::FreeList(ServiceList &sl)
	{
		int num_destroyed = 0;

		for (ServiceListIterator it = sl.begin(); it != sl.end(); it++, num_destroyed++)
			delete *it;

		sl.clear();

		return num_destroyed;
	}

	Service *ServiceManager::GetByName(const GString &service_name, ServiceList *sl, bool copy)
	{
		ServiceList list = sl ? *sl : ServiceManager::Enumerate();
		Service *s = nullptr;

		for (ServiceListIterator it = list.begin(); it != list.end(); it++) {
			if (service_name != (*it)->Name())
				continue;

			if (copy)
				s = new Service((*it)->Name(), (*it)->DisplayName(), (*it)->Status());
			else
				s = *it;
		}

		if (!sl)
			FreeList(list);

		return s;
	}

	SC_HANDLE ServiceManager::GetSCManager()
	{
		if (!scmgr)
			scmgr = OpenSCManager(0, 0, SC_MANAGER_ALL_ACCESS);

		return scmgr;
	}

	bool ServiceManager::StartService_(const GString &service_name)
	{
		SC_HANDLE handle = OpenService(GetSCManager(), service_name.c_str(), SERVICE_START);

		if (!handle)
			return false;

		BOOL ret = ::StartService(handle, NULL, NULL);

		CloseServiceHandle(handle);

		return ret != 0;
	}

	bool ServiceManager::StopService_(const GString &service_name)
	{
		SC_HANDLE handle = OpenService(GetSCManager(), service_name.c_str(), SERVICE_STOP);

		if (!handle)
			return false;

		SERVICE_STATUS st = { 0 };
		BOOL ret = ControlService(handle, SERVICE_CONTROL_STOP, &st);

		CloseServiceHandle(handle);

		if (!ret)
			return false;

		if (st.dwCurrentState == SERVICE_RUNNING)
			return false;

		return true;
	}

	bool ServiceManager::RegisterService_(const GString &service_name, const GString &file_path)
	{
		SC_HANDLE handle = CreateService(GetSCManager(), service_name.c_str(), service_name.c_str(),
			SERVICE_ALL_ACCESS, SERVICE_KERNEL_DRIVER, SERVICE_DEMAND_START, SERVICE_ERROR_NORMAL,
			file_path.c_str(), NULL, NULL, NULL, NULL, NULL);

		if (!handle)
			return false;

		CloseServiceHandle(handle);

		return true;
	}

	bool ServiceManager::UnregisterService_(const GString &service_name)
	{
		SC_HANDLE handle = OpenService(GetSCManager(), service_name.c_str(), DELETE);

		if (!handle)
			return false;

		BOOL ret = DeleteService(handle);

		CloseServiceHandle(handle);

		return ret != 0;
	}

	bool ServiceManager::LoadService(const GString &service_name, const GString &file_path)
	{
		return RegisterService_(service_name, file_path) && StartService_(service_name);
	}

	bool ServiceManager::UnloadService(const GString &service_name)
	{
		return StopService_(service_name) && UnregisterService_(service_name);
	}

	bool ServiceManager::IsValid(const GString &service_name)
	{
		//TODO: add functionality

		return service_name.length() > 0;
	}

	bool ServiceEnum::Add(Service *p)
	{
		for (ServiceListIterator it = list.begin(); it != list.end(); it++)
			if (p->Name() == (*it)->Name())
				return false;

		if (!ServiceManager::IsValid(p->Name()))
			return false;

		list.push_back(new Service(p->Name(), p->DisplayName(), p->Status()));

		return true;
	}

	ServiceList ServiceEnum::Added()
	{
		ServiceList buf = ServiceManager::Enumerate();
		ServiceList added;

		for (ServiceListIterator it = buf.begin(); it != buf.end(); it++)
			if (Add(*it))
				added.push_back(new Service((*it)->Name(), (*it)->DisplayName(), (*it)->Status()));

		ServiceManager::FreeList(buf);

		return added;
	}

	ServiceList ServiceEnum::Removed()
	{
		ServiceList removed;

		for (ServiceListIterator it = list.begin(); it != list.end(); it++) {
			if (!ServiceManager::IsValid((*it)->Name())) {
				removed.push_back(new Service((*it)->Name(), (*it)->DisplayName(), (*it)->Status()));
				delete *it;
				it = list.erase(it);
			}
		}

		return removed;
	}

	ServiceList ServiceEnum::Updated()
	{
		ServiceList added = Added();
		ServiceList removed = Removed();
		ServiceList updated;

		for (ServiceListIterator it = added.begin(); it != added.end(); it++)
			updated.push_back(new Service((*it)->Name(), (*it)->DisplayName(), (*it)->Status()));

		for (ServiceListIterator it = removed.begin(); it != removed.end(); it++)
			updated.push_back(new Service((*it)->Name(), (*it)->DisplayName(), (*it)->Status()));

		ServiceManager::FreeList(added);
		ServiceManager::FreeList(removed);

		return updated;
	}

	bool ServiceEnum::HasUpdated(const ServiceList &updated)
	{
		return updated.size() > 0;
	}

	ServiceList &ServiceEnum::List()
	{
		return list;
	}

	void ServiceEnum::Clear()
	{
		ServiceManager::FreeList(list);
	}

}