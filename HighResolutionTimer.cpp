#include <windows.h>
#include "HighResolutionTimer.h"

namespace GF {

	HighResolutionTimer::HighResolutionTimer()
	{
		started = 0;
	}

	double HighResolutionTimer::now_s()
	{
		LARGE_INTEGER li;
		QueryPerformanceFrequency(&li);
		double freq = 1.0 / static_cast<double>(li.QuadPart);

		QueryPerformanceCounter(&li);
		return li.QuadPart * freq;
	}

	double HighResolutionTimer::now_ms()
	{
		return now_s() * 1000;
	}

	double HighResolutionTimer::now_us()
	{
		return now_ms() * 1000;
	}

	double HighResolutionTimer::now_ns()
	{
		return now_us() * 1000;
	}

	void HighResolutionTimer::start()
	{
		started = true;
		start_time = now_s();
	}

	void HighResolutionTimer::reset()
	{
		start();
	}

	void HighResolutionTimer::stop()
	{
		cur = now_s() - start_time;
		started = false;
	}

	double HighResolutionTimer::s()
	{
		if (started)
			cur = now_s() - start_time;

		return cur;
	}

	double HighResolutionTimer::ms()
	{
		return s() * 1000;
	}

	double HighResolutionTimer::us()
	{
		return ms() * 1000;
	}

	double HighResolutionTimer::ns()
	{
		return us() * 1000;
	}

}