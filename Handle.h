#ifndef __GHANDLE__
#define __GHANDLE__

#include <list>
#include <windows.h>
#include "GString.h"

namespace GF {

	class Handle {
	public:
		Handle(USHORT Handle, DWORD Process_id, int Type, void *Object, DWORD Access);

		USHORT Handle_();
		DWORD Access();
		DWORD ProcessId();
		int Type();
		void *Object();
		GString Value();
		GString Typename();
	private:
		USHORT handle;
		DWORD access;
		DWORD pid;
		int type;
		void *object;
		GString value;
	};

	typedef std::list<Handle*> HandleList;
	typedef std::list<Handle*>::iterator HandleListIterator;

	class HandleManager {
	public:
		static HandleList Enumerate(DWORD pid = -1);
		static int FreeList(HandleList &hl);

		static HandleList GetByProcessHandle(USHORT handle, HandleList *hl = NULL);
		static HandleList GetByProcessId(DWORD pid, HandleList *hl = NULL);
		static HandleList GetByHandleProcessId(USHORT handle, DWORD pid, HandleList *hl = NULL);

		DWORD GetHandleCount();

		static bool IsValid(USHORT Handle, DWORD process_id);
	private:
	};

	class HandleEnum {
	public:
		HandleEnum(DWORD process_id = -1);
		bool Add(Handle *t);
		HandleList Added();
		HandleList Removed();
		HandleList Updated();
		bool HasUpdated(const HandleList &updated);
		HandleList &List();
		void Clear();
		DWORD Pid();
	private:
		HandleList list;
		DWORD pid;
	};

	typedef struct _GSYSTEM_HANDLE {
		ULONG ProcessId;
		BYTE ObjectTypeNumber;
		BYTE Flags;
		USHORT Handle;
		PVOID Object;
		ACCESS_MASK GrantedAccess;
	} GSYSTEM_HANDLE, *GPSYSTEM_HANDLE;

	typedef struct _GSYSTEM_HANDLE_INFORMATION {
		ULONG HandleCount;
		GSYSTEM_HANDLE Handles[1];
	} GSYSTEM_HANDLE_INFORMATION, *GPSYSTEM_HANDLE_INFORMATION;

}

#undef GetFinalPathNameByHandle
#ifdef _UNICODE
#define GetFinalPathNameByHandle GetFinalPathNameByHandleW
#else
#define GetFinalPathNameByHandle GetFinalPathNameByHandleA
#endif

#endif