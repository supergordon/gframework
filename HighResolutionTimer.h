#ifndef __GHIGH_RESOLUTION_TIMER__
#define __GHIGH_RESOLUTION_TIMER__

namespace GF {

	class HighResolutionTimer {
	public:
		HighResolutionTimer();

		static double now_s();
		static double now_ms();
		static double now_us();
		static double now_ns();

		void start();
		void reset();
		void stop();

		double s();
		double ms();
		double us();
		double ns();
	private:
		double start_time;
		double cur;
		bool started;
	};

}

#endif