#ifndef __G_UNDOCUMENTED_NT__
#define __G_UNDOCUMENTED_NT__

#include <windows.h>

#pragma warning(disable: 4311 4312)

namespace GF {

	class NT {
	public:

		typedef LONG NTSTATUS;
		typedef ULONG ACCESS_MASK;
		typedef NTSTATUS *PNTSTATUS;

		#define NT_SUCCESS(Status) ((NTSTATUS)(Status) >= 0)
		#define STATUS_INFO_LENGTH_MISMATCH ((NTSTATUS)0xC0000004L)

		typedef struct _CLIENT_ID32 {
			__int32 ProcessId; //0x0000 
			__int32 ThreadId; //0x0004 
		} CLIENT_ID32, *PCLIENT_ID32; //Size=0x0008

		typedef struct _CLIENT_ID64 {
			__int64 ProcessId; //0x0000 
			__int64 ThreadId; //0x0008 
		} CLIENT_ID64, *PCLIENT_ID64; //Size=0x0010

		typedef enum _PROCESS_INFORMATION_CLASS {
			ProcessBasicInformation,
			ProcessQuotaLimits,
			ProcessIoCounters,
			ProcessVmCounters,
			ProcessTimes,
			ProcessBasePriority,
			ProcessRaisePriority,
			ProcessDebugPort,
			ProcessExceptionPort,
			ProcessAccessToken,
			ProcessLdtInformation,
			ProcessLdtSize,
			ProcessDefaultHardErrorMode,
			ProcessIoPortHandlers,
			ProcessPooledUsageAndLimits,
			ProcessWorkingSetWatch,
			ProcessUserModeIOPL,
			ProcessEnableAlignmentFaultFixup,
			ProcessPriorityClass,
			ProcessWx86Information,
			ProcessHandleCount,
			ProcessAffinityMask,
			ProcessPriorityBoost,
			MaxProcessInfoClass,
		} PROCESS_INFORMATION_CLASS, *PPROCESS_INFORMATION_CLASS;

		typedef struct _MEMORY_BASIC_INFORMATION32 {
			DWORD BaseAddress;
			DWORD AllocationBase;
			DWORD AllocationProtect;
			DWORD RegionSize;
			DWORD State;
			DWORD Protect;
			DWORD Type;
		} MEMORY_BASIC_INFORMATION32, *PMEMORY_BASIC_INFORMATION32;

		typedef struct DECLSPEC_ALIGN(16) _MEMORY_BASIC_INFORMATION64{
			ULONGLONG BaseAddress;
			ULONGLONG AllocationBase;
			DWORD     AllocationProtect;
			DWORD     __alignment1;
			ULONGLONG RegionSize;
			DWORD     State;
			DWORD     Protect;
			DWORD     Type;
			DWORD     __alignment2;
		} MEMORY_BASIC_INFORMATION64, *PMEMORY_BASIC_INFORMATION64;

		typedef struct _UNICODE_STRING {
			USHORT Length;
			USHORT MaximumLength;
			PWSTR  Buffer;
		} UNICODE_STRING, *PUNICODE_STRING;

		typedef struct _UNICODE_STRING_WOW64 {
			USHORT Length;
			USHORT MaximumLength;
			PVOID64 Buffer;
		} UNICODE_STRING_WOW64;

		typedef struct _OBJECT_ATTRIBUTES {
			ULONG           Length;
			HANDLE          RootDirectory;
			PUNICODE_STRING ObjectName;
			ULONG           Attributes;
			PVOID           SecurityDescriptor;
			PVOID           SecurityQualityOfService;
		}  OBJECT_ATTRIBUTES, *POBJECT_ATTRIBUTES;

		typedef LONG KPRIORITY;

		typedef struct _PROCESS_BASIC_INFORMATION_WOW64 {
			DWORD Reserved1[2];
			PVOID64 PebBaseAddress;
			DWORD Reserved2[4];
			DWORD UniqueProcessId[2];
			DWORD Reserved3[2];
		} PROCESS_BASIC_INFORMATION_WOW64;

		typedef struct _CLIENT_ID {
			DWORD UniqueProcess;
			DWORD UniqueThread;
		} CLIENT_ID, *PCLIENT_ID;

		typedef enum _MEMORY_INFORMATION_CLASS {
			MemoryBasicInformation,
			MemoryWorkingSetList,
			MemorySectionName,
			MemoryBasicVlmInformation,
			MemoryWorkingSetExList
		} MEMORY_INFORMATION_CLASS;

		typedef enum _THREADINFOCLASS {
			ThreadBasicInformation,
			ThreadTimes,
			ThreadPriority,
			ThreadBasePriority,
			ThreadAffinityMask,
			ThreadImpersonationToken,
			ThreadDescriptorTableEntry,
			ThreadEnableAlignmentFaultFixup,
			ThreadEventPair_Reusable,
			ThreadQuerySetWin32StartAddress,
			ThreadZeroTlsCell,
			ThreadPerformanceCount,
			ThreadAmILastThread,
			ThreadIdealProcessor,
			ThreadPriorityBoost,
			ThreadSetTlsArrayAddress,
			ThreadIsIoPending,
			ThreadHideFromDebugger,
			ThreadBreakOnTermination,
			ThreadSwitchLegacyState,
			ThreadIsTerminated,
			ThreadLastSystemCall,
			ThreadIoPriority,
			ThreadCycleTime,
			ThreadPagePriority,
			ThreadActualBasePriority,
			ThreadTebInformation,
			ThreadCSwitchMon,
			ThreadCSwitchPmu,
			ThreadWow64Context,
			ThreadGroupInformation,
			ThreadUmsInformation,
			ThreadCounterProfiling,
			ThreadIdealProcessorEx,
			MaxThreadInfoClass
		} THREADINFOCLASS;

		typedef struct _THREAD_BASIC_INFORMATION {
			NTSTATUS   ExitStatus;
			PVOID64      TebBaseAddress;
			CLIENT_ID64  ClientId;
			KAFFINITY  AffinityMask;
			KPRIORITY  Priority;
			KPRIORITY  BasePriority;
		} THREAD_BASIC_INFORMATION, *PTHREAD_BASIC_INFORMATION;

		typedef struct _THREAD_BASIC_INFORMATION_WOW64 {
			NTSTATUS   ExitStatus;
			DWORD TebBaseAddress;
			CLIENT_ID32  ClientId;
			KAFFINITY  AffinityMask;
			KPRIORITY  Priority;
			KPRIORITY  BasePriority;
		} THREAD_BASIC_INFORMATION_WOW64, *PTHREAD_BASIC_INFORMATION_WOW64;

		typedef struct _STRING {
			WORD Length;
			WORD MaximumLength;
			CHAR * Buffer;
		} STRING, *PSTRING;

		typedef struct _RTL_DRIVE_LETTER_CURDIR {
			WORD Flags;
			WORD Length;
			ULONG TimeStamp;
			STRING DosPath;
		} RTL_DRIVE_LETTER_CURDIR, *PRTL_DRIVE_LETTER_CURDIR;

		typedef struct _RTL_USER_PROCESS_PARAMETERS {
			ULONG                   MaximumLength;
			ULONG                   Length;
			ULONG                   Flags;
			ULONG                   DebugFlags;
			PVOID                   ConsoleHandle;
			ULONG                   ConsoleFlags;
			HANDLE                  StdInputHandle;
			HANDLE                  StdOutputHandle;
			HANDLE                  StdErrorHandle;
			UNICODE_STRING          CurrentDirectoryPath;
			HANDLE                  CurrentDirectoryHandle;
			UNICODE_STRING          DllPath;
			UNICODE_STRING          ImagePathName;
			UNICODE_STRING          CommandLine;
			PVOID                   Environment;
			ULONG                   StartingPositionLeft;
			ULONG                   StartingPositionTop;
			ULONG                   Width;
			ULONG                   Height;
			ULONG                   CharWidth;
			ULONG                   CharHeight;
			ULONG                   ConsoleTextAttributes;
			ULONG                   WindowFlags;
			ULONG                   ShowWindowFlags;
			UNICODE_STRING          WindowTitle;
			UNICODE_STRING          DesktopName;
			UNICODE_STRING          ShellInfo;
			UNICODE_STRING          RuntimeData;
			RTL_DRIVE_LETTER_CURDIR DLCurrentDirectory[0x20];

		} RTL_USER_PROCESS_PARAMETERS, *PRTL_USER_PROCESS_PARAMETERS;

		typedef struct _IO_STATUS_BLOCK {
			union {
				NTSTATUS Status;
				PVOID    Pointer;
			};
			ULONG_PTR Information;
		} IO_STATUS_BLOCK, *PIO_STATUS_BLOCK;

		typedef struct _SECTION_IMAGE_INFORMATION {
			PVOID                   EntryPoint;
			ULONG                   StackZeroBits;
			ULONG                   StackReserved;
			ULONG                   StackCommit;
			ULONG                   ImageSubsystem;
			WORD                    SubSystemVersionLow;
			WORD                    SubSystemVersionHigh;
			ULONG                   Unknown1;
			ULONG                   ImageCharacteristics;
			ULONG                   ImageMachineType;
			ULONG                   Unknown2[3];
		} SECTION_IMAGE_INFORMATION, *PSECTION_IMAGE_INFORMATION;

		typedef struct _SECTION_BASIC_INFORMATION {
			ULONG                   Unknown;
			ULONG                   SectionAttributes;
			LARGE_INTEGER           SectionSize;
		} SECTION_BASIC_INFORMATION, *PSECTION_BASIC_INFORMATION;

		typedef struct _RTL_USER_PROCESS_INFORMATION {
			ULONG                   Size;
			HANDLE                  ProcessHandle;
			HANDLE                  ThreadHandle;
			CLIENT_ID               ClientId;
			SECTION_IMAGE_INFORMATION ImageInformation;
		} RTL_USER_PROCESS_INFORMATION, *PRTL_USER_PROCESS_INFORMATION;

		typedef USHORT SECURITY_DESCRIPTOR_CONTROL, *PSECURITY_DESCRIPTOR_CONTROL;
		typedef PVOID PSID;

		typedef struct _ACL {
			BYTE  AclRevision;
			BYTE  Sbz1;
			WORD   AclSize;
			WORD   AceCount;
			WORD   Sbz2;
		} ACL, *PACL;

		typedef struct _SECURITY_DESCRIPTOR {
			UCHAR  Revision;
			UCHAR  Sbz1;
			SECURITY_DESCRIPTOR_CONTROL  Control;
			PSID  Owner;
			PSID  Group;
			PACL  Sacl;
			PACL  Dacl;
		} SECURITY_DESCRIPTOR, *PSECURITY_DESCRIPTOR;

		typedef LONG KPRIORITY;

		typedef struct _VM_COUNTERS {
			ULONG PeakVirtualSize;
			ULONG VirtualSize;
			ULONG PageFaultCount;
			ULONG PeakWorkingSetSize;
			ULONG WorkingSetSize;
			ULONG QuotaPeakPagedPoolUsage;
			ULONG QuotaPagedPoolUsage;
			ULONG QuotaPeakNonPagedPoolUsage;
			ULONG QuotaNonPagedPoolUsage;
			ULONG PagefileUsage;
			ULONG PeakPagefileUsage;
		} VM_COUNTERS, *PVM_COUNTERS;

		typedef struct _SYSTEM_THREADS32 {
			__int64 KernelTime; //0x0000 
			__int64 UserTime; //0x0008 
			__int64 CreateTime; //0x0010 
			__int32 WaitTime; //0x0018 
			__int32 StartAddress; //0x001C 
			CLIENT_ID32 ClientId; //0x0020 
			__int32 Priority; //0x0028 
			__int32 BasePriority; //0x002C 
			__int32 ContextSwitches; //0x0030 
			__int32 PagePriority; //0x0034 5
			__int32 State; //0x0038 15
			__int32 WaitReason; //0x003C 0
		} SYSTEM_THREADS32, *PSYSTEM_THREADS32; //Size=0x0040

		typedef struct _SYSTEM_THREADS64 {
			__int64 StartAddress; //0x0000 
			CLIENT_ID64 ClientId; //0x0008 
			__int32 Priority; //0x0018 
			__int32 BasePriority; //0x001C 
			__int32 ContextSwitches; //0x0020 
			__int32 PagePriority; //0x0024 
			__int32 State; //0x0028 
			__int32 WaitReason; //0x002C 
			__int64 KernelTime; //0x0030 
			__int64 UserTime; //0x0038 
			__int64 CreateTime; //0x0040 
			__int64 WaitTime; //0x0048 
		}SYSTEM_THREADS64, *PSYSTEM_THREADS64;//Size=0x0050

#ifdef _AMD64_
		typedef PSYSTEM_THREADS64 PSYSTEM_THREADS;
		typedef SYSTEM_THREADS64 SYSTEM_THREADS;
#else
		typedef PSYSTEM_THREADS32 PSYSTEM_THREADS;
		typedef SYSTEM_THREADS32 SYSTEM_THREADS;
#endif


		typedef struct _SYSTEM_PROCESSES {
			ULONG          NextEntryDelta;
			ULONG          ThreadCount;
			ULONG          Reserved1[6];
			LARGE_INTEGER  CreateTime;
			LARGE_INTEGER  UserTime;
			LARGE_INTEGER  KernelTime;
			UNICODE_STRING ProcessName;
			KPRIORITY      BasePriority;
	#if defined(_WIN64)
			ULONG pad1;
			ULONG          ProcessId;
			ULONG pad2;
			ULONG          InheritedFromProcessId;
			ULONG pad3, pad4, pad5;
	#else
			ULONG          ProcessId;
			ULONG          InheritedFromProcessId;
	#endif
			ULONG          HandleCount;
			ULONG          Reserved2[2];
			VM_COUNTERS    VmCounters;
			IO_COUNTERS    IoCounters;
			SYSTEM_THREADS Threads[1];
		} SYSTEM_PROCESSES, *PSYSTEM_PROCESSES;

		typedef struct _SYSTEM_EXTENDED_THREAD_INFORMATION32 {
			SYSTEM_THREADS32 ThreadInfo;
			DWORD StackBase;
			DWORD StackLimit;
			DWORD Win32StartAddress;
			DWORD TebBase;
			DWORD Reserved2;
			DWORD Reserved3;
			DWORD Reserved4;
		} SYSTEM_EXTENDED_THREAD_INFORMATION32, *PSYSTEM_EXTENDED_THREAD_INFORMATION32;

		typedef struct _SYSTEM_EXTENDED_THREAD_INFORMATION64 {
			SYSTEM_THREADS64 ThreadInfo;
			DWORD64 StackBase;
			DWORD64 StackLimit;
			DWORD64 Win32StartAddress;
			DWORD64 TebBase;
			DWORD64 Reserved2;
			DWORD64 Reserved3;
			DWORD64 Reserved4;
		} SYSTEM_EXTENDED_THREAD_INFORMATION64, *PSYSTEM_EXTENDED_THREAD_INFORMATION64;



		typedef struct _TIME_FIELDS {
			USHORT                  Year;
			USHORT                  Month;
			USHORT                  Day;
			USHORT                  Hour;
			USHORT                  Minute;
			USHORT                  Second;
			USHORT                  Milliseconds;
			USHORT                  Weekday;
		} TIME_FIELDS, *PTIME_FIELDS;

		typedef enum _SYSTEM_INFORMATION_CLASS {
			SystemBasicInformation, // q: SYSTEM_BASIC_INFORMATION
			SystemProcessorInformation, // q: SYSTEM_PROCESSOR_INFORMATION
			SystemPerformanceInformation, // q: SYSTEM_PERFORMANCE_INFORMATION
			SystemTimeOfDayInformation, // q: SYSTEM_TIMEOFDAY_INFORMATION
			SystemPathInformation, // not implemented
			SystemProcessInformation, // q: SYSTEM_PROCESS_INFORMATION
			SystemCallCountInformation, // q: SYSTEM_CALL_COUNT_INFORMATION
			SystemDeviceInformation, // q: SYSTEM_DEVICE_INFORMATION
			SystemProcessorPerformanceInformation, // q: SYSTEM_PROCESSOR_PERFORMANCE_INFORMATION
			SystemFlagsInformation, // q: SYSTEM_FLAGS_INFORMATION
			SystemCallTimeInformation, // 10, not implemented
			SystemModuleInformation, // q: RTL_PROCESS_MODULES
			SystemLocksInformation,
			SystemStackTraceInformation,
			SystemPagedPoolInformation, // not implemented
			SystemNonPagedPoolInformation, // not implemented
			SystemHandleInformation, // q: SYSTEM_HANDLE_INFORMATION
			SystemObjectInformation, // q: SYSTEM_OBJECTTYPE_INFORMATION mixed with SYSTEM_OBJECT_INFORMATION
			SystemPageFileInformation, // q: SYSTEM_PAGEFILE_INFORMATION
			SystemVdmInstemulInformation, // q
			SystemVdmBopInformation, // 20, not implemented
			SystemFileCacheInformation, // q: SYSTEM_FILECACHE_INFORMATION; s (requires SeIncreaseQuotaPrivilege) (info for WorkingSetTypeSystemCache)
			SystemPoolTagInformation, // q: SYSTEM_POOLTAG_INFORMATION
			SystemInterruptInformation, // q: SYSTEM_INTERRUPT_INFORMATION
			SystemDpcBehaviorInformation, // q: SYSTEM_DPC_BEHAVIOR_INFORMATION; s: SYSTEM_DPC_BEHAVIOR_INFORMATION (requires SeLoadDriverPrivilege)
			SystemFullMemoryInformation, // not implemented
			SystemLoadGdiDriverInformation, // s (kernel-mode only)
			SystemUnloadGdiDriverInformation, // s (kernel-mode only)
			SystemTimeAdjustmentInformation, // q: SYSTEM_QUERY_TIME_ADJUST_INFORMATION; s: SYSTEM_SET_TIME_ADJUST_INFORMATION (requires SeSystemtimePrivilege)
			SystemSummaryMemoryInformation, // not implemented
			SystemMirrorMemoryInformation, // 30, s (requires license value "Kernel-MemoryMirroringSupported") (requires SeShutdownPrivilege)
			SystemPerformanceTraceInformation, // s
			SystemObsolete0, // not implemented
			SystemExceptionInformation, // q: SYSTEM_EXCEPTION_INFORMATION
			SystemCrashDumpStateInformation, // s (requires SeDebugPrivilege)
			SystemKernelDebuggerInformation, // q: SYSTEM_KERNEL_DEBUGGER_INFORMATION
			SystemContextSwitchInformation, // q: SYSTEM_CONTEXT_SWITCH_INFORMATION
			SystemRegistryQuotaInformation, // q: SYSTEM_REGISTRY_QUOTA_INFORMATION; s (requires SeIncreaseQuotaPrivilege)
			SystemExtendServiceTableInformation, // s (requires SeLoadDriverPrivilege) // loads win32k only
			SystemPrioritySeperation, // s (requires SeTcbPrivilege)
			SystemVerifierAddDriverInformation, // 40, s (requires SeDebugPrivilege)
			SystemVerifierRemoveDriverInformation, // s (requires SeDebugPrivilege)
			SystemProcessorIdleInformation, // q: SYSTEM_PROCESSOR_IDLE_INFORMATION
			SystemLegacyDriverInformation, // q: SYSTEM_LEGACY_DRIVER_INFORMATION
			SystemCurrentTimeZoneInformation, // q
			SystemLookasideInformation, // q: SYSTEM_LOOKASIDE_INFORMATION
			SystemTimeSlipNotification, // s (requires SeSystemtimePrivilege)
			SystemSessionCreate, // not implemented
			SystemSessionDetach, // not implemented
			SystemSessionInformation, // not implemented
			SystemRangeStartInformation, // 50, q
			SystemVerifierInformation, // q: SYSTEM_VERIFIER_INFORMATION; s (requires SeDebugPrivilege)
			SystemVerifierThunkExtend, // s (kernel-mode only)
			SystemSessionProcessInformation, // q: SYSTEM_SESSION_PROCESS_INFORMATION
			SystemLoadGdiDriverInSystemSpace, // s (kernel-mode only) (same as SystemLoadGdiDriverInformation)
			SystemNumaProcessorMap, // q
			SystemPrefetcherInformation, // q: PREFETCHER_INFORMATION; s: PREFETCHER_INFORMATION // PfSnQueryPrefetcherInformation
			SystemExtendedProcessInformation, // q: SYSTEM_PROCESS_INFORMATION
			SystemRecommendedSharedDataAlignment, // q
			SystemComPlusPackage, // q; s
			SystemNumaAvailableMemory, // 60
			SystemProcessorPowerInformation, // q: SYSTEM_PROCESSOR_POWER_INFORMATION
			SystemEmulationBasicInformation, // q
			SystemEmulationProcessorInformation,
			SystemExtendedHandleInformation, // q: SYSTEM_HANDLE_INFORMATION_EX
			SystemLostDelayedWriteInformation, // q: ULONG
			SystemBigPoolInformation, // q: SYSTEM_BIGPOOL_INFORMATION
			SystemSessionPoolTagInformation, // q: SYSTEM_SESSION_POOLTAG_INFORMATION
			SystemSessionMappedViewInformation, // q: SYSTEM_SESSION_MAPPED_VIEW_INFORMATION
			SystemHotpatchInformation, // q; s
			SystemObjectSecurityMode, // 70, q
			SystemWatchdogTimerHandler, // s (kernel-mode only)
			SystemWatchdogTimerInformation, // q (kernel-mode only); s (kernel-mode only)
			SystemLogicalProcessorInformation, // q: SYSTEM_LOGICAL_PROCESSOR_INFORMATION
			SystemWow64SharedInformationObsolete, // not implemented
			SystemRegisterFirmwareTableInformationHandler, // s (kernel-mode only)
			SystemFirmwareTableInformation, // not implemented
			SystemModuleInformationEx, // q: RTL_PROCESS_MODULE_INFORMATION_EXd
			SystemVerifierTriageInformation, // not implemented
			SystemSuperfetchInformation, // q: SUPERFETCH_INFORMATION; s: SUPERFETCH_INFORMATION // PfQuerySuperfetchInformation
			SystemMemoryListInformation, // 80, q: SYSTEM_MEMORY_LIST_INFORMATION; s: SYSTEM_MEMORY_LIST_COMMAND (requires SeProfileSingleProcessPrivilege)
			SystemFileCacheInformationEx, // q: SYSTEM_FILECACHE_INFORMATION; s (requires SeIncreaseQuotaPrivilege) (same as SystemFileCacheInformation)
			SystemThreadPriorityClientIdInformation, // s: SYSTEM_THREAD_CID_PRIORITY_INFORMATION (requires SeIncreaseBasePriorityPrivilege)
			SystemProcessorIdleCycleTimeInformation, // q: SYSTEM_PROCESSOR_IDLE_CYCLE_TIME_INFORMATION[]
			SystemVerifierCancellationInformation, // not implemented // name:wow64:whNT32QuerySystemVerifierCancellationInformation
			SystemProcessorPowerInformationEx, // not implemented
			SystemRefTraceInformation, // q; s // ObQueryRefTraceInformation
			SystemSpecialPoolInformation, // q; s (requires SeDebugPrivilege) // MmSpecialPoolTag, then MmSpecialPoolCatchOverruns != 0
			SystemProcessIdInformation, // q: SYSTEM_PROCESS_ID_INFORMATION
			SystemErrorPortInformation, // s (requires SeTcbPrivilege)
			SystemBootEnvironmentInformation, // 90, q: SYSTEM_BOOT_ENVIRONMENT_INFORMATION
			SystemHypervisorInformation, // q; s (kernel-mode only)
			SystemVerifierInformationEx, // q; s
			SystemTimeZoneInformation, // s (requires SeTimeZonePrivilege)
			SystemImageFileExecutionOptionsInformation, // s: SYSTEM_IMAGE_FILE_EXECUTION_OPTIONS_INFORMATION (requires SeTcbPrivilege)
			SystemCoverageInformation, // q; s // name:wow64:whNT32QuerySystemCoverageInformation; ExpCovQueryInformation
			SystemPrefetchPatchInformation, // not implemented
			SystemVerifierFaultsInformation, // s (requires SeDebugPrivilege)
			SystemSystemPartitionInformation, // q: SYSTEM_SYSTEM_PARTITION_INFORMATION
			SystemSystemDiskInformation, // q: SYSTEM_SYSTEM_DISK_INFORMATION
			SystemProcessorPerformanceDistribution, // 100, q: SYSTEM_PROCESSOR_PERFORMANCE_DISTRIBUTION
			SystemNumaProximityNodeInformation, // q
			SystemDynamicTimeZoneInformation, // q; s (requires SeTimeZonePrivilege)
			SystemCodeIntegrityInformation, // q // SeCodeIntegrityQueryInformation
			SystemProcessorMicrocodeUpdateInformation, // s
			SystemProcessorBrandString, // q // HaliQuerySystemInformation -> HalpGetProcessorBrandString, info class 23
			SystemVirtualAddressInformation, // q: SYSTEM_VA_LIST_INFORMATION[]; s: SYSTEM_VA_LIST_INFORMATION[] (requires SeIncreaseQuotaPrivilege) // MmQuerySystemVaInformation
			SystemLogicalProcessorAndGroupInformation, // q: SYSTEM_LOGICAL_PROCESSOR_INFORMATION_EX // since WIN7 // KeQueryLogicalProcessorRelationship
			SystemProcessorCycleTimeInformation, // q: SYSTEM_PROCESSOR_CYCLE_TIME_INFORMATION[]
			SystemStoreInformation, // q; s // SmQueryStoreInformation
			SystemRegistryAppendString, // 110, s: SYSTEM_REGISTRY_APPEND_STRING_PARAMETERS
			SystemAitSamplingValue, // s: ULONG (requires SeProfileSingleProcessPrivilege)
			SystemVhdBootInformation, // q: SYSTEM_VHD_BOOT_INFORMATION
			SystemCpuQuotaInformation, // q; s // PsQueryCpuQuotaInformation
			SystemNativeBasicInformation, // not implemented
			SystemSpare1, // not implemented
			SystemLowPriorityIoInformation, // q: SYSTEM_LOW_PRIORITY_IO_INFORMATION
			SystemTpmBootEntropyInformation, // q: TPM_BOOT_ENTROPY_NT_RESULT // ExQueryTpmBootEntropyInformation
			SystemVerifierCountersInformation, // q: SYSTEM_VERIFIER_COUNTERS_INFORMATION
			SystemPagedPoolInformationEx, // q: SYSTEM_FILECACHE_INFORMATION; s (requires SeIncreaseQuotaPrivilege) (info for WorkingSetTypePagedPool)
			SystemSystemPtesInformationEx, // 120, q: SYSTEM_FILECACHE_INFORMATION; s (requires SeIncreaseQuotaPrivilege) (info for WorkingSetTypeSystemPtes)
			SystemNodeDistanceInformation, // q
			SystemAcpiAuditInformation, // q: SYSTEM_ACPI_AUDIT_INFORMATION // HaliQuerySystemInformation -> HalpAuditQueryResults, info class 26
			SystemBasicPerformanceInformation, // q: SYSTEM_BASIC_PERFORMANCE_INFORMATION // name:wow64:whNtQuerySystemInformation_SystemBasicPerformanceInformation
			SystemQueryPerformanceCounterInformation, // q: SYSTEM_QUERY_PERFORMANCE_COUNTER_INFORMATION // since WIN7 SP1
			SystemSessionBigPoolInformation, // since WIN8
			SystemBootGraphicsInformation,
			SystemScrubPhysicalMemoryInformation,
			SystemBadPageInformation,
			SystemProcessorProfileControlArea,
			SystemCombinePhysicalMemoryInformation, // 130
			SystemEntropyInterruptTimingCallback,
			SystemConsoleInformation,
			SystemPlatformBinaryInformation,
			SystemThrottleNotificationInformation,
			SystemHypervisorProcessorCountInformation,
			SystemDeviceDataInformation,
			SystemDeviceDataEnumerationInformation,
			SystemMemoryTopologyInformation,
			SystemMemoryChannelInformation,
			SystemBootLogoInformation, // 140
			SystemProcessorPerformanceInformationEx, // since WINBLUE
			SystemSpare0,
			SystemSecureBootPolicyInformation,
			SystemPageFileInformationEx,
			SystemSecureBootInformation,
			SystemEntropyInterruptTimingRawInformation,
			SystemPortableWorkspaceEfiLauncherInformation,
			SystemFullProcessInformation, // q: SYSTEM_PROCESS_INFORMATION with SYSTEM_PROCESS_INFORMATION_EXTENSION (requires admin)
			SystemKernelDebuggerInformationEx,
			SystemBootMetadataInformation, // 150
			SystemSoftRebootInformation,
			SystemElamCertificateInformation,
			SystemOfflineDumpConfigInformation,
			SystemProcessorFeaturesInformation,
			SystemRegistryReconciliationInformation,
			SystemEdidInformation,
			MaxSystemInfoClass
		} SYSTEM_INFORMATION_CLASS, *PSYSTEM_INFORMATION_CLASS;

		typedef DWORD ACCESS_MASK;
		typedef DWORD  SECURITY_INFORMATION, *PSECURITY_INFORMATION;

		typedef struct _GENERIC_MAPPING {
			ACCESS_MASK GenericRead;
			ACCESS_MASK GenericWrite;
			ACCESS_MASK GenericExecute;
			ACCESS_MASK GenericAll;
		} GENERIC_MAPPING, *PGENERIC_MAPPING;

		typedef struct _LUID {
			DWORD LowPart;
			LONG HighPart;
		} LUID, *PLUID;

		typedef struct _LUID_AND_ATTRIBUTES {
			LUID Luid;
			DWORD Attributes;
		} LUID_AND_ATTRIBUTES, *PLUID_AND_ATTRIBUTES;

		typedef struct _PRIVILEGE_SET {
			DWORD PrivilegeCount;
			DWORD Control;
			LUID_AND_ATTRIBUTES Privilege[1];
		} PRIVILEGE_SET, *PPRIVILEGE_SET;

		typedef DWORD LCID;
		typedef PDWORD PLCID;
		typedef WORD   LANGID;

		typedef enum  _SHUTDOWN_ACTION {
			ShutdownNoReboot,
			ShutdownReboot,
			ShutdownPowerOff
		}  SHUTDOWN_ACTION, *PSHUTDOWN_ACTION;

		typedef enum _ATOM_INFORMATION_CLASS {
			AtomBasicInformation,
			AtomTableInformation
		} ATOM_INFORMATION_CLASS, *PATOM_INFORMATION_CLASS;

		typedef unsigned short RTL_ATOM, *PRTL_ATOM;

		typedef enum _SYSDBG_COMMAND {
			SysDbgQueryModuleInformation = 1,
			SysDbgQueryTraceInformation,
			SysDbgSetTracepoint,
			SysDbgSetSpecialCall,
			SysDbgClearSpecialCalls,
			SysDbgQuerySpecialCalls
		} SYSDBG_COMMAND, *PSYSDBG_COMMAND;

		typedef struct _EXCEPTION_RECORD {
			DWORD    ExceptionCode;
			DWORD ExceptionFlags;
			struct _EXCEPTION_RECORD *ExceptionRecord;
			PVOID ExceptionAddress;
			DWORD NumberParameters;
			ULONG_PTR ExceptionInformation[15];
		} EXCEPTION_RECORD;

		typedef enum _HARDERROR_RESPONSE {
			ResponseReturnToCaller,
			ResponseNotHandled,
			ResponseAbort,
			ResponseCancel,
			ResponseIgnore,
			ResponseNo,
			ResponseOk,
			ResponseRetry,
			ResponseYes
		} HARDERROR_RESPONSE, *PHARDERROR_RESPONSE;

		typedef enum _HARDERROR_RESPONSE_OPTION {
			OptionAbortRetryIgnore,
			OptionOk,
			OptionOkCancel,
			OptionRetryCancel,
			OptionYesNo,
			OptionYesNoCancel,
			OptionShutdownSystem
		} HARDERROR_RESPONSE_OPTION, *PHARDERROR_RESPONSE_OPTION;

		typedef struct _ANSI_STRING {
			USHORT Length;
			USHORT MaximumLength;
			PCHAR  Buffer;
		} ANSI_STRING, *PANSI_STRING;

		typedef struct _SYSTEM_MODULE {
			ULONG                Reserved1;
			ULONG                Reserved2;
			PVOID                ImageBaseAddress;
			ULONG                ImageSize;
			ULONG                Flags;
			WORD                 Id;
			WORD                 Rank;
			WORD                 w018;
			WORD                 NameOffset;
			BYTE                 Name[255];
		} SYSTEM_MODULE, *PSYSTEM_MODULE;

		typedef struct _SYSTEM_MODULE_INFORMATION {
			ULONG                ModulesCount;
			SYSTEM_MODULE        Modules;
		} SYSTEM_MODULE_INFORMATION, *PSYSTEM_MODULE_INFORMATION;

		typedef struct _RTL_HEAP_DEFINITION {
			ULONG                   Length;
			ULONG                   Unknown1;
			ULONG                   Unknown2;
			ULONG                   Unknown3;
			ULONG                   Unknown4;
			ULONG                   Unknown5;
			ULONG                   Unknown6;
			ULONG                   Unknown7;
			ULONG                   Unknown8;
			ULONG                   Unknown9;
			ULONG                   Unknown10;
			ULONG                   Unknown11;
			ULONG                   Unknown12;
		} RTL_HEAP_DEFINITION, *PRTL_HEAP_DEFINITION;

		typedef NTSTATUS(*PHEAP_ENUMERATION_ROUTINE)(IN PVOID HeapHandle, IN PVOID UserParam);
		typedef void(*PTIMER_APC_ROUTINE)(IN PVOID TimerContext, IN ULONG TimerLowValue, IN LONG TimerHighValue);

		typedef struct _PROCESS_HEAP_ENTRY {
			PVOID lpData;
			DWORD cbData;
			BYTE cbOverhead;
			BYTE iRegionIndex;
			WORD wFlags;
			union {
				struct {
					HANDLE hMem;
					DWORD dwReserved[3];
				} Block;
				struct {
					DWORD dwCommittedSize;
					DWORD dwUnCommittedSize;
					LPVOID lpFirstBlock;
					LPVOID lpLastBlock;
				} Region;
			};
		} PROCESS_HEAP_ENTRY, *LPPROCESS_HEAP_ENTRY, *PPROCESS_HEAP_ENTRY;

		typedef enum _SECURITY_IMPERSONATION_LEVEL {
			SecurityAnonymous,
			SecurityIdentification,
			SecurityImpersonation,
			SecurityDelegation
		} SECURITY_IMPERSONATION_LEVEL, *PSECURITY_IMPERSONATION_LEVEL;

		typedef enum _TOKEN_INFORMATION_CLASS {
			TokenUser = 1,
			TokenGroups,
			TokenPrivileges,
			TokenOwner,
			TokenPrimaryGroup,
			TokenDefaultDacl,
			TokenSource,
			TokenType,
			TokenImpersonationLevel,
			TokenStatistics,
			TokenRestrictedSids,
			TokenSessionId,
			TokenGroupsAndPrivileges,
			TokenSessionReference,
			TokenSandBoxInert,
			TokenAuditPolicy,
			TokenOrigin,
			TokenElevationType,
			TokenLinkedToken,
			TokenElevation,
			TokenHasRestrictions,
			TokenAccessInformation,
			TokenVirtualizationAllowed,
			TokenVirtualizationEnabled,
			TokenIntegrityLevel,
			TokenUIAccess,
			TokenMandatoryPolicy,
			TokenLogonSid,
			TokenIsAppContainer,
			TokenCapabilities,
			TokenAppContainerSid,
			TokenAppContainerNumber,
			TokenUserClaimAttributes,
			TokenDeviceClaimAttributes,
			TokenRestrictedUserClaimAttributes,
			TokenRestrictedDeviceClaimAttributes,
			TokenDeviceGroups,
			TokenRestrictedDeviceGroups,
			TokenSecurityAttributes,
			TokenIsRestricted,
			MaxTokenInfoClass
		} TOKEN_INFORMATION_CLASS, *PTOKEN_INFORMATION_CLASS;

		typedef enum _TIMER_INFORMATION_CLASS {
			TimerBasicInformation
		} TIMER_INFORMATION_CLASS, *PTIMER_INFORMATION_CLASS;

		typedef struct _INITIAL_TEB {
			PVOID                StackBase;
			PVOID                StackLimit;
			PVOID                StackCommit;
			PVOID                StackCommitMax;
			PVOID                StackReserved;
		} INITIAL_TEB, *PINITIAL_TEB;

		typedef struct _LIST_ENTRY {
			struct _LIST_ENTRY *Flink;
			struct _LIST_ENTRY *Blink;
		} LIST_ENTRY, *PLIST_ENTRY, *RESTRICTED_POINTER PRLIST_ENTRY;

		typedef struct _LIST_ENTRY32 {
			DWORD Flink;
			DWORD Blink;
		} LIST_ENTRY32, *PLIST_ENTRY32;

		typedef struct _NT_TIB {
			struct _EXCEPTION_REGISTRATION_RECORD *ExceptionList;
			PVOID StackBase;
			PVOID StackLimit;
			PVOID SubSystemTib;
			union {
				PVOID FiberData;
				DWORD Version;
			};

			PVOID ArbitraryUserPointer;
			struct _NT_TIB *Self;
		} NT_TIB, *PNT_TIB;

		typedef struct _PEB_LDR_DATA {
			BYTE       Reserved1[8];
			PVOID      Reserved2[3];
			LIST_ENTRY InMemoryOrderModuleList;
		} PEB_LDR_DATA, *PPEB_LDR_DATA;

		typedef struct _PEB32 {
			BYTE                          Reserved1[2];
			BYTE                          BeingDebugged;
			BYTE                          Reserved2[1];
			DWORD                         Reserved3[2];
			DWORD bla1;
			DWORD                 Ldr;
			DWORD bla2;
			DWORD  ProcessParameters;
			BYTE                          Reserved4[104];
			DWORD                         Reserved5[52];
			DWORD PostProcessInitRoutine;
			BYTE                          Reserved6[128];
			PVOID                         Reserved7[1];
			ULONG                         SessionId;
		} PEB32, *PPEB32;

		typedef struct _PEB64 {
			BYTE Reserved1[2];
			BYTE BeingDebugged;
			BYTE Reserved2[21];
			DWORD64 LoaderData;
			DWORD64 ProcessParameters;
			BYTE Reserved3[520];
			DWORD64 PostProcessInitRoutine;
			BYTE Reserved4[136];
			ULONG SessionId;
		} PEB64;

		typedef struct _TEB {
			NT_TIB                  Tib;
			PVOID                   EnvironmentPointer;
			CLIENT_ID               Cid;
			PVOID                   ActiveRpcInfo;
			PVOID                   ThreadLocalStoragePointer;
			PPEB32                    Peb;
			ULONG                   LastErrorValue;
			ULONG                   CountOfOwnedCriticalSections;
			PVOID                   CsrClientThread;
			PVOID                   Win32ThreadInfo;
			ULONG                   Win32ClientInfo[0x1F];
			PVOID                   WOW32Reserved;
			ULONG                   CurrentLocale;
			ULONG                   FpSoftwareStatusRegister;
			PVOID                   SystemReserved1[0x36];
			PVOID                   Spare1;
			ULONG                   ExceptionCode;
			ULONG                   SpareBytes1[0x28];
			PVOID                   SystemReserved2[0xA];
			ULONG                   GdiRgn;
			ULONG                   GdiPen;
			ULONG                   GdiBrush;
			CLIENT_ID               RealClientId;
			PVOID                   GdiCachedProcessHandle;
			ULONG                   GdiClientPID;
			ULONG                   GdiClientTID;
			PVOID                   GdiThreadLocaleInfo;
			PVOID                   UserReserved[5];
			PVOID                   GlDispatchTable[0x118];
			ULONG                   GlReserved1[0x1A];
			PVOID                   GlReserved2;
			PVOID                   GlSectionInfo;
			PVOID                   GlSection;
			PVOID                   GlTable;
			PVOID                   GlCurrentRC;
			PVOID                   GlContext;
			NTSTATUS                LastStatusValue;
			UNICODE_STRING          StaticUnicodeString;
			WCHAR                   StaticUnicodeBuffer[0x105];
			PVOID                   DeallocationStack;
			PVOID                   TlsSlots[0x40];
			LIST_ENTRY              TlsLinks;
			PVOID                   Vdm;
			PVOID                   ReservedForNtRpc;
			PVOID                   DbgSsReserved[0x2];
			ULONG                   HardErrorDisabled;
			PVOID                   Instrumentation[0x10];
			PVOID                   WinSockData;
			ULONG                   GdiBatchCount;
			ULONG                   Spare2;
			ULONG                   Spare3;
			ULONG                   Spare4;
			PVOID                   ReservedForOle;
			ULONG                   WaitingOnLoaderLock;
			PVOID                   StackCommit;
			PVOID                   StackCommitMax;
			PVOID                   StackReserved;
		} TEB, *PTEB;

		typedef struct _TEB32 {
			BYTE  Spare[0x30];
			DWORD ProcessEnvironmentBlock;
		} TEB32, *PTEB32;

		typedef BOOLEAN SECURITY_CONTEXT_TRACKING_MODE, *PSECURITY_CONTEXT_TRACKING_MODE;

		typedef struct _SECURITY_QUALITY_OF_SERVICE {
			DWORD                          Length;
			SECURITY_IMPERSONATION_LEVEL   ImpersonationLevel;
			SECURITY_CONTEXT_TRACKING_MODE ContextTrackingMode;
			BOOLEAN                        EffectiveOnly;
		} SECURITY_QUALITY_OF_SERVICE, *PSECURITY_QUALITY_OF_SERVICE;

		typedef enum _SEMAPHORE_INFORMATION_CLASS {
			SemaphoreBasicInformation
		} SEMAPHORE_INFORMATION_CLASS, *PSEMAPHORE_INFORMATION_CLASS;

		typedef enum _SECTION_INHERIT {
			ViewShare = 1,
			ViewUnmap = 2
		} SECTION_INHERIT, *PSECTION_INHERIT;

		typedef enum _SECTION_INFORMATION_CLASS {
			SectionBasicInformation,
			SectionImageInformation
		} SECTION_INFORMATION_CLASS, *PSECTION_INFORMATION_CLASS;

		typedef enum _KPROFILE_SOURCE {
			ProfileTime,
			ProfileAlignmentFixup,
			ProfileTotalIssues,
			ProfilePipelineDry,
			ProfileLoadInstructions,
			ProfilePipelineFrozen,
			ProfileBranchInstructions,
			ProfileTotalNonissues,
			ProfileDcacheMisses,
			ProfileIcacheMisses,
			ProfileCacheMisses,
			ProfileBranchMispredictions,
			ProfileStoreInstructions,
			ProfileFpInstructions,
			ProfileIntegerInstructions,
			Profile2Issue,
			Profile3Issue,
			Profile4Issue,
			ProfileSpecialInstructions,
			ProfileTotalCycles,
			ProfileIcacheIssues,
			ProfileDcacheAccesses,
			ProfileMemoryBarrierCycles,
			ProfileLoadLinkedIssues,
			ProfileMaximum
		} KPROFILE_SOURCE, *PKPROFILE_SOURCE;

		typedef ULONG_PTR  KAFFINITY;

		typedef enum  _OBJECT_INFORMATION_CLASS {
			ObjectBasicInformation,
			ObjectNameInformation,
			ObjectTypeInformation,
			ObjectAllInformation,
			ObjectDataInformation
		}  OBJECT_INFORMATION_CLASS, *POBJECT_INFORMATION_CLASS;

		typedef enum _OBJECT_WAIT_TYPE {
			WaitAllObject,
			WaitAnyObject
		} OBJECT_WAIT_TYPE, *POBJECT_WAIT_TYPE;

		typedef struct _OBJDIR_INFORMATION {
			UNICODE_STRING          ObjectName;
			UNICODE_STRING          ObjectTypeName;
			BYTE                    Data[1];
		} OBJDIR_INFORMATION, *POBJDIR_INFORMATION;

		typedef enum _EVENT_TYPE {
			NotificationEvent,
			SynchronizationEvent
		} EVENT_TYPE, *PEVENT_TYPE;

		typedef enum _EVENT_INFORMATION_CLASS {
			EventBasicInformation
		} EVENT_INFORMATION_CLASS, *PEVENT_INFORMATION_CLASS;

		typedef enum _MUTANT_INFORMATION_CLASS {
			MutantBasicInformation,
			MutantOwnerInformation
		} MUTANT_INFORMATION_CLASS, *PMUTANT_INFORMATION_CLASS;

		typedef struct _PROCESS_BASIC_INFORMATION { // Information Class 0
			NTSTATUS ExitStatus;
			DWORD PebBaseAddress;
			DWORD AffinityMask;
			DWORD BasePriority;
			ULONG UniqueProcessId;
			ULONG InheritedFromUniqueProcessId;
		} PROCESS_BASIC_INFORMATION, *PPROCESS_BASIC_INFORMATION;

		typedef struct _QUOTA_LIMITS { // Information Class 1
			ULONG PagedPoolLimit;
			ULONG NonPagedPoolLimit;
			ULONG MinimumWorkingSetSize;
			ULONG MaximumWorkingSetSize;
			ULONG PagefileLimit;
			LARGE_INTEGER TimeLimit;
		} QUOTA_LIMITS, *PQUOTA_LIMITS;

		typedef struct _IO_COUNTERS { // Information Class 2
			LARGE_INTEGER ReadOperationCount;
			LARGE_INTEGER WriteOperationCount;
			LARGE_INTEGER OtherOperationCount;
			LARGE_INTEGER ReadTransferCount;
			LARGE_INTEGER WriteTransferCount;
			LARGE_INTEGER OtherTransferCount;
		} IO_COUNTERS, *PIO_COUNTERS;

		typedef struct _KERNEL_USER_TIMES { // Information Class 4
			LARGE_INTEGER CreateTime;
			LARGE_INTEGER ExitTime;
			LARGE_INTEGER KernelTime;
			LARGE_INTEGER UserTime;
		} KERNEL_USER_TIMES, *PKERNEL_USER_TIMES;

		typedef struct _PROCESS_ACCESS_TOKEN { // Information Class 9
			HANDLE Token;
			HANDLE Thread;
		} PROCESS_ACCESS_TOKEN, *PPROCESS_ACCESS_TOKEN;

		typedef struct _POOLED_USAGE_AND_LIMITS { // Information Class 14
			ULONG PeakPagedPoolUsage;
			ULONG PagedPoolUsage;
			ULONG PagedPoolLimit;
			ULONG PeakNonPagedPoolUsage;
			ULONG NonPagedPoolUsage;
			ULONG NonPagedPoolLimit;
			ULONG PeakPagefileUsage;
			ULONG PagefileUsage;
			ULONG PagefileLimit;
		} POOLED_USAGE_AND_LIMITS, *PPOOLED_USAGE_AND_LIMITS;

		typedef struct _PROCESS_WS_WATCH_INFORMATION { // Information Class 15
			PVOID FaultingPc;
			PVOID FaultingVa;
		} PROCESS_WS_WATCH_INFORMATION, *PPROCESS_WS_WATCH_INFORMATION;

		typedef struct _PROCESS_PRIORITY_CLASS { // Information Class 18
			BOOLEAN Foreground;
			UCHAR PriorityClass;
		} PROCESS_PRIORITY_CLASS, *PPROCESS_PRIORITY_CLASS;

		typedef struct _PROCESS_PARAMETERS {
			ULONG AllocationSize;
			ULONG Size;
			ULONG Flags;
			ULONG Reserved;
			LONG Console;
			ULONG ProcessGroup;
			HANDLE hStdInput;
			HANDLE hStdOutput;
			HANDLE hStdError;
			UNICODE_STRING CurrentDirectoryName;
			HANDLE CurrentDirectoryHandle;
			UNICODE_STRING DllPath;
			UNICODE_STRING ImageFile;
			UNICODE_STRING CommandLine;
			PWSTR Environment;
			ULONG dwX;
			ULONG dwY;
			ULONG dwXSize;
			ULONG dwYSize;
			ULONG dwXCountChars;
			ULONG dwYCountChars;
			ULONG dwFillAttribute;
			ULONG dwFlags;
			ULONG wShowWindow;
			UNICODE_STRING WindowTitle;
			UNICODE_STRING Desktop;
			UNICODE_STRING Reserved1;
			UNICODE_STRING Reserved2;
		} PROCESS_PARAMETERS, *PPROCESS_PARAMETERS;

		typedef struct _DEBUG_BUFFER {
			HANDLE SectionHandle;
			PVOID SectionBase;
			PVOID RemoteSectionBase;
			ULONG SectionBaseDelta;
			HANDLE EventPairHandle;
			ULONG Unknown[2];
			HANDLE RemoteThreadHandle;
			ULONG InfoClassMask;
			ULONG SizeOfInfo;
			ULONG AllocatedSize;
			ULONG SectionSize;
			PVOID ModuleInformation;
			PVOID BackTraceInformation;
			PVOID HeapInformation;
			PVOID LockInformation;
			PVOID Reserved[8];
		} DEBUG_BUFFER, *PDEBUG_BUFFER;

		typedef enum _JOBOBJECTINFOCLASS {
			JobObjectBasicAccountingInformation = 1, // Y N
			JobObjectBasicLimitInformation, // Y Y
			JobObjectBasicProcessIdList, // Y N
			JobObjectBasicUIRestrictions, // Y Y
			JobObjectSecurityLimitInformation, // Y Y
			JobObjectEndOfJobTimeInformation, // N Y
			JobObjectAssociateCompletionPortInformation, // N Y
			JobObjectBasicAndIoAccountingInformation, // Y N
			JobObjectExtendedLimitInformation // Y Y
		} JOBOBJECTINFOCLASS;

		typedef struct _JOBOBJECT_BASIC_ACCOUNTING_INFORMATION {
			LARGE_INTEGER TotalUserTime;
			LARGE_INTEGER TotalKernelTime;
			LARGE_INTEGER ThisPeriodTotalUserTime;
			LARGE_INTEGER ThisPeriodTotalKernelTime;
			ULONG TotalPageFaultCount;
			ULONG TotalProcesses;
			ULONG ActiveProcesses;
			ULONG TotalTerminatedProcesses;
		} JOBOBJECT_BASIC_ACCOUNTING_INFORMATION, *PJOBOBJECT_BASIC_ACCOUNTING_INFORMATION;

		typedef struct _JOBOBJECT_BASIC_LIMIT_INFORMATION {
			LARGE_INTEGER PerProcessUserTimeLimit;
			LARGE_INTEGER PerJobUserTimeLimit;
			ULONG LimitFlags;
			ULONG MinimumWorkingSetSize;
			ULONG MaximumWorkingSetSize;
			ULONG ActiveProcessLimit;
			ULONG Affinity;
			ULONG PriorityClass;
			ULONG SchedulingClass;
		} JOBOBJECT_BASIC_LIMIT_INFORMATION, *PJOBOBJECT_BASIC_LIMIT_INFORMATION;

		typedef struct _JOBOBJECT_BASIC_PROCESS_ID_LIST {
			ULONG NumberOfAssignedProcesses;
			ULONG NumberOfProcessIdsInList;
			ULONG_PTR ProcessIdList[1];
		} JOBOBJECT_BASIC_PROCESS_ID_LIST, *PJOBOBJECT_BASIC_PROCESS_ID_LIST;

		typedef struct _JOBOBJECT_SECURITY_LIMIT_INFORMATION {
			ULONG SecurityLimitFlags;
			HANDLE JobToken;
			PTOKEN_GROUPS SidsToDisable;
			PTOKEN_PRIVILEGES PrivilegesToDelete;
			PTOKEN_GROUPS RestrictedSids;
		} JOBOBJECT_SECURITY_LIMIT_INFORMATION, *PJOBOBJECT_SECURITY_LIMIT_INFORMATION;

		typedef struct _JOBOBJECT_END_OF_JOB_TIME_INFORMATION {
			ULONG EndOfJobTimeAction;
		} JOBOBJECT_END_OF_JOB_TIME_INFORMATION, *PJOBOBJECT_END_OF_JOB_TIME_INFORMATION;

		typedef struct _JOBOBJECT_ASSOCIATE_COMPLETION_PORT {
			PVOID CompletionKey;
			HANDLE CompletionPort;
		} JOBOBJECT_ASSOCIATE_COMPLETION_PORT, *PJOBOBJECT_ASSOCIATE_COMPLETION_PORT;

		typedef struct JOBOBJECT_BASIC_AND_IO_ACCOUNTING_INFORMATION {
			JOBOBJECT_BASIC_ACCOUNTING_INFORMATION BasicInfo;
			IO_COUNTERS IoInfo;
		} JOBOBJECT_BASIC_AND_IO_ACCOUNTING_INFORMATION, *PJOBOBJECT_BASIC_AND_IO_ACCOUNTING_INFORMATION;

		typedef struct _JOBOBJECT_EXTENDED_LIMIT_INFORMATION {
			JOBOBJECT_BASIC_LIMIT_INFORMATION BasicLimitInformation;
			IO_COUNTERS IoInfo;
			ULONG ProcessMemoryLimit;
			ULONG JobMemoryLimit;
			ULONG PeakProcessMemoryUsed;
			ULONG PeakJobMemoryUsed;
		} JOBOBJECT_EXTENDED_LIMIT_INFORMATION, *PJOBOBJECT_EXTENDED_LIMIT_INFORMATION;

		typedef struct _SID_AND_ATTRIBUTES {
			PSID Sid;
			DWORD Attributes;
		} SID_AND_ATTRIBUTES, *PSID_AND_ATTRIBUTES;

		typedef struct _TOKEN_USER { // Information Class 1
			SID_AND_ATTRIBUTES User;
		} TOKEN_USER, *PTOKEN_USER;

		typedef enum _IO_COMPLETION_INFORMATION_CLASS {
			IoCompletionBasicInformation
		} IO_COMPLETION_INFORMATION_CLASS;

		typedef struct _IO_COMPLETION_BASIC_INFORMATION {
			LONG SignalState;
		} IO_COMPLETION_BASIC_INFORMATION, *PIO_COMPLETION_BASIC_INFORMATION;

		typedef struct _PORT_MESSAGE {
			USHORT DataSize;
			USHORT MessageSize;
			USHORT MessageType;
			USHORT VirtualRangesOffset;
			CLIENT_ID ClientId;
			ULONG MessageId;
			ULONG SectionSize;
			// UCHAR Data[];
		} PORT_MESSAGE, *PPORT_MESSAGE;

		typedef struct _PORT_SECTION_WRITE {
			ULONG Length;
			HANDLE SectionHandle;
			ULONG SectionOffset;
			ULONG ViewSize;
			PVOID ViewBase;
			PVOID TargetViewBase;
		} PORT_SECTION_WRITE, *PPORT_SECTION_WRITE;

		typedef struct _PORT_SECTION_READ {
			ULONG Length;
			ULONG ViewSize;
			ULONG ViewBase;
		} PORT_SECTION_READ, *PPORT_SECTION_READ;

		typedef enum _PORT_INFORMATION_CLASS {
			PortBasicInformation
		} PORT_INFORMATION_CLASS;

		typedef struct _PORT_BASIC_INFORMATION {
		} PORT_BASIC_INFORMATION, *PPORT_BASIC_INFORMATION;

		typedef VOID(NTAPI *PIO_APC_ROUTINE)(PVOID ApcContext,
			PIO_STATUS_BLOCK IoStatusBlock,
			ULONG Reserved);

		typedef struct _FILE_NOTIFY_INFORMATION {
			ULONG NextEntryOffset;
			ULONG Action;
			ULONG NameLength;
			ULONG Name[1];
		} FILE_NOTIFY_INFORMATION, *PFILE_NOTIFY_INFORMATION;

		typedef struct _FILE_FULL_EA_INFORMATION {
			ULONG NextEntryOffset;
			UCHAR Flags;
			UCHAR EaNameLength;
			USHORT EaValueLength;
			CHAR EaName[1];
			// UCHAR EaData[]; // Variable length data not declared
		} FILE_FULL_EA_INFORMATION, *PFILE_FULL_EA_INFORMATION;

		typedef struct _FILE_GET_EA_INFORMATION {
			ULONG NextEntryOffset;
			UCHAR EaNameLength;
			CHAR EaName[1];
		} FILE_GET_EA_INFORMATION, *PFILE_GET_EA_INFORMATION;

		typedef enum _FSINFOCLASS {
			FileFsVolumeInformation = 1, // 1 Y N
			FileFsLabelInformation, // 2 N Y
			FileFsSizeInformation, // 3 Y N
			FileFsDeviceInformation, // 4 Y N
			FileFsAttributeInformation, // 5 Y N
			FileFsControlInformation, // 6 Y Y
			FileFsFullSizeInformation, // 7 Y N
			FileFsObjectIdInformation // 8 Y Y
		} FS_INFORMATION_CLASS, *PFS_INFORMATION_CLASS;

		typedef struct _FILE_FS_VOLUME_INFORMATION {
			LARGE_INTEGER VolumeCreationTime;
			ULONG VolumeSerialNumber;
			ULONG VolumeLabelLength;
			UCHAR Unknown;
			WCHAR VolumeLabel[1];
		} FILE_FS_VOLUME_INFORMATION, *PFILE_FS_VOLUME_INFORMATION;

		typedef struct _FILE_FS_LABEL_INFORMATION {
			ULONG VolumeLabelLength;
			WCHAR VolumeLabel;
		} FILE_FS_LABEL_INFORMATION, *PFILE_FS_LABEL_INFORMATION;

		typedef struct _FILE_FS_SIZE_INFORMATION {
			LARGE_INTEGER TotalAllocationUnits;
			LARGE_INTEGER AvailableAllocationUnits;
			ULONG SectorsPerAllocationUnit;
			ULONG BytesPerSector;
		} FILE_FS_SIZE_INFORMATION, *PFILE_FS_SIZE_INFORMATION;

		typedef struct _FILE_FS_DEVICE_INFORMATION {
			DWORD DeviceType;
			ULONG Characteristics;
		} FILE_FS_DEVICE_INFORMATION, *PFILE_FS_DEVICE_INFORMATION;

		typedef struct _FILE_FS_ATTRIBUTE_INFORMATION {
			ULONG FileSystemFlags;
			ULONG MaximumComponentNameLength;
			ULONG FileSystemNameLength;
			WCHAR FileSystemName[1];
		} FILE_FS_ATTRIBUTE_INFORMATION, *PFILE_FS_ATTRIBUTE_INFORMATION;

		typedef struct _FILE_FS_CONTROL_INFORMATION {
			LARGE_INTEGER Reserved[3];
			LARGE_INTEGER DefaultQuotaThreshold;
			LARGE_INTEGER DefaultQuotaLimit;
			ULONG QuotaFlags;
		} FILE_FS_CONTROL_INFORMATION, *PFILE_FS_CONTROL_INFORMATION;

		typedef struct _FILE_FS_FULL_SIZE_INFORMATION {
			LARGE_INTEGER TotalQuotaAllocationUnits;
			LARGE_INTEGER AvailableQuotaAllocationUnits;
			LARGE_INTEGER AvailableAllocationUnits;
			ULONG SectorsPerAllocationUnit;
			ULONG BytesPerSector;
		} FILE_FS_FULL_SIZE_INFORMATION, *PFILE_FS_FULL_SIZE_INFORMATION;

		typedef struct _FILE_FS_OBJECT_ID_INFORMATION {
			GUID VolumeObjectId;
			ULONG VolumeObjectIdExtendedInfo[12];
		} FILE_FS_OBJECT_ID_INFORMATION, *PFILE_FS_OBJECT_ID_INFORMATION;

		typedef struct _FILE_USER_QUOTA_INFORMATION {
			ULONG NextEntryOffset;
			ULONG SidLength;
			LARGE_INTEGER ChangeTime;
			LARGE_INTEGER QuotaUsed;
			LARGE_INTEGER QuotaThreshold;
			LARGE_INTEGER QuotaLimit;
			SID Sid[1];
		} FILE_USER_QUOTA_INFORMATION, *PFILE_USER_QUOTA_INFORMATION;

		typedef struct _FILE_QUOTA_LIST_INFORMATION {
			ULONG NextEntryOffset;
			ULONG SidLength;
			SID Sid[1];
		} FILE_QUOTA_LIST_INFORMATION, *PFILE_QUOTA_LIST_INFORMATION;

		typedef enum _FILE_INFORMATION_CLASS {
			FileDirectoryInformation = 1, // 1 Y N D
			FileFullDirectoryInformation, // 2 Y N D
			FileBothDirectoryInformation, // 3 Y N D
			FileBasicInformation, // 4 Y Y F
			FileStandardInformation, // 5 Y N F
			FileInternalInformation, // 6 Y N F
			FileEaInformation, // 7 Y N F
			FileAccessInformation, // 8 Y N F
			FileNameInformation, // 9 Y N F
			FileRenameInformation, // 10 N Y F
			FileLinkInformation, // 11 N Y F
			FileNamesInformation, // 12 Y N D
			FileDispositionInformation, // 13 N Y F
			FilePositionInformation, // 14 Y Y F
			FileModeInformation = 16, // 16 Y Y F
			FileAlignmentInformation, // 17 Y N F
			FileAllInformation, // 18 Y N F
			FileAllocationInformation, // 19 N Y F
			FileEndOfFileInformation, // 20 N Y F
			FileAlternateNameInformation, // 21 Y N F
			FileStreamInformation, // 22 Y N F
			FilePipeInformation, // 23 Y Y F
			FilePipeLocalInformation, // 24 Y N F
			FilePipeRemoteInformation, // 25 Y Y F
			FileMailslotQueryInformation, // 26 Y N F
			FileMailslotSetInformation, // 27 N Y F
			FileCompressionInformation, // 28 Y N F
			FileObjectIdInformation, // 29 Y Y F
			FileCompletionInformation, // 30 N Y F
			FileMoveClusterInformation, // 31 N Y F
			FileQuotaInformation, // 32 Y Y F
			FileReparsePointInformation, // 33 Y N F
			FileNetworkOpenInformation, // 34 Y N F
			FileAttributeTagInformation, // 35 Y N F
			FileTrackingInformation // 36 N Y F
		} FILE_INFORMATION_CLASS, *PFILE_INFORMATION_CLASS;

		typedef struct _FILE_DIRECTORY_INFORMATION { // Information Class 1
			ULONG NextEntryOffset;
			ULONG Unknown;
			LARGE_INTEGER CreationTime;
			LARGE_INTEGER LastAccessTime;
			LARGE_INTEGER LastWriteTime;
			LARGE_INTEGER ChangeTime;
			LARGE_INTEGER EndOfFile;
			LARGE_INTEGER AllocationSize;
			ULONG FileAttributes;
			ULONG FileNameLength;
			WCHAR FileName[1];
		} FILE_DIRECTORY_INFORMATION, *PFILE_DIRECTORY_INFORMATION;

		typedef struct _FILE_FULL_DIRECTORY_INFORMATION { // Information Class 2
			ULONG NextEntryOffset;
			ULONG Unknown;
			LARGE_INTEGER CreationTime;
			LARGE_INTEGER LastAccessTime;
			LARGE_INTEGER LastWriteTime;
			LARGE_INTEGER ChangeTime;
			LARGE_INTEGER EndOfFile;
			LARGE_INTEGER AllocationSize;
			ULONG FileAttributes;
			ULONG FileNameLength;
			ULONG EaInformationLength;
			WCHAR FileName[1];
		} FILE_FULL_DIRECTORY_INFORMATION, *PFILE_FULL_DIRECTORY_INFORMATION;

		typedef struct _FILE_BOTH_DIRECTORY_INFORMATION { // Information Class 3
			ULONG NextEntryOffset;
			ULONG Unknown;
			LARGE_INTEGER CreationTime;
			LARGE_INTEGER LastAccessTime;
			LARGE_INTEGER LastWriteTime;
			LARGE_INTEGER ChangeTime;
			LARGE_INTEGER EndOfFile;
			LARGE_INTEGER AllocationSize;
			ULONG FileAttributes;
			ULONG FileNameLength;
			ULONG EaInformationLength;
			UCHAR AlternateNameLength;
			WCHAR AlternateName[12];
			WCHAR FileName[1];
		} FILE_BOTH_DIRECTORY_INFORMATION, *PFILE_BOTH_DIRECTORY_INFORMATION;

		typedef struct _FILE_BASIC_INFORMATION { // Information Class 4
			LARGE_INTEGER CreationTime;
			LARGE_INTEGER LastAccessTime;
			LARGE_INTEGER LastWriteTime;
			LARGE_INTEGER ChangeTime;
			ULONG FileAttributes;
		} FILE_BASIC_INFORMATION, *PFILE_BASIC_INFORMATION;

		typedef struct _FILE_STANDARD_INFORMATION { // Information Class 5
			LARGE_INTEGER AllocationSize;
			LARGE_INTEGER EndOfFile;
			ULONG NumberOfLinks;
			BOOLEAN DeletePending;
			BOOLEAN Directory;
		} FILE_STANDARD_INFORMATION, *PFILE_STANDARD_INFORMATION;

		typedef struct _FILE_INTERNAL_INFORMATION { // Information Class 6
			LARGE_INTEGER FileId;
		} FILE_INTERNAL_INFORMATION, *PFILE_INTERNAL_INFORMATION;

		typedef struct _FILE_EA_INFORMATION { // Information Class 7
			ULONG EaInformationLength;
		} FILE_EA_INFORMATION, *PFILE_EA_INFORMATION;

		typedef struct _FILE_ACCESS_INFORMATION { // Information Class 8
			ACCESS_MASK GrantedAccess;
		} FILE_ACCESS_INFORMATION, *PFILE_ACCESS_INFORMATION;

		typedef struct _FILE_NAME_INFORMATION { // Information Classes 9 and 21
			ULONG FileNameLength;
			WCHAR FileName[1];
		} FILE_NAME_INFORMATION, *PFILE_NAME_INFORMATION, 
			FILE_ALTERNATE_NAME_INFORMATION, *PFILE_ALTERNATE_NAME_INFORMATION;

		typedef struct _FILE_LINK_RENAME_INFORMATION { // Info Classes 10 and 11
			BOOLEAN ReplaceIfExists;
			HANDLE RootDirectory;
			ULONG FileNameLength;
			WCHAR FileName[1];
		} FILE_LINK_INFORMATION, *PFILE_LINK_INFORMATION,
			FILE_RENAME_INFORMATION, *PFILE_RENAME_INFORMATION;

		typedef struct _FILE_NAMES_INFORMATION { // Information Class 12
			ULONG NextEntryOffset;
			ULONG Unknown;
			ULONG FileNameLength;
			WCHAR FileName[1];
		} FILE_NAMES_INFORMATION, *PFILE_NAMES_INFORMATION;

		typedef struct _FILE_DISPOSITION_INFORMATION { // Information Class 13
			BOOLEAN DeleteFile;
		} FILE_DISPOSITION_INFORMATION, *PFILE_DISPOSITION_INFORMATION;

		typedef struct _FILE_POSITION_INFORMATION { // Information Class 14
			LARGE_INTEGER CurrentByteOffset;
		} FILE_POSITION_INFORMATION, *PFILE_POSITION_INFORMATION;

		typedef struct _FILE_MODE_INFORMATION { // Information Class 16
			ULONG Mode;
		} FILE_MODE_INFORMATION, *PFILE_MODE_INFORMATION;

		typedef struct _FILE_ALIGNMENT_INFORMATION { // Information Class 17
			ULONG AlignmentRequirement;
		} FILE_ALIGNMENT_INFORMATION, *PFILE_ALIGNMENT_INFORMATION;

		typedef struct _FILE_ALL_INFORMATION { // Information Class 18
			FILE_BASIC_INFORMATION BasicInformation;
			FILE_STANDARD_INFORMATION StandardInformation;
			FILE_INTERNAL_INFORMATION InternalInformation;
			FILE_EA_INFORMATION EaInformation;
			FILE_ACCESS_INFORMATION AccessInformation;
			FILE_POSITION_INFORMATION PositionInformation;
			FILE_MODE_INFORMATION ModeInformation;
			FILE_ALIGNMENT_INFORMATION AlignmentInformation;
			FILE_NAME_INFORMATION NameInformation;
		} FILE_ALL_INFORMATION, *PFILE_ALL_INFORMATION;

		typedef struct _FILE_ALLOCATION_INFORMATION { // Information Class 19
			LARGE_INTEGER AllocationSize;
		} FILE_ALLOCATION_INFORMATION, *PFILE_ALLOCATION_INFORMATION;

		typedef struct _FILE_END_OF_FILE_INFORMATION { // Information Class 20
			LARGE_INTEGER EndOfFile;
		} FILE_END_OF_FILE_INFORMATION, *PFILE_END_OF_FILE_INFORMATION;

		typedef struct _FILE_STREAM_INFORMATION { // Information Class 22
			ULONG NextEntryOffset;
			ULONG StreamNameLength;
			LARGE_INTEGER EndOfStream;
			LARGE_INTEGER AllocationSize;
			WCHAR StreamName[1];
		} FILE_STREAM_INFORMATION, *PFILE_STREAM_INFORMATION;

		typedef struct _FILE_PIPE_INFORMATION { // Information Class 23
			ULONG ReadModeMessage;
			ULONG WaitModeBlocking;
		} FILE_PIPE_INFORMATION, *PFILE_PIPE_INFORMATION;

		typedef struct _FILE_PIPE_LOCAL_INFORMATION { // Information Class 24
			ULONG MessageType;
			ULONG Unknown1;
			ULONG MaxInstances;
			ULONG CurInstances;
			ULONG InBufferSize;
			ULONG Unknown2;
			ULONG OutBufferSize;
			ULONG Unknown3[2];
			ULONG ServerEnd;
		} FILE_PIPE_LOCAL_INFORMATION, *PFILE_PIPE_LOCAL_INFORMATION;

		typedef struct _FILE_PIPE_REMOTE_INFORMATION { // Information Class 25
			LARGE_INTEGER CollectDataTimeout;
			ULONG MaxCollectionCount;
		} FILE_PIPE_REMOTE_INFORMATION, *PFILE_PIPE_REMOTE_INFORMATION;

		typedef struct _FILE_MAILSLOT_QUERY_INFORMATION { // Information Class 26
			ULONG MaxMessageSize;
			ULONG Unknown;
			ULONG NextSize;
			ULONG MessageCount;
			LARGE_INTEGER ReadTimeout;
		} FILE_MAILSLOT_QUERY_INFORMATION, *PFILE_MAILSLOT_QUERY_INFORMATION;

		typedef struct _FILE_MAILSLOT_SET_INFORMATION { // Information Class 27
			LARGE_INTEGER ReadTimeout;
		} FILE_MAILSLOT_SET_INFORMATION, *PFILE_MAILSLOT_SET_INFORMATION;

		typedef struct _FILE_COMPRESSION_INFORMATION { // Information Class 28
			LARGE_INTEGER CompressedSize;
			USHORT CompressionFormat;
			UCHAR CompressionUnitShift;
			UCHAR Unknown;
			UCHAR ClusterSizeShift;
		} FILE_COMPRESSION_INFORMATION, *PFILE_COMPRESSION_INFORMATION;

		typedef struct _FILE_COMPLETION_INFORMATION { // Information Class 30
			HANDLE IoCompletionHandle;
			ULONG CompletionKey;
		} FILE_COMPLETION_INFORMATION, *PFILE_COMPLETION_INFORMATION;

		typedef struct _FILE_NETWORK_OPEN_INFORMATION { // Information Class 34
			LARGE_INTEGER CreationTime;
			LARGE_INTEGER LastAccessTime;
			LARGE_INTEGER LastWriteTime;
			LARGE_INTEGER ChangeTime;
			LARGE_INTEGER AllocationSize;
			LARGE_INTEGER EndOfFile;
			ULONG FileAttributes;
		} FILE_NETWORK_OPEN_INFORMATION, *PFILE_NETWORK_OPEN_INFORMATION;

		typedef struct _FILE_ATTRIBUTE_TAG_INFORMATION {// Information Class 35
			ULONG FileAttributes;
			ULONG ReparseTag;
		} FILE_ATTRIBUTE_TAG_INFORMATION, *PFILE_ATTRIBUTE_TAG_INFORMATION;

		typedef enum _KEY_SET_INFORMATION_CLASS {
			KeyLastWriteTimeInformation
		} KEY_SET_INFORMATION_CLASS;

		typedef struct _KEY_LAST_WRITE_TIME_INFORMATION {
			LARGE_INTEGER LastWriteTime;
		} KEY_LAST_WRITE_TIME_INFORMATION, *PKEY_LAST_WRITE_TIME_INFORMATION;

		typedef enum _KEY_INFORMATION_CLASS {
			KeyBasicInformation,
			KeyNodeInformation,
			KeyFullInformation,
			KeyNameInformation
		} KEY_INFORMATION_CLASS;

		typedef struct _KEY_BASIC_INFORMATION {
			LARGE_INTEGER LastWriteTime;
			ULONG TitleIndex;
			ULONG NameLength;
			WCHAR Name[1]; // Variable length string
		} KEY_BASIC_INFORMATION, *PKEY_BASIC_INFORMATION;

		typedef struct _KEY_NODE_INFORMATION {
			LARGE_INTEGER LastWriteTime;
			ULONG TitleIndex;
			ULONG ClassOffset;
			ULONG ClassLength;
			ULONG NameLength;
			WCHAR Name[1]; // Variable length string
			// Class[1]; // Variable length string not declared
		} KEY_NODE_INFORMATION, *PKEY_NODE_INFORMATION;

		typedef struct _KEY_FULL_INFORMATION {
			LARGE_INTEGER LastWriteTime;
			ULONG TitleIndex;
			ULONG ClassOffset;
			ULONG ClassLength;
			ULONG SubKeys;
			ULONG MaxNameLen;
			ULONG MaxClassLen;
			ULONG Values;
			ULONG MaxValueNameLen;
			ULONG MaxValueDataLen;
			WCHAR Class[1]; // Variable length string
		} KEY_FULL_INFORMATION, *PKEY_FULL_INFORMATION;

		typedef struct _KEY_NAME_INFORMATION {
			ULONG NameLength;
			WCHAR Name[1]; // Variable length string
		} KEY_NAME_INFORMATION, *PKEY_NAME_INFORMATION;

		typedef enum _KEY_VALUE_INFORMATION_CLASS {
			KeyValueBasicInformation,
			KeyValueFullInformation,
			KeyValuePartialInformation,
			KeyValueFullInformationAlign64
		} KEY_VALUE_INFORMATION_CLASS;

		typedef struct _KEY_VALUE_BASIC_INFORMATION {
			ULONG TitleIndex;
			ULONG Type;
			ULONG NameLength;
			WCHAR Name[1]; // Variable length string
		} KEY_VALUE_BASIC_INFORMATION, *PKEY_VALUE_BASIC_INFORMATION;

		typedef struct _KEY_VALUE_FULL_INFORMATION {
			ULONG TitleIndex;
			ULONG Type;
			ULONG DataOffset;
			ULONG DataLength;
			ULONG NameLength;
			WCHAR Name[1]; // Variable length string
			// Data[1]; // Variable length data not declared
		} KEY_VALUE_FULL_INFORMATION, *PKEY_VALUE_FULL_INFORMATION;

		typedef struct _KEY_VALUE_PARTIAL_INFORMATION {
			ULONG TitleIndex;
			ULONG Type;
			ULONG DataLength;
			UCHAR Data[1]; // Variable length data
		} KEY_VALUE_PARTIAL_INFORMATION, *PKEY_VALUE_PARTIAL_INFORMATION;

		typedef struct _KEY_VALUE_ENTRY {
			PUNICODE_STRING ValueName;
			ULONG DataLength;
			ULONG DataOffset;
			ULONG Type;
		} KEY_VALUE_ENTRY, *PKEY_VALUE_ENTRY;

		typedef struct _SYSTEM_POWER_POLICY {
			ULONG Revision;
			POWER_ACTION_POLICY PowerButton;
			POWER_ACTION_POLICY SleepButton;
			POWER_ACTION_POLICY LidClose;
			SYSTEM_POWER_STATE LidOpenWake;
			ULONG Reserved1;
			POWER_ACTION_POLICY Idle;
			ULONG IdleTimeout;
			UCHAR IdleSensitivity;
			UCHAR Reserved2[3];
			SYSTEM_POWER_STATE MinSleep;
			SYSTEM_POWER_STATE MaxSleep;
			SYSTEM_POWER_STATE ReducedLatencySleep;
			ULONG WinLogonFlags;
			ULONG Reserved3;
			ULONG DozeS4Timeout;
			ULONG BroadcastCapacityResolution;
			SYSTEM_POWER_LEVEL DischargePolicy[4];
			ULONG VideoTimeout;
			ULONG VideoReserved[4];
			ULONG SpindownTimeout;
			BOOLEAN OptimizeForPower;
			UCHAR FanThrottleTolerance;
			UCHAR ForcedThrottle;
			UCHAR MinThrottle;
			POWER_ACTION_POLICY OverThrottled;
		} SYSTEM_POWER_POLICY, *PSYSTEM_POWER_POLICY;

		typedef struct _SYSTEM_POWER_CAPABILITIES {
			BOOLEAN PowerButtonPresent;
			BOOLEAN SleepButtonPresent;
			BOOLEAN LidPresent;
			BOOLEAN SystemS1;
			BOOLEAN SystemS2;
			BOOLEAN SystemS3;
			BOOLEAN SystemS4;
			BOOLEAN SystemS5;
			BOOLEAN HiberFilePresent;
			BOOLEAN FullWake;
			UCHAR Reserved1[3];
			BOOLEAN ThermalControl;
			BOOLEAN ProcessorThrottle;
			UCHAR ProcessorMinThrottle;
			UCHAR ProcessorThrottleScale;
			UCHAR Reserved2[4];
			BOOLEAN DiskSpinDown;
			UCHAR Reserved3[8];
			BOOLEAN SystemBatteriesPresent;
			BOOLEAN BatteriesAreShortTerm;
			BATTERY_REPORTING_SCALE BatteryScale[3];
			SYSTEM_POWER_STATE AcOnLineWake;
			SYSTEM_POWER_STATE SoftLidWake;
			SYSTEM_POWER_STATE RtcWake;
			SYSTEM_POWER_STATE MinDeviceWakeState;
			SYSTEM_POWER_STATE DefaultLowLatencyWake;
		} SYSTEM_POWER_CAPABILITIES, *PSYSTEM_POWER_CAPABILITIES;

		typedef struct _SYSTEM_BATTERY_STATE {
			BOOLEAN AcOnLine;
			BOOLEAN BatteryPresent;
			BOOLEAN Charging;
			BOOLEAN Discharging;
			BOOLEAN Reserved[4];
			ULONG MaxCapacity;
			ULONG RemainingCapacity;
			ULONG Rate;
			ULONG EstimatedTime;
			ULONG DefaultAlert1;
			ULONG DefaultAlert2;
		} SYSTEM_BATTERY_STATE, *PSYSTEM_BATTERY_STATE;

		typedef struct _ADMINISTRATOR_POWER_POLICY {
			SYSTEM_POWER_STATE MinSleep;
			SYSTEM_POWER_STATE MaxSleep;
			ULONG MinVideoTimeout;
			ULONG MaxVideoTimeout;
			ULONG MinSpindownTimeout;
			ULONG MaxSpindownTimeout;
		} ADMINISTRATOR_POWER_POLICY, *PADMINISTRATOR_POWER_POLICY;

		typedef struct _PROCESSOR_POWER_INFORMATION {
			ULONG Number;
			ULONG MaxMhz;
			ULONG CurrentMhz;
			ULONG MhzLimit;
			ULONG MaxIdleState;
			ULONG CurrentIdleState;
		} PROCESSOR_POWER_INFORMATION, *PPROCESSOR_POWER_INFORMATION;

		typedef struct _SYSTEM_POWER_INFORMATION {
			ULONG MaxIdlenessAllowed;
			ULONG Idleness;
			ULONG TimeRemaining;
			UCHAR CoolingMode;
		} SYSTEM_POWER_INFORMATION, *PSYSTEM_POWER_INFORMATION;

		typedef USHORT LANGID, *PLANGID;

		//static const SYSTEM_INFORMATION_CLASS SystemProcessesAndThreadInformation = 5;

		static DWORD_PTR GetExportedFunction(const char *exportname);

		//========== APC ==========
	
		static VOID NTAPI KiUserApcDispatcher(
			IN PVOID Unused1,
			IN PVOID Unused2, 
			IN PVOID Unused3,
			IN PVOID ContextStart, 
			IN PVOID ContextBody);

		static NTSTATUS NTAPI ZwAlertThread(
			IN HANDLE ThreadHandle);

		static NTSTATUS NTAPI ZwCallbackReturn(
			IN PVOID Result OPTIONAL,
			IN ULONG ResultLength, 
			IN NTSTATUS Status);

		static NTSTATUS NTAPI ZwQueueApcThread(
			IN HANDLE ThreadHandle,
			IN PVOID ApcRoutine, 
			IN PVOID ApcRoutineContext OPTIONAL,
			IN PVOID ApcStatusBlock OPTIONAL, 
			IN ULONG ApcReserved OPTIONAL);

		static NTSTATUS NTAPI ZwTestAlert();

		//========== ATOMS ==========

		static NTSTATUS NTAPI ZwAddAtom(
			IN PWCHAR AtomName,
			OUT PRTL_ATOM Atom);

		static NTSTATUS NTAPI ZwDeleteAtom(
			IN RTL_ATOM Atom);

		static NTSTATUS NTAPI ZwFindAtom(
			IN PWCHAR AtomName,
			OUT PRTL_ATOM Atom OPTIONAL);

		static NTSTATUS NTAPI ZwQueryInformationAtom(
			IN RTL_ATOM Atom,
			IN ATOM_INFORMATION_CLASS AtomInformationClass,
			OUT PVOID AtomInformation, 
			IN ULONG AtomInformationLength,
			OUT PULONG ReturnLength OPTIONAL);

		//========== COMPRESSION ==========

		static NTSTATUS NTAPI RtlCompressBuffer(
			IN ULONG CompressionFormat,
			IN PVOID SourceBuffer, 
			IN ULONG SourceBufferLength,
			OUT PVOID DestinationBuffer, 
			IN ULONG DestinationBufferLength,
			IN ULONG Unknown, 
			OUT PULONG pDestinationSize,
			IN PVOID WorkspaceBuffer);

		static NTSTATUS NTAPI RtlDecompressBuffer(
			IN ULONG CompressionFormat,
			OUT PVOID DestinationBuffer, 
			IN ULONG DestinationBufferLength,
			IN PVOID SourceBuffer, 
			IN ULONG SourceBufferLength, 
			OUT PULONG pDestinationSize);

		static NTSTATUS NTAPI RtlGetCompressionWorkSpaceSize(
			IN ULONG CompressionFormat,
			OUT PULONG pNeededBufferSize, 
			OUT PULONG pUnknown);

		//========== DEBUG ==========

		static NTSTATUS NTAPI DbgPrint(
			IN LPCSTR Format, ...);

		static NTSTATUS NTAPI ZwSystemDebugControl(
			IN SYSDBG_COMMAND Command,
			IN PVOID InputBuffer OPTIONAL, 
			IN  ULONG InputBufferLength,
			OUT PVOID OutputBuffer OPTIONAL, 
			IN ULONG OutputBufferLength,
			OUT PULONG ReturnLength OPTIONAL);

		static USHORT NTAPI RtlCaptureStackBackTrace(
			IN ULONG FramesToSkip,
			IN ULONG FramesToCapture, 
			OUT PVOID *BackTrace, 
			OUT PULONG BackTraceHash);

		static PVOID NTAPI RtlGetCallersAddress(
			OUT PVOID *CallersAddress,
			OUT PVOID *CallersCaller);

		//========== ERROR HANDLING ==========

		static NTSTATUS NTAPI ZwDisplayString(
			IN PUNICODE_STRING String);

		static NTSTATUS NTAPI ZwRaiseException(
			IN EXCEPTION_RECORD* ExceptionRecord,
			IN PCONTEXT ThreadContext, 
			IN BOOLEAN HandleException);

		static NTSTATUS NTAPI ZwRaiseHardError(
			IN NTSTATUS ErrorStatus,
			ULONG NumberOfParameters,
			PUNICODE_STRING UnicodeStringParameterMask OPTIONAL,
			IN HARDERROR_RESPONSE_OPTION ResponseOption,
			OUT PHARDERROR_RESPONSE Response);

		static NTSTATUS NTAPI ZwSetDefaultHardErrorPort(
			IN HANDLE PortHandle);

		//========== EXECUTEABLE IMAGES ==========

		static NTSTATUS NTAPI LdrGetDllHandle(IN PWORD pwPath OPTIONAL, 
			IN PVOID Unused OPTIONAL,
			IN PUNICODE_STRING ModuleFileName, 
			OUT PHANDLE pHModule);

		static NTSTATUS NTAPI LdrGetProcedureAddress(
			IN HMODULE ModuleHandle,
			PANSI_STRING FunctionName OPTIONAL,
			IN WORD Ordinal OPTIONAL, 
			OUT PVOID *FunctionAddress);

		static NTSTATUS NTAPI LdrLoadDll(IN PWCHAR PathToFile OPTIONAL, 
			IN ULONG Flags OPTIONAL, 
			IN PUNICODE_STRING ModuleFileName, 
			OUT PHANDLE ModuleHandle);

		static NTSTATUS NTAPI LdrQueryProcessModuleInformation(
			PSYSTEM_MODULE_INFORMATION SystemModuleInformationBuffer,
			IN ULONG BufferSize,  
			OUT PULONG RequiredSize OPTIONAL);

		static VOID NTAPI LdrShutdownProcess();

		static VOID NTAPI LdrShutdownThread();

		static NTSTATUS NTAPI LdrUnloadDll(
			IN HANDLE ModuleHandle);

		static NTSTATUS NTAPI ZwLoadDriver(
			IN PUNICODE_STRING DriverServiceName);

		static NTSTATUS NTAPI ZwUnloadDriver(
			IN PUNICODE_STRING DriverServiceName);

		static PIMAGE_NT_HEADERS NTAPI RtlImageNtHeader(
			IN PVOID ModuleAddress);

		static PVOID NTAPI RtlImageRvaToVa(
			IN PIMAGE_NT_HEADERS NtHeaders,
			IN PVOID ModuleBase, 
			IN ULONG Rva,
			IN OUT PIMAGE_SECTION_HEADER pLastSection OPTIONAL);

		//========== EXECUTEABLE IMAGES - ENVIRONMENT ==========

		static NTSTATUS NTAPI ZwQuerySystemEnviromentValue(IN PUNICODE_STRING VariableName,
			OUT PWCHAR Value, 
			IN ULONG ValueBufferLength, 
			OUT PULONG RequiredLength OPTIONAL);

		static NTSTATUS NTAPI ZwSetSystemEnviromentValue(
			IN PUNICODE_STRING VariableName,
			IN PUNICODE_STRING Value);

		static NTSTATUS NTAPI RtlCreateEnviroment(
			IN BOOLEAN Inherit, 
			OUT PVOID *Enviroment);

		static NTSTATUS NTAPI RtlDestroyEnviroment(
			IN PVOID Enviroment);

		static NTSTATUS NTAPI RtlSetCurrentEnviroment(
			IN PVOID NewEnviroment,
			OUT PVOID *OldEnviroment OPTIONAL);

		static NTSTATUS NTAPI RtlSetEnvironmentVariable(
			IN OUT PVOID *Enviroment OPTIONAL,
			IN PUNICODE_STRING VariableName,
			IN PUNICODE_STRING VariableValue);

		//========== HARDWARE CONTROL ==========

		static NTSTATUS NTAPI ZwFlushWriteBuffer();

		static NTSTATUS NTAPI ZwShutdownSystem(
			IN SHUTDOWN_ACTION Action);

		//========== LOCALE ==========

		static NTSTATUS NTAPI ZwQueryDefaultLocale(
			IN BOOLEAN UserProfile, 
			OUT PLCID DefaultLocaleId);

		static NTSTATUS NTAPI ZwSetDefaultLocale(
			IN BOOLEAN UserProfile,
			IN LCID DefaultLocaleId);

		//========== HEAP MEMORY ==========

		static PVOID NTAPI RtlAllocateHeap(
			IN PVOID HeapHandle, 
			IN ULONG Flags, 
			IN ULONG Size);

		static PVOID NTAPI RtlCompactHeap(
			IN PVOID HeapHandle, 
			IN ULONG Flags);

		static PVOID NTAPI RtlCreateHeap(
			IN ULONG Flags, 
			IN PVOID Base OPTIONAL,
			IN ULONG Reserve OPTIONAL, 
			IN ULONG Commit, 
			IN BOOLEAN Lock OPTIONAL,
			IN PRTL_HEAP_DEFINITION RtlHeapParams OPTIONAL);

		static NTSTATUS NTAPI RtlDestroyHeap(
			IN PVOID HeapHandle);

		static NTSTATUS NTAPI RtlEnumProcessHeaps(
			IN PHEAP_ENUMERATION_ROUTINE HeapEnumerationRoutine,
			IN PVOID Param OPTIONAL);

		static BOOLEAN NTAPI RtlFreeHeap(
			IN PVOID HeapHandle, 
			IN ULONG Flags OPTIONAL, 
			IN PVOID MemoryPointer);

		static ULONG NTAPI RtlGetProcessHeaps(
			IN ULONG MaxNumberOfHeaps, 
			OUT PVOID *HeapArray);

		static BOOLEAN NTAPI RtlLockHeap(
			IN PVOID HeapHandle);

		static PVOID NTAPI RtlProtectHeap(
			IN PVOID HeapHandle, 
			IN BOOLEAN Protect);

		static PVOID NTAPI RtlReAllocateHeap(
			IN PVOID HeapHandle, 
			IN ULONG Flags,
			IN PVOID MemoryPointer, 
			IN ULONG Size);

		static ULONG NTAPI RtlSizeHeap(
			IN PVOID HeapHandle, 
			IN ULONG Flags, 
			IN PVOID MemoryPointer);

		static BOOLEAN NTAPI RtlUnlockHeap(
			IN PVOID HeapHandle);

		static BOOLEAN NTAPI RtlValidateHeap(
			IN PVOID HeapHandle, 
			IN ULONG Flags,
			IN PVOID AddressToValidate OPTIONAL);

		static BOOLEAN NTAPI RtlValidateProcessHeaps();

		static NTSTATUS NTAPI RtlWalkHeap(
			IN PVOID HeapHandle, 
			IN OUT LPPROCESS_HEAP_ENTRY ProcessHeapEntry);

		//========== VIRTUAL MEMORY ==========

		static NTSTATUS NTAPI ZwAllocateVirtualMemory(
			IN HANDLE ProcessHandle, 
			IN OUT PVOID *BaseAddress,
			IN ULONG ZeroBits, 
			IN OUT PULONG RegionSize, 
			IN ULONG AllocationType, 
			IN ULONG Protect);

		static NTSTATUS NTAPI ZwFlushVirtualMemory(
			IN HANDLE ProcessHandle, 
			IN OUT PVOID *BaseAddress,
			IN OUT PULONG NumberOfBytesToFlush, 
			OUT PIO_STATUS_BLOCK IoStatusBlock);

		static NTSTATUS NTAPI ZwFreeVirtualMemory(
			IN HANDLE ProcessHandle, 
			IN PVOID *BaseAddress,
			IN OUT PULONG RegionSize, 
			IN ULONG FreeType);

		static NTSTATUS NTAPI ZwLockVirtualMemory(
			IN HANDLE ProcessHandle, 
			IN PVOID *BaseAddress,
			IN OUT PULONG NumberOfBytesToLock, 
			IN ULONG LockOption);

		static NTSTATUS NTAPI ZwProtectVirtualMemory(
			IN HANDLE ProcessHandle, 
			IN OUT PVOID *BaseAddress,
			IN OUT PULONG NumberOfBytesToProtect, 
			IN ULONG NewAccessProtection,
			OUT PULONG OldAccessProtection);

		static NTSTATUS NTAPI ZwQueryVirtualMemory(
			IN HANDLE ProcessHandle,
			IN PVOID BaseAddress,
			IN MEMORY_INFORMATION_CLASS MemoryInformationClass,
			OUT PVOID MemoryInformation,
			IN SIZE_T MemoryInformationLength,
			OUT PSIZE_T ReturnLength);

		static NTSTATUS NTAPI ZwWow64QueryVirtualMemory64(
			IN HANDLE ProcessHandle,
			IN PVOID64 BaseAddress,
			IN MEMORY_INFORMATION_CLASS MemoryInformationClass,
			OUT PVOID MemoryInformation,
			IN ULONG64 MemoryInformationLength,
			OUT PULONG64 ReturnLength);

		static NTSTATUS NTAPI ZwReadVirtualMemory(
			IN HANDLE ProcessHandle,
			IN PVOID BaseAddress, 
			OUT PVOID Buffer,
			IN SIZE_T NumberOfBytesToRead,
			OUT PSIZE_T NumberOfBytesRead);

		static NTSTATUS NTAPI ZwWow64ReadVirtualMemory64(
			IN HANDLE ProcessHandle,
			IN PVOID64 BaseAddress, 
			OUT PVOID Buffer,
			IN ULONG64 NumberOfBytesToRead,
			OUT PULONG64 NumberOfBytesRead);

		static NTSTATUS NTAPI ZwUnlockVirtualMemory(
			IN HANDLE ProcessHandle, 
			IN PVOID *BaseAddress,
			IN OUT PULONG NumberOfBytesToUnlock, 
			IN ULONG LockType);

		static NTSTATUS NTAPI ZwWriteVirtualMemory(
			IN HANDLE ProcessHandle,
			IN PVOID BaseAddress, 
			OUT PVOID Buffer,
			IN SIZE_T NumberOfBytesToWrite,
			OUT PSIZE_T NumberOfBytesWritten);

		static NTSTATUS NTAPI ZwWow64WriteVirtualMemory64(
			IN HANDLE ProcessHandle,
			IN PVOID64 BaseAddress, 
			OUT PVOID Buffer,
			IN ULONG64 NumberOfBytesToWrite,
			OUT PULONG64 NumberOfBytesWritten);

		//========== TYPE INDEPENDED ==========

		static NTSTATUS NTAPI ZwClose(
			IN HANDLE ObjectHandle);

		static NTSTATUS NTAPI ZwDuplicateObject(
			IN HANDLE SourceProcessHandle, 
			IN PHANDLE SourceHandle, 
			IN HANDLE TargetProcessHandle, 
			OUT PHANDLE TargetHandle, 
			IN ACCESS_MASK DesiredAccess OPTIONAL, 
			IN BOOLEAN InheritHandle, 
			IN ULONG Options);

		static NTSTATUS NTAPI ZwMakeTemporaryObject(
			IN HANDLE ObjectHandle);

		static NTSTATUS NTAPI ZwQueryObject(
			IN HANDLE ObjectHandle, 
			IN OBJECT_INFORMATION_CLASS ObjectInformationClass, 
			OUT PVOID ObjectInformation, 
			IN ULONG Length, 
			OUT PULONG ResultLength);

		static NTSTATUS NTAPI ZwSetInformationObject(
			IN HANDLE ObjectHandle,
			IN OBJECT_INFORMATION_CLASS ObjectInformationClass, 
			OUT PVOID ObjectInformation,
			IN ULONG Length);

		static NTSTATUS NTAPI ZwSignalAndWaitForSingleObject(
			IN HANDLE ObjectToSignal, 
			IN HANDLE WaitableObject,
			IN BOOLEAN Alertable, 
			IN PLARGE_INTEGER Time OPTIONAL);

		static NTSTATUS NTAPI ZwWaitForMultipleObjects(
			IN ULONG ObjectCount, 
			IN PHANDLE ObjectsArray,
			IN OBJECT_WAIT_TYPE WaitType, 
			IN BOOLEAN Alertable, 
			IN PLARGE_INTEGER TimeOut OPTIONAL);

		static NTSTATUS NTAPI ZwWaitForSingleObject(
			IN HANDLE ObjectHandle, 
			IN BOOLEAN Alertable, 
			IN PLARGE_INTEGER TimeOut OPTIONAL);

		//========== DIRECTORYOBJECT ==========

		static NTSTATUS NTAPI ZwCreateDirectoryObject(
			OUT PHANDLE DirectoryHandle,
			IN ACCESS_MASK DesiredAccess, 
			IN POBJECT_ATTRIBUTES ObjectAttributes);

		static NTSTATUS NTAPI ZwOpenDirectoryObject(
			OUT PHANDLE DirectoryObjectHandle,
			IN ACCESS_MASK DesiredAccess, 
			IN POBJECT_ATTRIBUTES ObjectAttributes);

		static NTSTATUS NTAPI ZwQueryDirectoryObject(
			IN HANDLE DirectoryObjectHandle, 
			OUT POBJDIR_INFORMATION DirObjInformation, 
			IN ULONG BufferLength, 
			IN BOOLEAN GetNextIndex,
			IN BOOLEAN IgnoreInputIndex, 
			IN OUT PULONG ObjectIndex, 
			OUT PULONG DataWritten OPTIONAL);

		//========== EVENT ==========

		static NTSTATUS NTAPI ZwClearEvent(
			IN HANDLE EventHandle);

		static NTSTATUS NTAPI ZwCreateEvent(
			IN HANDLE EventHandle,
			IN ACCESS_MASK DesiredAccess, 
			IN POBJECT_ATTRIBUTES ObjectAttributes,
			IN EVENT_TYPE EventType, 
			IN BOOLEAN InitialState);

		static NTSTATUS NTAPI ZwOpenEvent(
			OUT PHANDLE EventHandle,
			IN ACCESS_MASK DesiredAccess, 
			IN POBJECT_ATTRIBUTES ObjectAttributes);

		static NTSTATUS NTAPI ZwPulseEvent(
			IN HANDLE EventHandle, 
			OUT PLONG PreviousState OPTIONAL);

		static NTSTATUS NTAPI ZwQueryEvent(
			IN HANDLE EventHandle,
			IN EVENT_INFORMATION_CLASS EventInformationClass, 
			OUT PVOID EventInformation, 
			IN ULONG EventInformationLength, 
			OUT PULONG ReturnLength OPTIONAL);

		static NTSTATUS NTAPI ZwResetEvent(
			IN HANDLE EventHandle, 
			OUT PLONG PreviousState OPTIONAL);

		static NTSTATUS NTAPI ZwSetEvent(
			IN HANDLE EventHandle, 
			OUT PLONG PreviousState OPTIONAL);

		//========== EVENTPAIR ==========

		//========== FILE ==========

		//========== IOCOMPLECTION ==========

		//========== KEY ==========

		//========== MUTANT ==========

		static NTSTATUS NTAPI ZwCreateMutant(
			OUT PHANDLE MutantHandle, 
			IN ACCESS_MASK DesiredAccess, 
			IN POBJECT_ATTRIBUTES ObjectAttributes,
			IN BOOLEAN InitialOwner);

		static NTSTATUS NTAPI ZwOpenMutant(
			OUT PHANDLE MutantHandle,
			IN ACCESS_MASK DesiredAccess, 
			IN POBJECT_ATTRIBUTES ObjectAttributes);

		static NTSTATUS NTAPI ZwQueryMutant(
			IN HANDLE MutantHandle,
			IN MUTANT_INFORMATION_CLASS MutantInformationClass,
			OUT PVOID MutantInformation, 
			IN ULONG MutantInformationLength,
			OUT PULONG ResultLength OPTIONAL);

		static NTSTATUS NTAPI ZwReleaseMutant(
			IN HANDLE MutantHandle, 
			OUT PLONG PreviousState OPTIONAL);

		//========== PORT ==========

		//========== PROCESS ==========

		static NTSTATUS NTAPI ZwCreateProcess(
			OUT PHANDLE ProcessHandle, 
			IN POBJECT_ATTRIBUTES ObjectAttributes OPTIONAL,
			IN HANDLE ParentProcess, 
			IN BOOLEAN InheritObjectTable,
			IN HANDLE SectionHandle OPTIONAL,
			IN HANDLE DebugPort OPTIONAL,
			IN HANDLE ExceptionPort OPTIONAL);

		static NTSTATUS NTAPI ZwFlushInstructionCache(
			IN HANDLE ProcessHandle,
			IN PVOID BaseAddress, 
			IN ULONG NumberOfBytesToFlush);

		static NTSTATUS NTAPI ZwOpenProcess(
			OUT PHANDLE ProcessHandle,
			IN ACCESS_MASK AccessMask,
			IN POBJECT_ATTRIBUTES ObjectAttributes,
			IN PCLIENT_ID ClientId);

		static NTSTATUS NTAPI ZwQueryInformationProcess(
			IN HANDLE ProcessHandle,
			IN _PROCESS_INFORMATION_CLASS ProcessInformationClass,
			OUT PVOID ProcessInformation, 
			IN ULONG ProcessInformationLength,
			OUT PULONG ReturnLength);

		static NTSTATUS NTAPI ZwWow64QueryInformationProcess64(
			IN HANDLE ProcessHandle,
			IN _PROCESS_INFORMATION_CLASS ProcessInformationClass,
			OUT PVOID ProcessInformation, 
			IN ULONG ProcessInformationLength,
			OUT PULONG ReturnLength);

		static NTSTATUS NTAPI ZwSetInformationProcess(
			IN HANDLE ProcessHandle,
			IN PROCESS_INFORMATION_CLASS ProcessInformationClass,
			IN PVOID ProcessInformation, 
			IN ULONG ProcessInformationLength);

		static NTSTATUS NTAPI ZwTerminateProcess(
			IN HANDLE ProcessHandle OPTIONAL, 
			IN NTSTATUS ExitStatus);

		static NTSTATUS NTAPI RtlCreateUserProcess(
			IN PUNICODE_STRING ImagePath,
			IN ULONG ObjectAttributes, 
			IN OUT PRTL_USER_PROCESS_PARAMETERS ProcessParameters,
			IN PSECURITY_DESCRIPTOR ProcessSecurityDescriptor OPTIONAL,
			IN PSECURITY_DESCRIPTOR ThreadSecurityDescriptor OPTIONAL,
			IN HANDLE ParentProcess, 
			IN BOOLEAN InhertHandles,
			IN HANDLE DebugPort OPTIONAL, 
			IN HANDLE ExceptionPort OPTIONAL);

		//========== PROFILE ==========

		static NTSTATUS NTAPI ZwCreateProfile(
			OUT PHANDLE ProfileHandle, 
			IN HANDLE Process OPTIONAL,
			IN PVOID ImageBase, 
			IN ULONG ImageSize, 
			IN ULONG BucketSize, 
			IN PVOID Buffer,
			IN ULONG BufferSize, 
			IN KPROFILE_SOURCE ProfileSource,
			IN KAFFINITY Affinity);

		static NTSTATUS NTAPI ZwQueryIntervalProfile(
			IN KPROFILE_SOURCE ProfileSource, 
			OUT PULONG Inverval);

		static NTSTATUS NTAPI ZwSetIntervalProfile(
			IN ULONG Interval, 
			IN KPROFILE_SOURCE Source);

		static NTSTATUS NTAPI ZwStartProfile(
			IN HANDLE ProfileHandle);

		static NTSTATUS NTAPI ZwStopProfile(
			IN HANDLE ProfileHandle);

		//========== SECTION ==========

		static NTSTATUS NTAPI ZwCreateSection(
			OUT PHANDLE SectionHandle, 
			IN ACCESS_MASK DesiredAccess, 
			IN POBJECT_ATTRIBUTES ObjectAttributes,
			IN PLARGE_INTEGER MaximumSize OPTIONAL, 
			IN ULONG PageAttributes,
			IN ULONG SectionAttributes, 
			IN HANDLE FileHandle OPTIONAL);

		static NTSTATUS NTAPI ZwExtendSection(
			IN HANDLE SectionHandle, 
			IN PLARGE_INTEGER NewSectionSize);

		static NTSTATUS NTAPI ZwMapViewOfSection(
			IN HANDLE SectionHandle, 
			IN HANDLE ProcessHandle,
			IN OUT PVOID *BaseAddress OPTIONAL, 
			IN ULONG ZeroBits OPTIONAL,
			IN ULONG CommitSize, 
			IN OUT PLARGE_INTEGER SectionOffset OPTIONAL,
			IN OUT PULONG ViewSize, 
			IN SECTION_INHERIT InheritDisposition,
			IN ULONG AllocationType OPTIONAL, 
			IN ULONG Protect);

		static NTSTATUS NTAPI ZwOpenSection(
			OUT PHANDLE SectionHandle,
			IN ACCESS_MASK DesiredAccess, 
			IN POBJECT_ATTRIBUTES ObjectAttributes);

		static NTSTATUS NTAPI ZwQuerySection(
			IN HANDLE SectionHandle,
			SECTION_INFORMATION_CLASS InformationClass,
			OUT PVOID InformationBuffer, 
			IN ULONG InformationBufferSize,
			OUT PULONG ResultLength OPTIONAL);

		static NTSTATUS NTAPI ZwUnmapViewOfSection(
			IN HANDLE ProcessHandle, 
			IN PVOID BaseAddress);

		//========== SEMAPHORE ==========

		static NTSTATUS NTAPI ZwCreateSemaphore(
			OUT PHANDLE SemaphoreHandle, 
			IN ACCESS_MASK DesiredAccess, 
			IN POBJECT_ATTRIBUTES ObjectAttributes, 
			IN ULONG InitialCount, 
			IN ULONG MaximalCount);

		static NTSTATUS NTAPI ZwOpenSemaphore
			(OUT PHANDLE SemaphoreHandle,
			IN ACCESS_MASK DesiredAccess, 
			IN POBJECT_ATTRIBUTES ObjectAttributes);

		static NTSTATUS NTAPI ZwQuerySemaphore(
			IN PHANDLE SemaphoreHandle,
			IN SEMAPHORE_INFORMATION_CLASS SemaphoreInformationClass,
			OUT PVOID SemaphoreInformation, 
			IN ULONG SemaphoreInformationLength,
			OUT PULONG ReturnLength OPTIONAL);

		static NTSTATUS NTAPI ZwReleaseSemaphore(
			IN HANDLE SemaphoreHandle,
			IN ULONG ReleaseCount, 
			OUT PULONG PreviousCount OPTIONAL);

		//========== SYMBOLICLINK ==========

		static NTSTATUS NTAPI ZwCreateSymbolicLinkObject(
			OUT PHANDLE pHandle,
			IN ACCESS_MASK DesiredAccess, 
			IN POBJECT_ATTRIBUTES ObjectAttributes, 
			IN PUNICODE_STRING DestinationName);

		static NTSTATUS NTAPI ZwOpenSymbolicLinkObject(
			OUT PHANDLE pHandle,
			IN ACCESS_MASK DesiredAccess, 
			IN POBJECT_ATTRIBUTES ObjectAttributes);

		static NTSTATUS NTAPI ZwQuerySymbolicLinkObject(
			IN HANDLE SymbolicLinkHandle,
			OUT PUNICODE_STRING pLinkName, 
			OUT PULONG pDataWritten OPTIONAL);

		//========== THREAD ==========

		static NTSTATUS NTAPI ZwGetContextThread(
			IN HANDLE ThreadHandle, 
			OUT PCONTEXT pContext);

		static NTSTATUS NTAPI ZwSetContextThread(
			IN HANDLE ThreadHandle, 
			IN PCONTEXT Context);

		static PVOID NTAPI RtlInitializeContext(
			IN HANDLE ProcessHandle, 
			OUT PCONTEXT ThreadContext,
			IN PVOID ThreadStartParam OPTIONAL, 
			IN PTHREAD_START_ROUTINE ThreadStartAddress,
			IN PINITIAL_TEB InitialTeb);

		static NTSTATUS NTAPI ZwAlertResumeThread(
			IN HANDLE ThreadHandle, 
			OUT PULONG SuspendCount);

		static NTSTATUS NTAPI ZwContinue(
			IN PCONTEXT ThreadContext, 
			IN BOOLEAN RaiseAlert);

		static NTSTATUS NTAPI ZwCreateThread(
			OUT PHANDLE ThreadHandle, 
			IN ACCESS_MASK DesiredAccess, 
			IN POBJECT_ATTRIBUTES ObjectAttributes,
			IN HANDLE ProcessHandles, 
			OUT PCLIENT_ID ClientId, 
			IN PCONTEXT ThreadContext, 
			PINITIAL_TEB InitialTeb,
			IN BOOLEAN CreateSuspended);

		static PTEB NTAPI NtCurrentTeb();

		static NTSTATUS NTAPI ZwDelayExecution(
			IN BOOLEAN Alertable, 
			IN PLARGE_INTEGER DelayInterval);

		static NTSTATUS NTAPI ZwImpersonateThread(
			IN HANDLE ThreadHandle, 
			IN HANDLE ThreadToImpersonate,
			IN PSECURITY_QUALITY_OF_SERVICE SecurityQualityOfService);

		static NTSTATUS NTAPI ZwOpenThread(
			OUT PHANDLE ThreadHandle, 
			IN ACCESS_MASK DesiredAccess, 
			IN POBJECT_ATTRIBUTES ObjectAttributes,
			OUT PCLIENT_ID ClientId);

		static NTSTATUS NTAPI ZwQueryInformationThread(
			IN HANDLE ThreadHandle,
			IN THREADINFOCLASS ThreadInformationClass,
			OUT PVOID ThreadInformation, 
			IN ULONG ThreadInformationLength,
			OUT PULONG ReturnLength);

		static NTSTATUS NTAPI ZwRegisterThreadTerminatePort(
			IN HANDLE PortHandle);

		static NTSTATUS NTAPI ZwResumeThread(
			IN HANDLE ThreadHandle, 
			OUT PULONG SuspendCount OPTIONAL);

		static NTSTATUS NTAPI ZwSetInformationThread(
			IN HANDLE ThreadHandle, 
			IN THREADINFOCLASS ThreadInformationClass,
			OUT PVOID ThreadInformation, 
			IN ULONG ThreadInformationLength);

		static NTSTATUS NTAPI ZwSuspendThread(
			IN HANDLE ThreadHandle, 
			OUT PULONG PreviousSuspendCount OPTIONAL);

		static NTSTATUS NTAPI ZwTerminateThread(
			IN HANDLE ThreadHandle, 
			IN NTSTATUS ExitStatus);

		static NTSTATUS NTAPI ZwYieldExecution();

		static NTSTATUS NTAPI RtlCreateUserThread(
			IN HANDLE ProcessHandle,
			IN PSECURITY_DESCRIPTOR SecurityDescriptor OPTIONAL,
			IN BOOLEAN CreateSupsended, 
			IN ULONG StackZeroBits,
			IN OUT PULONG StackReserved, 
			IN OUT PULONG StackCommit,
			IN PVOID StartAddress, 
			IN PVOID StartParameter OPTIONAL,
			OUT PHANDLE ThreadHandle, 
			OUT PCLIENT_ID ClientId);

		//========== TIMER ==========

		static NTSTATUS NTAPI ZwCancelTimer(
			IN HANDLE TimerHandle, 
			OUT PBOOLEAN CurrentState OPTIONAL);

		static NTSTATUS NTAPI ZwCreateTimer(
			OUT PHANDLE TimerHandle, 
			IN ACCESS_MASK DesiredAccess,
			IN POBJECT_ATTRIBUTES ObjectAttributes, 
			IN ULONG TimerType);

		static NTSTATUS NTAPI ZwOpenTimer(
			OUT PHANDLE TimerHandle, 
			IN ACCESS_MASK DesiredAccess, 
			IN POBJECT_ATTRIBUTES ObjectAttributes);

		static NTSTATUS NTAPI ZwQueryTimer(
			IN HANDLE TimerHandle,
			IN TIMER_INFORMATION_CLASS TimerInformationClass,
			OUT PVOID TimerInformation, 
			IN ULONG TimerInformationLength,
			OUT PULONG ReturnLength OPTIONAL);

		static NTSTATUS NTAPI ZwSetTimer(
			IN HANDLE TimerHandle, 
			IN PLARGE_INTEGER DueTime,
			IN PTIMER_APC_ROUTINE TimerApcRoutine OPTIONAL,
			IN PVOID TimerContext OPTIONAL, 
			IN BOOLEAN ResumeTimer,
			IN LONG Period OPTIONAL, 
			OUT PBOOLEAN PreviousState OPTIONAL);

		//========== TOKEN ==========

		static NTSTATUS NTAPI ZwAdjustGroupsToken(
			IN HANDLE TokenHandle, 
			IN BOOLEAN ResetToDefault, 
			IN PTOKEN_GROUPS TokenGroups, 
			IN ULONG PreviousGroupsLength, 
			OUT PTOKEN_GROUPS PreviousGroups OPTIONAL, 
			OUT PULONG RequiredLength OPTIONAL);

		static NTSTATUS NTAPI ZwAdjustPrivilegesToken(
			IN HANDLE TokenHandle, 
			IN BOOLEAN DisableAllPrivileges,
			IN PTOKEN_PRIVILEGES TokenPrivileges, 
			IN ULONG PreviousPrivilegesLength,
			OUT PTOKEN_PRIVILEGES PreviousPrivileges OPTIONAL, 
			OUT PULONG RequiredLength OPTIONAL);

		static NTSTATUS NTAPI ZwCreateToken(
			OUT PHANDLE TokenHandle, 
			IN ACCESS_MASK DesiredAccess,
			IN POBJECT_ATTRIBUTES ObjectAttributes, 
			IN TOKEN_TYPE TokenType, 
			IN PLUID AuthenticationId, 
			IN PLARGE_INTEGER ExpirationTime, 
			IN PTOKEN_USER TokenUser, 
			IN PTOKEN_GROUPS TokenGroups,
			IN PTOKEN_PRIVILEGES TokenPrivileges,
			IN PTOKEN_OWNER TokenOwner,
			IN PTOKEN_PRIMARY_GROUP TokenPrimaryGroup,
			IN PTOKEN_DEFAULT_DACL TokenDefaultDacl,
			IN PTOKEN_SOURCE TokenSource);

		static NTSTATUS NTAPI ZwDuplicateToken(
			IN HANDLE ExistingToken, 
			IN ACCESS_MASK DesiredAccess,
			IN POBJECT_ATTRIBUTES ObjectAttributes OPTIONAL,
			IN SECURITY_IMPERSONATION_LEVEL ImpersonationLevel,
			IN ULONG TokenType, 
			OUT PHANDLE NewToken);

		static NTSTATUS NTAPI ZwOpenProcessToken(
			IN HANDLE ProcessHandle,
			IN ACCESS_MASK DesiredAccess, 
			OUT PHANDLE TokenHandle);

		static NTSTATUS NTAPI ZwOpenThreadToken(
			IN HANDLE ProcessHandle,
			IN ACCESS_MASK DesiredAccess, 
			IN BOOLEAN OpenAsSelf,
			OUT PHANDLE TokenHandle);

		static NTSTATUS NTAPI ZwQueryInformationToken(
			IN HANDLE TokenHandle,
			IN TOKEN_INFORMATION_CLASS TokenInformationClass,
			OUT PVOID TokenInformation, 
			IN ULONG TokenInformationLength,
			OUT PULONG ReturnLength);

		static NTSTATUS NTAPI ZwSetInformationToken(
			IN HANDLE TokenHandle,
			IN TOKEN_INFORMATION_CLASS TokenInformationClass,
			OUT PVOID TokenInformation, 
			IN ULONG TokenInformationLength);

		//========== SECURITY ==========

		static NTSTATUS NTAPI ZwAccessCheck(
			IN PSECURITY_DESCRIPTOR SecurityDescriptor,
			IN HANDLE ClientToken, 
			IN ACCESS_MASK DesiredAccess, 
			IN PPRIVILEGE_SET RequiredPrivilegesBuffer,
			IN OUT PULONG BufferLength, 
			OUT ACCESS_MASK *GrantedAccess,
			OUT PNTSTATUS AccessStatus);

		static NTSTATUS NTAPI ZwAllocateLocallyUniqueId(
			OUT PLUID LocallyUniqueId);

		static NTSTATUS NTAPI ZwAllocateUuids(
			OUT PLARGE_INTEGER Time, 
			OUT PULONG Range,
			OUT PULONG Sequence);

		static NTSTATUS NTAPI ZwPrivilegeCheck(
			IN HANDLE TokenHandle,
			IN PPRIVILEGE_SET RequiredPrivileges,
			IN PBOOLEAN Result);

		static NTSTATUS NTAPI ZwQuerySecurityObject(
			IN HANDLE ObjectHandle,
			IN SECURITY_INFORMATION SecurityInformationClass,
			OUT PSECURITY_DESCRIPTOR DescriptorBuffer,
			IN ULONG DescriptorBufferLength,
			OUT PULONG RequiredLength);

		static NTSTATUS NTAPI ZwSetSecurityObject(
			IN HANDLE ObjectHandle,
			IN SECURITY_INFORMATION SecurityInformationClass,
			IN PSECURITY_DESCRIPTOR DescriptorBuffer);

		//========== SYSTEM INFORMATION ==========

		static NTSTATUS NTAPI ZwQuerySystemInformation(
			IN SYSTEM_INFORMATION_CLASS SystemInformationClass,
			OUT PVOID SystemInformation,
			IN ULONG SystemInformationLength,
			OUT PULONG ReturnLength OPTIONAL);

		static NTSTATUS NTAPI ZwSetSystemInformation(
			IN SYSTEM_INFORMATION_CLASS SystemInformationClass,
			IN PVOID SystemInformation,
			IN ULONG SystemInformationLength);

		//========== TIME ==========

		static ULONG NTAPI NtGetTickCount();

		static NTSTATUS NTAPI ZwQueryPerformanceCounter(
			OUT PLARGE_INTEGER PerformanceCounter,
			OUT PLARGE_INTEGER PerformanceFrequency OPTIONAL);

		static NTSTATUS NTAPI ZwQuerySystemTime(
			OUT PLARGE_INTEGER SystemTime);

		static NTSTATUS NTAPI ZwQueryTimerResolution(
			OUT PULONG MinimumResolution,
			OUT PULONG MaximumResolution, 
			OUT PULONG CurrentResolution);

		static NTSTATUS NTAPI ZwSetSystemTime(
			IN PLARGE_INTEGER SystemTime, 
			OUT PLARGE_INTEGER PreviousTime OPTIONAL);

		static NTSTATUS NTAPI ZwSetTimerResolution(
			IN ULONG DesiredResolution,
			IN BOOLEAN SetResolution, 
			OUT PULONG CurrentResolution);

		static BOOLEAN NTAPI RtlTimeFieldsToTime(
			IN PTIME_FIELDS TimeFields,
			OUT PLARGE_INTEGER Time);

		static VOID NTAPI RtlTimeToTimeFields(
			IN PLARGE_INTEGER Time,
			OUT PTIME_FIELDS TimeFields);

		//========== UNSORTED ==========

		// --- VIRTUAL MEMORY

		static NTSTATUS NTAPI ZwAllocateUserPhysicalPages(
			IN HANDLE ProcessHandle,
			IN PULONG NumberOfPages,
			OUT PULONG PageFrameNumbers);

		static NTSTATUS NTAPI ZwFreeUserPhysicalPages(
			IN HANDLE ProcessHandle,
			IN OUT PULONG NumberOfPages,
			IN PULONG PageFrameNumbers);

		static NTSTATUS NTAPI ZwMapUserPhysicalPages(
			IN PVOID BaseAddress,
			IN PULONG NumberOfPages,
			IN PULONG PageFrameNumbers);

		static NTSTATUS NTAPI ZwMapUserPhysicalPagesScatter(
			IN PVOID *BaseAddresses,
			IN PULONG NumberOfPages,
			IN PULONG PageFrameNumbers);

		static NTSTATUS NTAPI ZwGetWriteWatch(
			IN HANDLE ProcessHandle,
			IN ULONG Flags,
			IN PVOID BaseAddress,
			IN ULONG RegionSize,
			OUT PULONG Buffer,
			IN OUT PULONG BufferEntries,
			OUT PULONG Granularity);

		static NTSTATUS NTAPI ZwResetWriteWatch(
			IN HANDLE ProcessHandle,
			IN PVOID BaseAddress,
			IN ULONG RegionSize);

		// --- SECTIONS

		static NTSTATUS NTAPI ZwAreMappedFilesTheSame(
			IN PVOID Address1,
			IN PVOID Address2);

		// --- THREADS

		static NTSTATUS NTAPI ZwImpersonateAnonymousToken(
			IN HANDLE ThreadHandle);

		// --- PROCESSES

		static NTSTATUS NTAPI RtlCreateProcessParameters(
			OUT PPROCESS_PARAMETERS *ProcessParameters,
			IN PUNICODE_STRING ImageFile,
			IN PUNICODE_STRING DllPath OPTIONAL,
			IN PUNICODE_STRING CurrentDirectory OPTIONAL,
			IN PUNICODE_STRING CommandLine OPTIONAL,
			IN ULONG CreationFlags,
			IN PUNICODE_STRING WindowTitle OPTIONAL,
			IN PUNICODE_STRING Desktop OPTIONAL,
			IN PUNICODE_STRING Reserved OPTIONAL,
			IN PUNICODE_STRING Reserved2 OPTIONAL);

		static NTSTATUS NTAPI RtlDestroyProcessParameters(
			IN PPROCESS_PARAMETERS ProcessParameters);

		PDEBUG_BUFFER NTAPI RtlCreateQueryDebugBuffer(
			IN ULONG Size,
			IN BOOLEAN EventPair);

		static NTSTATUS NTAPI RtlQueryProcessDebugInformation(
			IN ULONG ProcessId,
			IN ULONG DebugInfoClassMask,
			IN OUT PDEBUG_BUFFER DebugBuffer);

		static NTSTATUS NTAPI RtlDestroyQueryDebugBuffer(
			IN PDEBUG_BUFFER DebugBuffer);

		// --- JOBS

		static NTSTATUS NTAPI ZwCreateJobObject(
			OUT PHANDLE JobHandle,
			IN ACCESS_MASK DesiredAccess,
			IN POBJECT_ATTRIBUTES ObjectAttributes);

		static NTSTATUS NTAPI ZwOpenJobObject(
			OUT PHANDLE JobHandle,
			IN ACCESS_MASK DesiredAccess,
			IN POBJECT_ATTRIBUTES ObjectAttributes);

		static NTSTATUS NTAPI ZwTerminateJobObject(
			IN HANDLE JobHandle,
			IN NTSTATUS ExitStatus);

		static NTSTATUS NTAPI ZwAssignProcessToJobObject(
			IN HANDLE JobHandle,
			IN HANDLE ProcessHandle);

		static NTSTATUS NTAPI ZwQueryInformationJobObject(
			IN HANDLE JobHandle,
			IN JOBOBJECTINFOCLASS JobInformationClass,
			OUT PVOID JobInformation,
			IN ULONG JobInformationLength,
			OUT PULONG ReturnLength OPTIONAL);

		static NTSTATUS NTAPI ZwSetInformationJobObject(
			IN HANDLE JobHandle,
			IN JOBOBJECTINFOCLASS JobInformationClass,
			IN PVOID JobInformation,
			IN ULONG JobInformationLength);

		// --- TOKENS

		static NTSTATUS NTAPI ZwFilterToken(
			IN HANDLE ExistingTokenHandle,
			IN ULONG Flags,
			IN PTOKEN_GROUPS SidsToDisable,
			IN PTOKEN_PRIVILEGES PrivilegesToDelete,
			IN PTOKEN_GROUPS SidsToRestricted,
			OUT PHANDLE NewTokenHandle);

		// --- IOCOMPLECTION

		static NTSTATUS NTAPI ZwCreateIoCompletion(
			OUT PHANDLE IoCompletionHandle,
			IN ACCESS_MASK DesiredAccess,
			IN POBJECT_ATTRIBUTES ObjectAttributes,
			IN ULONG NumberOfConcurrentThreads);

		static NTSTATUS NTAPI ZwOpenIoCompletion(
			OUT PHANDLE IoCompletionHandle,
			IN ACCESS_MASK DesiredAccess,
			IN POBJECT_ATTRIBUTES ObjectAttributes);

		static NTSTATUS NTAPI ZwSetIoCompletion(
			IN HANDLE IoCompletionHandle,
			IN ULONG CompletionKey,
			IN ULONG CompletionValue,
			IN NTSTATUS Status,
			IN ULONG Information);

		static NTSTATUS NTAPI ZwRemoveIoCompletion(
			IN HANDLE IoCompletionHandle,
			OUT PULONG CompletionKey,
			OUT PULONG CompletionValue,
			OUT PIO_STATUS_BLOCK IoStatusBlock,
			IN PLARGE_INTEGER Timeout OPTIONAL);

		static NTSTATUS NTAPI ZwQueryIoCompletion(
			IN HANDLE IoCompletionHandle,
			IN IO_COMPLETION_INFORMATION_CLASS IoCompletionInformationClass,
			OUT PVOID IoCompletionInformation,
			IN ULONG IoCompletionInformationLength,
			OUT PULONG ResultLength OPTIONAL);

		// --- EVENTPAIR

		static NTSTATUS NTAPI ZwCreateEventPair(
			OUT PHANDLE EventPairHandle,
			IN ACCESS_MASK DesiredAccess,
			IN POBJECT_ATTRIBUTES ObjectAttributes);

		static NTSTATUS NTAPI ZwOpenEventPair(
			OUT PHANDLE EventPairHandle,
			IN ACCESS_MASK DesiredAccess,
			IN POBJECT_ATTRIBUTES ObjectAttributes);

		static NTSTATUS NTAPI ZwWaitLowEventPair(
			IN HANDLE EventPairHandle);

		static NTSTATUS NTAPI ZwWaitHighEventPair(
			IN HANDLE EventPairHandle);

		static NTSTATUS NTAPI ZwSetLowWaitHighEventPair(
			IN HANDLE EventPairHandle);

		static NTSTATUS NTAPI ZwSetHighWaitLowEventPair(
			IN HANDLE EventPairHandle);

		static NTSTATUS NTAPI ZwSetLowEventPair(
			IN HANDLE EventPairHandle);

		static NTSTATUS NTAPI ZwSetHighEventPair(
			IN HANDLE EventPairHandle);

		// --- PORTS

		static NTSTATUS NTAPI ZwCreatePort(
			OUT PHANDLE PortHandle,
			IN POBJECT_ATTRIBUTES ObjectAttributes,
			IN ULONG MaxDataSize,
			IN ULONG MaxMessageSize,
			IN ULONG Reserved);

		static NTSTATUS NTAPI ZwCreateWaitablePort(
			OUT PHANDLE PortHandle,
			IN POBJECT_ATTRIBUTES ObjectAttributes,
			IN ULONG MaxDataSize,
			IN ULONG MaxMessageSize,
			IN ULONG Reserved);

		static NTSTATUS NTAPI ZwConnectPort(
			OUT PHANDLE PortHandle,
			IN PUNICODE_STRING PortName,
			IN PSECURITY_QUALITY_OF_SERVICE SecurityQos,
			IN OUT PPORT_SECTION_WRITE WriteSection OPTIONAL,
			IN OUT PPORT_SECTION_READ ReadSection OPTIONAL,
			OUT PULONG MaxMessageSize OPTIONAL,
			IN OUT PVOID ConnectData OPTIONAL,
			IN OUT PULONG ConnectDataLength OPTIONAL);

		static NTSTATUS NTAPI ZwSecureConnectPort(
			OUT PHANDLE PortHandle,
			IN PUNICODE_STRING PortName,
			IN PSECURITY_QUALITY_OF_SERVICE SecurityQos,
			IN OUT PPORT_SECTION_WRITE WriteSection OPTIONAL,
			IN PSID ServerSid OPTIONAL,
			IN OUT PPORT_SECTION_READ ReadSection OPTIONAL,
			OUT PULONG MaxMessageSize OPTIONAL,
			IN OUT PVOID ConnectData OPTIONAL,
			IN OUT PULONG ConnectDataLength OPTIONAL);

		static NTSTATUS NTAPI ZwListenPort(
			IN HANDLE PortHandle,
			OUT PPORT_MESSAGE Message);

		static NTSTATUS NTAPI ZwAcceptConnectPort(
			OUT PHANDLE PortHandle,
			IN ULONG PortIdentifier,
			IN PPORT_MESSAGE Message,
			IN BOOLEAN Accept,
			IN OUT PPORT_SECTION_WRITE WriteSection OPTIONAL,
			IN OUT PPORT_SECTION_READ ReadSection OPTIONAL);

		static NTSTATUS NTAPI ZwCompleteConnectPort(
			IN HANDLE PortHandle);

		static NTSTATUS NTAPI ZwRequestPort(
			IN HANDLE PortHandle,
			IN PPORT_MESSAGE RequestMessage);

		static NTSTATUS NTAPI ZwRequestWaitReplyPort(
			IN HANDLE PortHandle,
			IN PPORT_MESSAGE RequestMessage,
			OUT PPORT_MESSAGE ReplyMessage);

		static NTSTATUS NTAPI ZwReplyPort(
			IN HANDLE PortHandle,
			IN PPORT_MESSAGE ReplyMessage);

		static NTSTATUS NTAPI ZwReplyWaitReplyPort(
			IN HANDLE PortHandle,
			IN OUT PPORT_MESSAGE ReplyMessage);

		static NTSTATUS NTAPI ZwReplyWaitReceivePort(
			IN HANDLE PortHandle,
			OUT PULONG PortIdentifier OPTIONAL,
			IN PPORT_MESSAGE ReplyMessage OPTIONAL,
			OUT PPORT_MESSAGE Message);

		static NTSTATUS NTAPI ZwReplyWaitReceivePortEx(
			IN HANDLE PortHandle,
			OUT PULONG PortIdentifier OPTIONAL,
			IN PPORT_MESSAGE ReplyMessage OPTIONAL,
			OUT PPORT_MESSAGE Message,
			IN PLARGE_INTEGER Timeout);

		static NTSTATUS NTAPI ZwReadRequestData(
			IN HANDLE PortHandle,
			IN PPORT_MESSAGE Message,
			IN ULONG Index,
			OUT PVOID Buffer,
			IN ULONG BufferLength,
			OUT PULONG ReturnLength OPTIONAL);

		static NTSTATUS NTAPI ZwWriteRequestData(
			IN HANDLE PortHandle,
			IN PPORT_MESSAGE Message,
			IN ULONG Index,
			IN PVOID Buffer,
			IN ULONG BufferLength,
			OUT PULONG ReturnLength OPTIONAL);

		static NTSTATUS NTAPI ZwQueryInformationPort(
			IN HANDLE PortHandle,
			IN PORT_INFORMATION_CLASS PortInformationClass,
			OUT PVOID PortInformation,
			IN ULONG PortInformationLength,
			OUT PULONG ReturnLength OPTIONAL);

		static NTSTATUS NTAPI ZwImpersonateClientOfPort(
			IN HANDLE PortHandle,
			IN PPORT_MESSAGE Message);

		// --- FILE

		static NTSTATUS NTAPI ZwCreateFile(
			OUT PHANDLE FileHandle,
			IN ACCESS_MASK DesiredAccess,
			IN POBJECT_ATTRIBUTES ObjectAttributes,
			OUT PIO_STATUS_BLOCK IoStatusBlock,
			IN PLARGE_INTEGER AllocationSize OPTIONAL,
			IN ULONG FileAttributes,
			IN ULONG ShareAccess,
			IN ULONG CreateDisposition,
			IN ULONG CreateOptions,
			IN PVOID EaBuffer OPTIONAL,
			IN ULONG EaLength);

		static NTSTATUS NTAPI ZwOpenFile(
			OUT PHANDLE FileHandle,
			IN ACCESS_MASK DesiredAccess,
			IN POBJECT_ATTRIBUTES ObjectAttributes,
			OUT PIO_STATUS_BLOCK IoStatusBlock,
			IN ULONG ShareAccess,
			IN ULONG OpenOptions);

		static NTSTATUS NTAPI ZwDeleteFile(
			IN POBJECT_ATTRIBUTES ObjectAttributes);

		static NTSTATUS NTAPI ZwFlushBuffersFile(
			IN HANDLE FileHandle,
			OUT PIO_STATUS_BLOCK IoStatusBlock);

		static NTSTATUS NTAPI ZwCancelIoFile(
			IN HANDLE FileHandle,
			OUT PIO_STATUS_BLOCK IoStatusBlock);

		static NTSTATUS NTAPI ZwReadFile(
			IN HANDLE FileHandle,
			IN HANDLE Event OPTIONAL,
			IN PIO_APC_ROUTINE ApcRoutine OPTIONAL,
			IN PVOID ApcContext OPTIONAL,
			OUT PIO_STATUS_BLOCK IoStatusBlock,
			OUT PVOID Buffer,
			IN ULONG Length,
			IN PLARGE_INTEGER ByteOffset OPTIONAL,
			IN PULONG Key OPTIONAL);

		static NTSTATUS NTAPI ZwWriteFile(
			IN HANDLE FileHandle,
			IN HANDLE Event OPTIONAL,
			IN PIO_APC_ROUTINE ApcRoutine OPTIONAL,
			IN PVOID ApcContext OPTIONAL,
			OUT PIO_STATUS_BLOCK IoStatusBlock,
			IN PVOID Buffer,
			IN ULONG Length,
			IN PLARGE_INTEGER ByteOffset OPTIONAL,
			IN PULONG Key OPTIONAL);

		static NTSTATUS NTAPI ZwReadFileScatter(
			IN HANDLE FileHandle,
			IN HANDLE Event OPTIONAL,
			IN PIO_APC_ROUTINE ApcRoutine OPTIONAL,
			IN PVOID ApcContext OPTIONAL,
			OUT PIO_STATUS_BLOCK IoStatusBlock,
			IN PFILE_SEGMENT_ELEMENT Buffer,
			IN ULONG Length,
			IN PLARGE_INTEGER ByteOffset OPTIONAL,
			IN PULONG Key OPTIONAL);

		static NTSTATUS NTAPI ZwWriteFileGather(
			IN HANDLE FileHandle,
			IN HANDLE Event OPTIONAL,
			IN PIO_APC_ROUTINE ApcRoutine OPTIONAL,
			IN PVOID ApcContext OPTIONAL,
			OUT PIO_STATUS_BLOCK IoStatusBlock,
			IN PFILE_SEGMENT_ELEMENT Buffer,
			IN ULONG Length,
			IN PLARGE_INTEGER ByteOffset OPTIONAL,
			IN PULONG Key OPTIONAL);

		static NTSTATUS NTAPI ZwLockFile(
			IN HANDLE FileHandle,
			IN HANDLE Event OPTIONAL,
			IN PIO_APC_ROUTINE ApcRoutine OPTIONAL,
			IN PVOID ApcContext OPTIONAL,
			OUT PIO_STATUS_BLOCK IoStatusBlock,
			IN PULARGE_INTEGER LockOffset,
			IN PULARGE_INTEGER LockLength,
			IN ULONG Key,
			IN BOOLEAN FailImmediately,
			IN BOOLEAN ExclusiveLock);

		static NTSTATUS NTAPI ZwUnlockFile(
			IN HANDLE FileHandle,
			OUT PIO_STATUS_BLOCK IoStatusBlock,
			IN PULARGE_INTEGER LockOffset,
			IN PULARGE_INTEGER LockLength,
			IN ULONG Key);

		static NTSTATUS NTAPI ZwDeviceIoControlFile(
			IN HANDLE FileHandle,
			IN HANDLE Event OPTIONAL,
			IN PIO_APC_ROUTINE ApcRoutine OPTIONAL,
			IN PVOID ApcContext OPTIONAL,
			OUT PIO_STATUS_BLOCK IoStatusBlock,
			IN ULONG IoControlCode,
			IN PVOID InputBuffer OPTIONAL,
			IN ULONG InputBufferLength,
			OUT PVOID OutputBuffer OPTIONAL,
			IN ULONG OutputBufferLength);

		static NTSTATUS NTAPI ZwFsControlFile(
			IN HANDLE FileHandle,
			IN HANDLE Event OPTIONAL,
			IN PIO_APC_ROUTINE ApcRoutine OPTIONAL,
			IN PVOID ApcContext OPTIONAL,
			OUT PIO_STATUS_BLOCK IoStatusBlock,
			IN ULONG FsControlCode,
			IN PVOID InputBuffer OPTIONAL,
			IN ULONG InputBufferLength,
			OUT PVOID OutputBuffer OPTIONAL,
			IN ULONG OutputBufferLength);

		static NTSTATUS NTAPI ZwNotifyChangeDirectoryFile(
			IN HANDLE FileHandle,
			IN HANDLE Event OPTIONAL,
			IN PIO_APC_ROUTINE ApcRoutine OPTIONAL,
			IN PVOID ApcContext OPTIONAL,
			OUT PIO_STATUS_BLOCK IoStatusBlock,
			OUT PFILE_NOTIFY_INFORMATION Buffer,
			IN ULONG BufferLength,
			IN ULONG NotifyFilter,
			IN BOOLEAN WatchSubtree);

		static NTSTATUS NTAPI ZwQueryEaFile(
			IN HANDLE FileHandle,
			OUT PIO_STATUS_BLOCK IoStatusBlock,
			OUT PFILE_FULL_EA_INFORMATION Buffer,
			IN ULONG BufferLength,
			IN BOOLEAN ReturnSingleEntry,
			IN PFILE_GET_EA_INFORMATION EaList OPTIONAL,
			IN ULONG EaListLength,
			IN PULONG EaIndex OPTIONAL,
			IN BOOLEAN RestartScan);

		static NTSTATUS NTAPI ZwSetEaFile(
			IN HANDLE FileHandle,
			OUT PIO_STATUS_BLOCK IoStatusBlock,
			IN PFILE_FULL_EA_INFORMATION Buffer,
			IN ULONG BufferLength);

		static NTSTATUS NTAPI ZwCreateNamedPipeFile(
			OUT PHANDLE FileHandle,
			IN ACCESS_MASK DesiredAccess,
			IN POBJECT_ATTRIBUTES ObjectAttributes,
			OUT PIO_STATUS_BLOCK IoStatusBlock,
			IN ULONG ShareAccess,
			IN ULONG CreateDisposition,
			IN ULONG CreateOptions,
			IN BOOLEAN TypeMessage,
			IN BOOLEAN ReadmodeMessage,
			IN BOOLEAN Nonblocking,
			IN ULONG MaxInstances,
			IN ULONG InBufferSize,
			IN ULONG OutBufferSize,
			IN PLARGE_INTEGER DefaultTimeout OPTIONAL);

		static NTSTATUS NTAPI ZwCreateMailslotFile(
			OUT PHANDLE FileHandle,
			IN ACCESS_MASK DesiredAccess,
			IN POBJECT_ATTRIBUTES ObjectAttributes,
			OUT PIO_STATUS_BLOCK IoStatusBlock,
			IN ULONG CreateOptions,
			IN ULONG InBufferSize,
			IN ULONG MaxMessageSize,
			IN PLARGE_INTEGER ReadTimeout OPTIONAL);

		static NTSTATUS NTAPI ZwQueryVolumeInformationFile(
			IN HANDLE FileHandle,
			OUT PIO_STATUS_BLOCK IoStatusBlock,
			OUT PVOID VolumeInformation,
			IN ULONG VolumeInformationLength,
			IN FS_INFORMATION_CLASS VolumeInformationClass);

		static NTSTATUS NTAPI ZwSetVolumeInformationFile(
			IN HANDLE FileHandle,
			OUT PIO_STATUS_BLOCK IoStatusBlock,
			IN PVOID Buffer,
			IN ULONG BufferLength,
			IN FS_INFORMATION_CLASS VolumeInformationClass);

		static NTSTATUS NTAPI ZwQueryQuotaInformationFile(
			IN HANDLE FileHandle,
			OUT PIO_STATUS_BLOCK IoStatusBlock,
			OUT PFILE_USER_QUOTA_INFORMATION Buffer,
			IN ULONG BufferLength,
			IN BOOLEAN ReturnSingleEntry,
			IN PFILE_QUOTA_LIST_INFORMATION QuotaList OPTIONAL,
			IN ULONG QuotaListLength,
			IN PSID ResumeSid OPTIONAL,
			IN BOOLEAN RestartScan);

		static NTSTATUS NTAPI ZwSetQuotaInformationFile(
			IN HANDLE FileHandle,
			OUT PIO_STATUS_BLOCK IoStatusBlock,
			IN PFILE_USER_QUOTA_INFORMATION Buffer,
			IN ULONG BufferLength);

		static NTSTATUS NTAPI ZwQueryAttributesFile(
			IN POBJECT_ATTRIBUTES ObjectAttributes,
			OUT PFILE_BASIC_INFORMATION FileInformation);

		static NTSTATUS NTAPI ZwQueryFullAttributesFile(
			IN POBJECT_ATTRIBUTES ObjectAttributes,
			OUT PFILE_NETWORK_OPEN_INFORMATION FileInformation);

		static NTSTATUS NTAPI ZwQueryInformationFile(
			IN HANDLE FileHandle,
			OUT PIO_STATUS_BLOCK IoStatusBlock,
			OUT PVOID FileInformation,
			IN ULONG FileInformationLength,
			IN FILE_INFORMATION_CLASS FileInformationClass);

		static NTSTATUS NTAPI ZwSetInformationFile(
			IN HANDLE FileHandle,
			OUT PIO_STATUS_BLOCK IoStatusBlock,
			IN PVOID FileInformation,
			IN ULONG FileInformationLength,
			IN FILE_INFORMATION_CLASS FileInformationClass);

		static NTSTATUS NTAPI ZwQueryDirectoryFile(
			IN HANDLE FileHandle,
			IN HANDLE Event OPTIONAL,
			IN PIO_APC_ROUTINE ApcRoutine OPTIONAL,
			IN PVOID ApcContext OPTIONAL,
			OUT PIO_STATUS_BLOCK IoStatusBlock,
			OUT PVOID FileInformation,
			IN ULONG FileInformationLength,
			IN FILE_INFORMATION_CLASS FileInformationClass,
			IN BOOLEAN ReturnSingleEntry,
			IN PUNICODE_STRING FileName OPTIONAL,
			IN BOOLEAN RestartScan);

		static NTSTATUS NTAPI ZwQueryOleDirectoryFile(
			IN HANDLE FileHandle,
			IN HANDLE Event OPTIONAL,
			IN PIO_APC_ROUTINE ApcRoutine OPTIONAL,
			IN PVOID ApcContext OPTIONAL,
			OUT PIO_STATUS_BLOCK IoStatusBlock,
			OUT PVOID Buffer,
			IN ULONG BufferLength,
			IN FILE_INFORMATION_CLASS FileInformationClass,
			IN BOOLEAN ReturnSingleEntry,
			IN PUNICODE_STRING FileName,
			IN BOOLEAN RestartScan);

		// --- KEY

		static NTSTATUS NTAPI ZwCreateKey(
			OUT PHANDLE KeyHandle,
			IN ACCESS_MASK DesiredAccess,
			IN POBJECT_ATTRIBUTES ObjectAttributes,
			IN ULONG TitleIndex,
			IN PUNICODE_STRING Class OPTIONAL,
			IN ULONG CreateOptions,
			OUT PULONG Disposition OPTIONAL);

		static NTSTATUS NTAPI ZwOpenKey(
			OUT PHANDLE KeyHandle,
			IN ACCESS_MASK DesiredAccess,
			IN POBJECT_ATTRIBUTES ObjectAttributes);

		static NTSTATUS NTAPI ZwDeleteKey(
			IN HANDLE KeyHandle);

		static NTSTATUS NTAPI ZwFlushKey(
			IN HANDLE KeyHandle);

		static NTSTATUS NTAPI ZwSaveKey(
			IN HANDLE KeyHandle,
			IN HANDLE FileHandle);

		static NTSTATUS NTAPI ZwSaveMergedKeys(
			IN HANDLE KeyHandle1,
			IN HANDLE KeyHandle2,
			IN HANDLE FileHandle);

		static NTSTATUS NTAPI ZwRestoreKey(
			IN HANDLE KeyHandle,
			IN HANDLE FileHandle,
			IN ULONG Flags);

		static NTSTATUS NTAPI ZwLoadKey(
			IN POBJECT_ATTRIBUTES KeyObjectAttributes,
			IN POBJECT_ATTRIBUTES FileObjectAttributes);

		static NTSTATUS NTAPI ZwLoadKey2(
			IN POBJECT_ATTRIBUTES KeyObjectAttributes,
			IN POBJECT_ATTRIBUTES FileObjectAttributes,
			IN ULONG Flags);

		static NTSTATUS NTAPI ZwUnloadKey(
			IN POBJECT_ATTRIBUTES KeyObjectAttributes);

		static NTSTATUS NTAPI ZwQueryOpenSubKeys(
			IN POBJECT_ATTRIBUTES KeyObjectAttributes,
			OUT PULONG NumberOfKeys);

		static NTSTATUS NTAPI ZwReplaceKey(
			IN POBJECT_ATTRIBUTES NewFileObjectAttributes,
			IN HANDLE KeyHandle,
			IN POBJECT_ATTRIBUTES OldFileObjectAttributes);

		static NTSTATUS NTAPI ZwSetInformationKey(
			IN HANDLE KeyHandle,
			IN KEY_SET_INFORMATION_CLASS KeyInformationClass,
			IN PVOID KeyInformation,
			IN ULONG KeyInformationLength);

		static NTSTATUS NTAPI ZwQueryKey(
			IN HANDLE KeyHandle,
			IN KEY_INFORMATION_CLASS KeyInformationClass,
			OUT PVOID KeyInformation,
			IN ULONG KeyInformationLength,
			OUT PULONG ResultLength);

		static NTSTATUS NTAPI ZwEnumerateKey(
			IN HANDLE KeyHandle,
			IN ULONG Index,
			IN KEY_INFORMATION_CLASS KeyInformationClass,
			OUT PVOID KeyInformation,
			IN ULONG KeyInformationLength,
			OUT PULONG ResultLength);

		static NTSTATUS NTAPI ZwNotifyChangeKey(
			IN HANDLE KeyHandle,
			IN HANDLE EventHandle OPTIONAL,
			IN PIO_APC_ROUTINE ApcRoutine OPTIONAL,
			IN PVOID ApcContext OPTIONAL,
			OUT PIO_STATUS_BLOCK IoStatusBlock,
			IN ULONG NotifyFilter,
			IN BOOLEAN WatchSubtree,
			IN PVOID Buffer,
			IN ULONG BufferLength,
			IN BOOLEAN Asynchronous);

		static NTSTATUS NTAPI ZwNotifyChangeMultipleKeys(
			IN HANDLE KeyHandle,
			IN ULONG Flags,
			IN POBJECT_ATTRIBUTES KeyObjectAttributes,
			IN HANDLE EventHandle OPTIONAL,
			IN PIO_APC_ROUTINE ApcRoutine OPTIONAL,
			IN PVOID ApcContext OPTIONAL,
			OUT PIO_STATUS_BLOCK IoStatusBlock,
			IN ULONG NotifyFilter,
			IN BOOLEAN WatchSubtree,
			IN PVOID Buffer,
			IN ULONG BufferLength,
			IN BOOLEAN Asynchronous);

		static NTSTATUS NTAPI ZwDeleteValueKey(
			IN HANDLE KeyHandle,
			IN PUNICODE_STRING ValueName);

		static NTSTATUS NTAPI ZwSetValueKey(
			IN HANDLE KeyHandle,
			IN PUNICODE_STRING ValueName,
			IN ULONG TitleIndex,
			IN ULONG Type,
			IN PVOID Data,
			IN ULONG DataSize);

		static NTSTATUS NTAPI ZwQueryValueKey(
			IN HANDLE KeyHandle,
			IN PUNICODE_STRING ValueName,
			IN KEY_VALUE_INFORMATION_CLASS KeyValueInformationClass,
			OUT PVOID KeyValueInformation,
			IN ULONG KeyValueInformationLength,
			OUT PULONG ResultLength);

		static NTSTATUS NTAPI ZwEnumerateValueKey(
			IN HANDLE KeyHandle,
			IN ULONG Index,
			IN KEY_VALUE_INFORMATION_CLASS KeyValueInformationClass,
			OUT PVOID KeyValueInformation,
			IN ULONG KeyValueInformationLength,
			OUT PULONG ResultLength);

		static NTSTATUS NTAPI ZwQueryMultipleValueKey(
			IN HANDLE KeyHandle,
			IN OUT PKEY_VALUE_ENTRY ValueList,
			IN ULONG NumberOfValues,
			OUT PVOID Buffer,
			IN OUT PULONG Length,
			OUT PULONG ReturnLength);

		static NTSTATUS NTAPI ZwInitializeRegistry(
			IN BOOLEAN Setup);

		// --- SECURITY

		static NTSTATUS NTAPI ZwPrivilegeObjectAuditAlarm(
			IN PUNICODE_STRING SubsystemName,
			IN PVOID HandleId,
			IN HANDLE TokenHandle,
			IN ACCESS_MASK DesiredAccess,
			IN PPRIVILEGE_SET Privileges,
			IN BOOLEAN AccessGranted);

		static NTSTATUS NTAPI ZwPrivilegedServiceAuditAlarm(
			IN PUNICODE_STRING SubsystemName,
			IN PUNICODE_STRING ServiceName,
			IN HANDLE TokenHandle,
			IN PPRIVILEGE_SET Privileges,
			IN BOOLEAN AccessGranted);

		static NTSTATUS NTAPI ZwAccessCheckAndAuditAlarm(
			IN PUNICODE_STRING SubsystemName,
			IN PVOID HandleId,
			IN PUNICODE_STRING ObjectTypeName,
			IN PUNICODE_STRING ObjectName,
			IN PSECURITY_DESCRIPTOR SecurityDescriptor,
			IN ACCESS_MASK DesiredAccess,
			IN PGENERIC_MAPPING GenericMapping,
			IN BOOLEAN ObjectCreation,
			OUT PACCESS_MASK GrantedAccess,
			OUT PBOOLEAN AccessStatus,
			OUT PBOOLEAN GenerateOnClose);

		static NTSTATUS NTAPI ZwAccessCheckByType(
			IN PSECURITY_DESCRIPTOR SecurityDescriptor,
			IN PSID PrincipalSelfSid,
			IN HANDLE TokenHandle,
			IN ULONG DesiredAccess,
			IN POBJECT_TYPE_LIST ObjectTypeList,
			IN ULONG ObjectTypeListLength,
			IN PGENERIC_MAPPING GenericMapping,
			IN PPRIVILEGE_SET PrivilegeSet,
			IN PULONG PrivilegeSetLength,
			OUT PACCESS_MASK GrantedAccess,
			OUT PULONG AccessStatus);

		static NTSTATUS NTAPI ZwAccessCheckByTypeAndAuditAlarm(
			IN PUNICODE_STRING SubsystemName,
			IN PVOID HandleId,
			IN PUNICODE_STRING ObjectTypeName,
			IN PUNICODE_STRING ObjectName,
			IN PSECURITY_DESCRIPTOR SecurityDescriptor,
			IN PSID PrincipalSelfSid,
			IN ACCESS_MASK DesiredAccess,
			IN AUDIT_EVENT_TYPE AuditType,
			IN ULONG Flags,
			IN POBJECT_TYPE_LIST ObjectTypeList,
			IN ULONG ObjectTypeListLength,
			IN PGENERIC_MAPPING GenericMapping,
			IN BOOLEAN ObjectCreation,
			OUT PACCESS_MASK GrantedAccess,
			OUT PULONG AccessStatus,
			OUT PBOOLEAN GenerateOnClose);

		static NTSTATUS NTAPI ZwAccessCheckByTypeResultList(
			IN PSECURITY_DESCRIPTOR SecurityDescriptor,
			IN PSID PrincipalSelfSid,
			IN HANDLE TokenHandle,
			IN ACCESS_MASK DesiredAccess,
			IN POBJECT_TYPE_LIST ObjectTypeList,
			IN ULONG ObjectTypeListLength,
			IN PGENERIC_MAPPING GenericMapping,
			IN PPRIVILEGE_SET PrivilegeSet,
			IN PULONG PrivilegeSetLength,
			OUT PACCESS_MASK GrantedAccessList,
			OUT PULONG AccessStatusList);

		static NTSTATUS NTAPI ZwAccessCheckByTypeResultListAndAuditAlarm(
			IN PUNICODE_STRING SubsystemName,
			IN PVOID HandleId,
			IN PUNICODE_STRING ObjectTypeName,
			IN PUNICODE_STRING ObjectName,
			IN PSECURITY_DESCRIPTOR SecurityDescriptor,
			IN PSID PrincipalSelfSid,
			IN ACCESS_MASK DesiredAccess,
			IN AUDIT_EVENT_TYPE AuditType,
			IN ULONG Flags,
			IN POBJECT_TYPE_LIST ObjectTypeList,
			IN ULONG ObjectTypeListLength,
			IN PGENERIC_MAPPING GenericMapping,
			IN BOOLEAN ObjectCreation,
			OUT PACCESS_MASK GrantedAccessList,
			OUT PULONG AccessStatusList,
			OUT PULONG GenerateOnClose);

		static NTSTATUS NTAPI ZwAccessCheckByTypeResultListAndAuditAlarmByHandle(
			IN PUNICODE_STRING SubsystemName,
			IN PVOID HandleId,
			IN HANDLE TokenHandle,
			IN PUNICODE_STRING ObjectTypeName,
			IN PUNICODE_STRING ObjectName,
			IN PSECURITY_DESCRIPTOR SecurityDescriptor,
			IN PSID PrincipalSelfSid,
			IN ACCESS_MASK DesiredAccess,
			IN AUDIT_EVENT_TYPE AuditType,
			IN ULONG Flags,
			IN POBJECT_TYPE_LIST ObjectTypeList,
			IN ULONG ObjectTypeListLength,
			IN PGENERIC_MAPPING GenericMapping,
			IN BOOLEAN ObjectCreation,
			OUT PACCESS_MASK GrantedAccessList,
			OUT PULONG AccessStatusList,
			OUT PULONG GenerateOnClose);

		static NTSTATUS NTAPI ZwOpenObjectAuditAlarm(
			IN PUNICODE_STRING SubsystemName,
			IN PVOID *HandleId,
			IN PUNICODE_STRING ObjectTypeName,
			IN PUNICODE_STRING ObjectName,
			IN PSECURITY_DESCRIPTOR SecurityDescriptor,
			IN HANDLE TokenHandle,
			IN ACCESS_MASK DesiredAccess,
			IN ACCESS_MASK GrantedAccess,
			IN PPRIVILEGE_SET Privileges OPTIONAL,
			IN BOOLEAN ObjectCreation,
			IN BOOLEAN AccessGranted,
			OUT PBOOLEAN GenerateOnClose);

		static NTSTATUS NTAPI ZwCloseObjectAuditAlarm(
			IN PUNICODE_STRING SubsystemName,
			IN PVOID HandleId,
			IN BOOLEAN GenerateOnClose);

		static NTSTATUS NTAPI ZwDeleteObjectAuditAlarm(
			IN PUNICODE_STRING SubsystemName,
			IN PVOID HandleId,
			IN BOOLEAN GenerateOnClose);

		// --- PNP + POWER MANAGEMENT

		static NTSTATUS NTAPI ZwRequestWakeupLatency(
			IN LATENCY_TIME Latency);

		static NTSTATUS NTAPI ZwRequestDeviceWakeup(
			IN HANDLE DeviceHandle);

		static NTSTATUS NTAPI ZwCancelDeviceWakeupRequest(
			IN HANDLE DeviceHandle);

		static BOOLEAN NTAPI ZwIsSystemResumeAutomatic(
			VOID);

		static NTSTATUS NTAPI ZwSetThreadExecutionState(
			IN EXECUTION_STATE ExecutionState,
			OUT PEXECUTION_STATE PreviousExecutionState);

		static NTSTATUS NTAPI ZwGetDevicePowerState(
			IN HANDLE DeviceHandle,
			OUT PDEVICE_POWER_STATE DevicePowerState);

		static NTSTATUS NTAPI ZwSetSystemPowerState(
			IN POWER_ACTION SystemAction,
			IN SYSTEM_POWER_STATE MinSystemState,
			IN ULONG Flags);

		static NTSTATUS NTAPI ZwInitiatePowerAction(
			IN POWER_ACTION SystemAction,
			IN SYSTEM_POWER_STATE MinSystemState,
			IN ULONG Flags,
			IN BOOLEAN Asynchronous);

		static NTSTATUS NTAPI ZwPowerInformation(
			IN POWER_INFORMATION_LEVEL PowerInformationLevel,
			IN PVOID InputBuffer OPTIONAL,
			IN ULONG InputBufferLength,
			OUT PVOID OutputBuffer OPTIONAL,
			IN ULONG OutputBufferLength);

		static NTSTATUS NTAPI ZwPlugPlayControl(
			IN ULONG ControlCode,
			IN OUT PVOID Buffer,
			IN ULONG BufferLength);

		static NTSTATUS NTAPI ZwGetPlugPlayEvent(
			IN ULONG Reserved1,
			IN ULONG Reserved2,
			OUT PVOID Buffer,
			IN ULONG BufferLength);

		// --- MISC

		static NTSTATUS NTAPI ZwW32Call(
			IN ULONG RoutineIndex,
			IN PVOID Argument,
			IN ULONG ArgumentLength,
			OUT PVOID *Result OPTIONAL,
			OUT PULONG ResultLength OPTIONAL);

		static NTSTATUS NTAPI ZwSetLowWaitHighThread(
			VOID);

		static NTSTATUS NTAPI ZwSetHighWaitLowThread(
			VOID);

		static NTSTATUS NTAPI ZwQueryDefaultUILanguage(
			OUT PLANGID LanguageId);

		static NTSTATUS NTAPI ZwSetDefaultUILanguage(
			IN LANGID LanguageId);

		static NTSTATUS NTAPI ZwQueryInstallUILanguage(
			OUT PLANGID LanguageId);

		static NTSTATUS NTAPI ZwSetUuidSeed(
			IN PUCHAR UuidSeed);

		static NTSTATUS NTAPI ZwCreatePagingFile(
			IN PUNICODE_STRING FileName,
			IN PULARGE_INTEGER InitialSize,
			IN PULARGE_INTEGER MaximumSize,
			IN ULONG Reserved);

		static NTSTATUS NTAPI ZwSetLdtEntries(
			IN ULONG Selector1,
			IN LDT_ENTRY LdtEntry1,
			IN ULONG Selector2,
			IN LDT_ENTRY LdtEntry2);

		static NTSTATUS NTAPI ZwVdmControl(
			IN ULONG ControlCode,
			IN PVOID ControlData);

		static NTSTATUS NTAPI RtlAdjustPrivilege(
			IN ULONG Privilege,
			IN BOOLEAN Enable,
			IN BOOLEAN CurrentThread,
			OUT PBOOLEAN Enabled);

		static NTSTATUS NTAPI ZwResumeProcess(IN HANDLE ProcessHandle);

		static NTSTATUS NTAPI ZwSuspendProcess(IN HANDLE ProcessHandle);
	private:
	};

}

#endif