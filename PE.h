#ifndef __GPE__
#define __GPE__

#include <windows.h>
#include "GString.h"

namespace GF {

	class PE {
	public:
		static PIMAGE_DOS_HEADER GetDosHeader(DWORD_PTR module_base);
		static PIMAGE_NT_HEADERS32 GetNtHeader32(DWORD_PTR module_base);
		static PIMAGE_NT_HEADERS64 GetNtHeader64(DWORD_PTR module_base);

		static bool ReadPeHeader(const GString &path, PBYTE pe_header);
		static bool ReadPeHeader(DWORD process_id, DWORD64 module_base, PBYTE pe_header);

		static DWORD64 GetImageBase(DWORD pid);
		static DWORD64 GetImageSize(DWORD pid);
		static DWORD64 GetEntrypoint(DWORD pid);
	private:
	};

}

#endif