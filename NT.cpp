#include "NT.h"

namespace GF {

	DWORD_PTR NT::GetExportedFunction(const char *exportname)
	{
		static HMODULE hmod = NULL;
		if(!hmod) 
			hmod = GetModuleHandleA("ntdll.dll");

		return reinterpret_cast<DWORD_PTR>(GetProcAddress(hmod, exportname));
	}

	//========== APC ==========

	VOID NTAPI NT::KiUserApcDispatcher(
		IN PVOID Unused1,
		IN PVOID Unused2, 
		IN PVOID Unused3,
		IN PVOID ContextStart, 
		IN PVOID ContextBody)
	{
		typedef VOID(NTAPI *_KiUserApcDispatcher)(PVOID, PVOID, PVOID, PVOID, PVOID);
		_KiUserApcDispatcher pKiUserApcDispatcher = (_KiUserApcDispatcher)GetExportedFunction("KiUserApcDispatcher");

		return pKiUserApcDispatcher(Unused1, Unused2, Unused3, ContextStart, ContextBody);
	}

	NTSTATUS NTAPI NT::ZwAlertThread(IN HANDLE ThreadHandle)
	{
		typedef NTSTATUS(NTAPI *_ZwAlertThread)(HANDLE);
		_ZwAlertThread pZwAlertThread = (_ZwAlertThread)GetExportedFunction("ZwAlertThread");

		return pZwAlertThread(ThreadHandle);
	}

	NTSTATUS NTAPI NT::ZwCallbackReturn(
		IN PVOID Result OPTIONAL,
		IN ULONG ResultLength, 
		IN NTSTATUS Status)
	{
		typedef NTSTATUS(NTAPI *_ZwCallbackReturn)(PVOID, ULONG, NTSTATUS);
		_ZwCallbackReturn pZwCallbackReturn = (_ZwCallbackReturn)GetExportedFunction("ZwCallbackReturn");

		return pZwCallbackReturn(Result, ResultLength, Status);
	}

	NTSTATUS NTAPI NT::ZwQueueApcThread(
		IN HANDLE ThreadHandle,
		IN PVOID ApcRoutine, 
		IN PVOID ApcRoutineContext OPTIONAL,
		IN PVOID ApcStatusBlock OPTIONAL,
		IN ULONG ApcReserved OPTIONAL)
	{
		typedef NTSTATUS(NTAPI *_ZwQueueApcThread)(HANDLE, PVOID, PVOID, PVOID, ULONG);
		_ZwQueueApcThread pZwQueueApcThread = (_ZwQueueApcThread)GetExportedFunction("ZwQueueApcThread");

		return pZwQueueApcThread(ThreadHandle, ApcRoutine, ApcRoutineContext, ApcStatusBlock, ApcReserved);
	}

	NTSTATUS NTAPI NT::ZwTestAlert()
	{
		typedef NTSTATUS(NTAPI *_ZwTestAlert)();
		_ZwTestAlert pZwTestAlert = (_ZwTestAlert)GetExportedFunction("ZwTestAlert");

		return pZwTestAlert();
	}

	//========== ATOMS ==========

	NTSTATUS NTAPI NT::ZwAddAtom(
		IN PWCHAR AtomName,
		OUT PRTL_ATOM Atom)
	{
		typedef NTSTATUS(NTAPI *_ZwAddAtom)(PWCHAR, PRTL_ATOM);
		_ZwAddAtom pZwAddAtom = (_ZwAddAtom)GetExportedFunction("ZwAddAtom");

		return pZwAddAtom(AtomName, Atom);
	}

	NTSTATUS NTAPI NT::ZwDeleteAtom(IN RTL_ATOM Atom)
	{
		typedef NTSTATUS(NTAPI *_ZwDeleteAtom)(RTL_ATOM);
		_ZwDeleteAtom pZwDeleteAtom = (_ZwDeleteAtom)GetExportedFunction("ZwDeleteAtom");

		return pZwDeleteAtom(Atom);
	}

	NTSTATUS NTAPI NT::ZwFindAtom(
		IN PWCHAR AtomName,
		OUT PRTL_ATOM Atom OPTIONAL)
	{
		typedef NTSTATUS(NTAPI *_ZwFindAtom)(PWCHAR, PRTL_ATOM);
		_ZwFindAtom pZwFindAtom = (_ZwFindAtom)GetExportedFunction("ZwFindAtom");

		return pZwFindAtom(AtomName, Atom);
	}

	NTSTATUS NTAPI NT::ZwQueryInformationAtom(
		IN RTL_ATOM Atom,
		IN ATOM_INFORMATION_CLASS AtomInformationClass,
		OUT PVOID AtomInformation, 
		IN ULONG AtomInformationLength,
		OUT PULONG ReturnLength OPTIONAL)
	{
		typedef NTSTATUS(NTAPI *_ZwQueryInformationAtom)(RTL_ATOM,
			ATOM_INFORMATION_CLASS, PVOID, ULONG, PULONG);
		_ZwQueryInformationAtom pZwQueryInformationAtom = (_ZwQueryInformationAtom)GetExportedFunction("ZwQueryInformationAtom");

		return pZwQueryInformationAtom(Atom, AtomInformationClass, AtomInformation, AtomInformationLength, ReturnLength);
	}

	//========== COMPRESSION ==========

	NTSTATUS NTAPI NT::RtlCompressBuffer(
		IN ULONG CompressionFormat,
		IN PVOID SourceBuffer, 
		IN ULONG SourceBufferLength,
		OUT PVOID DestinationBuffer, 
		IN ULONG DestinationBufferLength,
		IN ULONG Unknown, 
		OUT PULONG pDestinationSize,
		IN PVOID WorkspaceBuffer)
	{
		typedef NTSTATUS(NTAPI *_RtlCompressBuffer)(ULONG, PVOID, ULONG, PVOID, ULONG, ULONG, PULONG, PVOID);
		_RtlCompressBuffer pRtlCompressBuffer = (_RtlCompressBuffer)GetExportedFunction("RtlCompressBuffer");

		return pRtlCompressBuffer(CompressionFormat, SourceBuffer, SourceBufferLength,
			DestinationBuffer, DestinationBufferLength, Unknown, pDestinationSize, WorkspaceBuffer);
	}

	NTSTATUS NTAPI NT::RtlDecompressBuffer(
		IN ULONG CompressionFormat,
		OUT PVOID DestinationBuffer, 
		IN ULONG DestinationBufferLength,
		IN PVOID SourceBuffer, 
		IN ULONG SourceBufferLength,
		OUT PULONG pDestinationSize)
	{
		typedef NTSTATUS(NTAPI *_RtlDecompressBuffer)(ULONG, PVOID, ULONG, PVOID, ULONG, PULONG);
		_RtlDecompressBuffer pRtlDecompressBuffer = (_RtlDecompressBuffer)GetExportedFunction("RtlDecompressBuffer");

		return pRtlDecompressBuffer(CompressionFormat, DestinationBuffer, DestinationBufferLength,
			SourceBuffer, SourceBufferLength, pDestinationSize);
	}

	NTSTATUS NTAPI NT::RtlGetCompressionWorkSpaceSize(
		IN ULONG CompressionFormat,
		OUT PULONG pNeededBufferSize, 
		OUT PULONG pUnknown)
	{
		typedef NTSTATUS(NTAPI *_RtlGetCompressionWorkSpaceSize)(ULONG, PULONG, PULONG);
		_RtlGetCompressionWorkSpaceSize pRtlGetCompressionWorkSpaceSize = (_RtlGetCompressionWorkSpaceSize)
			GetExportedFunction("RtlGetCompressionWorkSpaceSize");

		return pRtlGetCompressionWorkSpaceSize(CompressionFormat, pNeededBufferSize, pUnknown);
	}

	//========== DEBUG ==========

	NTSTATUS NTAPI NT::DbgPrint(IN LPCSTR Format, ...)
	{
		typedef NTSTATUS(NTAPI *_DbgPrint)(LPCSTR, ...);
		_DbgPrint pDbgPrint = (_DbgPrint)GetExportedFunction("DbgPrint");

		return pDbgPrint(Format);
	}

	NTSTATUS NTAPI NT::ZwSystemDebugControl(
		IN SYSDBG_COMMAND Command,
		IN PVOID InputBuffer OPTIONAL, 
		IN  ULONG InputBufferLength,
		OUT PVOID OutputBuffer OPTIONAL, 
		IN ULONG OutputBufferLength,
		OUT PULONG ReturnLength OPTIONAL)
	{
		typedef NTSTATUS(NTAPI *_ZwSystemDebugControl)(SYSDBG_COMMAND, PVOID, ULONG,
			PVOID, ULONG, PULONG);
		_ZwSystemDebugControl pZwSystemDebugControl = (_ZwSystemDebugControl)
			GetExportedFunction("ZwSystemDebugControl");

		return pZwSystemDebugControl(Command, InputBuffer, InputBufferLength,
			OutputBuffer, OutputBufferLength, ReturnLength);
	}

	USHORT NTAPI NT::RtlCaptureStackBackTrace(
		IN ULONG FramesToSkip,
		IN ULONG FramesToCapture, 
		OUT PVOID *BackTrace, 
		OUT PULONG BackTraceHash)
	{
		typedef USHORT(NTAPI *_RtlCaptureStackBackTrace)(ULONG, ULONG, PVOID, PULONG);
		_RtlCaptureStackBackTrace pRtlCaptureStackBackTrace = (_RtlCaptureStackBackTrace)
			GetExportedFunction("RtlCaptureStackBackTrace");

		return pRtlCaptureStackBackTrace(FramesToSkip, FramesToCapture, BackTrace, BackTraceHash);
	}

	PVOID NTAPI NT::RtlGetCallersAddress(
		OUT PVOID *CallersAddress,
		OUT PVOID *CallersCaller)
	{
		typedef PVOID(NTAPI *_RtlGetCallersAddress)(PVOID, PVOID);
		_RtlGetCallersAddress pRtlGetCallersAddress = (_RtlGetCallersAddress)GetExportedFunction("RtlGetCallersAddress");

		return pRtlGetCallersAddress(CallersAddress, CallersCaller);
	}

	//========== ERROR HANDLING ==========

	NTSTATUS NTAPI NT::ZwDisplayString(IN PUNICODE_STRING String)
	{
		typedef NTSTATUS(NTAPI *_ZwDisplayString)(PUNICODE_STRING);
		_ZwDisplayString pZwDisplayString = (_ZwDisplayString)GetExportedFunction("ZwDisplayString");

		return pZwDisplayString(String);
	}

	NTSTATUS NTAPI NT::ZwRaiseException(
		IN EXCEPTION_RECORD* ExceptionRecord,
		IN PCONTEXT ThreadContext, 
		IN BOOLEAN HandleException)
	{
		typedef NTSTATUS(NTAPI *_ZwRaiseException)(EXCEPTION_RECORD*, PCONTEXT, BOOLEAN);
		_ZwRaiseException pZwRaiseException = (_ZwRaiseException)GetExportedFunction("ZwRaiseException");

		return pZwRaiseException(ExceptionRecord, ThreadContext, HandleException);
	}

	NTSTATUS NTAPI NT::ZwRaiseHardError(
		IN NTSTATUS ErrorStatus,
		ULONG NumberOfParameters,
		PUNICODE_STRING UnicodeStringParameterMask OPTIONAL,
		IN HARDERROR_RESPONSE_OPTION ResponseOption,
		OUT PHARDERROR_RESPONSE Response)
	{
		typedef NTSTATUS(NTAPI *_ZwRaiseHardError)(NTSTATUS, ULONG, PUNICODE_STRING, 
			HARDERROR_RESPONSE_OPTION, PHARDERROR_RESPONSE);
		_ZwRaiseHardError pZwRaiseHardError = (_ZwRaiseHardError)GetExportedFunction("ZwRaiseHardError");

		return pZwRaiseHardError(ErrorStatus, NumberOfParameters, 
			UnicodeStringParameterMask, ResponseOption, Response);
	}

	NTSTATUS NTAPI NT::ZwSetDefaultHardErrorPort(IN HANDLE PortHandle)
	{
		typedef NTSTATUS(NTAPI *_ZwSetDefaultHardErrorPort)(HANDLE);
		_ZwSetDefaultHardErrorPort pZwSetDefaultHardErrorPort = (_ZwSetDefaultHardErrorPort)
			GetExportedFunction("ZwSetDefaultHardErrorPort");

		return pZwSetDefaultHardErrorPort(PortHandle);
	}

	//========== EXECUTEABLE IMAGES ==========

	NTSTATUS NTAPI NT::LdrGetDllHandle(
		IN PWORD pwPath OPTIONAL, 
		IN PVOID Unused OPTIONAL,
		IN PUNICODE_STRING ModuleFileName, 
		OUT PHANDLE pHModule)
	{
		typedef NTSTATUS(NTAPI *_LdrGetDllHandle)(PWORD, PVOID, PUNICODE_STRING, PHANDLE);
		_LdrGetDllHandle pLdrGetDllHandle = (_LdrGetDllHandle)GetExportedFunction("LdrGetDllHandle");

		return pLdrGetDllHandle(pwPath, Unused, ModuleFileName, pHModule);
	}

	NTSTATUS NTAPI NT::LdrGetProcedureAddress(
		IN HMODULE ModuleHandle,
		PANSI_STRING FunctionName OPTIONAL,
		IN WORD Ordinal OPTIONAL, 
		OUT PVOID *FunctionAddress)
	{
		typedef NTSTATUS(NTAPI *_LdrGetProcedureAddress)(HMODULE, PANSI_STRING, WORD, PVOID*);
		_LdrGetProcedureAddress pLdrGetProcedureAddress = (_LdrGetProcedureAddress)
			GetExportedFunction("LdrGetProcedureAddress");

		return pLdrGetProcedureAddress(ModuleHandle, FunctionName, Ordinal, FunctionAddress);
	}

	NTSTATUS NTAPI NT::LdrLoadDll(
		IN PWCHAR PathToFile OPTIONAL, 
		IN ULONG Flags OPTIONAL,
		IN PUNICODE_STRING ModuleFileName, 
		OUT PHANDLE ModuleHandle)
	{
		typedef NTSTATUS(NTAPI *_LdrLoadDll)(PWCHAR, ULONG, PUNICODE_STRING, PHANDLE);
		_LdrLoadDll pLdrLoadDll = (_LdrLoadDll)GetExportedFunction("LdrLoadDll");

		return pLdrLoadDll(PathToFile, Flags, ModuleFileName, ModuleHandle);
	}

	NTSTATUS NTAPI NT::LdrQueryProcessModuleInformation(
		PSYSTEM_MODULE_INFORMATION SystemModuleInformationBuffer,
		IN ULONG BufferSize, 
		OUT PULONG RequiredSize OPTIONAL)
	{
		typedef NTSTATUS(NTAPI *_LdrQueryProcessModuleInformation)(
			PSYSTEM_MODULE_INFORMATION, ULONG, PULONG);

		_LdrQueryProcessModuleInformation pLdrQueryProcessModuleInformation = 
			(_LdrQueryProcessModuleInformation)GetExportedFunction("LdrQueryProcessModuleInformation");

		return pLdrQueryProcessModuleInformation(SystemModuleInformationBuffer, BufferSize, RequiredSize);
	}

	VOID NTAPI NT::LdrShutdownProcess()
	{
		typedef VOID(NTAPI *_LdrShutdownProcess)();
		_LdrShutdownProcess pLdrShutdownProcess = (_LdrShutdownProcess)GetExportedFunction("LdrShutdownProcess");

		return pLdrShutdownProcess();
	}

	VOID NTAPI NT::LdrShutdownThread()
	{
		typedef VOID(NTAPI *_LdrShutdownThread)();
		_LdrShutdownThread pLdrShutdownThread = (_LdrShutdownThread)GetExportedFunction("LdrShutdownThread");

		return pLdrShutdownThread();
	}

	NTSTATUS NTAPI NT::LdrUnloadDll(IN HANDLE ModuleHandle)
	{
		typedef NTSTATUS(NTAPI *_LdrUnloadDll)(HANDLE);
		_LdrUnloadDll pLdrUnloadDll = (_LdrUnloadDll)GetExportedFunction("LdrUnloadDll");

		return pLdrUnloadDll(ModuleHandle);
	}

	NTSTATUS NTAPI NT::ZwLoadDriver(IN PUNICODE_STRING DriverServiceName)
	{
		typedef NTSTATUS(NTAPI *_ZwLoadDriver)(PUNICODE_STRING);
		_ZwLoadDriver pZwLoadDriver = (_ZwLoadDriver)GetExportedFunction("ZwLoadDriver");

		return pZwLoadDriver(DriverServiceName);
	}

	NTSTATUS NTAPI NT::ZwUnloadDriver(IN PUNICODE_STRING DriverServiceName)
	{
		typedef NTSTATUS(NTAPI *_ZwUnloadDriver)(PUNICODE_STRING);
		_ZwUnloadDriver pZwUnloadDriver = (_ZwUnloadDriver)GetExportedFunction("ZwUnloadDriver");

		return pZwUnloadDriver(DriverServiceName);
	}

	PIMAGE_NT_HEADERS NTAPI NT::RtlImageNtHeader(IN PVOID ModuleAddress)
	{
		typedef PIMAGE_NT_HEADERS(NTAPI *_RtlImageNtHeader)(PVOID);
		_RtlImageNtHeader pRtlImageNtHeader = (_RtlImageNtHeader)GetExportedFunction("RtlImageNtHeader");

		return pRtlImageNtHeader(ModuleAddress);
	}

	PVOID NTAPI NT::RtlImageRvaToVa(
		IN PIMAGE_NT_HEADERS NtHeaders,
		IN PVOID ModuleBase, 
		IN ULONG Rva,
		IN OUT PIMAGE_SECTION_HEADER pLastSection OPTIONAL)
	{
		typedef PVOID(NTAPI *_RtlImageRvaToVa)(PIMAGE_NT_HEADERS, PVOID, ULONG, PIMAGE_SECTION_HEADER);
		_RtlImageRvaToVa pRtlImageRvaToVa = (_RtlImageRvaToVa)GetExportedFunction("RtlImageRvaToVa");

		return pRtlImageRvaToVa(NtHeaders, ModuleBase, Rva, pLastSection);
	}

	//========== EXECUTEABLE IMAGES - ENVIRONMENT ==========

	NTSTATUS NTAPI NT::ZwQuerySystemEnviromentValue(
		IN PUNICODE_STRING VariableName,
		OUT PWCHAR Value, 
		IN ULONG ValueBufferLength, 
		OUT PULONG RequiredLength OPTIONAL)
	{
		typedef NTSTATUS(NTAPI *_ZwQuerySystemEnviromentValue)(PUNICODE_STRING, PWCHAR, ULONG, PULONG);
		_ZwQuerySystemEnviromentValue pZwQuerySystemEnviromentValue = (_ZwQuerySystemEnviromentValue)
			GetExportedFunction("ZwQuerySystemEnviromentValue");

		return pZwQuerySystemEnviromentValue(VariableName, Value, ValueBufferLength, RequiredLength);
	}

	NTSTATUS NTAPI NT::ZwSetSystemEnviromentValue(
		IN PUNICODE_STRING VariableName,
		IN PUNICODE_STRING Value)
	{
		typedef NTSTATUS(NTAPI *_ZwSetSystemEnviromentValue)(PUNICODE_STRING, PUNICODE_STRING);
		_ZwSetSystemEnviromentValue pZwSetSystemEnviromentValue = (_ZwSetSystemEnviromentValue)
			GetExportedFunction("ZwSetSystemEnviromentValue");

		return pZwSetSystemEnviromentValue(VariableName, Value);
	}

	NTSTATUS NTAPI NT::RtlCreateEnviroment(
		IN BOOLEAN Inherit, 
		OUT PVOID *Enviroment)
	{
		typedef NTSTATUS(NTAPI *_RtlCreateEnviroment)(BOOLEAN, PVOID*);
		_RtlCreateEnviroment pRtlCreateEnviroment = (_RtlCreateEnviroment)GetExportedFunction("RtlCreateEnviroment");

		return pRtlCreateEnviroment(Inherit, Enviroment);
	}

	NTSTATUS NTAPI NT::RtlDestroyEnviroment(IN PVOID Enviroment)
	{
		typedef NTSTATUS(NTAPI *_RtlDestroyEnviroment)(PVOID);
		_RtlDestroyEnviroment pRtlDestroyEnviroment = (_RtlDestroyEnviroment)GetExportedFunction("RtlDestroyEnviroment");

		return pRtlDestroyEnviroment(Enviroment);
	}

	NTSTATUS NTAPI NT::RtlSetCurrentEnviroment(
		IN PVOID NewEnviroment,
		OUT PVOID *OldEnviroment OPTIONAL)
	{
		typedef NTSTATUS(NTAPI *_RtlSetCurrentEnviroment)(PVOID, PVOID);
		_RtlSetCurrentEnviroment pRtlSetCurrentEnviroment = (_RtlSetCurrentEnviroment)
			GetExportedFunction("RtlSetCurrentEnviroment");

		return pRtlSetCurrentEnviroment(NewEnviroment, OldEnviroment);
	}

	NTSTATUS NTAPI NT::RtlSetEnvironmentVariable(
		IN OUT PVOID *Enviroment OPTIONAL,
		IN PUNICODE_STRING VariableName,
		IN PUNICODE_STRING VariableValue)
	{
		typedef NTSTATUS(NTAPI *_RtlSetEnvironmentVariable)(PVOID, PUNICODE_STRING, PUNICODE_STRING);
		_RtlSetEnvironmentVariable pRtlSetEnvironmentVariable = (_RtlSetEnvironmentVariable)
			GetExportedFunction("RtlSetEnvironmentVariable");

		return pRtlSetEnvironmentVariable(Enviroment, VariableName, VariableValue);
	}

	//========== HARDWARE CONTROL ==========

	NTSTATUS NTAPI NT::ZwFlushWriteBuffer()
	{
		typedef NTSTATUS(NTAPI *_ZwFlushWriteBuffer)();
		_ZwFlushWriteBuffer pZwFlushWriteBuffer = (_ZwFlushWriteBuffer)GetExportedFunction("ZwFlushWriteBuffer");

		return pZwFlushWriteBuffer();
	}

	NTSTATUS NTAPI NT::ZwShutdownSystem(IN SHUTDOWN_ACTION Action)
	{
		typedef NTSTATUS(NTAPI *_ZwShutdownSystem)(SHUTDOWN_ACTION);
		_ZwShutdownSystem pZwShutdownSystem = (_ZwShutdownSystem)GetExportedFunction("ZwShutdownSystem");

		return pZwShutdownSystem(Action);
	}

	//========== LOCALE ==========

	NTSTATUS NTAPI NT::ZwQueryDefaultLocale(
		IN BOOLEAN UserProfile,
		OUT PLCID DefaultLocaleId)
	{
		typedef NTSTATUS(NTAPI *_ZwQueryDefaultLocale)(BOOLEAN, PLCID);
		_ZwQueryDefaultLocale pZwQueryDefaultLocale = (_ZwQueryDefaultLocale)GetExportedFunction("ZwQueryDefaultLocale");

		return pZwQueryDefaultLocale(UserProfile, DefaultLocaleId);
	}

	NTSTATUS NTAPI NT::ZwSetDefaultLocale(
		IN BOOLEAN UserProfile,
		IN LCID DefaultLocaleId)
	{
		typedef NTSTATUS(NTAPI *_ZwSetDefaultLocale)(BOOLEAN, LCID);
		_ZwSetDefaultLocale pZwSetDefaultLocale = (_ZwSetDefaultLocale)GetExportedFunction("ZwSetDefaultLocale");

		return pZwSetDefaultLocale(UserProfile, DefaultLocaleId);
	}

	//========== HEAP MEMORY ==========

	PVOID NTAPI NT::RtlAllocateHeap(
		IN PVOID HeapHandle, 
		IN ULONG Flags,
		IN ULONG Size)
	{
		typedef PVOID(NTAPI *_RtlAllocateHeap)(PVOID, ULONG, ULONG);
		_RtlAllocateHeap pRtlAllocateHeap = (_RtlAllocateHeap)GetExportedFunction("RtlAllocateHeap");

		return pRtlAllocateHeap(HeapHandle, Flags, Size);
	}

	PVOID NTAPI NT::RtlCompactHeap(
		IN PVOID HeapHandle, 
		IN ULONG Flags)
	{
		typedef PVOID(NTAPI *_RtlCompactHeap)(PVOID, ULONG);
		_RtlCompactHeap pRtlCompactHeap = (_RtlCompactHeap)GetExportedFunction("RtlCompactHeap");

		return pRtlCompactHeap(HeapHandle, Flags);
	}

	PVOID NTAPI NT::RtlCreateHeap(
		IN ULONG Flags, 
		IN PVOID Base OPTIONAL,
		IN ULONG Reserve OPTIONAL, 
		IN ULONG Commit, 
		IN BOOLEAN Lock OPTIONAL,
		IN PRTL_HEAP_DEFINITION RtlHeapParams OPTIONAL)
	{
		typedef PVOID(NTAPI *_RtlCreateHeap)(ULONG, PVOID, ULONG, ULONG, BOOLEAN, PRTL_HEAP_DEFINITION);
		_RtlCreateHeap pRtlCreateHeap = (_RtlCreateHeap)GetExportedFunction("RtlCreateHeap");

		return pRtlCreateHeap(Flags, Base, Reserve, Commit, Lock, RtlHeapParams);
	}

	NTSTATUS NTAPI NT::RtlDestroyHeap(IN PVOID HeapHandle)
	{
		typedef NTSTATUS(NTAPI *_RtlDestroyHeap)(PVOID);
		_RtlDestroyHeap pRtlDestroyHeap = (_RtlDestroyHeap)GetExportedFunction("RtlDestroyHeap");

		return pRtlDestroyHeap(HeapHandle);
	}

	NTSTATUS NTAPI NT::RtlEnumProcessHeaps(
		IN PHEAP_ENUMERATION_ROUTINE HeapEnumerationRoutine,
		IN PVOID Param OPTIONAL)
	{
		typedef NTSTATUS(NTAPI *_RtlEnumProcessHeaps)(PHEAP_ENUMERATION_ROUTINE, PVOID);
		_RtlEnumProcessHeaps pRtlEnumProcessHeaps = (_RtlEnumProcessHeaps)
			GetExportedFunction("RtlEnumProcessHeaps");

		return pRtlEnumProcessHeaps(HeapEnumerationRoutine, Param);
	}

	BOOLEAN NTAPI NT::RtlFreeHeap(
		IN PVOID HeapHandle, 
		IN ULONG Flags OPTIONAL,
		IN PVOID MemoryPointer)
	{
		typedef BOOLEAN(NTAPI *_RtlFreeHeap)(PVOID, ULONG, PVOID);
		_RtlFreeHeap pRtlFreeHeap = (_RtlFreeHeap)GetExportedFunction("RtlFreeHeap");

		return pRtlFreeHeap(HeapHandle, Flags, MemoryPointer);
	}

	ULONG NTAPI NT::RtlGetProcessHeaps(
		IN ULONG MaxNumberOfHeaps, 
		OUT PVOID *HeapArray)
	{
		typedef ULONG(NTAPI *_RtlGetProcessHeaps)(ULONG, PVOID);
		_RtlGetProcessHeaps pRtlGetProcessHeaps = (_RtlGetProcessHeaps)GetExportedFunction("RtlGetProcessHeaps");

		return pRtlGetProcessHeaps(MaxNumberOfHeaps, HeapArray);
	}

	BOOLEAN NTAPI NT::RtlLockHeap(IN PVOID HeapHandle)
	{
		typedef BOOLEAN(NTAPI *_RtlLockHeap)(PVOID);
		_RtlLockHeap pRtlLockHeap = (_RtlLockHeap)GetExportedFunction("RtlLockHeap");

		return pRtlLockHeap(HeapHandle);
	}

	PVOID NTAPI NT::RtlProtectHeap(
		IN PVOID HeapHandle, 
		IN BOOLEAN Protect)
	{
		typedef PVOID(NTAPI *_RtlProtectHeap)(PVOID, BOOLEAN);
		_RtlProtectHeap pRtlProtectHeap = (_RtlProtectHeap)GetExportedFunction("RtlProtectHeap");

		return pRtlProtectHeap(HeapHandle, Protect);
	}

	PVOID NTAPI NT::RtlReAllocateHeap(
		IN PVOID HeapHandle, 
		IN ULONG Flags,
		IN PVOID MemoryPointer, 
		IN ULONG Size)
	{
		typedef PVOID(NTAPI *_RtlReAllocateHeap)(PVOID, ULONG, PVOID, ULONG);
		_RtlReAllocateHeap pRtlReAllocateHeap = (_RtlReAllocateHeap)GetExportedFunction("RtlReAllocateHeap");

		return pRtlReAllocateHeap(HeapHandle, Flags, MemoryPointer, Size);
	}

	ULONG NTAPI NT::RtlSizeHeap(
		IN PVOID HeapHandle, 
		IN ULONG Flags, 
		IN PVOID MemoryPointer)
	{
		typedef ULONG(NTAPI *_RtlSizeHeap)(PVOID, ULONG, PVOID);
		_RtlSizeHeap pRtlSizeHeap = (_RtlSizeHeap)GetExportedFunction("RtlSizeHeap");

		return pRtlSizeHeap(HeapHandle, Flags, MemoryPointer);
	}

	BOOLEAN NTAPI NT::RtlUnlockHeap(IN PVOID HeapHandle)
	{
		typedef BOOLEAN(NTAPI *_RtlUnlockHeap)(PVOID);
		_RtlUnlockHeap pRtlUnlockHeap = (_RtlUnlockHeap)GetExportedFunction("RtlUnlockHeap");

		return pRtlUnlockHeap(HeapHandle);
	}

	NTSTATUS NTAPI NT::RtlWalkHeap(
		IN PVOID HeapHandle, 
		IN OUT LPPROCESS_HEAP_ENTRY ProcessHeapEntry)
	{
		typedef NTSTATUS(NTAPI *_RtlWalkHeap)(PVOID, LPPROCESS_HEAP_ENTRY);
		_RtlWalkHeap pRtlWalkHeap = (_RtlWalkHeap)GetExportedFunction("RtlWalkHeap");

		return pRtlWalkHeap(HeapHandle, ProcessHeapEntry);
	}

	BOOLEAN NTAPI NT::RtlValidateHeap(
		IN PVOID HeapHandle, 
		IN ULONG Flags,
		IN PVOID AddressToValidate OPTIONAL)
	{
		typedef BOOLEAN(NTAPI *_RtlValidateHeap)(PVOID, ULONG, PVOID);
		_RtlValidateHeap pRtlValidateHeap = (_RtlValidateHeap)GetExportedFunction("RtlValidateHeap");

		return pRtlValidateHeap(HeapHandle, Flags, AddressToValidate);
	}

	BOOLEAN NTAPI NT::RtlValidateProcessHeaps()
	{
		typedef BOOLEAN(NTAPI *_RtlValidateProcessHeaps)();
		_RtlValidateProcessHeaps pRtlValidateProcessHeaps = (_RtlValidateProcessHeaps)
			GetExportedFunction("RtlValidateProcessHeaps");

		return pRtlValidateProcessHeaps();
	}

	//========== VIRTUAL MEMORY ==========

	NTSTATUS NTAPI NT::ZwAllocateVirtualMemory(
		IN HANDLE ProcessHandle, 
		IN OUT PVOID *BaseAddress,
		IN ULONG ZeroBits, 
		IN OUT PULONG RegionSize, 
		IN ULONG AllocationType, 
		IN ULONG Protect)
	{
		typedef NTSTATUS(NTAPI *_ZwAllocateVirtualMemory)(HANDLE, PVOID*, ULONG, PULONG, ULONG, ULONG);
		_ZwAllocateVirtualMemory pZwAllocateVirtualMemory = (_ZwAllocateVirtualMemory)GetExportedFunction("ZwAllocateVirtualMemory");

		return pZwAllocateVirtualMemory(ProcessHandle, BaseAddress, ZeroBits, RegionSize, AllocationType, Protect);
	}

	NTSTATUS NTAPI NT::ZwFlushVirtualMemory(
		IN HANDLE ProcessHandle, 
		IN OUT PVOID *BaseAddress,
		IN OUT PULONG NumberOfBytesToFlush, 
		OUT PIO_STATUS_BLOCK IoStatusBlock)
	{
		typedef NTSTATUS(NTAPI *_ZwFlushVirtualMemory)(HANDLE, PVOID*, PULONG, PIO_STATUS_BLOCK);
		_ZwFlushVirtualMemory pZwFlushVirtualMemory = (_ZwFlushVirtualMemory)GetExportedFunction("ZwFlushVirtualMemory");

		return pZwFlushVirtualMemory(ProcessHandle, BaseAddress, NumberOfBytesToFlush, IoStatusBlock);
	}

	NTSTATUS NTAPI NT::ZwFreeVirtualMemory(
		IN HANDLE ProcessHandle, 
		IN PVOID *BaseAddress,
		IN OUT PULONG RegionSize,
		IN ULONG FreeType)
	{
		typedef NTSTATUS(NTAPI *_ZwFreeVirtualMemory)(HANDLE, PVOID*, PULONG, ULONG);
		_ZwFreeVirtualMemory pZwFreeVirtualMemory = (_ZwFreeVirtualMemory)GetExportedFunction("ZwFreeVirtualMemory");

		return pZwFreeVirtualMemory(ProcessHandle, BaseAddress, RegionSize, FreeType);
	}

	NTSTATUS NTAPI NT::ZwLockVirtualMemory(
		IN HANDLE ProcessHandle,
		IN PVOID *BaseAddress,
		IN OUT PULONG NumberOfBytesToLock, 
		IN ULONG LockOption)
	{
		typedef NTSTATUS(NTAPI *_ZwLockVirtualMemory)(HANDLE, PVOID*, PULONG, ULONG);
		_ZwLockVirtualMemory pZwLockVirtualMemory = (_ZwLockVirtualMemory)GetExportedFunction("ZwLockVirtualMemory");

		return pZwLockVirtualMemory(ProcessHandle, BaseAddress, NumberOfBytesToLock, LockOption);
	}

	NTSTATUS NTAPI NT::ZwProtectVirtualMemory(
		IN HANDLE ProcessHandle, 
		IN OUT PVOID *BaseAddress,
		IN OUT PULONG NumberOfBytesToProtect, 
		IN ULONG NewAccessProtection,
		OUT PULONG OldAccessProtection)
	{
		typedef NTSTATUS(NTAPI *_ZwProtectVirtualMemory)(HANDLE, PVOID*, PULONG, ULONG, PULONG);
		_ZwProtectVirtualMemory pZwProtectVirtualMemory = (_ZwProtectVirtualMemory)GetExportedFunction("ZwProtectVirtualMemory");

		return pZwProtectVirtualMemory(ProcessHandle, BaseAddress, NumberOfBytesToProtect, NewAccessProtection, OldAccessProtection);
	}

	NTSTATUS NTAPI NT::ZwQueryVirtualMemory(
		IN HANDLE ProcessHandle,
		IN PVOID BaseAddress,
		IN MEMORY_INFORMATION_CLASS MemoryInformationClass,
		OUT PVOID MemoryInformation,
		IN SIZE_T MemoryInformationLength,
		OUT PSIZE_T ReturnLength)
	{
		typedef NTSTATUS(NTAPI *_ZwQueryVirtualMemory)(HANDLE, PVOID, MEMORY_INFORMATION_CLASS, PVOID, SIZE_T, PSIZE_T);
		_ZwQueryVirtualMemory pZwQueryVirtualMemory = (_ZwQueryVirtualMemory)GetExportedFunction("ZwQueryVirtualMemory");

		return pZwQueryVirtualMemory(ProcessHandle, BaseAddress, MemoryInformationClass,
			MemoryInformation, MemoryInformationLength, ReturnLength);
	}

	NTSTATUS NTAPI NT::ZwWow64QueryVirtualMemory64(
		IN HANDLE ProcessHandle,
		IN PVOID64 BaseAddress,
		IN MEMORY_INFORMATION_CLASS MemoryInformationClass,
		OUT PVOID MemoryInformation,
		IN ULONG64 MemoryInformationLength,
		OUT PULONG64 ReturnLength)
	{
		typedef NTSTATUS(NTAPI *_ZwQueryVirtualMemory)(HANDLE, PVOID64, MEMORY_INFORMATION_CLASS, PVOID, ULONG64, PULONG64);
		_ZwQueryVirtualMemory pZwQueryVirtualMemory = (_ZwQueryVirtualMemory)GetExportedFunction("ZwWow64QueryVirtualMemory64");

		return pZwQueryVirtualMemory(ProcessHandle, BaseAddress, MemoryInformationClass,
			MemoryInformation, MemoryInformationLength, ReturnLength);
	}

	NTSTATUS NTAPI NT::ZwReadVirtualMemory(
		IN HANDLE ProcessHandle,
					   IN PVOID BaseAddress, 
					   OUT PVOID Buffer,
					   IN SIZE_T NumberOfBytesToRead,
					   OUT PSIZE_T NumberOfBytesRead)
	{
		typedef NTSTATUS (NTAPI *_ZwReadVirtualMemory)(HANDLE, PVOID, PVOID, SIZE_T, PSIZE_T);
		_ZwReadVirtualMemory pZwReadVirtualMemory = (_ZwReadVirtualMemory)GetExportedFunction("ZwReadVirtualMemory");

		return pZwReadVirtualMemory(ProcessHandle, BaseAddress, Buffer, NumberOfBytesToRead, NumberOfBytesRead);
	}

	NTSTATUS NTAPI NT::ZwWow64ReadVirtualMemory64(
		IN HANDLE ProcessHandle,
							 IN PVOID64 BaseAddress, 
							 OUT PVOID Buffer,
							 IN ULONG64 NumberOfBytesToRead,
							 OUT PULONG64 NumberOfBytesRead)
	{
		typedef NTSTATUS (NTAPI *_ZwReadVirtualMemory)(HANDLE, PVOID64, PVOID, ULONG64, PULONG64);
		_ZwReadVirtualMemory pZwReadVirtualMemory = (_ZwReadVirtualMemory)GetExportedFunction("ZwWow64ReadVirtualMemory64");

		return pZwReadVirtualMemory(ProcessHandle, BaseAddress, Buffer, NumberOfBytesToRead, NumberOfBytesRead);
	}

	NTSTATUS NTAPI NT::ZwUnlockVirtualMemory(
		IN HANDLE ProcessHandle, 
		IN PVOID *BaseAddress,
		IN OUT PULONG NumberOfBytesToUnlock, 
		IN ULONG LockType)
	{
		typedef NTSTATUS(NTAPI *_ZwUnlockVirtualMemory)(HANDLE, PVOID*, PULONG, ULONG);
		_ZwUnlockVirtualMemory pZwUnlockVirtualMemory = (_ZwUnlockVirtualMemory)GetExportedFunction("ZwUnlockVirtualMemory");

		return pZwUnlockVirtualMemory(ProcessHandle, BaseAddress, NumberOfBytesToUnlock, LockType);
	}

	NTSTATUS NTAPI NT::ZwWriteVirtualMemory(
		IN HANDLE ProcessHandle,
		IN PVOID BaseAddress, 
		OUT PVOID Buffer,
		IN SIZE_T NumberOfBytesToWrite,
		OUT PSIZE_T NumberOfBytesWritten)
	{
		typedef NTSTATUS (NTAPI *_ZwWriteVirtualMemory)(HANDLE, PVOID, PVOID, SIZE_T, PSIZE_T);
		_ZwWriteVirtualMemory pZwWriteVirtualMemory = (_ZwWriteVirtualMemory)GetExportedFunction("ZwReadVirtualMemory");

		return pZwWriteVirtualMemory(ProcessHandle, BaseAddress, Buffer, NumberOfBytesToWrite, NumberOfBytesWritten);
	}

	NTSTATUS NTAPI NT::ZwWow64WriteVirtualMemory64(
		IN HANDLE ProcessHandle,
		IN PVOID64 BaseAddress, 
		OUT PVOID Buffer,
		IN ULONG64 NumberOfBytesToWrite,
		OUT PULONG64 NumberOfBytesWritten)
	{
		typedef NTSTATUS (NTAPI *_ZwWriteVirtualMemory)(HANDLE, PVOID64, PVOID, ULONG64, PULONG64);
		_ZwWriteVirtualMemory pZwWriteVirtualMemory = (_ZwWriteVirtualMemory)GetExportedFunction("ZwWow64WriteVirtualMemory64");

		return pZwWriteVirtualMemory(ProcessHandle, BaseAddress, Buffer, NumberOfBytesToWrite, NumberOfBytesWritten);
	}

	//========== TYPE INDEPENDED ==========

	NTSTATUS NTAPI NT::ZwClose(IN HANDLE ObjectHandle)
	{
		typedef NTSTATUS(NTAPI *_ZwClose)(HANDLE);
		_ZwClose pZwClose = (_ZwClose)GetExportedFunction("ZwClose");

		return pZwClose(ObjectHandle);
	}

	NTSTATUS NTAPI NT::ZwDuplicateObject(
		IN HANDLE SourceProcessHandle,
		IN PHANDLE SourceHandle, 
		IN HANDLE TargetProcessHandle, 
		OUT PHANDLE TargetHandle,
		IN ACCESS_MASK DesiredAccess OPTIONAL, 
		IN BOOLEAN InheritHandle, 
		IN ULONG Options)
	{
		typedef NTSTATUS(NTAPI *_ZwDuplicateObject)(HANDLE, PHANDLE, HANDLE, PHANDLE, ACCESS_MASK, BOOLEAN, ULONG);
		_ZwDuplicateObject pZwDuplicateObject = (_ZwDuplicateObject)GetExportedFunction("ZwDuplicateObject");

		return pZwDuplicateObject(SourceProcessHandle, SourceHandle, 
			TargetProcessHandle, TargetHandle, DesiredAccess, InheritHandle, Options);
	}

	NTSTATUS NTAPI NT::ZwMakeTemporaryObject(IN HANDLE ObjectHandle)
	{
		typedef NTSTATUS(NTAPI *_ZwMakeTemporaryObject)(HANDLE);
		_ZwMakeTemporaryObject pZwMakeTemporaryObject = (_ZwMakeTemporaryObject)GetExportedFunction("ZwMakeTemporaryObject");

		return pZwMakeTemporaryObject(ObjectHandle);
	}

	NTSTATUS NTAPI NT::ZwQueryObject(
		IN HANDLE ObjectHandle,
		IN OBJECT_INFORMATION_CLASS ObjectInformationClass,
		OUT PVOID ObjectInformation,
		IN ULONG Length,
		OUT PULONG ResultLength)
	{
		typedef NTSTATUS(NTAPI *_ZwQueryObject)(HANDLE, OBJECT_INFORMATION_CLASS, PVOID, ULONG, PULONG);
		_ZwQueryObject pZwQueryObject = (_ZwQueryObject)GetExportedFunction("ZwQueryObject");

		return pZwQueryObject(ObjectHandle, ObjectInformationClass, ObjectInformation, Length, ResultLength);
	}

	NTSTATUS NTAPI NT::ZwSetInformationObject(
		IN HANDLE ObjectHandle,
		IN OBJECT_INFORMATION_CLASS ObjectInformationClass, 
		OUT PVOID ObjectInformation,
		IN ULONG Length)
	{
		typedef NTSTATUS(NTAPI *_ZwSetInformationObject)(HANDLE, OBJECT_INFORMATION_CLASS, PVOID, ULONG);
		_ZwSetInformationObject pZwSetInformationObject = (_ZwSetInformationObject)GetExportedFunction("ZwSetInformationObject");

		return pZwSetInformationObject(ObjectHandle, ObjectInformationClass, ObjectInformation, Length);
	}

	NTSTATUS NTAPI NT::ZwSignalAndWaitForSingleObject(
		IN HANDLE ObjectToSignal, 
		IN HANDLE WaitableObject,
		IN BOOLEAN Alertable, 
		IN PLARGE_INTEGER Time OPTIONAL)
	{
		typedef NTSTATUS(NTAPI *_ZwSignalAndWaitForSingleObject)(HANDLE, HANDLE, BOOLEAN, PLARGE_INTEGER);
		_ZwSignalAndWaitForSingleObject pZwSignalAndWaitForSingleObject = (_ZwSignalAndWaitForSingleObject)
			GetExportedFunction("ZwSignalAndWaitForSingleObject");

		return pZwSignalAndWaitForSingleObject(ObjectToSignal, WaitableObject, Alertable, Time);
	}

	NTSTATUS NTAPI NT::ZwWaitForMultipleObjects(
		IN ULONG ObjectCount, 
		IN PHANDLE ObjectsArray,
		IN OBJECT_WAIT_TYPE WaitType, 
		IN BOOLEAN Alertable, 
		IN PLARGE_INTEGER TimeOut OPTIONAL)
	{
		typedef NTSTATUS(NTAPI *_ZwWaitForMultipleObjects)(ULONG, PHANDLE, OBJECT_WAIT_TYPE, BOOLEAN, PLARGE_INTEGER);
		_ZwWaitForMultipleObjects pZwWaitForMultipleObjects = (_ZwWaitForMultipleObjects)GetExportedFunction("ZwWaitForMultipleObjects");

		return pZwWaitForMultipleObjects(ObjectCount, ObjectsArray, WaitType, Alertable, TimeOut);
	}

	NTSTATUS NTAPI NT::ZwWaitForSingleObject(
		IN HANDLE ObjectHandle, 
		IN BOOLEAN Alertable, 
		IN PLARGE_INTEGER TimeOut OPTIONAL)
	{
		typedef NTSTATUS(NTAPI *_ZwWaitForSingleObject)(HANDLE, BOOLEAN, PLARGE_INTEGER);
		_ZwWaitForSingleObject pZwWaitForSingleObject = (_ZwWaitForSingleObject)GetExportedFunction("ZwWaitForSingleObject");

		return pZwWaitForSingleObject(ObjectHandle, Alertable, TimeOut);
	}

	//========== DIRECTORYOBJECT ==========

	NTSTATUS NTAPI NT::ZwCreateDirectoryObject(
		OUT PHANDLE DirectoryHandle,
		IN ACCESS_MASK DesiredAccess, 
		IN POBJECT_ATTRIBUTES ObjectAttributes)
	{
		typedef NTSTATUS(NTAPI *_ZwCreateDirectoryObject)(PHANDLE, ACCESS_MASK, POBJECT_ATTRIBUTES);
		_ZwCreateDirectoryObject pZwCreateDirectoryObject = (_ZwCreateDirectoryObject)GetExportedFunction("ZwCreateDirectoryObject");

		return pZwCreateDirectoryObject(DirectoryHandle, DesiredAccess, ObjectAttributes);
	}

	NTSTATUS NTAPI NT::ZwOpenDirectoryObject(
		OUT PHANDLE DirectoryObjectHandle,
		IN ACCESS_MASK DesiredAccess, 
		IN POBJECT_ATTRIBUTES ObjectAttributes)
	{
		typedef NTSTATUS(NTAPI *_ZwOpenDirectoryObject)(PHANDLE, ACCESS_MASK, POBJECT_ATTRIBUTES);
		_ZwOpenDirectoryObject pZwOpenDirectoryObject = (_ZwOpenDirectoryObject)GetExportedFunction("ZwOpenDirectoryObject");

		return pZwOpenDirectoryObject(DirectoryObjectHandle, DesiredAccess, ObjectAttributes);
	}

	NTSTATUS NTAPI NT::ZwQueryDirectoryObject(
		IN HANDLE DirectoryObjectHandle,
		OUT POBJDIR_INFORMATION DirObjInformation, 
		IN ULONG BufferLength,
		IN BOOLEAN GetNextIndex,
		IN BOOLEAN IgnoreInputIndex, 
		IN OUT PULONG ObjectIndex,
		OUT PULONG DataWritten OPTIONAL)
	{
		typedef NTSTATUS(NTAPI *_ZwQueryDirectoryObject)(HANDLE, POBJDIR_INFORMATION, ULONG, 
			BOOLEAN, BOOLEAN, PULONG, PULONG);
		_ZwQueryDirectoryObject pZwQueryDirectoryObject = (_ZwQueryDirectoryObject)GetExportedFunction("ZwQueryDirectoryObject");

		return pZwQueryDirectoryObject(DirectoryObjectHandle, DirObjInformation, BufferLength,
			GetNextIndex, IgnoreInputIndex, ObjectIndex, DataWritten);
	}

	//========== EVENT ==========

	NTSTATUS NTAPI NT::ZwClearEvent(IN HANDLE EventHandle)
	{
		typedef NTSTATUS(NTAPI *_ZwClearEvent)(HANDLE);
		_ZwClearEvent pZwClearEvent = (_ZwClearEvent)GetExportedFunction("ZwClearEvent");

		return pZwClearEvent(EventHandle);
	}

	NTSTATUS NTAPI NT::ZwCreateEvent(
		IN HANDLE EventHandle,
		IN ACCESS_MASK DesiredAccess, 
		IN POBJECT_ATTRIBUTES ObjectAttributes,
		IN EVENT_TYPE EventType, 
		IN BOOLEAN InitialState)
	{
		typedef NTSTATUS(NTAPI *_ZwCreateEvent)(HANDLE, ACCESS_MASK, POBJECT_ATTRIBUTES, EVENT_TYPE, BOOLEAN);
		_ZwCreateEvent pZwCreateEvent = (_ZwCreateEvent)GetExportedFunction("ZwCreateEvent");

		return pZwCreateEvent(EventHandle, DesiredAccess, ObjectAttributes, EventType, InitialState);
	}

	NTSTATUS NTAPI NT::ZwOpenEvent(
		OUT PHANDLE EventHandle,
		IN ACCESS_MASK DesiredAccess, 
		IN POBJECT_ATTRIBUTES ObjectAttributes)
	{
		typedef NTSTATUS(NTAPI *_ZwOpenEvent)(PHANDLE, ACCESS_MASK, POBJECT_ATTRIBUTES);
		_ZwOpenEvent pZwOpenEvent = (_ZwOpenEvent)GetExportedFunction("ZwOpenEvent");

		return pZwOpenEvent(EventHandle, DesiredAccess, ObjectAttributes);
	}

	NTSTATUS NTAPI NT::ZwPulseEvent(
		IN HANDLE EventHandle, 
		OUT PLONG PreviousState OPTIONAL)
	{
		typedef NTSTATUS(NTAPI *_ZwEvent)(HANDLE, PLONG);
		_ZwEvent pZwEvent = (_ZwEvent)GetExportedFunction("ZwPulseEvent");

		return pZwEvent(EventHandle, PreviousState);
	}

	NTSTATUS NTAPI NT::ZwQueryEvent(
		IN HANDLE EventHandle,
		IN EVENT_INFORMATION_CLASS EventInformationClass, 
		OUT PVOID EventInformation,
		IN ULONG EventInformationLength,
		OUT PULONG ReturnLength OPTIONAL)
	{
		typedef NTSTATUS(NTAPI *_ZwQueryEvent)(HANDLE, EVENT_INFORMATION_CLASS, PVOID, ULONG, PULONG);
		_ZwQueryEvent pZwQueryEvent = (_ZwQueryEvent)GetExportedFunction("ZwQueryEvent");

		return pZwQueryEvent(EventHandle, EventInformationClass, EventInformation, EventInformationLength, ReturnLength);
	}

	NTSTATUS NTAPI NT::ZwResetEvent(
		IN HANDLE EventHandle, 
		OUT PLONG PreviousState OPTIONAL)
	{
		typedef NTSTATUS(NTAPI *_ZwEvent)(HANDLE, PLONG);
		_ZwEvent pZwEvent = (_ZwEvent)GetExportedFunction("ZwResetEvent");

		return pZwEvent(EventHandle, PreviousState);
	}

	NTSTATUS NTAPI NT::ZwSetEvent(
		IN HANDLE EventHandle, 
		OUT PLONG PreviousState OPTIONAL)
	{
		typedef NTSTATUS(NTAPI *_ZwEvent)(HANDLE, PLONG);
		_ZwEvent pZwEvent = (_ZwEvent)GetExportedFunction("ZwSetEvent");

		return pZwEvent(EventHandle, PreviousState);
	}

	//========== EVENTPAIR ==========

	//========== FILE ==========

	//========== IOCOMPLECTION ==========

	//========== KEY ==========

	//========== MUTANT ==========

	NTSTATUS NTAPI NT::ZwCreateMutant(
		OUT PHANDLE MutantHandle,
		IN ACCESS_MASK DesiredAccess, 
		IN POBJECT_ATTRIBUTES ObjectAttributes,
		IN BOOLEAN InitialOwner)
	{
		typedef NTSTATUS(NTAPI *_ZwCreateMutant)(PHANDLE, ACCESS_MASK, POBJECT_ATTRIBUTES, BOOLEAN);
		_ZwCreateMutant pZwCreateMutant = (_ZwCreateMutant)GetExportedFunction("ZwCreateMutant");

		return pZwCreateMutant(MutantHandle, DesiredAccess, ObjectAttributes, InitialOwner);
	}

	NTSTATUS NTAPI NT::ZwOpenMutant(
		OUT PHANDLE MutantHandle,
		IN ACCESS_MASK DesiredAccess, 
		IN POBJECT_ATTRIBUTES ObjectAttributes)
	{
		typedef NTSTATUS(NTAPI *_ZwOpenMutant)(PHANDLE, ACCESS_MASK, POBJECT_ATTRIBUTES);
		_ZwOpenMutant pZwOpenMutant = (_ZwOpenMutant)GetExportedFunction("ZwOpenMutant");

		return pZwOpenMutant(MutantHandle, DesiredAccess, ObjectAttributes);
	}

	NTSTATUS NTAPI NT::ZwQueryMutant(
		IN HANDLE MutantHandle,
		IN MUTANT_INFORMATION_CLASS MutantInformationClass,
		OUT PVOID MutantInformation, 
		IN ULONG MutantInformationLength,
		OUT PULONG ResultLength OPTIONAL)
	{
		typedef NTSTATUS(NTAPI *_ZwQueryMutant)(HANDLE, MUTANT_INFORMATION_CLASS, PVOID, ULONG, PULONG);
		_ZwQueryMutant pZwQueryMutant = (_ZwQueryMutant)GetExportedFunction("ZwQueryMutant");

		return pZwQueryMutant(MutantHandle, MutantInformationClass, MutantInformation, MutantInformationLength, ResultLength);
	}

	NTSTATUS NTAPI NT::ZwReleaseMutant(
		IN HANDLE MutantHandle, 
		OUT PLONG PreviousState OPTIONAL)
	{
		typedef NTSTATUS(NTAPI *_ZwReleaseMutant)(HANDLE, PLONG);
		_ZwReleaseMutant pZwReleaseMutant = (_ZwReleaseMutant)GetExportedFunction("ZwReleaseMutant");

		return pZwReleaseMutant(MutantHandle, PreviousState);
	}

	//========== PORT ==========

	//========== PROCESS ==========

	NTSTATUS NTAPI NT::ZwCreateProcess(
		OUT PHANDLE ProcessHandle,
		IN POBJECT_ATTRIBUTES ObjectAttributes OPTIONAL,
		IN HANDLE ParentProcess, 
		IN BOOLEAN InheritObjectTable,
		IN HANDLE SectionHandle OPTIONAL,
		IN HANDLE DebugPort OPTIONAL,
		IN HANDLE ExceptionPort OPTIONAL)
	{
		typedef NTSTATUS(NTAPI *_ZwCreateProcess)(PHANDLE, POBJECT_ATTRIBUTES, HANDLE, BOOLEAN,
			HANDLE, HANDLE, HANDLE);
		_ZwCreateProcess pZwCreateProcess = (_ZwCreateProcess)GetExportedFunction("ZwCreateProcess");

		return pZwCreateProcess(ProcessHandle, ObjectAttributes, ParentProcess, InheritObjectTable,
			SectionHandle, DebugPort, ExceptionPort);
	}

	NTSTATUS NTAPI NT::ZwFlushInstructionCache(
		IN HANDLE ProcessHandle,
		IN PVOID BaseAddress, 
		IN ULONG NumberOfBytesToFlush)
	{
		typedef NTSTATUS(NTAPI *_ZwFlushInstructionCache)(HANDLE, PVOID, ULONG);
		_ZwFlushInstructionCache pZwFlushInstructionCache = (_ZwFlushInstructionCache)
			GetExportedFunction("ZwFlushInstructionCache");

		return pZwFlushInstructionCache(ProcessHandle, BaseAddress, NumberOfBytesToFlush);
	}

	NTSTATUS NTAPI NT::ZwOpenProcess(
		OUT PHANDLE ProcessHandle,
		IN ACCESS_MASK AccessMask,
		IN POBJECT_ATTRIBUTES ObjectAttributes,
		IN PCLIENT_ID ClientId)
	{
		typedef NTSTATUS(NTAPI *_ZwOpenProcess)(PHANDLE, ACCESS_MASK, POBJECT_ATTRIBUTES, PCLIENT_ID);
		_ZwOpenProcess pZwOpenProcess = (_ZwOpenProcess)GetExportedFunction("ZwOpenProcess");

		return pZwOpenProcess(ProcessHandle, AccessMask, ObjectAttributes, ClientId);
	}

	NTSTATUS NTAPI NT::ZwQueryInformationProcess(
		IN HANDLE ProcessHandle,
		IN _PROCESS_INFORMATION_CLASS ProcessInformationClass,
		OUT PVOID ProcessInformation, 
		IN ULONG ProcessInformationLength,
		OUT PULONG ReturnLength)
	{
		typedef NTSTATUS(NTAPI *_ZwQueryInformationProcess)(HANDLE, _PROCESS_INFORMATION_CLASS, PVOID,
			ULONG, PULONG);

		_ZwQueryInformationProcess pZwQueryInformationProcess = (_ZwQueryInformationProcess)
			GetExportedFunction("ZwQueryInformationProcess");

		return pZwQueryInformationProcess(ProcessHandle, ProcessInformationClass, ProcessInformation,
			ProcessInformationLength, ReturnLength);
	}

	NTSTATUS NTAPI NT::ZwWow64QueryInformationProcess64(IN HANDLE ProcessHandle,
		IN _PROCESS_INFORMATION_CLASS ProcessInformationClass,
		OUT PVOID ProcessInformation,
		IN ULONG ProcessInformationLength,
		OUT PULONG ReturnLength)
	{
		typedef NTSTATUS(NTAPI *_ZwQueryInformationProcess)(HANDLE, _PROCESS_INFORMATION_CLASS, PVOID,
			ULONG, PULONG);

		_ZwQueryInformationProcess pZwQueryInformationProcess = (_ZwQueryInformationProcess)
			GetExportedFunction("ZwWow64QueryInformationProcess64");

		return pZwQueryInformationProcess(ProcessHandle, ProcessInformationClass, ProcessInformation,
			ProcessInformationLength, ReturnLength);
	}

	NTSTATUS NTAPI NT::ZwSetInformationProcess(
		IN HANDLE ProcessHandle,
		IN PROCESS_INFORMATION_CLASS ProcessInformationClass,
		IN PVOID ProcessInformation,
		IN ULONG ProcessInformationLength)
	{
		typedef NTSTATUS(NTAPI *_ZwSetInformationProcess)(HANDLE, PROCESS_INFORMATION_CLASS,
			PVOID, ULONG);
		_ZwSetInformationProcess pZwSetInformationProcess = (_ZwSetInformationProcess)GetExportedFunction("ZwSetInformationProcess");

		return pZwSetInformationProcess(ProcessHandle, ProcessInformationClass, 
			ProcessInformation, ProcessInformationLength);
	}

	NTSTATUS NTAPI NT::ZwTerminateProcess(
		IN HANDLE ProcessHandle OPTIONAL, 
		IN NTSTATUS ExitStatus)
	{
		typedef NTSTATUS (NTAPI *_ZwTerminateProcess)(HANDLE, NTSTATUS);
		_ZwTerminateProcess pZwTerminateProcess = (_ZwTerminateProcess)GetExportedFunction("ZwTerminateProcess");

		return pZwTerminateProcess(ProcessHandle, ExitStatus);
	}

	NTSTATUS NTAPI NT::RtlCreateUserProcess(
		IN PUNICODE_STRING ImagePath,
		IN ULONG ObjectAttributes, 
		IN OUT PRTL_USER_PROCESS_PARAMETERS ProcessParameters,
		IN PSECURITY_DESCRIPTOR ProcessSecurityDescriptor OPTIONAL,
		IN PSECURITY_DESCRIPTOR ThreadSecurityDescriptor OPTIONAL,
		IN HANDLE ParentProcess, 
		IN BOOLEAN InhertHandles,
		IN HANDLE DebugPort OPTIONAL, 
		IN HANDLE ExceptionPort OPTIONAL)
	{
		typedef NTSTATUS(NTAPI *_RtlCreateUserProcess)(PUNICODE_STRING, ULONG, 
			PRTL_USER_PROCESS_PARAMETERS, PSECURITY_DESCRIPTOR,
			PSECURITY_DESCRIPTOR, HANDLE, BOOLEAN, HANDLE, HANDLE);
		_RtlCreateUserProcess pRtlCreateUserProcess = (_RtlCreateUserProcess)GetExportedFunction("RtlCreateUserProcess");

		return pRtlCreateUserProcess(ImagePath, ObjectAttributes, ProcessParameters,
			ProcessSecurityDescriptor, ThreadSecurityDescriptor,
			ParentProcess, InhertHandles, DebugPort, ExceptionPort);
	}

	//========== PROFILE ==========

	NTSTATUS NTAPI NT::ZwCreateProfile(
		OUT PHANDLE ProfileHandle, 
		IN HANDLE Process OPTIONAL,
		IN PVOID ImageBase, 
		IN ULONG ImageSize, 
		IN ULONG BucketSize, 
		IN PVOID Buffer,
		IN ULONG BufferSize, 
		IN KPROFILE_SOURCE ProfileSource,
		IN KAFFINITY Affinity)
	{
		typedef NTSTATUS(NTAPI *_ZwCreateProfile)(PHANDLE, HANDLE, PVOID, ULONG,
			ULONG, PVOID, ULONG, KPROFILE_SOURCE, KAFFINITY);
		_ZwCreateProfile pZwCreateProfile = (_ZwCreateProfile)GetExportedFunction("ZwCreateProfile");

		return pZwCreateProfile(ProfileHandle, Process, ImageBase, ImageSize,
			BucketSize, Buffer, BufferSize, ProfileSource, Affinity);
	}

	NTSTATUS NTAPI NT::ZwQueryIntervalProfile(
		IN KPROFILE_SOURCE ProfileSource, 
		OUT PULONG Inverval)
	{
		typedef NTSTATUS(NTAPI *_ZwQueryIntervalProfile)(KPROFILE_SOURCE, PULONG);
		_ZwQueryIntervalProfile pZwQueryIntervalProfile = (_ZwQueryIntervalProfile)GetExportedFunction("ZwQueryIntervalProfile");

		return pZwQueryIntervalProfile(ProfileSource, Inverval);
	}

	NTSTATUS NTAPI NT::ZwSetIntervalProfile(
		IN ULONG Interval, 
		IN KPROFILE_SOURCE Source)
	{
		typedef NTSTATUS(NTAPI *_ZwSetIntervalProfile)(ULONG, KPROFILE_SOURCE);
		_ZwSetIntervalProfile pZwSetIntervalProfile = (_ZwSetIntervalProfile)GetExportedFunction("ZwSetIntervalProfile");

		return pZwSetIntervalProfile(Interval, Source);
	}

	NTSTATUS NTAPI NT::ZwStartProfile(IN HANDLE ProfileHandle)
	{
		typedef NTSTATUS(NTAPI *_ZwStartProfile)(HANDLE);
		_ZwStartProfile pZwStartProfile = (_ZwStartProfile)GetExportedFunction("ZwStartProfile");

		return pZwStartProfile(ProfileHandle);
	}

	NTSTATUS NTAPI NT::ZwStopProfile(IN HANDLE ProfileHandle)
	{
		typedef NTSTATUS(NTAPI *_ZwStopProfile)(HANDLE);
		_ZwStopProfile pZwStopProfile = (_ZwStopProfile)GetExportedFunction("ZwStopProfile");

		return pZwStopProfile(ProfileHandle);
	}

	//========== SECTION ==========

	NTSTATUS NTAPI NT::ZwCreateSection(
		OUT PHANDLE SectionHandle,
		IN ACCESS_MASK DesiredAccess, 
		IN POBJECT_ATTRIBUTES ObjectAttributes,
		IN PLARGE_INTEGER MaximumSize OPTIONAL, 
		IN ULONG PageAttributes,
		IN ULONG SectionAttributes, 
		IN HANDLE FileHandle OPTIONAL)
	{
		typedef NTSTATUS(NTAPI *_ZwCreateSection)(PHANDLE, ACCESS_MASK, POBJECT_ATTRIBUTES,
			PLARGE_INTEGER, ULONG, ULONG, HANDLE);
		_ZwCreateSection pZwCreateSection = (_ZwCreateSection)GetExportedFunction("ZwCreateSection");

		return pZwCreateSection(SectionHandle, DesiredAccess, ObjectAttributes, MaximumSize,
			PageAttributes, SectionAttributes, FileHandle);
	}

	NTSTATUS NTAPI NT::ZwExtendSection(
		IN HANDLE SectionHandle, 
		IN PLARGE_INTEGER NewSectionSize)
	{
		typedef NTSTATUS(NTAPI *_ZwExtendSection)(HANDLE, PLARGE_INTEGER);
		_ZwExtendSection pZwExtendSection = (_ZwExtendSection)GetExportedFunction("ZwExtendSection");

		return pZwExtendSection(SectionHandle, NewSectionSize);
	}

	NTSTATUS NTAPI NT::ZwMapViewOfSection(
		IN HANDLE SectionHandle, 
		IN HANDLE ProcessHandle,
		IN OUT PVOID *BaseAddress OPTIONAL, 
		IN ULONG ZeroBits OPTIONAL,
		IN ULONG CommitSize, 
		IN OUT PLARGE_INTEGER SectionOffset OPTIONAL,
		IN OUT PULONG ViewSize, 
		IN SECTION_INHERIT InheritDisposition,
		IN ULONG AllocationType OPTIONAL, 
		IN ULONG Protect)
	{
		typedef NTSTATUS(NTAPI *_ZwMapViewOfSection)(HANDLE, HANDLE, PVOID*, ULONG, ULONG,
			PLARGE_INTEGER, PULONG, SECTION_INHERIT, ULONG, ULONG);
		_ZwMapViewOfSection pZwMapViewOfSection = (_ZwMapViewOfSection)GetExportedFunction("ZwMapViewOfSection");

		return pZwMapViewOfSection(SectionHandle, ProcessHandle, BaseAddress, ZeroBits,
			CommitSize, SectionOffset, ViewSize, InheritDisposition, AllocationType, Protect);
	}

	NTSTATUS NTAPI NT::ZwOpenSection(
		OUT PHANDLE SectionHandle,
		IN ACCESS_MASK DesiredAccess, 
		IN POBJECT_ATTRIBUTES ObjectAttributes)
	{
		typedef NTSTATUS(NTAPI *_ZwOpenSection)(PHANDLE, ACCESS_MASK, POBJECT_ATTRIBUTES);
		_ZwOpenSection pZwOpenSection = (_ZwOpenSection)GetExportedFunction("ZwOpenSection");

		return pZwOpenSection(SectionHandle, DesiredAccess, ObjectAttributes);
	}

	NTSTATUS NTAPI NT::ZwQuerySection(
		IN HANDLE SectionHandle,
		SECTION_INFORMATION_CLASS InformationClass,
		OUT PVOID InformationBuffer, 
		IN ULONG InformationBufferSize,
		OUT PULONG ResultLength OPTIONAL)
	{
		typedef NTSTATUS(NTAPI *_ZwQuerySection)(HANDLE, SECTION_INFORMATION_CLASS, PVOID, ULONG, PULONG);
		_ZwQuerySection pZwQuerySection = (_ZwQuerySection)GetExportedFunction("ZwQuerySection");

		return pZwQuerySection(SectionHandle, InformationClass, InformationBuffer, InformationBufferSize, ResultLength);
	}

	NTSTATUS NTAPI NT::ZwUnmapViewOfSection(
		IN HANDLE ProcessHandle, 
		IN PVOID BaseAddress)
	{
		typedef NTSTATUS(NTAPI *_ZwUnmapViewOfSection)(HANDLE, PVOID);
		_ZwUnmapViewOfSection pZwUnmapViewOfSection = (_ZwUnmapViewOfSection)GetExportedFunction("ZwUnmapViewOfSection");

		return pZwUnmapViewOfSection(ProcessHandle, BaseAddress);
	}

	//========== SEMAPHORE ==========

	NTSTATUS NTAPI NT::ZwCreateSemaphore(
		OUT PHANDLE SemaphoreHandle,
		IN ACCESS_MASK DesiredAccess, 
		IN POBJECT_ATTRIBUTES ObjectAttributes,
		IN ULONG InitialCount, 
		IN ULONG MaximalCount)
	{
		typedef NTSTATUS(NTAPI *_ZwCreateSemaphore)(PHANDLE, ACCESS_MASK,
			POBJECT_ATTRIBUTES, ULONG, ULONG);
		_ZwCreateSemaphore pZwCreateSemaphore = (_ZwCreateSemaphore)GetExportedFunction("ZwCreateSemaphore");

		return pZwCreateSemaphore(SemaphoreHandle, DesiredAccess, ObjectAttributes, InitialCount, MaximalCount);
	}

	NTSTATUS NTAPI NT::ZwOpenSemaphore(
		OUT PHANDLE SemaphoreHandle,
		IN ACCESS_MASK DesiredAccess, 
		IN POBJECT_ATTRIBUTES ObjectAttributes)
	{
		typedef NTSTATUS(NTAPI *_ZwOpenSemaphore)(PHANDLE, ACCESS_MASK, POBJECT_ATTRIBUTES);
		_ZwOpenSemaphore pZwOpenSemaphore = (_ZwOpenSemaphore)GetExportedFunction("ZwOpenSemaphore");

		return pZwOpenSemaphore(SemaphoreHandle, DesiredAccess, ObjectAttributes);
	}

	NTSTATUS NTAPI NT::ZwQuerySemaphore(
		IN PHANDLE SemaphoreHandle,
		IN SEMAPHORE_INFORMATION_CLASS SemaphoreInformationClass,
		OUT PVOID SemaphoreInformation, 
		IN ULONG SemaphoreInformationLength,
		OUT PULONG ReturnLength OPTIONAL)
	{
		typedef NTSTATUS(NTAPI *_ZwQuerySemaphore)(PHANDLE, SEMAPHORE_INFORMATION_CLASS, PVOID, ULONG, PULONG);
		_ZwQuerySemaphore pZwQuerySemaphore = (_ZwQuerySemaphore)GetExportedFunction("ZwQuerySemaphore");

		return pZwQuerySemaphore(SemaphoreHandle, SemaphoreInformationClass,
			SemaphoreInformation, SemaphoreInformationLength, ReturnLength);
	}

	NTSTATUS NTAPI NT::ZwReleaseSemaphore(
		IN HANDLE SemaphoreHandle,
		IN ULONG ReleaseCount, 
		OUT PULONG PreviousCount OPTIONAL)
	{
		typedef NTSTATUS(NTAPI *_ZwReleaseSemaphore)(HANDLE, ULONG, PULONG);
		_ZwReleaseSemaphore pZwReleaseSemaphore = (_ZwReleaseSemaphore)GetExportedFunction("ZwReleaseSemaphore");

		return pZwReleaseSemaphore(SemaphoreHandle, ReleaseCount, PreviousCount);
	}

	//========== SYMBOLICLINK ==========

	NTSTATUS NTAPI NT::ZwCreateSymbolicLinkObject(
		OUT PHANDLE pHandle,
		IN ACCESS_MASK DesiredAccess, 
		IN POBJECT_ATTRIBUTES ObjectAttributes,
		IN PUNICODE_STRING DestinationName)
	{
		typedef NTSTATUS(NTAPI *_ZwCreateSymbolicLinkObject)(PHANDLE, ACCESS_MASK,
			POBJECT_ATTRIBUTES, PUNICODE_STRING);
		_ZwCreateSymbolicLinkObject pZwCreateSymbolicLinkObject = (_ZwCreateSymbolicLinkObject)
			GetExportedFunction("ZwCreateSymbolicLinkObject");

		return pZwCreateSymbolicLinkObject(pHandle, DesiredAccess, ObjectAttributes, DestinationName);
	}

	NTSTATUS NTAPI NT::ZwOpenSymbolicLinkObject(
		OUT PHANDLE pHandle,
		IN ACCESS_MASK DesiredAccess, 
		IN POBJECT_ATTRIBUTES ObjectAttributes)
	{
		typedef NTSTATUS(NTAPI *_ZwOpenSymbolicLinkObject)(PHANDLE, ACCESS_MASK, POBJECT_ATTRIBUTES);
		_ZwOpenSymbolicLinkObject pZwOpenSymbolicLinkObject = (_ZwOpenSymbolicLinkObject)
			GetExportedFunction("ZwOpenSymbolicLinkObject");

		return pZwOpenSymbolicLinkObject(pHandle, DesiredAccess, ObjectAttributes);
	}

	NTSTATUS NTAPI NT::ZwQuerySymbolicLinkObject(
		IN HANDLE SymbolicLinkHandle,
		OUT PUNICODE_STRING pLinkName, 
		OUT PULONG pDataWritten OPTIONAL)
	{
		typedef NTSTATUS(NTAPI *_ZwQuerySymbolicLinkObject)(HANDLE, PUNICODE_STRING, PULONG);
		_ZwQuerySymbolicLinkObject pZwQuerySymbolicLinkObject = (_ZwQuerySymbolicLinkObject)
			GetExportedFunction("ZwQuerySymbolicLinkObject");

		return pZwQuerySymbolicLinkObject(SymbolicLinkHandle, pLinkName, pDataWritten);
	}

	//========== THREAD ==========

	NTSTATUS NTAPI NT::ZwGetContextThread(
		IN HANDLE ThreadHandle, 
		OUT PCONTEXT pContext)
	{
		typedef NTSTATUS(NTAPI *_ZwGetContextThread)(HANDLE, PCONTEXT);
		_ZwGetContextThread pZwGetContextThread = (_ZwGetContextThread)GetExportedFunction("ZwGetContextThread");

		return pZwGetContextThread(ThreadHandle, pContext);
	}

	NTSTATUS NTAPI NT::ZwSetContextThread(
		IN HANDLE ThreadHandle, 
		IN PCONTEXT Context)
	{
		typedef NTSTATUS(NTAPI *_ZwSetContextThread)(HANDLE, PCONTEXT);
		_ZwSetContextThread pZwSetContextThread = (_ZwSetContextThread)GetExportedFunction("ZwSetContextThread");

		return pZwSetContextThread(ThreadHandle, Context);
	}

	PVOID NTAPI NT::RtlInitializeContext(
		IN HANDLE ProcessHandle, 
		OUT PCONTEXT ThreadContext,
		IN PVOID ThreadStartParam OPTIONAL, 
		IN PTHREAD_START_ROUTINE ThreadStartAddress,
		IN PINITIAL_TEB InitialTeb)
	{
		typedef PVOID(NTAPI *_RtlInitializeContext)(HANDLE, PCONTEXT, PVOID, PTHREAD_START_ROUTINE, PINITIAL_TEB);
		_RtlInitializeContext pRtlInitializeContext = (_RtlInitializeContext)GetExportedFunction("RtlInitializeContext");

		return pRtlInitializeContext(ProcessHandle, ThreadContext, ThreadStartParam, ThreadStartAddress, InitialTeb);
	}

	NTSTATUS NTAPI NT::ZwAlertResumeThread(
		IN HANDLE ThreadHandle, 
		OUT PULONG SuspendCount)
	{
		typedef NTSTATUS(NTAPI *_ZwAlertResumeThread)(HANDLE, PULONG);
		_ZwAlertResumeThread pZwAlertResumeThread = (_ZwAlertResumeThread)GetExportedFunction("ZwAlertResumeThread");

		return pZwAlertResumeThread(ThreadHandle, SuspendCount);
	}

	NTSTATUS NTAPI NT::ZwContinue(
		IN PCONTEXT ThreadContext, 
		IN BOOLEAN RaiseAlert)
	{
		typedef NTSTATUS(NTAPI *_ZwContinue)(PCONTEXT, BOOLEAN);
		_ZwContinue pZwContinue = (_ZwContinue)GetExportedFunction("ZwContinue");

		return pZwContinue(ThreadContext, RaiseAlert);
	}

	NTSTATUS NTAPI NT::ZwCreateThread(
		OUT PHANDLE ThreadHandle,
		IN ACCESS_MASK DesiredAccess, 
		IN POBJECT_ATTRIBUTES ObjectAttributes,
		IN HANDLE ProcessHandles, 
		OUT PCLIENT_ID ClientId,
		IN PCONTEXT ThreadContext, PINITIAL_TEB InitialTeb,
		IN BOOLEAN CreateSuspended)
	{
		typedef NTSTATUS(NTAPI *_ZwCreateThread)(PHANDLE, ACCESS_MASK, POBJECT_ATTRIBUTES, 
			HANDLE, PCLIENT_ID, PCONTEXT, PINITIAL_TEB, BOOLEAN);
		_ZwCreateThread pZwCreateThread = (_ZwCreateThread)GetExportedFunction("ZwCreateThread");

		return pZwCreateThread(ThreadHandle, DesiredAccess, ObjectAttributes, ProcessHandles, ClientId,
			ThreadContext, InitialTeb, CreateSuspended);
	}

	NT::PTEB NTAPI NT::NtCurrentTeb()
	{
		typedef PTEB(NTAPI *_NtCurrentTeb)();
		_NtCurrentTeb pNtCurrentTeb = (_NtCurrentTeb)GetExportedFunction("NtCurrentTeb");

		return pNtCurrentTeb();
	}

	NTSTATUS NTAPI NT::ZwDelayExecution(
		IN BOOLEAN Alertable, 
		IN PLARGE_INTEGER DelayInterval)
	{
		typedef NTSTATUS(NTAPI *_ZwDelayExecution)(BOOLEAN, PLARGE_INTEGER);
		_ZwDelayExecution pZwDelayExecution = (_ZwDelayExecution)GetExportedFunction("ZwDelayExecution");

		return pZwDelayExecution(Alertable, DelayInterval);
	}

	NTSTATUS NTAPI NT::ZwImpersonateThread(
		IN HANDLE ThreadHandle, 
		IN HANDLE ThreadToImpersonate,
		IN PSECURITY_QUALITY_OF_SERVICE SecurityQualityOfService)
	{
		typedef NTSTATUS(NTAPI *_ZwImpersonateThread)(HANDLE, HANDLE, PSECURITY_QUALITY_OF_SERVICE);
		_ZwImpersonateThread pZwImpersonateThread = (_ZwImpersonateThread)GetExportedFunction("ZwImpersonateThread");

		return pZwImpersonateThread(ThreadHandle, ThreadToImpersonate, SecurityQualityOfService);
	}

	NTSTATUS NTAPI NT::ZwOpenThread(
		OUT PHANDLE ThreadHandle,
		IN ACCESS_MASK DesiredAccess, 
		IN POBJECT_ATTRIBUTES ObjectAttributes,
		OUT PCLIENT_ID ClientId)
	{
		typedef NTSTATUS(NTAPI *_ZwOpenThread)(PHANDLE, ACCESS_MASK, POBJECT_ATTRIBUTES, PCLIENT_ID);
		_ZwOpenThread pZwOpenThread = (_ZwOpenThread)GetExportedFunction("ZwOpenThread");

		return pZwOpenThread(ThreadHandle, DesiredAccess, ObjectAttributes, ClientId);
	}

	NTSTATUS NTAPI NT::ZwQueryInformationThread(
		IN HANDLE ThreadHandle,
		IN THREADINFOCLASS ThreadInformationClass,
		OUT PVOID ThreadInformation, 
		IN ULONG ThreadInformationLength,
		OUT PULONG ReturnLength)
	{
		typedef NTSTATUS(NTAPI *_ZwQueryInformationThread)(HANDLE, THREADINFOCLASS, PVOID, ULONG, PULONG);
		_ZwQueryInformationThread pQueryInformationThread = (_ZwQueryInformationThread)
			GetExportedFunction("ZwQueryInformationThread");

		return pQueryInformationThread(ThreadHandle, ThreadInformationClass, ThreadInformation,
			ThreadInformationLength, ReturnLength);
	}

	NTSTATUS NTAPI NT::ZwRegisterThreadTerminatePort(IN HANDLE PortHandle)
	{
		typedef NTSTATUS(NTAPI *_ZwRegisterThreadTerminatePort)(HANDLE);
		_ZwRegisterThreadTerminatePort pZwRegisterThreadTerminatePort = (_ZwRegisterThreadTerminatePort)
			GetExportedFunction("ZwRegisterThreadTerminatePort");

		return pZwRegisterThreadTerminatePort(PortHandle);
	}

	NTSTATUS NTAPI NT::ZwResumeThread(
		IN HANDLE ThreadHandle, 
		OUT PULONG SuspendCount OPTIONAL)
	{
		typedef NTSTATUS(NTAPI *_ZwResumeThread)(HANDLE, PULONG);
		_ZwResumeThread pZwResumeThread = (_ZwResumeThread)GetExportedFunction("ZwResumeThread");

		return pZwResumeThread(ThreadHandle, SuspendCount);
	}

	NTSTATUS NTAPI NT::ZwSetInformationThread(
		IN HANDLE ThreadHandle,
		IN THREADINFOCLASS ThreadInformationClass,
		OUT PVOID ThreadInformation, 
		IN ULONG ThreadInformationLength)
	{
		typedef NTSTATUS(NTAPI *_ZwSetInformationThread)(HANDLE, THREADINFOCLASS, PVOID, ULONG);
		_ZwSetInformationThread pZwSetInformationThread = (_ZwSetInformationThread)GetExportedFunction("ZwSetInformationThread");

		return pZwSetInformationThread(ThreadHandle, ThreadInformationClass, ThreadInformation, ThreadInformationLength);
	}

	NTSTATUS NTAPI NT::ZwSuspendThread(
		IN HANDLE ThreadHandle, 
		OUT PULONG PreviousSuspendCount OPTIONAL)
	{
		typedef NTSTATUS(NTAPI *_ZwSuspendThread)(HANDLE, PULONG);
		_ZwSuspendThread pZwSuspendThread = (_ZwSuspendThread)GetExportedFunction("ZwSuspendThread");

		return pZwSuspendThread(ThreadHandle, PreviousSuspendCount);
	}

	NTSTATUS NTAPI NT::ZwTerminateThread(
		IN HANDLE ThreadHandle, 
		IN NTSTATUS ExitStatus)
	{
		typedef NTSTATUS(NTAPI *_ZwTerminateThread)(HANDLE, NTSTATUS);
		_ZwTerminateThread pZwTerminateThread = (_ZwTerminateThread)GetExportedFunction("ZwTerminateThread");

		return pZwTerminateThread(ThreadHandle, ExitStatus);
	}

	NTSTATUS NTAPI NT::ZwYieldExecution()
	{
		typedef NTSTATUS(NTAPI *_ZwYieldExecution)();
		_ZwYieldExecution pZwYieldExecution = (_ZwYieldExecution)GetExportedFunction("ZwYieldExecution");

		return pZwYieldExecution();
	}

	NTSTATUS NTAPI NT::RtlCreateUserThread(
		IN HANDLE ProcessHandle,
		IN PSECURITY_DESCRIPTOR SecurityDescriptor OPTIONAL,
		IN BOOLEAN CreateSupsended, 
		IN ULONG StackZeroBits,
		IN OUT PULONG StackReserved, 
		IN OUT PULONG StackCommit,
		IN PVOID StartAddress, 
		IN PVOID StartParameter OPTIONAL,
		OUT PHANDLE ThreadHandle, 
		OUT PCLIENT_ID ClientId)
	{
		typedef NTSTATUS(NTAPI *_RtlCreateUserThread)(HANDLE, PSECURITY_DESCRIPTOR, BOOLEAN, ULONG,
			PULONG, PULONG, PVOID, PVOID, PHANDLE, PCLIENT_ID);
		_RtlCreateUserThread pRtlCreateUserThread = (_RtlCreateUserThread)GetExportedFunction("RtlCreateUserThread");

		return pRtlCreateUserThread(ProcessHandle, SecurityDescriptor, CreateSupsended, StackZeroBits,
			StackReserved, StackCommit, StartAddress, StartParameter, ThreadHandle, ClientId);
	}

	//========== TIMER ==========

	NTSTATUS NTAPI NT::ZwCancelTimer(
		IN HANDLE TimerHandle, 
		OUT PBOOLEAN CurrentState OPTIONAL)
	{
		typedef NTSTATUS(NTAPI *_ZwCancelTimer)(HANDLE, PBOOLEAN);
		_ZwCancelTimer pZwCancelTimer = (_ZwCancelTimer)GetExportedFunction("ZwCancelTimer");

		return pZwCancelTimer(TimerHandle, CurrentState);
	}

	NTSTATUS NTAPI NT::ZwCreateTimer(
		OUT PHANDLE TimerHandle, 
		IN ACCESS_MASK DesiredAccess,
		IN POBJECT_ATTRIBUTES ObjectAttributes, 
		IN ULONG TimerType)
	{
		typedef NTSTATUS(NTAPI *_ZwCreateTimer)(PHANDLE, ACCESS_MASK, POBJECT_ATTRIBUTES, ULONG);
		_ZwCreateTimer pZwCreateTimer = (_ZwCreateTimer)GetExportedFunction("ZwCreateTimer");

		return pZwCreateTimer(TimerHandle, DesiredAccess, ObjectAttributes, TimerType);
	}

	NTSTATUS NTAPI NT::ZwOpenTimer(
		OUT PHANDLE TimerHandle,
		IN ACCESS_MASK DesiredAccess,
		IN POBJECT_ATTRIBUTES ObjectAttributes)
	{
		typedef NTSTATUS(NTAPI *_ZwOpenTimer)(PHANDLE, ACCESS_MASK, POBJECT_ATTRIBUTES);
		_ZwOpenTimer pZwOpenTimer = (_ZwOpenTimer)GetExportedFunction("ZwOpenTimer");

		return pZwOpenTimer(TimerHandle, DesiredAccess, ObjectAttributes);
	}

	NTSTATUS NTAPI NT::ZwQueryTimer(
		IN HANDLE TimerHandle,
		IN TIMER_INFORMATION_CLASS TimerInformationClass,
		OUT PVOID TimerInformation, 
		IN ULONG TimerInformationLength,
		OUT PULONG ReturnLength OPTIONAL)
	{
		typedef NTSTATUS(NTAPI *_ZwQueryTimer)(HANDLE, TIMER_INFORMATION_CLASS, PVOID, ULONG, PULONG);
		_ZwQueryTimer pZwQueryTimer = (_ZwQueryTimer)GetExportedFunction("ZwQueryTimer");

		return pZwQueryTimer(TimerHandle, TimerInformationClass, TimerInformation,
			TimerInformationLength, ReturnLength);
	}

	NTSTATUS NTAPI NT::ZwSetTimer(
		IN HANDLE TimerHandle, 
		IN PLARGE_INTEGER DueTime,
		IN PTIMER_APC_ROUTINE TimerApcRoutine OPTIONAL,
		IN PVOID TimerContext OPTIONAL, 
		IN BOOLEAN ResumeTimer,
		IN LONG Period OPTIONAL, 
		OUT PBOOLEAN PreviousState OPTIONAL)
	{
		typedef NTSTATUS(NTAPI *_ZwSetTimer)(HANDLE, PLARGE_INTEGER, PTIMER_APC_ROUTINE,
			PVOID, BOOLEAN, LONG, PBOOLEAN);
		_ZwSetTimer pZwSetTimer = (_ZwSetTimer)GetExportedFunction("ZwSetTimer");

		return pZwSetTimer(TimerHandle, DueTime, TimerApcRoutine, TimerContext,
			ResumeTimer, Period, PreviousState);
	}

	//========== TOKEN ==========

	NTSTATUS NTAPI NT::ZwAdjustGroupsToken(
		IN HANDLE TokenHandle, 
		IN BOOLEAN ResetToDefault,
		IN PTOKEN_GROUPS TokenGroups, 
		IN ULONG PreviousGroupsLength,
		OUT PTOKEN_GROUPS PreviousGroups OPTIONAL, 
		OUT PULONG RequiredLength OPTIONAL)
	{
		typedef NTSTATUS(NTAPI *_ZwAdjustGroupsToken)(HANDLE, BOOLEAN, PTOKEN_GROUPS,
			ULONG, PTOKEN_GROUPS, PULONG);
		_ZwAdjustGroupsToken pZwAdjustGroupsToken = (_ZwAdjustGroupsToken)GetExportedFunction("ZwAdjustGroupsToken");

		return pZwAdjustGroupsToken(TokenHandle, ResetToDefault, TokenGroups, 
			PreviousGroupsLength, PreviousGroups, RequiredLength);
	}

	NTSTATUS NTAPI NT::ZwAdjustPrivilegesToken(
		IN HANDLE TokenHandle, 
		IN BOOLEAN DisableAllPrivileges,
		IN PTOKEN_PRIVILEGES TokenPrivileges,
		IN ULONG PreviousPrivilegesLength,
		OUT PTOKEN_PRIVILEGES PreviousPrivileges OPTIONAL,
		OUT PULONG RequiredLength OPTIONAL)
	{
		typedef NTSTATUS(NTAPI *_ZwAdjustPrivilegesToken)(HANDLE, BOOLEAN, PTOKEN_PRIVILEGES,
			ULONG, PTOKEN_PRIVILEGES, PULONG);
		_ZwAdjustPrivilegesToken pZwAdjustPrivilegesToken = (_ZwAdjustPrivilegesToken)GetExportedFunction("ZwAdjustPrivilegesToken");

		return pZwAdjustPrivilegesToken(TokenHandle, DisableAllPrivileges, TokenPrivileges,
			PreviousPrivilegesLength, PreviousPrivileges, RequiredLength);
	}

	NTSTATUS NTAPI NT::ZwCreateToken(
		OUT PHANDLE TokenHandle,
		IN ACCESS_MASK DesiredAccess,
		IN POBJECT_ATTRIBUTES ObjectAttributes,
		IN TOKEN_TYPE TokenType,
		IN PLUID AuthenticationId,
		IN PLARGE_INTEGER ExpirationTime,
		IN PTOKEN_USER TokenUser,
		IN PTOKEN_GROUPS TokenGroups,
		IN PTOKEN_PRIVILEGES TokenPrivileges,
		IN PTOKEN_OWNER TokenOwner,
		IN PTOKEN_PRIMARY_GROUP TokenPrimaryGroup,
		IN PTOKEN_DEFAULT_DACL TokenDefaultDacl,
		IN PTOKEN_SOURCE TokenSource)
	{
		typedef NTSTATUS(NTAPI *_ZwCreateToken)(PHANDLE, ACCESS_MASK, POBJECT_ATTRIBUTES,
			TOKEN_TYPE, PLUID, PLARGE_INTEGER, PTOKEN_USER, PTOKEN_GROUPS, PTOKEN_PRIVILEGES, PTOKEN_OWNER, PTOKEN_PRIMARY_GROUP, PTOKEN_DEFAULT_DACL, PTOKEN_SOURCE);
		_ZwCreateToken pZwCreateToken = (_ZwCreateToken)GetExportedFunction("ZwCreateToken");

		return pZwCreateToken(TokenHandle, DesiredAccess, ObjectAttributes, TokenType,
			AuthenticationId, ExpirationTime, TokenUser, TokenGroups, TokenPrivileges,
			TokenOwner, TokenPrimaryGroup, TokenDefaultDacl, TokenSource);
	}

	NTSTATUS NTAPI NT::ZwDuplicateToken(
		IN HANDLE ExistingToken,
		IN ACCESS_MASK DesiredAccess,
		IN POBJECT_ATTRIBUTES ObjectAttributes OPTIONAL,
		IN SECURITY_IMPERSONATION_LEVEL ImpersonationLevel,
		IN ULONG TokenType, 
		OUT PHANDLE NewToken)
	{
		typedef NTSTATUS(NTAPI *_ZwDuplicateToken)(HANDLE, ACCESS_MASK, POBJECT_ATTRIBUTES,
			SECURITY_IMPERSONATION_LEVEL, ULONG, PHANDLE);
		_ZwDuplicateToken pZwDuplicateToken = (_ZwDuplicateToken)GetExportedFunction("ZwDuplicateToken");

		return pZwDuplicateToken(ExistingToken, DesiredAccess, ObjectAttributes, ImpersonationLevel, TokenType, NewToken);
	}

	NTSTATUS NTAPI NT::ZwOpenProcessToken(
		IN HANDLE ProcessHandle,
		IN ACCESS_MASK DesiredAccess, 
		OUT PHANDLE TokenHandle)
	{
		typedef NTSTATUS(NTAPI *_ZwOpenProcessToken)(HANDLE, ACCESS_MASK, PHANDLE);
		_ZwOpenProcessToken pZwOpenProcessToken = (_ZwOpenProcessToken)GetExportedFunction("ZwOpenProcessToken");

		return pZwOpenProcessToken(ProcessHandle, DesiredAccess, TokenHandle);
	}

	NTSTATUS NTAPI NT::ZwOpenThreadToken(
		IN HANDLE ProcessHandle,
		IN ACCESS_MASK DesiredAccess, 
		IN BOOLEAN OpenAsSelf,
		OUT PHANDLE TokenHandle)
	{
		typedef NTSTATUS(NTAPI *_ZwOpenThreadToken)(HANDLE, ACCESS_MASK, BOOLEAN, PHANDLE);
		_ZwOpenThreadToken pZwOpenThreadToken = (_ZwOpenThreadToken)GetExportedFunction("ZwOpenThreadToken");

		return pZwOpenThreadToken(ProcessHandle, DesiredAccess, OpenAsSelf, TokenHandle);
	}

	NTSTATUS NTAPI NT::ZwQueryInformationToken(
		IN HANDLE TokenHandle,
		IN TOKEN_INFORMATION_CLASS TokenInformationClass,
		OUT PVOID TokenInformation, 
		IN ULONG TokenInformationLength,
		OUT PULONG ReturnLength)
	{
		typedef NTSTATUS(NTAPI *_ZwQueryInformationToken)(HANDLE, TOKEN_INFORMATION_CLASS, PVOID, ULONG, PULONG);
		_ZwQueryInformationToken pZwQueryInformationToken = (_ZwQueryInformationToken)GetExportedFunction("ZwQueryInformationToken");

		return pZwQueryInformationToken(TokenHandle, TokenInformationClass,
			TokenInformation, TokenInformationLength, ReturnLength);
	}

	NTSTATUS NTAPI NT::ZwSetInformationToken(
		IN HANDLE TokenHandle,
		IN TOKEN_INFORMATION_CLASS TokenInformationClass,
		OUT PVOID TokenInformation, 
		IN ULONG TokenInformationLength)
	{
		typedef NTSTATUS(NTAPI *_ZwSetInformationToken)(HANDLE, TOKEN_INFORMATION_CLASS, PVOID, ULONG);
		_ZwSetInformationToken pZwSetInformationToken = (_ZwSetInformationToken)GetExportedFunction("ZwSetInformationToken");

		return pZwSetInformationToken(TokenHandle, TokenInformationClass, TokenInformation, TokenInformationLength);
	}

	//========== SECURITY ==========

	NTSTATUS NTAPI NT::ZwAccessCheck(
		IN PSECURITY_DESCRIPTOR SecurityDescriptor,
		IN HANDLE ClientToken, 
		IN ACCESS_MASK DesiredAccess,
		IN PPRIVILEGE_SET RequiredPrivilegesBuffer,
		IN OUT PULONG BufferLength, 
		OUT ACCESS_MASK *GrantedAccess,
		OUT PNTSTATUS AccessStatus)
	{
		typedef NTSTATUS(NTAPI *_ZwAccessCheck)(PSECURITY_DESCRIPTOR, HANDLE, ACCESS_MASK,
			PPRIVILEGE_SET, PULONG, ACCESS_MASK*, PNTSTATUS);
		_ZwAccessCheck pZwAccessCheck = (_ZwAccessCheck)GetExportedFunction("ZwAccessCheck");

		return pZwAccessCheck(SecurityDescriptor, ClientToken, DesiredAccess,
			RequiredPrivilegesBuffer, BufferLength, GrantedAccess, AccessStatus);
	}

	NTSTATUS NTAPI NT::ZwAllocateLocallyUniqueId(OUT PLUID LocallyUniqueId)
	{
		typedef NTSTATUS(NTAPI *_ZwAllocateLocallyUniqueId)(PLUID);
		_ZwAllocateLocallyUniqueId pZwAllocateLocallyUniqueId = (_ZwAllocateLocallyUniqueId)
			GetExportedFunction("ZwAllocateLocallyUniqueId");

		return pZwAllocateLocallyUniqueId(LocallyUniqueId);
	}

	NTSTATUS NTAPI NT::ZwAllocateUuids(
		OUT PLARGE_INTEGER Time, 
		OUT PULONG Range,
		OUT PULONG Sequence)
	{
		typedef NTSTATUS(NTAPI *_ZwAllocateUuids)(PLARGE_INTEGER, PULONG, PULONG);
		_ZwAllocateUuids pZwAllocateUuids = (_ZwAllocateUuids)GetExportedFunction("ZwAllocateUuids");

		return pZwAllocateUuids(Time, Range, Sequence);
	}

	NTSTATUS NTAPI NT::ZwPrivilegeCheck(
		IN HANDLE TokenHandle,
		IN PPRIVILEGE_SET RequiredPrivileges,
		IN PBOOLEAN Result)
	{
		typedef NTSTATUS(NTAPI *_ZwPrivilegeCheck)(HANDLE, PPRIVILEGE_SET, PBOOLEAN);
		_ZwPrivilegeCheck pZwPrivilegeCheck = (_ZwPrivilegeCheck)GetExportedFunction("ZwPrivilegeCheck");

		return pZwPrivilegeCheck(TokenHandle, RequiredPrivileges, Result);
	}

	NTSTATUS NTAPI NT::ZwQuerySecurityObject(
		IN HANDLE ObjectHandle,
		IN SECURITY_INFORMATION SecurityInformationClass,
		OUT PSECURITY_DESCRIPTOR DescriptorBuffer,
		IN ULONG DescriptorBufferLength,
		OUT PULONG RequiredLength)
	{
		typedef NTSTATUS(NTAPI *_ZwQuerySecurityObject)(HANDLE, SECURITY_INFORMATION,
			PSECURITY_DESCRIPTOR, ULONG, PULONG);
		_ZwQuerySecurityObject pZwQuerySecurityObject = (_ZwQuerySecurityObject)
			GetExportedFunction("ZwQuerySecurityObject");

		return pZwQuerySecurityObject(ObjectHandle, SecurityInformationClass, 
			DescriptorBuffer, DescriptorBufferLength, RequiredLength);
	}

	NTSTATUS NTAPI NT::ZwSetSecurityObject(
		IN HANDLE ObjectHandle,
		IN SECURITY_INFORMATION SecurityInformationClass,
		IN PSECURITY_DESCRIPTOR DescriptorBuffer)
	{
		typedef NTSTATUS(NTAPI *_ZwSetSecurityObject)(HANDLE, SECURITY_INFORMATION, PSECURITY_DESCRIPTOR);
		_ZwSetSecurityObject pZwSetSecurityObject = (_ZwSetSecurityObject)GetExportedFunction("ZwSetSecurityObject");

		return pZwSetSecurityObject(ObjectHandle, SecurityInformationClass, DescriptorBuffer);
	}

	//========== SYSTEM INFORMATION ==========

	NTSTATUS NTAPI NT::ZwQuerySystemInformation(
		IN SYSTEM_INFORMATION_CLASS SystemInformationClass,
		OUT PVOID SystemInformation,
		IN ULONG SystemInformationLength,
		OUT PULONG ReturnLength OPTIONAL)
	{
		typedef NTSTATUS(NTAPI *_ZwQuerySystemInformation)(SYSTEM_INFORMATION_CLASS, PVOID, ULONG, PULONG);
		_ZwQuerySystemInformation pZwQuerySystemInformation = (_ZwQuerySystemInformation)
			GetExportedFunction("ZwQuerySystemInformation");

		return pZwQuerySystemInformation(SystemInformationClass, SystemInformation, SystemInformationLength, ReturnLength);
	}

	NTSTATUS NTAPI NT::ZwSetSystemInformation(
		IN SYSTEM_INFORMATION_CLASS SystemInformationClass,
		IN PVOID SystemInformation,
		IN ULONG SystemInformationLength)
	{
		typedef NTSTATUS(NTAPI *_ZwSetSystemInformation)(SYSTEM_INFORMATION_CLASS, PVOID, ULONG);
		_ZwSetSystemInformation pZwSetSystemInformation = (_ZwSetSystemInformation)
			GetExportedFunction("ZwSetSystemInformation");

		return pZwSetSystemInformation(SystemInformationClass, SystemInformation, SystemInformationLength);
	}

	//========== TIME ==========

	ULONG NTAPI NT::NtGetTickCount()
	{
		typedef ULONG(NTAPI *_NtGetTickCount)();
		_NtGetTickCount pNtGetTickCount = (_NtGetTickCount)GetExportedFunction("NtGetTickCount");

		return pNtGetTickCount();
	}

	NTSTATUS NTAPI NT::ZwQueryPerformanceCounter(
		OUT PLARGE_INTEGER PerformanceCounter,
		OUT PLARGE_INTEGER PerformanceFrequency OPTIONAL)
	{
		typedef NTSTATUS(NTAPI *_ZwQueryPerformanceCounter)(PLARGE_INTEGER, PLARGE_INTEGER);
		_ZwQueryPerformanceCounter pZwQueryPerformanceCounter = (_ZwQueryPerformanceCounter)
			GetExportedFunction("ZwQueryPerformanceCounter");

		return pZwQueryPerformanceCounter(PerformanceCounter, PerformanceFrequency);
	}

	NTSTATUS NTAPI NT::ZwQuerySystemTime(OUT PLARGE_INTEGER SystemTime)
	{
		typedef NTSTATUS(NTAPI *_ZwQuerySystemTime)(PLARGE_INTEGER);
		_ZwQuerySystemTime pZwQuerySystemTime = (_ZwQuerySystemTime)GetExportedFunction("ZwQuerySystemTime");

		return pZwQuerySystemTime(SystemTime);
	}

	NTSTATUS NTAPI NT::ZwQueryTimerResolution(
		OUT PULONG MinimumResolution,
		OUT PULONG MaximumResolution, 
		OUT PULONG CurrentResolution)
	{
		typedef NTSTATUS(NTAPI *_ZwQueryTimerResolution)(PULONG, PULONG, PULONG);
		_ZwQueryTimerResolution pZwQueryTimerResolution = (_ZwQueryTimerResolution)GetExportedFunction("ZwQueryTimerResolution");

		return pZwQueryTimerResolution(MinimumResolution, MaximumResolution, CurrentResolution);
	}

	NTSTATUS NTAPI NT::ZwSetSystemTime(
		IN PLARGE_INTEGER SystemTime,
		OUT PLARGE_INTEGER PreviousTime OPTIONAL)
	{
		typedef NTSTATUS(NTAPI *_ZwSetSystemTime)(PLARGE_INTEGER, PLARGE_INTEGER);
		_ZwSetSystemTime pZwSetSystemTime = (_ZwSetSystemTime)GetExportedFunction("ZwSetSystemTime");

		return pZwSetSystemTime(SystemTime, PreviousTime);
	}

	NTSTATUS NTAPI NT::ZwSetTimerResolution(
		IN ULONG DesiredResolution,
		IN BOOLEAN SetResolution, OUT PULONG CurrentResolution)
	{
		typedef NTSTATUS(NTAPI *_ZwSetTimerResolution)(ULONG, BOOLEAN, PULONG);
		_ZwSetTimerResolution pZwSetTimerResolution = (_ZwSetTimerResolution)GetExportedFunction("ZwSetTimerResolution");

		return pZwSetTimerResolution(DesiredResolution, SetResolution, CurrentResolution);
	}

	BOOLEAN NTAPI NT::RtlTimeFieldsToTime(
		IN PTIME_FIELDS TimeFields,
		OUT PLARGE_INTEGER Time)
	{
		typedef BOOLEAN(NTAPI *_RtlTimeFieldsToTime)(PTIME_FIELDS, PLARGE_INTEGER);
		_RtlTimeFieldsToTime pRtlTimeFieldsToTime = (_RtlTimeFieldsToTime)GetExportedFunction("RtlTimeFieldsToTime");

		return pRtlTimeFieldsToTime(TimeFields, Time);
	}

	VOID NTAPI NT::RtlTimeToTimeFields(
		IN PLARGE_INTEGER Time,
		OUT PTIME_FIELDS TimeFields)
	{
		typedef VOID(NTAPI *_RtlTimeToTimeFields)(PLARGE_INTEGER, PTIME_FIELDS);
		_RtlTimeToTimeFields pRtlTimeToTimeFields = (_RtlTimeToTimeFields)GetExportedFunction("RtlTimeToTimeFields");

		return pRtlTimeToTimeFields(Time, TimeFields);
	}

	// --- VIRTUAL MEMORY

	NTSTATUS NTAPI NT::ZwAllocateUserPhysicalPages(
		IN HANDLE ProcessHandle,
		IN PULONG NumberOfPages,
		OUT PULONG PageFrameNumbers)
	{
		typedef NTSTATUS(NTAPI *_ZwUserPhysicalPages)(HANDLE, PULONG, PULONG);
		_ZwUserPhysicalPages pZwUserPhysicalPages = (_ZwUserPhysicalPages)GetExportedFunction("ZwAllocateUserPhysicalPages");

		return pZwUserPhysicalPages(ProcessHandle, NumberOfPages, PageFrameNumbers);
	}

	NTSTATUS NTAPI NT::ZwFreeUserPhysicalPages(
		IN HANDLE ProcessHandle,
		IN OUT PULONG NumberOfPages,
		IN PULONG PageFrameNumbers)
	{
		typedef NTSTATUS(NTAPI *_ZwUserPhysicalPages)(HANDLE, PULONG, PULONG);
		_ZwUserPhysicalPages pZwUserPhysicalPages = (_ZwUserPhysicalPages)GetExportedFunction("ZwFreeUserPhysicalPages");

		return pZwUserPhysicalPages(ProcessHandle, NumberOfPages, PageFrameNumbers);
	}

	NTSTATUS NTAPI NT::ZwMapUserPhysicalPages(
		IN PVOID BaseAddress,
		IN PULONG NumberOfPages,
		IN PULONG PageFrameNumbers)
	{
		typedef NTSTATUS(NTAPI *_ZwUserPhysicalPages)(PVOID, PULONG, PULONG);
		_ZwUserPhysicalPages pZwUserPhysicalPages = (_ZwUserPhysicalPages)GetExportedFunction("ZwMapUserPhysicalPages");

		return pZwUserPhysicalPages(BaseAddress, NumberOfPages, PageFrameNumbers);
	}

	NTSTATUS NTAPI NT::ZwMapUserPhysicalPagesScatter(
		IN PVOID *BaseAddresses,
		IN PULONG NumberOfPages,
		IN PULONG PageFrameNumbers)
	{
		typedef NTSTATUS(NTAPI *_ZwUserPhysicalPages)(PVOID*, PULONG, PULONG);
		_ZwUserPhysicalPages pZwUserPhysicalPages = (_ZwUserPhysicalPages)GetExportedFunction("ZwMapUserPhysicalPagesScatter");

		return pZwUserPhysicalPages(BaseAddresses, NumberOfPages, PageFrameNumbers);
	}

	NTSTATUS NTAPI NT::ZwGetWriteWatch(
		IN HANDLE ProcessHandle,
		IN ULONG Flags,
		IN PVOID BaseAddress,
		IN ULONG RegionSize,
		OUT PULONG Buffer,
		IN OUT PULONG BufferEntries,
		OUT PULONG Granularity)
	{
		typedef NTSTATUS(NTAPI *_ZwGetWriteWatch)(HANDLE, ULONG, PVOID, ULONG, PULONG, PULONG, PULONG);
		_ZwGetWriteWatch pZwGetWriteWatch = (_ZwGetWriteWatch)GetExportedFunction("ZwGetWriteWatch");

		return pZwGetWriteWatch(ProcessHandle, Flags, BaseAddress, RegionSize, Buffer, BufferEntries, Granularity);
	}

	NTSTATUS NTAPI NT::ZwResetWriteWatch(
		IN HANDLE ProcessHandle,
		IN PVOID BaseAddress,
		IN ULONG RegionSize)
	{
		typedef NTSTATUS(NTAPI *_ZwResetWriteWatch)(HANDLE, PVOID, ULONG);
		_ZwResetWriteWatch pZwResetWriteWatch = (_ZwResetWriteWatch)GetExportedFunction("ZwResetWriteWatch");

		return pZwResetWriteWatch(ProcessHandle, BaseAddress, RegionSize);
	}

	// --- SECTIONS

	NTSTATUS NTAPI NT::ZwAreMappedFilesTheSame(
		IN PVOID Address1,
		IN PVOID Address2)
	{
		typedef NTSTATUS(NTAPI *_ZwAreMappedFilesTheSame)(PVOID, PVOID);
		_ZwAreMappedFilesTheSame pZwAreMappedFilesTheSame = (_ZwAreMappedFilesTheSame)GetExportedFunction("ZwAreMappedFilesTheSame");

		return pZwAreMappedFilesTheSame(Address1, Address2);
	}

	// --- THREADS

	NTSTATUS NTAPI NT::ZwImpersonateAnonymousToken(
		IN HANDLE ThreadHandle)
	{
		typedef NTSTATUS(NTAPI *_ZwImpersonateAnonymousToken)(HANDLE);
		_ZwImpersonateAnonymousToken pZwImpersonateAnonymousToken = (_ZwImpersonateAnonymousToken)
			GetExportedFunction("ZwImpersonateAnonymousToken");

		return pZwImpersonateAnonymousToken(ThreadHandle);
	}

	// --- PROCESSES

	NTSTATUS NTAPI NT::RtlCreateProcessParameters(
		OUT PPROCESS_PARAMETERS *ProcessParameters,
		IN PUNICODE_STRING ImageFile,
		IN PUNICODE_STRING DllPath OPTIONAL,
		IN PUNICODE_STRING CurrentDirectory OPTIONAL,
		IN PUNICODE_STRING CommandLine OPTIONAL,
		IN ULONG CreationFlags,
		IN PUNICODE_STRING WindowTitle OPTIONAL,
		IN PUNICODE_STRING Desktop OPTIONAL,
		IN PUNICODE_STRING Reserved OPTIONAL,
		IN PUNICODE_STRING Reserved2 OPTIONAL)
	{
		typedef NTSTATUS(NTAPI *_RtlCreateProcessParameters)(PPROCESS_PARAMETERS*, PUNICODE_STRING, PUNICODE_STRING, PUNICODE_STRING,
			PUNICODE_STRING, ULONG, PUNICODE_STRING, PUNICODE_STRING, PUNICODE_STRING, PUNICODE_STRING);

		_RtlCreateProcessParameters pRtlCreateProcessParameters = (_RtlCreateProcessParameters)GetExportedFunction("RtlCreateProcessParameters");

		return pRtlCreateProcessParameters(ProcessParameters, ImageFile, DllPath, CurrentDirectory, CommandLine,
			CreationFlags, WindowTitle, Desktop, Reserved, Reserved2);
	}

	NTSTATUS NTAPI NT::RtlDestroyProcessParameters(
		IN PPROCESS_PARAMETERS ProcessParameters)
	{
		typedef NTSTATUS(NTAPI *_RtlDestroyProcessParameters)(PPROCESS_PARAMETERS);
		_RtlDestroyProcessParameters pRtlDestroyProcessParameters = (_RtlDestroyProcessParameters)GetExportedFunction("RtlDestroyProcessParameters");

		return pRtlDestroyProcessParameters(ProcessParameters);
	}

	NT::PDEBUG_BUFFER NTAPI NT::RtlCreateQueryDebugBuffer(
		IN ULONG Size,
		IN BOOLEAN EventPair)
	{
		typedef PDEBUG_BUFFER(NTAPI *_RtlCreateQueryDebugBuffer)(ULONG, BOOLEAN);
		_RtlCreateQueryDebugBuffer pRtlCreateQueryDebugBuffer = (_RtlCreateQueryDebugBuffer)GetExportedFunction("RtlCreateQueryDebugBuffer");

		return pRtlCreateQueryDebugBuffer(Size, EventPair);
	}

	NTSTATUS NTAPI NT::RtlQueryProcessDebugInformation(
		IN ULONG ProcessId,
		IN ULONG DebugInfoClassMask,
		IN OUT PDEBUG_BUFFER DebugBuffer)
	{
		typedef NTSTATUS(NTAPI *_RtlQueryProcessDebugInformation)(ULONG, ULONG, PDEBUG_BUFFER);
		_RtlQueryProcessDebugInformation pRtlQueryProcessDebugInformation = (_RtlQueryProcessDebugInformation)GetExportedFunction("RtlQueryProcessDebugInformation");

		return pRtlQueryProcessDebugInformation(ProcessId, DebugInfoClassMask, DebugBuffer);
	}

	NTSTATUS NTAPI NT::RtlDestroyQueryDebugBuffer(
		IN PDEBUG_BUFFER DebugBuffer)
	{
		typedef NTSTATUS(NTAPI *_RtlDestroyQueryDebugBuffer)(PDEBUG_BUFFER);
		_RtlDestroyQueryDebugBuffer pRtlDestroyQueryDebugBuffer = (_RtlDestroyQueryDebugBuffer)GetExportedFunction("RtlDestroyQueryDebugBuffer");

		return pRtlDestroyQueryDebugBuffer(DebugBuffer);
	}

	// --- JOBS

	NTSTATUS NTAPI NT::ZwCreateJobObject(
		OUT PHANDLE JobHandle,
		IN ACCESS_MASK DesiredAccess,
		IN POBJECT_ATTRIBUTES ObjectAttributes)
	{
		typedef NTSTATUS(NTAPI *_ZwCreateJobObject)(PHANDLE, ACCESS_MASK, POBJECT_ATTRIBUTES);
		_ZwCreateJobObject pZwCreateJobObject = (_ZwCreateJobObject)GetExportedFunction("ZwCreateJobObject");

		return pZwCreateJobObject(JobHandle, DesiredAccess, ObjectAttributes);
	}

	NTSTATUS NTAPI NT::ZwOpenJobObject(
		OUT PHANDLE JobHandle,
		IN ACCESS_MASK DesiredAccess,
		IN POBJECT_ATTRIBUTES ObjectAttributes)
	{
		typedef NTSTATUS(NTAPI *_ZwOpenJobObject)(PHANDLE, ACCESS_MASK, POBJECT_ATTRIBUTES);
		_ZwOpenJobObject pZwOpenJobObject = (_ZwOpenJobObject)GetExportedFunction("ZwOpenJobObject");

		return pZwOpenJobObject(JobHandle, DesiredAccess, ObjectAttributes);
	}

	NTSTATUS NTAPI NT::ZwTerminateJobObject(
		IN HANDLE JobHandle,
		IN NTSTATUS ExitStatus)
	{
		typedef NTSTATUS(NTAPI *_ZwTerminateJobObject)(HANDLE, NTSTATUS);
		_ZwTerminateJobObject pZwTerminateJobObject = (_ZwTerminateJobObject)GetExportedFunction("ZwTerminateJobObject");

		return pZwTerminateJobObject(JobHandle, ExitStatus);
	}

	NTSTATUS NTAPI NT::ZwAssignProcessToJobObject(
		IN HANDLE JobHandle,
		IN HANDLE ProcessHandle)
	{
		typedef NTSTATUS(NTAPI *_ZwAssignProcessToJobObject)(HANDLE, HANDLE);
		_ZwAssignProcessToJobObject pZwAssignProcessToJobObject = (_ZwAssignProcessToJobObject)GetExportedFunction("ZwAssignProcessToJobObject");

		return pZwAssignProcessToJobObject(JobHandle, ProcessHandle);
	}

	NTSTATUS NTAPI NT::ZwQueryInformationJobObject(
		IN HANDLE JobHandle,
		IN JOBOBJECTINFOCLASS JobInformationClass,
		OUT PVOID JobInformation,
		IN ULONG JobInformationLength,
		OUT PULONG ReturnLength OPTIONAL)
	{
		typedef NTSTATUS(NTAPI *_ZwQueryInformationJobObject)(HANDLE, JOBOBJECTINFOCLASS, PVOID, ULONG, PULONG);
		_ZwQueryInformationJobObject pZwQueryInformationJobObject = (_ZwQueryInformationJobObject)GetExportedFunction("ZwQueryInformationJobObject");

		return pZwQueryInformationJobObject(JobHandle, JobInformationClass, JobInformation, JobInformationLength, ReturnLength);
	}

	NTSTATUS NTAPI NT::ZwSetInformationJobObject(
		IN HANDLE JobHandle,
		IN JOBOBJECTINFOCLASS JobInformationClass,
		IN PVOID JobInformation,
		IN ULONG JobInformationLength)
	{
		typedef NTSTATUS(NTAPI *_ZwSetInformationJobObject)(HANDLE, JOBOBJECTINFOCLASS, PVOID, ULONG);
		_ZwSetInformationJobObject pZwSetInformationJobObject = (_ZwSetInformationJobObject)GetExportedFunction("ZwSetInformationJobObject");

		return pZwSetInformationJobObject(JobHandle, JobInformationClass, JobInformation, JobInformationLength);
	}

	NTSTATUS NTAPI NT::ZwResumeProcess(IN HANDLE ProcessHandle)
	{
		typedef NTSTATUS(NTAPI *_Process)(HANDLE);
		_Process pProcess = (_Process)GetExportedFunction("ZwResumeProcess");

		return pProcess(ProcessHandle);
	}

	NTSTATUS NTAPI NT::ZwSuspendProcess(IN HANDLE ProcessHandle)
	{
		typedef NTSTATUS(NTAPI *_Process)(HANDLE);
		_Process pProcess = (_Process)GetExportedFunction("ZwSuspendProcess");

		return pProcess(ProcessHandle);
	}

}