#include "Module.h"
#include "Process.h"
#include "NT.h"
#include "PE.h"

namespace GF {

	Module::Module(MODULEENTRY32 me32)
	{
		UNREFERENCED_PARAMETER(me32);
		/*pid = me32.th32ProcessID;
		address = reinterpret_cast<DWORD_PTR>(me32.modBaseAddr);
		size = me32.modBaseSize;
		module = me32.hModule;
		name = me32.szModule;
		path = me32.szExePath;*/
	}

	DWORD Module::ProcessId()
	{
		return pid;
	}

	DWORD64 Module::Address()
	{
		return address;
	}

	DWORD64 Module::Size()
	{
		return size;
	}

	HMODULE Module::Module_()
	{
		return module;
	}

	GString Module::Name()
	{
		return name;
	}

	GString Module::Path()
	{
		return path;
	}

	ModuleList ModuleManager::Enumerate(DWORD process_id)
	{
		ModuleList buf;

		UNREFERENCED_PARAMETER(process_id);

		return buf;
	}

	Module *ModuleManager::GetByName(GString module_name, DWORD process_id)
	{
		UNREFERENCED_PARAMETER(module_name);
		UNREFERENCED_PARAMETER(process_id);

		return nullptr;
	}

	Module *ModuleManager::GetByName(GString module_name, ModuleList *ml)
	{
		UNREFERENCED_PARAMETER(module_name);
		UNREFERENCED_PARAMETER(ml);

		return nullptr;
	}

	template<class T>
	void GetModuleNameFromPeb(DWORD pid, DWORD64 peb, DWORD64 startaddress, GString &out)
	{
		T buf1[20] = { 0 }, buf2[20] = { 0 };
		ProcessManager::ReadMemory(pid, peb, &buf1, sizeof(buf1));
		ProcessManager::ReadMemory(pid, DWORD64(buf1[3] + 0x10), &buf1, sizeof(T));
		T dwStart = buf1[0];

		do {
			GUnicodeChar name[64] = { 0 };
			ProcessManager::ReadMemory(pid, DWORD64(buf1[0]), &buf1, sizeof(T));
			ProcessManager::ReadMemory(pid, DWORD64(buf1[0]), &buf2, sizeof(buf2));
			ProcessManager::ReadMemory(pid, DWORD64(buf2[12]), &name, sizeof(name));

			if (wcslen(name) && buf2[6] == startaddress) {
#ifdef _UNICODE
				out = name;
#else
				out = UnicodeToMultibyte(name, wcslen(name));
#endif
				return;
			}

		} while (buf1[0] != dwStart);
	}

	void ModuleManager::GetModuleNameFromPeb32(DWORD pid, DWORD64 peb, DWORD64 startaddress, GString &out)
	{
		GetModuleNameFromPeb<DWORD>(pid, peb, startaddress, out);
	}

	void ModuleManager::GetModuleNameFromPeb64(DWORD pid, DWORD64 peb, DWORD64 startaddress, GString &out)
	{
		GetModuleNameFromPeb<DWORD64>(pid, peb, startaddress, out);
	}

	bool ModuleManager::IsValid(DWORD64 module_address)
	{
		//TODO: implement functionality

		return module_address > 0;
	}

	bool ModuleManager::IsValid(GString module_name)
	{
		UNREFERENCED_PARAMETER(module_name);

		return true;
	}

}