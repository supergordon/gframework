#include "PE.h"
#include "Process.h"
#include "NT.h"

namespace GF {

	PIMAGE_DOS_HEADER PE::GetDosHeader(DWORD_PTR module_base)
	{
		PIMAGE_DOS_HEADER dos = reinterpret_cast<PIMAGE_DOS_HEADER>(module_base);

		if (!dos || dos->e_magic != 0x5A4D)
			return NULL;

		return dos;
	}

	PIMAGE_NT_HEADERS32 PE::GetNtHeader32(DWORD_PTR module_base)
	{
		PIMAGE_DOS_HEADER dos_header = GetDosHeader(module_base);

		if (!dos_header)
			return NULL;

		PIMAGE_NT_HEADERS32 nt32 = reinterpret_cast<PIMAGE_NT_HEADERS32>((DWORD_PTR)dos_header + dos_header->e_lfanew);

		if (nt32->OptionalHeader.Magic != IMAGE_NT_OPTIONAL_HDR32_MAGIC)
			return NULL;

		return nt32;
	}

	PIMAGE_NT_HEADERS64 PE::GetNtHeader64(DWORD_PTR module_base)
	{
		PIMAGE_DOS_HEADER dos_header = GetDosHeader(module_base);

		if (!dos_header)
			return NULL;

		PIMAGE_NT_HEADERS64 nt64 = reinterpret_cast<PIMAGE_NT_HEADERS64>((DWORD_PTR)dos_header + dos_header->e_lfanew);

		if (nt64->OptionalHeader.Magic != IMAGE_NT_OPTIONAL_HDR64_MAGIC)
			return NULL;

		return nt64;
	}

	bool PE::ReadPeHeader(const GString &path, PBYTE pe_header)
	{
		FILE *file = NULL;

		PVOID value = 0;
		Wow64DisableWow64FsRedirection(&value);

		errno_t err = GFileOpen(&file, path.c_str(), _G("rb"));
		if (err || !file)
			return false;

		size_t len = fread(pe_header, 1, 0x1000, file);
		fclose(file);

		Wow64RevertWow64FsRedirection(value);

		return len == 0x1000;
	}

	bool PE::ReadPeHeader(DWORD process_id, DWORD64 module_base, PBYTE pe_header)
	{
		HANDLE handle = OpenProcess(PROCESS_VM_READ, FALSE, process_id);
		if (!handle)
			return false;

#ifdef _AMD64_
		SIZE_T dwLength = 0;
		BOOL ret = ReadProcessMemory(handle, reinterpret_cast<LPCVOID>(module_base), pe_header, 0x1000, &dwLength);
#else
		DWORD64 dwLength = 0;
		BOOL ret = NT::ZwWow64ReadVirtualMemory64(handle, reinterpret_cast<PVOID64>(module_base), pe_header, 0x1000, &dwLength) == 0;
#endif

		CloseHandle(handle);

		if (!ret || dwLength != 0x1000)
			return false;

		return true;
	}

}