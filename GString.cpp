#include "GString.h"

namespace GF {

	GAnsiString UnicodeToMultibyte(const GUnicodeString &input, size_t length)
	{
		if (length == 0)
			length = input.length();

		if (length == 0)
			return GEmptyAnsiString;

		int num = WideCharToMultiByte(CP_UTF8, 0, input.c_str(), int(length), NULL, 0, NULL, NULL);
		if (num == 0)
			return GEmptyAnsiString;

		GAnsiChar *buffer = new GAnsiChar[num];
		if (!WideCharToMultiByte(CP_UTF8, 0, input.c_str(), int(length), buffer, num, NULL, NULL)) {
			delete [] buffer;
			return GEmptyAnsiString;
		}

		GAnsiString ret(buffer, num);
		delete [] buffer;

		return ret;
	}

	GUnicodeString MultibyteToUnicode(const GAnsiString &input, size_t length)
	{
		if (length == 0)
			length = input.length();

		if (length == 0)
			return GEmptyUnicodeString;

		int num = MultiByteToWideChar(CP_UTF8, 0, input.c_str(), int(length), NULL, 0);
		if (num == 0)
			return GEmptyUnicodeString;

		GUnicodeChar *buffer = new GUnicodeChar[num];
		if (!MultiByteToWideChar(CP_UTF8, 0, input.c_str(), int(length), buffer, num)) {
			delete [] buffer;
			return GEmptyUnicodeString;
		}

		GUnicodeString ret(buffer, num);
		delete [] buffer;

		return ret;
	}
}