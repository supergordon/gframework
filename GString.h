#ifndef __GSTRING__
#define __GSTRING__

#include <windows.h>
#include <string>

typedef std::string GAnsiString;
typedef std::wstring GUnicodeString;
typedef char GAnsiChar;
typedef wchar_t GUnicodeChar;
#define GEmptyAnsiString ""
#define GEmptyUnicodeString L""

#if defined (_UNICODE)
typedef GUnicodeChar GChar;
typedef GUnicodeString GString;
#define GSprintf wsprintfW
#define GPrintf wprintf
#define GLen wcslen
#define GCat wcscat
#define GFileOpen _wfopen_s
#define GEmptyString GEmptyUnicodeString
#define _G(s) L##s
#else
typedef GAnsiChar GChar;
typedef GAnsiString GString;
#define GSprintf sprintf
#define GPrintf printf
#define GLen strlen
#define GCat strcat
#define GFileOpen fopen_s
#define GEmptyString GEmptyAnsiString
#define _G(s) s
#endif

namespace GF {

	GAnsiString UnicodeToMultibyte(const GUnicodeString &input, size_t length = 0);
	GUnicodeString MultibyteToUnicode(const GAnsiString &input, size_t length = 0);
}

#endif