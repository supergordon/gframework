#include "Window.h"
#include "Process.h"

#include <stdio.h>
#include <string>
#include <list>

namespace GF {

	Window::Window(HWND h)
	{
		hwnd = h;

		name.clear();
		path.clear();
		title.clear();
		class_.clear();

		memset(&point, 0, sizeof(point));
		memset(&size, 0, sizeof(size));
	}

	HWND Window::Handle()
	{
		return hwnd;
	}

	DWORD Window::ProcessId()
	{
		return WindowManager::GetProcessId(hwnd);
	}

	HICON Window::Icon()
	{
		return WindowManager::GetAppIcon(hwnd);
	}

	bool Window::Wow64()
	{
		return WindowManager::IsWow64(hwnd);
	}

	bool Window::Terminate()
	{
		return WindowManager::TerminateProcess(hwnd);
	}

	bool Window::Minimize()
	{
		return WindowManager::MinimizeWindow(hwnd);
	}

	bool Window::Maximize()
	{
		return WindowManager::MaximizeWindow(hwnd);
	}

	bool Window::Restore()
	{
		return WindowManager::RestoreWindow(hwnd);
	}

	bool Window::Destroy()
	{
		return WindowManager::DestroyWindow(hwnd);
	}

	bool Window::Hung()
	{
		return WindowManager::IsHungWindow(hwnd);
	}

	bool Window::Visible()
	{
		return WindowManager::IsVisible(hwnd);
	}

	bool Window::Minimized()
	{
		return WindowManager::IsMinimized(hwnd);
	}

	bool Window::Maximized()
	{
		return WindowManager::IsMaximized(hwnd);
	}

	const GString &Window::Name()
	{
		if (name.length() == 0)
			WindowManager::GetProcessName(hwnd, name);

		return name;
	}

	const GString &Window::Path()
	{
		if (path.length() == 0)
			WindowManager::GetPath(hwnd, path);

		return path;
	}

	const GString &Window::Title()
	{
		WindowManager::GetWindowName(hwnd, title);

		return title;
	}

	const GString &Window::Class()
	{
		WindowManager::GetWindowClassName(hwnd, class_);

		return class_;
	}

	POINT *Window::Position()
	{
		WindowManager::GetPosition(hwnd, point);

		return &point;
	}

	SIZE *Window::Size()
	{
		WindowManager::GetSize(hwnd, size);

		return &size;
	}

	LONG Window::Id()
	{
		return WindowManager::GetId(hwnd);
	}

	HWND Window::Parent()
	{
		return WindowManager::GetParent(hwnd);
	}

	LONG_PTR Window::Style()
	{
		return WindowManager::GetStyle(hwnd);
	}

	LONG_PTR Window::ExtendedStyle()
	{
		return WindowManager::GetExtendedStyle(hwnd);
	}

	HINSTANCE Window::Instance()
	{
		return WindowManager::GetInstance(hwnd);
	}

	WindowList WindowManager::Enumerate()
	{
		WindowList buf;

		HWND hwnd = ::GetWindow(GetDesktopWindow(), GW_CHILD);

		do {
			if (!IsValid(hwnd))
				continue;

			buf.push_back(new Window(hwnd));

		} while (hwnd = ::GetWindow(hwnd, GW_HWNDNEXT));

		return buf;
	}

	int WindowManager::FreeList(WindowList &wl)
	{
		int num_destroyed = 0;

		for (WindowListIterator it = wl.begin(); it != wl.end(); it++, num_destroyed++)
			delete *it;

		wl.clear();

		return num_destroyed;
	}

	Window *WindowManager::GetByProcessId(DWORD pid, WindowList *wl, bool copy)
	{
		if (pid == 0)
			return nullptr;

		WindowList list = wl ? *wl : WindowManager::Enumerate();
		Window *w = nullptr;

		for (WindowListIterator it = list.begin(); it != list.end(); it++) {
			if (pid != (*it)->ProcessId())
				continue;

			if (copy)
				w = new Window((*it)->Handle());
			else
				w = *it;
		}

		if (!wl)
			FreeList(list);

		return w;
	}

	Window *WindowManager::GetByHwnd(HWND hwnd)
	{
		return hwnd ? new Window(hwnd) : NULL;
	}

	Window *WindowManager::GetByWindowName(const GString &window_name)
	{
		return window_name.length() ? new Window(GetWindow(window_name)) : nullptr;
	}

	Window *WindowManager::GetByClassName(const GString &class_name)
	{
		return class_name.length() ? new Window(GetWindowClass(class_name)) : nullptr;
	}

	HWND WindowManager::GetWindow(const GString &windowname)
	{
		return ::FindWindow(0, windowname.c_str());
	}

	HWND WindowManager::GetWindowClass(const GString &classname)
	{
		return ::FindWindow(classname.c_str(), 0);
	}

	DWORD WindowManager::GetProcessId(HWND hwnd)
	{
		DWORD pid = 0;
		::GetWindowThreadProcessId(hwnd, &pid);

		return pid;
	}

	DWORD WindowManager::GetMainThreadId(HWND hwnd)
	{
		return GetWindowThreadProcessId(hwnd, NULL);
	}

	bool WindowManager::GetProcessName(HWND hwnd, GString &procname)
	{
		DWORD pid = 0;
		::GetWindowThreadProcessId(hwnd, &pid);

		return ProcessManager::GetName(pid, procname);
	}

	bool WindowManager::GetWindowName(HWND hwnd, GString &title)
	{
		GChar buf_title[MAX_WINDOWNAME_LEN] = _G("");
		int ret = GetWindowText(hwnd, buf_title, MAX_WINDOWNAME_LEN) != 0;

		if (ret == 0)
			return false;

		title = buf_title;

		return true;
	}

	bool WindowManager::GetWindowClassName(HWND hwnd, GString &title)
	{
		GChar buf_title[MAX_WINDOWNAME_LEN] = _G("");
		int ret = GetClassName(hwnd, buf_title, MAX_WINDOWNAME_LEN) != 0;

		if (ret == 0)
			return false;

		title = buf_title;

		return true;
	}

	bool WindowManager::GetPath(HWND hwnd, GString &path)
	{
		DWORD pid = 0;
		GetWindowThreadProcessId(hwnd, &pid);

		return ProcessManager::GetPath(pid, path);
	}

	HICON WindowManager::GetAppIcon(HWND hwnd, int type)
	{
		DWORD pid = 0;
		::GetWindowThreadProcessId(hwnd, &pid);

		return ProcessManager::GetAppIcon(pid, type);
	}

	bool WindowManager::GetPosition(HWND hwnd, POINT &point)
	{
		RECT rect = { 0 };
		BOOL ret = GetWindowRect(hwnd, &rect);

		point.x = rect.left;
		point.y = rect.top;

		return ret != 0;
	}

	bool WindowManager::GetSize(HWND hwnd, SIZE &size)
	{
		RECT rect = { 0 };
		BOOL ret = GetWindowRect(hwnd, &rect);

		size.cx = rect.right - rect.left;
		size.cy = rect.bottom - rect.top;

		return ret != 0;
	}

	LONG_PTR WindowManager::GetStyle(HWND hwnd)
	{
		return GetWindowLongPtrW(hwnd, GWL_STYLE);
	}

	LONG_PTR WindowManager::GetExtendedStyle(HWND hwnd)
	{
		return GetWindowLongPtrW(hwnd, GWL_EXSTYLE);
	}

	HINSTANCE WindowManager::GetInstance(HWND hwnd)
	{
		return reinterpret_cast<HINSTANCE>(GetWindowLongPtrW(hwnd, -6));
	}

	LONG WindowManager::GetId(HWND hwnd)
	{
		return static_cast<LONG>(GetWindowLongPtrW(hwnd, GWL_ID));
	}

	HWND  WindowManager::GetParent(HWND hwnd)
	{
		return reinterpret_cast<HWND>(GetWindowLongPtrW(hwnd, -8));
	}

	bool WindowManager::IsWow64(HWND hwnd)
	{
		DWORD pid = 0;
		::GetWindowThreadProcessId(hwnd, &pid);

		return ProcessManager::IsWow64(pid);
	}

	bool WindowManager::OpenPropertyDialog(const GString &path)
	{
#ifdef _UNICODE
		SHELLEXECUTEINFOW sei;
#else
		SHELLEXECUTEINFOA sei;
#endif
		ZeroMemory(&sei, sizeof(sei));

		sei.cbSize = sizeof(sei);
		sei.fMask = SEE_MASK_INVOKEIDLIST | SEE_MASK_FLAG_NO_UI | SEE_MASK_DOENVSUBST;
		sei.lpVerb = _G("properties");
		sei.lpFile = path.c_str();
		sei.hwnd = HWND_DESKTOP;

		PVOID value = 0;
		Wow64DisableWow64FsRedirection(&value);

		BOOL ret = ShellExecuteEx(&sei);

		Wow64RevertWow64FsRedirection(value);

		return ret == TRUE;
	}

	bool WindowManager::OpenInExplorer(const GString &path)
	{
		PVOID value = 0;
		Wow64DisableWow64FsRedirection(&value);

		LPITEMIDLIST dir = ILCreateFromPath(path.c_str());
		HRESULT h = SHOpenFolderAndSelectItems(dir, 0, 0, 0);
		ILFree(dir);

		Wow64RevertWow64FsRedirection(value);

		return h > 0;
	}

	bool WindowManager::TerminateProcess(HWND hwnd)
	{
		DWORD pid = 0;
		GetWindowThreadProcessId(hwnd, &pid);

		return ProcessManager::Terminate(pid);
	}

	bool WindowManager::MinimizeWindow(HWND hwnd)
	{
		BOOL r = CloseWindow(hwnd);

		if (!r) {
			r = ShowWindow(hwnd, SW_MINIMIZE);
			ShowWindow(hwnd, SW_FORCEMINIMIZE);
		}

		return true;
	}

	bool WindowManager::MaximizeWindow(HWND hwnd)
	{
		::ShowWindow(hwnd, SW_MAXIMIZE);
		return true;
	}

	bool WindowManager::RestoreWindow(HWND hwnd)
	{
		::ShowWindow(hwnd, SW_RESTORE);

		return true;
	}

	bool WindowManager::DestroyWindow(HWND hwnd)
	{
		BOOL r = ::DestroyWindow(hwnd);
		if (!r) {

			r = ::SendNotifyMessage(hwnd, WM_CLOSE, 0, 0);
			r = ::SendNotifyMessage(hwnd, WM_DESTROY, 0, 0);
			r = ::SendNotifyMessage(hwnd, WM_QUIT, 0, 0);

			return r == 1;
		}

		return false;
	}

	bool WindowManager::IsHungWindow(HWND hwnd)
	{
		typedef BOOL(WINAPI* IsHungAppWindow_t)(HWND hWnd);
		IsHungAppWindow_t pHung = (IsHungAppWindow_t)GetProcAddress(GetModuleHandleW(L"user32.dll"), "IsHungAppWindow");

		return pHung(hwnd) == TRUE;
	}

	bool WindowManager::IsMinimized(HWND hwnd)
	{
		return IsIconic(hwnd) != 0;
	}

	bool WindowManager::IsMaximized(HWND hwnd)
	{
		return IsZoomed(hwnd) != 0;
	}

	bool WindowManager::IsVisible(HWND hwnd)
	{
		return IsWindowVisible(hwnd) != 0;
	}

	bool WindowManager::IsValid(HWND hwnd)
	{
		if (IsWindow(hwnd) == FALSE || !IsVisible(hwnd) || GetWindowTextLength(hwnd) == 0)
			return false;

		return true;
	}

	bool WindowEnum::Add(Window *w)
	{
		for (WindowListIterator it = list.begin(); it != list.end(); it++)
			if (w->Handle() == (*it)->Handle())
				return false;

		if (!WindowManager::IsValid(w->Handle()))
			return false;

		list.push_back(new Window(w->Handle()));

		return true;
	}

	WindowList WindowEnum::Added()
	{
		WindowList buf = WindowManager::Enumerate();
		WindowList added;

		for (WindowListIterator it = buf.begin(); it != buf.end(); it++)
			if (Add(*it))
				added.push_back(new Window((*it)->Handle()));

		WindowManager::FreeList(buf);

		return added;
	}

	WindowList WindowEnum::Removed()
	{
		WindowList removed;

		for (WindowListIterator it = list.begin(); it != list.end(); it++) {
			if (!WindowManager::IsValid((*it)->Handle())) {
				removed.push_back(new Window((*it)->Handle()));
				delete *it;
				it = list.erase(it);
			}
		}

		return removed;
	}

	WindowList WindowEnum::Updated()
	{
		WindowList added = Added();
		WindowList removed = Removed();
		WindowList updated;

		for (WindowListIterator it = added.begin(); it != added.end(); it++)
			updated.push_back(new Window((*it)->Handle()));

		for (WindowListIterator it = removed.begin(); it != removed.end(); it++)
			updated.push_back(new Window((*it)->Handle()));

		WindowManager::FreeList(added);
		WindowManager::FreeList(removed);

		return updated;
	}

	bool WindowEnum::HasUpdated(const WindowList &updated)
	{
		return updated.size() > 0;
	}

	WindowList &WindowEnum::List()
	{
		return list;
	}

	void WindowEnum::Clear()
	{
		WindowManager::FreeList(list);
	}

}