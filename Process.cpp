#include "Process.h"
#include "Thread.h"
#include "Window.h"
#include "Handle.h"
#include "NT.h"

namespace GF {

	bool GetProcessUsername(HANDLE hProcess, GString &user, GString &domain);

	/*
	GetProcessIoCounters => http://msdn.microsoft.com/en-us/library/windows/desktop/ms683218%28v=vs.85%29.aspx


	*/

	Process::Process()
	{
		Process(0, GEmptyString, 0);
	}

	Process::Process(DWORD process_id, const GString &process_name, DWORD threads)
	{
		pid = process_id;
		num_threads = threads;
		wow64 = -1;
		icon = NULL;
		handle = NULL;
		peb_address32 = NULL;
		peb_address64 = NULL;

		name.clear();
		user.clear();
		path.clear();
		cmdline.clear();

		for (int i = 0; i < 32; i++)
			desc[i].clear();

		if (process_name.length())
			name = process_name;

		cpu = new ProcessCPU;
	}

	Process::~Process()
	{
		delete cpu;
		Close();
	}

	DWORD Process::ProcessId()
	{
		return pid;
	}

	const GString &Process::Name()
	{
		if (name.length() == 0)
			ProcessManager::GetName(pid, name);

		return name;
	}

	const GString &Process::User()
	{
		if (user.length() == 0)
			ProcessManager::GetUser(pid, user);

		return user;
	}

	const GString &Process::Description(const GString &get_desc, int index)
	{
		if (index == 0) {
			ProcessManager::GetDescription(pid, desc[0], get_desc);
			return desc[0];
		}
		else {
			if (desc[index].length() == 0)
				ProcessManager::GetDescription(pid, desc[index], get_desc);

			return desc[index];
		}

		return desc[0];
	}

	const GString &Process::Path()
	{
		if (path.length() == 0)
			ProcessManager::GetPath(pid, path);

		return path;
	}

	const GString &Process::CommandLine()
	{
		if (cmdline.length() == 0)
			ProcessManager::GetCommandLine(pid, cmdline);

		return cmdline;
	}

	HICON Process::Icon(int type)
	{
		return ProcessManager::GetAppIcon(pid, type);
	}

	bool Process::Wow64()
	{
		if (wow64 == -1)
			wow64 = ProcessManager::IsWow64(pid);

		return wow64 == 1;
	}

	bool Process::Admin()
	{
		return ProcessManager::RunningAsAdmin(pid);
	}

	float Process::UsedCPU()
	{
		if (!cpu->Initialized())
			cpu->Init(pid);

		return cpu->GetUsage();
	}

	DWORD Process::UsedMemory()
	{
		GPROCESS_MEMORY_COUNTERS_EX pmc = { 0 };
		ProcessManager::GetMemoryUsage(pid, pmc);

		return static_cast<DWORD>(pmc.PrivateUsage);
	}

	DWORD Process::NumThreads()
	{
		return num_threads;
	}

	DWORD Process::NumHandles()
	{
		return ProcessManager::GetNumHandles(pid);
	}

	DWORD64 Process::Peb32()
	{
		if (!peb_address32)
			peb_address32 = ProcessManager::GetPeb32(pid);

		return peb_address32;
	}

	DWORD64 Process::Peb64()
	{
		if (!peb_address64)
			peb_address64 = ProcessManager::GetPeb64(pid);

		return peb_address64;
	}

	bool Process::Read(DWORD64 address, PVOID buffer, DWORD64 size)
	{
		return ProcessManager::ReadMemory(pid, address, buffer, size);
	}

	bool Process::Write(DWORD64 address, PVOID buffer, DWORD64 size)
	{
		return ProcessManager::WriteMemory(pid, address, buffer, size);
	}

	bool Process::Terminate(int exitcode)
	{
		return ProcessManager::Terminate(pid, exitcode);
	}

	bool Process::Suspend()
	{
		return ProcessManager::Suspend(pid);
	}

	bool Process::Resume()
	{
		return ProcessManager::Resume(pid);
	}

	HANDLE Process::Open(DWORD dwDesiredAccess)
	{
		if (handle)
			CloseHandle(handle);

		handle = OpenProcess(dwDesiredAccess, FALSE, pid);

		return handle;
	}

	void Process::Close()
	{
		if (handle)
			CloseHandle(handle);

		handle = NULL;
	}

	ProcessList ProcessManager::Enumerate()
	{
		ProcessList buf;

		ULONG size = 0x20000;
		PVOID buffer = malloc(size);
		DWORD dwLength = 0;
		int status = 0;

		while ((status = NT::ZwQuerySystemInformation(NT::SystemProcessInformation, buffer, size, &dwLength)) == STATUS_INFO_LENGTH_MISMATCH) 
			buffer = realloc(buffer, size *= 2);

		if (status != 0) 
			return buf;

		NT::PSYSTEM_PROCESSES sp = reinterpret_cast<NT::PSYSTEM_PROCESSES>(buffer);

		do {
			sp = reinterpret_cast<NT::PSYSTEM_PROCESSES>(ULONG_PTR(sp) + sp->NextEntryDelta);
#ifdef _UNICODE
			buf.push_back(new Process(sp->ProcessId, sp->ProcessName.Buffer, sp->ThreadCount));
#else
			buf.push_back(new Process(sp->ProcessId, UnicodeToMultibyte(sp->ProcessName.Buffer, sp->ProcessName.Length), sp->ThreadCount));
#endif
		} while (sp->NextEntryDelta);

		free(buffer);

		return buf;

		HANDLE snap = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
		if (snap == INVALID_HANDLE_VALUE)
			return buf;

	#ifdef _UNICODE
		PROCESSENTRY32W pe32;
		pe32.dwSize = sizeof(pe32);

		if (!Process32FirstW(snap, &pe32)) {
			CloseHandle(snap);
			return buf;
		}
		do {
			buf.push_back(new Process(pe32.th32ProcessID, pe32.szExeFile, pe32.cntThreads));
		} while (Process32NextW(snap, &pe32));
	#else
		PROCESSENTRY32 pe32;
		pe32.dwSize = sizeof(pe32);

		if (!Process32First(snap, &pe32)) {
			CloseHandle(snap);
			return buf;
		}
		do {
			buf.push_back(new Process(pe32.th32ProcessID, pe32.szExeFile, pe32.cntThreads));
		} while (Process32Next(snap, &pe32));
	#endif

		CloseHandle(snap);

		return buf;
	}

	int ProcessManager::FreeList(ProcessList &pl)
	{
		int num_destroyed = 0;

		for (ProcessListIterator it = pl.begin(); it != pl.end(); it++, num_destroyed++)
			delete *it;

		pl.clear();

		return num_destroyed;
	}

	Process *ProcessManager::GetByName(const GString &name, ProcessList *pl, bool copy)
	{
		ProcessList list = pl ? *pl : ProcessManager::Enumerate();
		Process *p = nullptr;

		for (ProcessListIterator it = list.begin(); it != list.end(); it++) {
			if (name != (*it)->Name())
				continue;

			if (copy)
				p = new Process((*it)->ProcessId(), (*it)->Name(), (*it)->NumThreads());
			else
				p = *it;
		}

		if (!pl)
			FreeList(list);

		return p;
	}

	Process *ProcessManager::GetByProcessId(DWORD pid, ProcessList *pl, bool copy)
	{
		ProcessList list = pl ? *pl : ProcessManager::Enumerate();
		Process *p = nullptr;

		for (ProcessListIterator it = list.begin(); it != list.end(); it++) {
			if (pid != (*it)->ProcessId())
				continue;

			if (copy)
				p = new Process((*it)->ProcessId(), (*it)->Name(), (*it)->NumThreads());
			else
				p = *it;
		}

		if (!pl) 
			FreeList(list);

		return p;
	}

	DWORD ProcessManager::GetProcessId(const GString &process_name)
	{
		DWORD process_id = -1;
		ProcessList buf = Enumerate();

		for (ProcessListIterator it = buf.begin(); it != buf.end(); it++) {
			if ((*it)->Name() == process_name) {
				process_id = (*it)->ProcessId();
				break;
			}
		}

		FreeList(buf);

		return process_id;
	}

	DWORD ProcessManager::GetCurrentProcessId()
	{
		return ::GetCurrentProcessId();
	}

	bool ProcessManager::GetName(DWORD pid, GString &name)
	{
		ProcessList list = Enumerate();
		for (ProcessListIterator it = list.begin(); it != list.end(); it++) {
			if (pid == (*it)->ProcessId()) {
				name = (*it)->Name();
				break;
			}
		}

		FreeList(list);

		return name.length() > 0;
	}

	bool ProcessManager::GetUser(DWORD pid, GString &user)
	{
		if (pid == 0 || pid == 4) {
			user = _G("SYSTEM");
			return true;
		}

		HANDLE handle = OpenProcess(PROCESS_QUERY_LIMITED_INFORMATION, false, pid);
		if (!handle)
			return false;

		GString domain;
		bool ret = GetProcessUsername(handle, user, domain);
		CloseHandle(handle);

		return ret;
	}

	bool ProcessManager::GetDomain(DWORD pid, GString &domain)
	{
		if (pid == 0 || pid == 4) {
			domain = _G("SYSTEM");
			return true;
		}

		HANDLE handle = OpenProcess(PROCESS_QUERY_LIMITED_INFORMATION, false, pid);
		if (!handle)
			return false;

		GString user;
		bool ret = GetProcessUsername(handle, user, domain);
		CloseHandle(handle);

		return ret;
	}

	bool ProcessManager::GetDescription(DWORD pid, GString &out, const GString &desc)
	{
		GString path;
		if (!ProcessManager::GetPath(pid, path))
			return false;

		if (path.length() == 0)
			return false;

		PVOID value = 0;
		Wow64DisableWow64FsRedirection(&value);

		DWORD size = GetFileVersionInfoSize(path.c_str(), NULL);
		if (size == 0) {
			Wow64RevertWow64FsRedirection(value);
			return false;
		}

		void *data = reinterpret_cast<void*>(malloc(size));

		if (!GetFileVersionInfo(path.c_str(), NULL, size, data)) {
			free(data);
			Wow64RevertWow64FsRedirection(value);
			return false;
		}

		Wow64RevertWow64FsRedirection(value);

		VS_FIXEDFILEINFO *ffi;
		UINT buffer_len;
		DWORD *lang_code = NULL;

		BOOL success = VerQueryValue(data, _G("\\\\"), (LPVOID*)&ffi, &buffer_len);
		if (success == 0 || buffer_len == 0)
			return false;

		success = VerQueryValue(data, _G("\\VarFileInfo\\Translation"), (LPVOID*)&lang_code, &buffer_len);

		if (success == 0 || buffer_len == 0) {
			free(data);
			return false;
		}

		if (!lang_code) {
			free(data);
			return false;
		}

		GChar buf[256] = { 0 };
		GSprintf(buf, _G("\\StringFileInfo\\%04x%04x\\%s"), LOWORD(lang_code[0]), HIWORD(lang_code[0]), desc.c_str());

		void *ret = nullptr;
		success = VerQueryValue(data, buf, (LPVOID*)&ret, &buffer_len);

		if (success == 0 || buffer_len == 0 || !ret) {
			free(data);
			return false;
		}

		GChar *ret_gchar = reinterpret_cast<GChar*>(ret);
		ret_gchar[buffer_len] = 0;

		if (GLen(ret_gchar) < 260)
			out.assign(ret_gchar, 260);

		free(data);

		return out.length() != 0;
	}

	bool ProcessManager::GetMemoryUsage(DWORD pid, GPROCESS_MEMORY_COUNTERS_EX &pmc)
	{
		HANDLE handle = OpenProcess(PROCESS_QUERY_LIMITED_INFORMATION, FALSE, pid);
		if (!handle)
			return false;

		BOOL ret = GetProcessMemoryInfo(handle, reinterpret_cast<PROCESS_MEMORY_COUNTERS*>(&pmc), sizeof(pmc));

		CloseHandle(handle);

		return ret != 0;
	}

	bool ProcessManager::GetPath(DWORD pid, GString &path)
	{
		if (pid == 0 || pid == 4) {
			GChar system32_dir[MAX_PATH] = { 0 };
			GetSystemDirectory(system32_dir, MAX_PATH);

			path = system32_dir + GString(_G("\\ntoskrnl.exe"));
			return true;
		}

		HANDLE handle = OpenProcess(PROCESS_QUERY_LIMITED_INFORMATION, FALSE, pid);
		if (handle) {
			GChar buf_path[MAX_PATH] = _G("");

			DWORD size = MAX_PATH;
			BOOL ret = QueryFullProcessImageName(handle, 0, buf_path, &size);

			if (!ret || GLen(buf_path) == 0)
				ret = ::GetModuleFileNameEx(handle, 0, buf_path, MAX_PATH);

			path = buf_path;

			::CloseHandle(handle);

			return ret != 0;
		}

		return false;
	}

	HICON ProcessManager::GetAppIcon(DWORD pid, int type)
	{
		GString path;
		if (!ProcessManager::GetPath(pid, path))
			return NULL;

		return GetAppIcon(path, type);
	}

	HICON ProcessManager::GetAppIcon(const GString &path, int type)
	{
	#ifdef _UNICODE
		SHFILEINFOW st = { 0 };
	#else
		SHFILEINFOA st = { 0 };
	#endif
		PVOID value = 0;
		Wow64DisableWow64FsRedirection(&value);

		HICON icon = NULL;
		if (SHGetFileInfo(path.c_str(), 0, &st, sizeof(st), SHGFI_SYSICONINDEX)) {
			IImageList *imageList;

			if (type == GICON_SMALL)
				if (SUCCEEDED(SHGetImageList(SHIL_SMALL, IID_PPV_ARGS(&imageList))))
					imageList->GetIcon(st.iIcon, 0, &icon);

			else if (type == GICON_LARGE) 
				if (SUCCEEDED(SHGetImageList(SHIL_LARGE, IID_PPV_ARGS(&imageList))))
					imageList->GetIcon(st.iIcon, 0, &icon);

			else if (type == GICON_EXTRALARGE) 
				if (SUCCEEDED(SHGetImageList(SHIL_EXTRALARGE, IID_PPV_ARGS(&imageList))))
					imageList->GetIcon(st.iIcon, 0, &icon);
	
			else if (type == GICON_JUMBO) 
				if (SUCCEEDED(SHGetImageList(SHIL_JUMBO, IID_PPV_ARGS(&imageList))))
					imageList->GetIcon(st.iIcon, 0, &icon);
		}

		if (icon) {
			Wow64RevertWow64FsRedirection(value);
			return icon;
		}

		UINT flags = SHGFI_ICON;
		if (type == GICON_SMALL)
			flags |= SHGFI_SMALLICON;
		else
			flags |= SHGFI_LARGEICON;

		SHGetFileInfo(path.c_str(), FILE_ATTRIBUTE_READONLY, &st, sizeof(st), flags);

		if (st.hIcon) {
			Wow64RevertWow64FsRedirection(value);
			return st.hIcon;
		}

		HICON iconLarge = NULL, iconSmall = NULL;
		if (!ExtractIconEx(path.c_str(), 0, &iconLarge, &iconSmall, 1)) {
			Wow64RevertWow64FsRedirection(value);
			return NULL;
		}

		Wow64RevertWow64FsRedirection(value);

		if (type == GICON_SMALL) {
			DestroyIcon(iconLarge);
			return iconSmall;
		}
		else {
			DestroyIcon(iconSmall);
			return iconLarge;
		}

		return NULL;
	}

	DWORD ProcessManager::GetExitcode(DWORD pid)
	{
		HANDLE handle = OpenProcess(PROCESS_QUERY_LIMITED_INFORMATION, FALSE, pid);
		if (!handle)
			return NULL;

		DWORD exitcode = 0;
		GetExitCodeProcess(handle, &exitcode);
		CloseHandle(handle);

		return exitcode;
	}

	DWORD ProcessManager::GetNumProcesses()
	{
		ProcessList list = Enumerate();
		DWORD size = static_cast<DWORD>(list.size());

		FreeList(list);

		return size;
	}

	DWORD ProcessManager::GetNumThreads(DWORD pid)
	{
		/*ThreadList list = ThreadManager::Enumerate(pid);
		DWORD size = static_cast<DWORD>(list.size());

		ThreadManager::FreeList(list);

		return size;*/

		DWORD count = 0;
		DWORD dwLength = 0;
		ULONG sizeBuffer = 0x20000;
		PVOID buffer = malloc(sizeBuffer);

		while (NT::ZwQuerySystemInformation(NT::SystemProcessInformation, buffer, sizeBuffer, &dwLength) == STATUS_INFO_LENGTH_MISMATCH)
			buffer = realloc(buffer, sizeBuffer *= 2);

		NT::PSYSTEM_PROCESSES sp = reinterpret_cast<NT::PSYSTEM_PROCESSES>(buffer);

		do {
			sp = reinterpret_cast<NT::PSYSTEM_PROCESSES>(ULONG_PTR(sp) + sp->NextEntryDelta);
			
			if (pid == sp->ProcessId)
				count += sp->ThreadCount;

		} while (sp->NextEntryDelta);

		free(buffer);

		return count;
	}

	DWORD ProcessManager::GetNumHandles(DWORD pid)
	{
		HANDLE handle = OpenProcess(PROCESS_QUERY_LIMITED_INFORMATION, FALSE, pid);
		if (!handle)
			return NULL;

		DWORD count = 0;
		if (!GetProcessHandleCount(handle, &count))
			count = 0;

		CloseHandle(handle);

		return count;
	}

	DWORD64 ProcessManager::GetPeb32(DWORD pid)
	{
		HANDLE handle = OpenProcess(PROCESS_VM_READ, FALSE, pid);
		if (!handle)
			return NULL;

		auto list = ThreadManager::Enumerate(pid);
		Thread *t = *(list.begin());
		
		NT::TEB32 teb32 = { 0 };
#ifdef _AMD64_
		int ret = ReadProcessMemory(handle, LPCVOID(t->Teb32()), &teb32, sizeof(teb32), NULL);
#else
		int ret = NT::ZwWow64ReadVirtualMemory64(handle, PVOID64(t->Teb32()), &teb32, sizeof(teb32), NULL) == 0;
#endif
		ThreadManager::FreeList(list);
		CloseHandle(handle);

		return ret ? DWORD64(teb32.ProcessEnvironmentBlock) : NULL;
	}

	DWORD64 ProcessManager::GetPeb64(DWORD pid)
	{
		HANDLE handle = OpenProcess(PROCESS_QUERY_LIMITED_INFORMATION, FALSE, pid);
		if (!handle)
			return NULL;

		ULONG dwLength = 0;
		NT::PROCESS_BASIC_INFORMATION_WOW64 pbi = { 0 };

	#ifdef _AMD64_
		int ret = NT::ZwQueryInformationProcess(handle, NT::ProcessBasicInformation, &pbi, sizeof(pbi), &dwLength);
	#else
		int ret = NT::ZwWow64QueryInformationProcess64(handle, NT::ProcessBasicInformation, &pbi, sizeof(pbi), &dwLength);
	#endif

		CloseHandle(handle);

		if (ret != 0)
			return NULL;

		return DWORD64(pbi.PebBaseAddress) /*- DWORD64(IsWow64(pid) ? 0x1000 : 0x0000)*/;
	}

	bool ProcessManager::GetCommandLine(DWORD pid, GString &cmdline)
	{
		HANDLE handle = OpenProcess(PROCESS_VM_READ, FALSE, pid);
		if (!handle)
			return false;

		GUnicodeChar cmd[0x1000] = { 0 };

		if (IsWow64(pid)) {
			DWORD buffer[40] = { 0 };
			ProcessManager::ReadMemory(pid, ProcessManager::GetPeb32(pid), &buffer, sizeof(buffer));
			ProcessManager::ReadMemory(pid, DWORD64(buffer[4]), &buffer, sizeof(buffer));
			ProcessManager::ReadMemory(pid, DWORD64(buffer[17]), &cmd, LOWORD(buffer[16]) * sizeof(GUnicodeChar));
			cmd[LOWORD(buffer[16])] = '\0';
		}
		else {
			DWORD64 buffer[20] = { 0 };
			ProcessManager::ReadMemory(pid, ProcessManager::GetPeb64(pid), &buffer, sizeof(buffer));
			ProcessManager::ReadMemory(pid, DWORD64(buffer[4]), &buffer, sizeof(buffer));
			ProcessManager::ReadMemory(pid, DWORD64(buffer[15]), &cmd, LOWORD(buffer[14]) * sizeof(GUnicodeChar));
			cmd[LOWORD(buffer[14])] = '\0';
		}

		CloseHandle(handle);

#ifdef _UNICODE
		cmdline = cmd;
#else
		cmdline = UnicodeToMultibyte(cmd, wcslen(cmd));
#endif

		return cmdline.length() > 0;
	}

	bool ProcessManager::RunningAsAdmin(DWORD pid)
	{
		HANDLE handle = OpenProcess(PROCESS_QUERY_INFORMATION, FALSE, pid);
		if (!handle)
			return false;

		HANDLE token = NULL;
		if (!OpenProcessToken(handle, TOKEN_QUERY, &token)) {
			CloseHandle(handle);
			return false;
		}

		if (!token) {
			CloseHandle(handle);
			return false;
		}

		BOOL elevated = FALSE;
		DWORD dwLength = 0;

		BOOL ret = GetTokenInformation(token, TokenElevation, &elevated, sizeof(elevated), &dwLength);

		CloseHandle(token);
		CloseHandle(handle);

		if (ret == FALSE)
			return false;

		return elevated == TRUE;
	}

	bool ProcessManager::ReadMemory(DWORD pid, DWORD64 address, PVOID buffer, DWORD64 size)
	{
		HANDLE handle = OpenProcess(PROCESS_VM_READ, FALSE, pid);
		if (!handle)
			return false;

	#ifdef _AMD64_
		SIZE_T dwLength = 0;
		int ret = ReadProcessMemory(handle, reinterpret_cast<LPCVOID>(address), buffer, SIZE_T(size), &dwLength);
	#else
		DWORD64 dwLength = 0;
		int ret = NT::ZwWow64ReadVirtualMemory64(handle, reinterpret_cast<PVOID64>(address), buffer, size, &dwLength) == 0;
	#endif

		bool success = true;

		if (!ret)
			success = false;

		if (dwLength != size)
			success = false;

		CloseHandle(handle);

		return success;
	}

	bool ProcessManager::WriteMemory(DWORD pid, DWORD64 address, PVOID buffer, DWORD64 size)
	{
		HANDLE handle = OpenProcess(PROCESS_VM_OPERATION | PROCESS_VM_WRITE, FALSE, pid);
		if (!handle)
			return false;

	#ifdef _AMD64_
		SIZE_T dwLength = 0;
		int ret = WriteProcessMemory(handle, reinterpret_cast<LPVOID>(address), buffer, SIZE_T(size), &dwLength);
	#else
		DWORD64 dwLength = 0;
		int ret = NT::ZwWow64WriteVirtualMemory64(handle, reinterpret_cast<PVOID64>(address), buffer, size, &dwLength) == 0;
	#endif

		bool success = true;

		if (!ret)
			success = false;

		if (dwLength != size)
			success = false;

		CloseHandle(handle);

		return success;
	}

	bool ProcessManager::Terminate(DWORD pid, int exitcode)
	{
		HANDLE handle = OpenProcess(PROCESS_TERMINATE | SYNCHRONIZE, FALSE, pid);
		if (!handle)
			return false;

		BOOL ret = TerminateProcess(handle, exitcode == -1 ? GetExitcode(pid) : exitcode);
		WaitForSingleObject(handle, 1000);
		CloseHandle(handle);

		return ret != 0;
	}

	bool ProcessManager::Suspend(DWORD pid)
	{
		HANDLE handle = OpenProcess(PROCESS_SUSPEND_RESUME, FALSE, pid);
		if (!handle)
			return false;

		NTSTATUS ret = NT::ZwSuspendProcess(handle);

		CloseHandle(handle);

		return ret == 0;
	}

	bool ProcessManager::Resume(DWORD pid)
	{
		HANDLE handle = OpenProcess(PROCESS_SUSPEND_RESUME, FALSE, pid);
		if (!handle)
			return false;

		NTSTATUS ret = NT::ZwResumeProcess(handle);

		CloseHandle(handle);

		return ret == 0;
	}

	bool ProcessManager::Exit(int exitcode)
	{
		ExitProcess(exitcode == -1 ? GetExitcode() : exitcode);
	}

	BOOL LoadSeDebugPrivilege(DWORD pid)
	{
		HANDLE hToken = NULL;
		HANDLE handle = NULL;
		LUID Luid = { NULL };
		TOKEN_PRIVILEGES Privileges = { NULL };
		BOOL Success = FALSE;

		if (pid == -1)
			pid = GetCurrentProcessId();

		__try {
			handle = OpenProcess(PROCESS_QUERY_INFORMATION, FALSE, pid);
			if (!handle)
				return false;

			if (!OpenProcessToken(handle, TOKEN_ADJUST_PRIVILEGES
				| TOKEN_QUERY, &hToken)) __leave;
			if (hToken == NULL) __leave;

			if (!LookupPrivilegeValue(NULL, SE_DEBUG_NAME, &Luid)) __leave;
			if (Luid.LowPart == NULL && Luid.HighPart == NULL) __leave;

			Privileges.PrivilegeCount = 1;
			Privileges.Privileges[0].Luid = Luid;
			Privileges.Privileges[0].Attributes = SE_PRIVILEGE_ENABLED;

			if (!AdjustTokenPrivileges(hToken, FALSE, &Privileges,
				sizeof (Privileges), NULL, NULL)) __leave;

			WaitForSingleObject(hToken, INFINITE);

			Success = TRUE;
		}
		__finally {
			if (hToken != NULL)
				CloseHandle(hToken);
			if (handle)
				CloseHandle(handle);
		}
		return Success;
	}

	bool ProcessManager::SetDebugPrivilege(DWORD pid)
	{
		//typedef INT(WINAPI *_RtlAdjustPrivilege)(ULONG Privilege, BOOLEAN Enable, BOOLEAN CurrentThread, PBOOLEAN Enabled);
		//_RtlAdjustPrivilege RtlAdjustPrivilege = (_RtlAdjustPrivilege)GetProcAddress(GetModuleHandleW(L"ntdll.dll"), "RtlAdjustPrivilege");

		//RtlAdjustPrivilege(20, 1, 0, 0);
		BOOL ret = LoadSeDebugPrivilege(pid);

		return ret == TRUE;
	}

	bool ProcessManager::RestartAsAdmin(DWORD pid)
	{
		if (pid == -1)
			pid = GetCurrentProcessId();

		if (RunningAsAdmin(pid))
			return false;

		GString path;
		GString directory;
		if (!GetPath(pid, path))
			return false;

		directory = path.substr(0, path.rfind('\\') - 1);

		if (!Terminate(pid)) {
			//...
		}

#ifdef _UNICODE
		SHELLEXECUTEINFOW sei = { 0 };
#else
		SHELLEXECUTEINFOA sei = { 0 };
#endif
		sei.cbSize = sizeof(sei);
		sei.hwnd = GetDesktopWindow();
		sei.lpVerb = _G("runas");
		sei.lpFile = path.c_str();
		sei.lpDirectory = directory.c_str();
		sei.nShow = SW_SHOW;

		return ShellExecuteEx(&sei) == TRUE;
	}

	bool ProcessManager::IsWow64(DWORD pid)
	{
		HANDLE handle = ::OpenProcess(PROCESS_QUERY_LIMITED_INFORMATION, FALSE, pid);
		if (!handle)
			return false;

		BOOL wow64Process = FALSE;;
		IsWow64Process(handle, &wow64Process);

		CloseHandle(handle);

		return wow64Process == TRUE;
	}

	bool ProcessManager::IsValid(DWORD pid)
	{
		/*if (pid == 0)
			return true;*/

		//TODO: improve functionality
		HANDLE handle = OpenProcess(PROCESS_QUERY_LIMITED_INFORMATION, FALSE, pid);

		if (!handle)
			return false;

		bool ret = true;
		if (GetNumHandles(pid) == 0)
			ret = false;

		CloseHandle(handle);

		return ret;
	}

	bool ProcessEnum::Add(Process *p)
	{
		for (ProcessListIterator it = list.begin(); it != list.end(); it++)
			if (p->ProcessId() == (*it)->ProcessId())
				return false;

		if (!ProcessManager::IsValid(p->ProcessId()))
			return false;

		list.push_back(new Process(p->ProcessId(), p->Name(), p->NumThreads()));

		return true;
	}

	ProcessList ProcessEnum::Added()
	{
		ProcessList buf = ProcessManager::Enumerate();
		ProcessList added;

		for (ProcessListIterator it = buf.begin(); it != buf.end(); it++)
			if (Add(*it))
				added.push_back(new Process((*it)->ProcessId(), (*it)->Name(), (*it)->NumThreads()));

		ProcessManager::FreeList(buf);

		return added;
	}

	ProcessList ProcessEnum::Removed()
	{
		ProcessList removed;

		for (ProcessListIterator it = list.begin(); it != list.end(); it++) {
			if (!ProcessManager::IsValid((*it)->ProcessId())) {
				removed.push_back(new Process((*it)->ProcessId(), (*it)->Name(), (*it)->NumThreads()));
				delete *it;
				it = list.erase(it);
			}
		}

		return removed;
	}

	ProcessList ProcessEnum::Updated()
	{
		ProcessList added = Added();
		ProcessList removed = Removed();
		ProcessList updated;

		for (ProcessListIterator it = added.begin(); it != added.end(); it++)
			updated.push_back(new Process((*it)->ProcessId(), (*it)->Name(), (*it)->NumThreads()));

		for (ProcessListIterator it = removed.begin(); it != removed.end(); it++)
			updated.push_back(new Process((*it)->ProcessId(), (*it)->Name(), (*it)->NumThreads()));

		ProcessManager::FreeList(added);
		ProcessManager::FreeList(removed);

		return updated;
	}

	bool ProcessEnum::HasUpdated(const ProcessList &updated)
	{
		return updated.size() > 0;
	}

	ProcessList &ProcessEnum::List()
	{
		return list;
	}

	void ProcessEnum::Clear()
	{
		ProcessManager::FreeList(list);
	}

	ProcessCPU::ProcessCPU()
	{
		self = NULL;
		process_id = 0;
		num_processors = 0;
		usage = 0;
		initialized = false;

		memset(&last_cpu, 0, sizeof(last_cpu));
		memset(&last_kernel_cpu, 0, sizeof(last_kernel_cpu));
		memset(&last_user_cpu, 0, sizeof(last_user_cpu));
	}

	ProcessCPU::~ProcessCPU()
	{
		Cleanup();
	}

	void ProcessCPU::Init(DWORD pid)
	{
		if (initialized)
			return;

		process_id = pid;

		self = OpenProcess(PROCESS_QUERY_LIMITED_INFORMATION, false, process_id);
		if (!self)
			return;

		SYSTEM_INFO sysInfo;
		GetSystemInfo(&sysInfo);
		num_processors = sysInfo.dwNumberOfProcessors;

		if (num_processors == 0) {
			CloseHandle(self);
			return;
		}

		FILETIME fttime, ftkernel, ftuser;

		GetSystemTimeAsFileTime(&fttime);
		memcpy(&last_cpu, &fttime, sizeof(last_cpu));

		GetProcessTimes(self, &fttime, &fttime, &ftkernel, &ftuser);
		memcpy(&last_kernel_cpu, &ftkernel, sizeof(last_kernel_cpu));
		memcpy(&last_user_cpu, &ftuser, sizeof(last_user_cpu));

		timer.start();
		initialized = true;
	}

	bool ProcessCPU::Initialized()
	{
		return initialized;
	}

	float ProcessCPU::GetUsage()
	{
		if (!initialized || !self)
			return 0;

		//if (timer.ms() < 500)
		//	return usage;

		FILETIME fttime, ftkernel, ftuser;
		ULARGE_INTEGER now_, kernel, user;

		GetSystemTimeAsFileTime(&fttime);
		memcpy(&now_, &fttime, sizeof(FILETIME));

		GetProcessTimes(self, &fttime, &fttime, &ftkernel, &ftuser);
		timer.reset();

		memcpy(&kernel, &ftkernel, sizeof(FILETIME));
		memcpy(&user, &ftuser, sizeof(FILETIME));

		double percent = double(kernel.QuadPart - last_kernel_cpu.QuadPart) + double(user.QuadPart - last_user_cpu.QuadPart);
		percent /= (now_.QuadPart - last_cpu.QuadPart);
		percent /= num_processors;

		last_cpu = now_;
		last_kernel_cpu = kernel;
		last_user_cpu = user;

		usage = (float)percent * 100;

		if (!isnormal(usage))
			usage = 0;

		return usage;
	}

	void ProcessCPU::Cleanup()
	{
		if (self)
			CloseHandle(self);

		self = NULL;
	}

	DWORD GetAccountSid(DWORD pid)
	{
		HANDLE handle = OpenProcess(PROCESS_QUERY_INFORMATION, FALSE, pid);
		if (!handle || handle == INVALID_HANDLE_VALUE)
			return NULL;

		HANDLE token = NULL;
		if (!OpenProcessToken(handle, TOKEN_QUERY, &token)) {
			CloseHandle(handle);
			return NULL;
		}

		if (!token || token == INVALID_HANDLE_VALUE) {
			CloseHandle(handle);
			return NULL;
		}

		//DWORD dwLength = 0;
		//if (GetTokenInformation(token, TokenUser, 0, 0, &dwLength)) {
		//	CloseHandle(token);
		//	CloseHandle(handle);
		//	return NULL;
		//}

		//if (GetLastError() != ERROR_INSUFFICIENT_BUFFER) {
		//	CloseHandle(token);
		//	CloseHandle(handle);
		//	return NULL;
		//}

		//BYTE *test = new BYTE[dwLength];
		//PTOKEN_USER ptu = new TOKEN_USER;
		//memset(test, 0, dwLength);
		//if (!GetTokenInformation(token, TokenUser, (void*)test, dwLength, &dwLength)) {
		//	CloseHandle(token);
		//	CloseHandle(handle);
		//	return NULL;
		//}

		////return 0;

		//TOKEN_USER* lol = (TOKEN_USER*)test;

		//PSID sid = lol->User.Sid;

		//CloseHandle(token);
		//CloseHandle(handle);

		//delete ptu;

		//printf("alles ok %X\n", sid);
		//return sid;

		TOKEN_USER ptu = { 0 };
		DWORD nlen = 300;
		if (!GetTokenInformation(token, (TOKEN_INFORMATION_CLASS)1, (void*)&ptu, 300, &nlen)) {

			printf("Error: %d\n", GetLastError());
			return false;
		}
		printf("ee%X\n", 1);
		printf("SID=%X", (DWORD)ptu.User.Sid);
		return (DWORD)ptu.User.Sid;
	}

	bool GetProcessUsername(HANDLE hProcess, GString &user, GString &domain)
	{
		TOKEN_USER ptu[20] = { 0 };
		GChar name[300] = _G(""), dom[300] = _G("");

		HANDLE tok = 0;
		if (!OpenProcessToken(hProcess, TOKEN_QUERY, &tok))
			return false;

		if (!tok)
			return false;

		DWORD nlen = 300;
		if (!GetTokenInformation(tok, (TOKEN_INFORMATION_CLASS)1, &ptu[0], 300, &nlen)) {
			CloseHandle(tok);
			return false;
		}

		DWORD dlen = 300;
		int iUse;
		if (!LookupAccountSid(0, ptu[0].User.Sid, name, &nlen, dom, &dlen, (PSID_NAME_USE)&iUse)) {
			CloseHandle(tok);
			return false;
		}

		user = name;
		domain = dom;

		CloseHandle(tok);

		return user.length() != 0;
	}

}