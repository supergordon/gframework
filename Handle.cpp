#include "Handle.h"
#include "Process.h"
#include "NT.h"

namespace GF {

	Handle::Handle(USHORT Handle, DWORD Process_id, int Type, void *Object, DWORD Access)
	{
		value.clear();
		handle = Handle;
		pid = Process_id;
		type = Type;
		object = Object;
		access = Access;
	}

	USHORT Handle::Handle_()
	{
		return handle;
	}

	DWORD Handle::Access()
	{
		return access;
	}

	DWORD Handle::ProcessId()
	{
		return pid;
	}

	int Handle::Type()
	{
		return type;
	}

	void *Handle::Object()
	{
		return object;
	}

	GString Handle::Typename()
	{
		GString type_name[] = { _G(""), _G(""), _G("Type"), _G("Directory"), _G("SymbolicLink"), _G("Token"), _G("Job"), _G("Process"), _G("Thread"), _G("UserApcReserve"), _G("IoCompletionReserve"), _G("DebugObject"), _G("Event"), _G("EventPair"), _G("Mutant"), _G("Callback"), _G("Semaphore"),
			_G("Timer"), _G("Profile"), _G("KeyedEvent"), _G("WindowStation"), _G("Desktop"), _G("TpWorkerFactory"), _G("Adapter"), _G("Controller"), _G("Device"), _G("Driver"), _G("IoCompletion"), _G("File"), _G("TmTm"), _G("TmTx"), _G("TmRm"), _G("TmEn"),
			_G("Section"), _G("Session"), _G("Key"), _G("ALPC Port"), _G("PowerRequest"), _G("WmiGuid"), _G("EtwRegistration"), _G("EtwConsumer"),
			_G("FilterConnectionPort"), _G("FilterCommunicationPort"), _G("PcwObject")
		};

		if (type >= 0 && type <= 43)
			return GString(type_name[type]);
		else
			return GEmptyString;
	}

	GString Handle::Value()
	{
		HANDLE cur_handle = OpenProcess(PROCESS_DUP_HANDLE, false, pid);
		if (!cur_handle)
			return GEmptyString;

		HANDLE dup_handle = 0;

		if (type == 7) { //process 
			if (!DuplicateHandle(cur_handle, (HANDLE)handle, GetCurrentProcess(),
				&dup_handle, PROCESS_QUERY_LIMITED_INFORMATION, FALSE, 0)) {
				CloseHandle(cur_handle);
				return GEmptyString;
			}

			DWORD target_pid = GetProcessId(dup_handle);
			ProcessManager::GetName(target_pid, value);

			if (value.length() == 0)
				value = _G("Non-existent process");
		}
		else if (type == 28) { //file
			if (!DuplicateHandle(cur_handle, (HANDLE)handle, GetCurrentProcess(),
				&dup_handle, 0, FALSE, DUPLICATE_SAME_ACCESS)) {
				CloseHandle(cur_handle);
				return GEmptyString;
			}

			//if (WaitForSingleObject(cur_handle, 1000) == WAIT_TIMEOUT)
			//	return GEmptyString;

			//PVOID objectNameInfo = malloc(0x10000);
			//DWORD dwRet = 0;
			//NTSTATUS status = NT::ZwQueryObject(dup_handle, NT::ObjectNameInformation, objectNameInfo, 0x1000, &dwRet);
			//if (!NT_SUCCESS(status))
			//	return GEmptyString;

			//NT::PUNICODE_STRING ss = (NT::PUNICODE_STRING)objectNameInfo;
			////printf("%ls\n", ss->Buffer);
			//value.assign(ss->Buffer, ss->Length);
			//free(objectNameInfo);

			//if (value.length() == 0) {
			//GChar path[1024] = { 0 };
			//DWORD ret = GetFinalPathNameByHandle(dup_handle, path, sizeof(path), VOLUME_NAME_NT);
			//if (ret)
			//	value = path;

			//	if (value.length() == 0) {
			//		PFILE_NAME_INFO fni = (PFILE_NAME_INFO)malloc(sizeof(PFILE_NAME_INFO)+sizeof(wchar_t)*MAX_PATH);
			//		BOOL ret = GetFileInformationByHandleEx(dup_handle, FileNameInfo, fni, sizeof(PFILE_NAME_INFO)+sizeof(wchar_t)*MAX_PATH);
			//		value.assign(&fni->FileName[0], wcslen(fni->FileName));
			//		//value = fni->FileName;
			//		free(fni);
			//	}
			//}
		}
		else {
			if (!DuplicateHandle(cur_handle, (HANDLE)handle, GetCurrentProcess(),
				&dup_handle, 0, FALSE, DUPLICATE_SAME_ACCESS)) {
				CloseHandle(cur_handle);
				return GEmptyString;
			}

			PVOID objectNameInfo = malloc(0x10000);
			DWORD dwRet = 0;
			NTSTATUS status = NT::ZwQueryObject(dup_handle, NT::ObjectNameInformation, objectNameInfo, 0x1000, &dwRet);
			if (status != 0) {
				CloseHandle(cur_handle);
				return GEmptyString;
			}

			NT::PUNICODE_STRING stringValue = (NT::PUNICODE_STRING)objectNameInfo;

#ifdef _UNICODE
			value.assign(stringValue->Buffer, stringValue->Length);
#else
			value = UnicodeToMultibyte(stringValue->Buffer, stringValue->Length);
#endif
			free(objectNameInfo);
		}

		if (dup_handle)
			CloseHandle(dup_handle);

		CloseHandle(cur_handle);

		return value;
	}

	HandleList HandleManager::Enumerate(DWORD pid)
	{
		HandleList buf;

		NTSTATUS status = 0;
		ULONG handleInfoSize = 0x100000;
		GPSYSTEM_HANDLE_INFORMATION handleInfo = (GPSYSTEM_HANDLE_INFORMATION)malloc(handleInfoSize);

		while ((status = NT::ZwQuerySystemInformation(NT::SystemHandleInformation, handleInfo, handleInfoSize, NULL)) == STATUS_INFO_LENGTH_MISMATCH)
			handleInfo = (GPSYSTEM_HANDLE_INFORMATION)realloc(handleInfo, handleInfoSize *= 2);

		if (!NT_SUCCESS(status)) {
			free(handleInfo);
			return buf;
		}

		for (ULONG i = 0; i < handleInfo->HandleCount; i++) {
			GSYSTEM_HANDLE &handle = handleInfo->Handles[i];

			if (pid != -1 && pid != handle.ProcessId)
				continue;

			//10? 4? 29? 31? 34=session? 37? 38? 39? 40? 41? 42? 43?
			if (handle.ObjectTypeNumber != 7 &&
				handle.ObjectTypeNumber != 3 &&
				handle.ObjectTypeNumber != 35 &&
				handle.ObjectTypeNumber != 6 &&
				handle.ObjectTypeNumber != 5 &&
				handle.ObjectTypeNumber != 17 &&
				handle.ObjectTypeNumber != 36)
				continue;

			buf.push_back(new Handle(handle.Handle, handle.ProcessId, handle.ObjectTypeNumber, handle.Object, handle.GrantedAccess));
		}

		free(handleInfo);

		return buf;
	}

	int HandleManager::FreeList(HandleList &hl)
	{
		int num_destroyed = 0;

		for (HandleListIterator it = hl.begin(); it != hl.end(); it++, num_destroyed++)
			delete *it;

		hl.clear();

		return num_destroyed;
	}

	HandleList HandleManager::GetByProcessHandle(USHORT handle, HandleList *hl)
	{
		HandleList buf;
		HandleList list = hl ? *hl : HandleManager::Enumerate();

		for (HandleListIterator it = list.begin(); it != list.end(); it++) {
			if (handle != (*it)->Handle_())
				continue;

			buf.push_back(new Handle((*it)->Handle_(), (*it)->ProcessId(), (*it)->Type(), (*it)->Object(), (*it)->Access()));
		}

		if (!hl)
			HandleManager::FreeList(list);

		return buf;
	}

	HandleList HandleManager::GetByProcessId(DWORD pid, HandleList *hl)
	{
		HandleList buf;
		HandleList list = hl ? *hl : HandleManager::Enumerate(pid);

		for (HandleListIterator it = list.begin(); it != list.end(); it++) {
			if ((*it)->ProcessId() != pid)
				continue;

			buf.push_back(new Handle((*it)->Handle_(), (*it)->ProcessId(), (*it)->Type(), (*it)->Object(), (*it)->Access()));
		}

		if (!hl)
			HandleManager::FreeList(list);

		return buf;
	}

	HandleList HandleManager::GetByHandleProcessId(USHORT handle, DWORD pid, HandleList *hl)
	{
		HandleList buf;
		HandleList list = hl ? *hl : HandleManager::Enumerate(pid);

		for (HandleListIterator it = list.begin(); it != list.end(); it++) {
			if (pid != (*it)->ProcessId() && handle != (*it)->Handle_())
				continue;

			buf.push_back(new Handle((*it)->Handle_(), (*it)->ProcessId(), (*it)->Type(), (*it)->Object(), (*it)->Access()));
		}

		if (!hl)
			HandleManager::FreeList(list);

		return buf;
	}

	DWORD HandleManager::GetHandleCount()
	{
		NTSTATUS status = 0;
		ULONG handleInfoSize = 0x20000;
		GF::GPSYSTEM_HANDLE_INFORMATION handleInfo = (GPSYSTEM_HANDLE_INFORMATION)malloc(handleInfoSize);

		while ((status = NT::ZwQuerySystemInformation(NT::SystemHandleInformation, handleInfo, handleInfoSize, NULL)) == STATUS_INFO_LENGTH_MISMATCH)
			handleInfo = (GPSYSTEM_HANDLE_INFORMATION)realloc(handleInfo, handleInfoSize *= 2);

		DWORD count = status == 0 ? handleInfo->HandleCount : 0;
		free(handleInfo);

		return count;
	}

	bool HandleManager::IsValid(USHORT Handle, DWORD process_id)
	{
		UNREFERENCED_PARAMETER(Handle);
		UNREFERENCED_PARAMETER(process_id);
		//HANDLE cur_handle = OpenProcess(PROCESS_DUP_HANDLE, false, process_id);
		//if (!cur_handle)
		//	return false;

		//HANDLE dup_handle = 0;

		//if (!DuplicateHandle(cur_handle, (HANDLE)Handle, GetCurrentProcess(),
		//	&dup_handle, PROCESS_QUERY_LIMITED_INFORMATION, FALSE, 0)) {
		//	CloseHandle(cur_handle);
		//	return false;
		//}

		//CloseHandle(dup_handle);
		//CloseHandle(cur_handle);

		return true;
	}

	HandleEnum::HandleEnum(DWORD process_id)
	{
		pid = process_id;
	}

	bool HandleEnum::Add(Handle *h)
	{
		for (HandleListIterator it = list.begin(); it != list.end(); it++)
			if (h->Handle_() == (*it)->Handle_() && h->ProcessId() == (*it)->ProcessId())
				return false;

		if (!HandleManager::IsValid(h->Handle_(), h->ProcessId()))
			return false;

		list.push_back(new Handle(h->Handle_(), h->ProcessId(), h->Type(), h->Object(), h->Access()));

		return true;
	}

	HandleList HandleEnum::Added()
	{
		HandleList buf = HandleManager::Enumerate(pid);
		HandleList added;

		for (HandleListIterator it = buf.begin(); it != buf.end(); it++) {
			if (Add(*it))
				added.push_back(new Handle((*it)->Handle_(), (*it)->ProcessId(), (*it)->Type(), (*it)->Object(), (*it)->Access()));
		}

		HandleManager::FreeList(buf);

		return added;
	}

	HandleList HandleEnum::Removed()
	{
		HandleList removed;

		for (HandleListIterator it = list.begin(); it != list.end(); it++) {

			if (!HandleManager::IsValid((*it)->Handle_(), (*it)->ProcessId())) {
				removed.push_back(new Handle((*it)->Handle_(), (*it)->ProcessId(), (*it)->Type(), (*it)->Object(), (*it)->Access()));
				delete *it;
				it = list.erase(it);
			}
		}

		return removed;
	}

	HandleList HandleEnum::Updated()
	{
		HandleList added = Added();
		HandleList removed = Removed();
		HandleList updated;

		for (HandleListIterator it = added.begin(); it != added.end(); it++)
			updated.push_back(new Handle((*it)->Handle_(), (*it)->ProcessId(), (*it)->Type(), (*it)->Object(), (*it)->Access()));

		for (HandleListIterator it = removed.begin(); it != removed.end(); it++)
			updated.push_back(new Handle((*it)->Handle_(), (*it)->ProcessId(), (*it)->Type(), (*it)->Object(), (*it)->Access()));

		HandleManager::FreeList(added);
		HandleManager::FreeList(removed);

		return updated;
	}

	bool HandleEnum::HasUpdated(const HandleList &updated)
	{
		return updated.size() > 0;
	}

	HandleList &HandleEnum::List()
	{
		return list;
	}

	void HandleEnum::Clear()
	{
		HandleManager::FreeList(list);
	}

	DWORD HandleEnum::Pid()
	{
		return pid;
	}

}