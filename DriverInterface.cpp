#include "DriverInterface.h"
#include "Service.h"

namespace GF {

	DriverInterface::DriverInterface()
	{
		file = INVALID_HANDLE_VALUE;
	}

	DriverInterface::~DriverInterface()
	{
		Close();
	}

	bool DriverInterface::Register()
	{
		return ServiceManager::RegisterService_(GDRIVER_NAME, GDRIVER_FILEPATH);
	}

	bool DriverInterface::Unregister()
	{
		return ServiceManager::UnregisterService_(GDRIVER_NAME);
	}

	bool DriverInterface::Start()
	{
		return ServiceManager::StartService_(GDRIVER_NAME);
	}

	bool DriverInterface::Stop()
	{
		return ServiceManager::StopService_(GDRIVER_NAME);
	}

	HANDLE DriverInterface::Open()
	{
		Close();

		file = CreateFile(GDRIVER_FILENAME, GENERIC_READ | GENERIC_WRITE, FILE_SHARE_READ |
			FILE_SHARE_WRITE, 0, OPEN_EXISTING, FILE_FLAG_OVERLAPPED, 0);

		return file;
	}

	void DriverInterface::Close()
	{
		if (file != INVALID_HANDLE_VALUE)
			CloseHandle(file);
	}

	bool DriverInterface::TerminateProcess(DWORD process_id)
	{
		if (file == INVALID_HANDLE_VALUE)
			return false;

		DWORD dwLength = 0;
		KILLPROCESS killProcess = { 0 };

		killProcess.pid[0] = process_id;
		killProcess.num = 1;

		Open();
		BOOL ret = DeviceIoControl(file, IOCTL_GDRIVER_TERMINATEPROCESS, &killProcess, sizeof(KILLPROCESS), &killProcess, sizeof(KILLPROCESS), &dwLength, 0);
		Close();

		if (!ret)
			return false;

		return killProcess.status[0] == 0;
	}

	bool DriverInterface::HideProcess(DWORD process_id)
	{
		if (file == INVALID_HANDLE_VALUE)
			return false;

		DWORD dwLength = 0;
		HIDEPROCESS hideProcess = { 0 };

		hideProcess.pid[0] = process_id;
		hideProcess.num = 1;

		Open();
		BOOL ret = DeviceIoControl(file, IOCTL_GDRIVER_HIDEPROCESS, &hideProcess, sizeof(HIDEPROCESS), &hideProcess, sizeof(HIDEPROCESS), &dwLength, 0);
		Close();

		return ret != 0;
	}

	bool DriverInterface::ProtectProcess(DWORD process_id)
	{
		if (file == INVALID_HANDLE_VALUE)
			return false;

		DWORD dwLength = 0;
		PROTECTPROCESS protectProcess = { 0 };

		protectProcess.pid[0] = process_id;
		protectProcess.num = 1;

		Open();
		BOOL ret = DeviceIoControl(file, IOCTL_GDRIVER_PROTECTPROCESS, &protectProcess, sizeof(PROTECTPROCESS), &protectProcess, sizeof(PROTECTPROCESS), &dwLength, 0);
		Close();

		return ret != 0;
	}

	GString DriverInterface::GetHandleValue(DEVICE_CONTROL_DATA dcd)
	{
		DWORD returnLength = 0;
		FILE_INFO fi = { 0 };

		Open();
		BOOL ret = DeviceIoControl(file, IOCTL_GDRIVER_OPENFILES, &dcd, sizeof(DEVICE_CONTROL_DATA), &fi, sizeof(FILE_INFO), &returnLength, NULL);
		Close();

		if (!ret)
			return GEmptyString;

#ifdef _UNICODE
		return GString(fi.filename);
#else
		return UnicodeToMultibyte(fi.filename, wcslen(fi.filename));
#endif
	}

}