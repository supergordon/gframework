#ifndef _GWINDOW_
#define _GWINDOW_

#include <windows.h>
#include <psapi.h>
#include <tlhelp32.h>
#include <shlobj.h>
#include <commoncontrols.h>
#include <list>
#include "GString.h"

#pragma comment(lib, "psapi.lib")

#define MAX_WINDOWNAME_LEN 256

namespace GF {

	class Window {
	public:
		Window(HWND h);
		HWND Handle();
		DWORD ProcessId();

		HICON Icon();
		bool Wow64();

		bool Terminate();
		bool Minimize();
		bool Maximize();
		bool Restore();
		bool Destroy();
		bool Hung();

		bool Visible();
		bool Minimized();
		bool Maximized();

		const GString &Name();
		const GString &Path();
		const GString &Title();
		const GString &Class();

		POINT *Position();
		SIZE *Size();

		LONG Id();
		HWND Parent();
		LONG_PTR Style();
		LONG_PTR ExtendedStyle();
		HINSTANCE Instance();
	private:
		HWND hwnd;
		GString name;
		GString path;
		GString title;
		GString class_;
		POINT point;
		SIZE size;
	};

	typedef std::list<Window*> WindowList;
	typedef std::list<Window*>::iterator WindowListIterator;

	class WindowManager {
	public:
		static WindowList Enumerate();
		static int FreeList(WindowList &wl);

		static Window *GetByProcessId(DWORD pid, WindowList *wl = NULL, bool copy = true);
		static Window *GetByHwnd(HWND hwnd);
		static Window *GetByWindowName(const GString &window_name);
		static Window *GetByClassName(const GString &class_name);

		static HWND GetWindow(const GString &windowname);
		static HWND GetWindowClass(const GString &classname);
		static DWORD GetProcessId(HWND hwnd);
		static DWORD GetMainThreadId(HWND hwnd);
		static bool GetProcessName(HWND hwnd, GString &procname);
		static bool GetWindowName(HWND hwnd, GString &title);
		static bool GetWindowClassName(HWND hwnd, GString &classname);
		static bool GetPath(HWND hwnd, GString &path);
		static HICON GetAppIcon(HWND hwnd, int type = 0);
		static bool GetPosition(HWND hwnd, POINT &pt);
		static bool GetSize(HWND hwnd, SIZE &size);
		static LONG_PTR GetStyle(HWND hwnd);
		static LONG_PTR GetExtendedStyle(HWND hwnd);
		static HINSTANCE GetInstance(HWND hwnd);
		static LONG GetId(HWND hwnd);
		static HWND GetParent(HWND hwnd);
		static bool IsWow64(HWND hwnd);
		static bool OpenPropertyDialog(const GString &path);
		static bool OpenInExplorer(const GString &path);

		static bool TerminateProcess(HWND hwnd);
		static bool MinimizeWindow(HWND hwnd);
		static bool MaximizeWindow(HWND hwnd);
		static bool RestoreWindow(HWND hwnd);
		static bool DestroyWindow(HWND hwnd);
		static bool IsHungWindow(HWND hwnd);

		static bool IsMinimized(HWND hwnd);
		static bool IsMaximized(HWND hwnd);
		static bool IsVisible(HWND hwnd);
		static bool IsValid(HWND hwnd);
	private:
		static BOOL CALLBACK EnumWindowsProc(HWND hwnd, LPARAM lParam);
		static bool AddEntry(HWND hwnd);
	};

	class WindowEnum {
	public:
		bool Add(Window *w);
		WindowList Added();
		WindowList Removed();
		WindowList Updated();
		bool HasUpdated(const WindowList &updated);
		WindowList &List();
		void Clear();
	private:
		WindowList list;
	};

}

#undef GetWindowText
#ifdef _UNICODE
#define GetWindowText GetWindowTextW
#else
#define GetWindowText GetWindowTextA
#endif

#undef GetClassName
#ifdef _UNICODE
#define GetClassName GetClassNameW
#else
#define GetClassName GetClassNameA
#endif

#undef FindWindow
#ifdef _UNICODE
#define FindWindow FindWindowW
#else
#define FindWindow FindWindowA
#endif

#undef GetModuleFileNameEx
#ifdef _UNICODE
#define GetModuleFileNameEx GetModuleFileNameExW
#else
#define GetModuleFileNameEx GetModuleFileNameExA
#endif

#undef QueryFullProcessImageName
#ifdef _UNICODE
#define QueryFullProcessImageName QueryFullProcessImageNameW
#else
#define QueryFullProcessImageName QueryFullProcessImageNameA
#endif

#undef SHGetFileInfo
#ifdef _UNICODE
#define SHGetFileInfo SHGetFileInfoW
#else
#define SHGetFileInfo SHGetFileInfoA
#endif

#undef ExtractIconEx
#ifdef _UNICODE
#define ExtractIconEx ExtractIconExW
#else
#define ExtractIconEx ExtractIconExA
#endif

#undef ShellExecuteEx
#ifdef _UNICODE
#define ShellExecuteEx ShellExecuteExW
#else
#define ShellExecuteEx ShellExecuteExA
#endif

#undef ILCreateFromPath
#ifdef _UNICODE
#define ILCreateFromPath ILCreateFromPathW
#else
#define ILCreateFromPath ILCreateFromPathA
#endif

#endif