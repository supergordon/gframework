#ifndef __GDRIVERINTERFACE__
#define __GDRIVERINTERFACE__

#include <windows.h>
#include <list>
#include "GString.h"

namespace GF {

#define GDRIVER_NAME _G("GDriver")
#define GDRIVER_FILENAME _G("\\\\.\\GDriver")
#define GDRIVER_FILEPATH _G("D:\\GDriver\\x64\\Win7Release\\GDriver.sys")

#define IOCTL_GDRIVER_OPENFILES CTL_CODE(FILE_DEVICE_UNKNOWN, 0x800, METHOD_BUFFERED, FILE_READ_DATA | FILE_WRITE_DATA)
#define IOCTL_GDRIVER_TERMINATEPROCESS CTL_CODE(FILE_DEVICE_UNKNOWN, 0x801, METHOD_BUFFERED, FILE_READ_DATA | FILE_WRITE_DATA)
#define IOCTL_GDRIVER_HIDEPROCESS CTL_CODE(FILE_DEVICE_UNKNOWN, 0x802, METHOD_BUFFERED, FILE_READ_DATA | FILE_WRITE_DATA)
#define IOCTL_GDRIVER_PROTECTPROCESS CTL_CODE(FILE_DEVICE_UNKNOWN, 0x803, METHOD_BUFFERED, FILE_READ_DATA | FILE_WRITE_DATA)

	typedef std::list<GString> GDriverHandleList;
	typedef std::list<GString>::iterator GDriverHandleListIterator;

	typedef struct _DEVICE_CONTROL_DATA {
		PVOID64 address;
		LONG pid;
		LONG type;
		USHORT handle;
		wchar_t pad[260];
	} DEVICE_CONTROL_DATA;

	typedef struct _FILE_INFO {
		wchar_t filename[260];
		ULONG uType;
	} FILE_INFO;

	typedef struct _KILLPROCESS {
		ULONG pid[256];
		ULONG num;
		UCHAR status[256]; //tell usermode app the status of termination
	} KILLPROCESS, *PKILLPROCESS;

	typedef struct _HIDEPROCESS {
		ULONG pid[256];
		ULONG num;
	} HIDEPROCESS, *PHIDEPROCESS;

	typedef struct _PROTECTPROCESS {
		ULONG pid[256];
		ULONG num;
	} PROTECTPROCESS, *PPROTECTPROCESS;

	class DriverInterface {
	public:
		DriverInterface();
		~DriverInterface();
		bool Register();
		bool Unregister();
		bool Start();
		bool Stop();

		bool TerminateProcess(DWORD process_id);
		bool HideProcess(DWORD process_id);
		bool ProtectProcess(DWORD process_id);
		GString GetHandleValue(DEVICE_CONTROL_DATA dcd);
	private:
		HANDLE Open();
		void Close();
		HANDLE file;
	};

}

#endif