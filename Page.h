#ifndef __GPAGE__
#define __GPAGE__

#include <list>
#include <windows.h>
#include <tlhelp32.h>
#include "GString.h"

namespace GF {

	class Page {
	public:
		Page(DWORD64 BaseAddress, DWORD64 AllocationBase, DWORD AllocationProtect,
			DWORD64 RegionSize, DWORD state, DWORD protect, DWORD type);

		DWORD64 Size();
		DWORD64 Address();
		DWORD64 AllocationBase();
		DWORD Type();
		DWORD State();
		DWORD Protect();
		DWORD AllocationProtect();

		bool MemCommit();
		bool MemFree();
		bool MemReserve();

		bool MemImage();
		bool MemMapped();
		bool MemPrivate();

		bool PageExecute();
		bool PageExecuteRead();
		bool PageExecuteReadWrite();
		bool PageExecuteWriteCopy();
		bool PageNoAccess();
		bool PageReadOnly();
		bool PageReadWrite();
		bool PageWriteCopy();

		bool PageGuard();
		bool PageNoCache();
		bool PageWriteCombine();

	private:
		DWORD64 size;
		DWORD64 base_address;
		DWORD64 allocation_base;
		DWORD type;
		DWORD state;
		DWORD protect;
		DWORD allocation_protect;
	};

	typedef std::list<Page*> PageList;
	typedef std::list<Page*>::iterator PageListIterator;

	class PageManager {
	public:
		static PageList Enumerate(DWORD process_id);
		static int FreeList(PageList &pl);

		static Page *GetByAddress(DWORD process_id, DWORD64 address);


		static bool IsValid(DWORD process_id, DWORD64 address);
	private:
	};

}

#endif