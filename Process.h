#ifndef __GPROCESS__
#define __GPROCESS__

#include <list>
#include <string>
#include <windows.h>
#include <psapi.h>
#include <tlhelp32.h>
#include "GString.h"
#include "HighResolutionTimer.h"

#pragma comment(lib, "version.lib")

namespace GF {

	class ProcessCPU;

	typedef struct _GPROCESS_MEMORY_COUNTERS_EX {
		DWORD cb;
		DWORD PageFaultCount;
		SIZE_T PeakWorkingSetSize;
		SIZE_T WorkingSetSize;
		SIZE_T QuotaPeakPagedPoolUsage;
		SIZE_T QuotaPagedPoolUsage;
		SIZE_T QuotaPeakNonPagedPoolUsage;
		SIZE_T QuotaNonPagedPoolUsage;
		SIZE_T PagefileUsage;
		SIZE_T PeakPagefileUsage;
		SIZE_T PrivateUsage;
	} GPROCESS_MEMORY_COUNTERS_EX;

	class Process {
	public:
		Process();
		~Process();
		Process(DWORD process_id, const GString &process_name = GEmptyString, DWORD threads = 0);

		DWORD ProcessId();

		const GString &Name();
		const GString &User();
		const GString &Description(const GString &get_desc = _G("ProductName"), int index = 0);
		const GString &Path();
		const GString &CommandLine();
		HICON Icon(int type = 0/*GICON_SMALL*/);
		bool Wow64();
		bool Admin();

		float UsedCPU();
		DWORD UsedMemory();
		DWORD NumThreads();
		DWORD NumHandles();
		DWORD64 Peb32();
		DWORD64 Peb64();

		bool Read(DWORD64 address, PVOID buffer, DWORD64 size);
		bool Write(DWORD64 address, PVOID buffer, DWORD64 size);
		bool Terminate(int exitcode = -1);
		bool Suspend();
		bool Resume();
		HANDLE Open(DWORD dwDesiredAccess = PROCESS_ALL_ACCESS);
		void Close();
	private:
		HANDLE handle;
		HICON icon;
		int wow64;
		DWORD pid;
		DWORD num_threads;
		GString name;
		GString user;
		GString desc[32];
		GString path;
		GString cmdline;
		ProcessCPU *cpu;
		DWORD64 peb_address32;
		DWORD64 peb_address64;
	};

	typedef std::list<Process*> ProcessList;
	typedef std::list<Process*>::iterator ProcessListIterator;

	class ProcessManager {
	public:
		enum {
			GICON_SMALL,
			GICON_LARGE,
			GICON_EXTRALARGE,
			GICON_JUMBO
		};

		static ProcessList Enumerate();
		static int FreeList(ProcessList &pl);

		static Process *GetByName(const GString &name, ProcessList *pl = NULL, bool copy = true);
		static Process *GetByProcessId(DWORD pid, ProcessList *pl = NULL, bool copy = true);
		static DWORD GetProcessId(const GString &process_name);
		static DWORD GetCurrentProcessId();
		static DWORD GetMainThreadId(HWND hwnd);
		static bool GetName(DWORD pid, GString &name);
		static bool GetUser(DWORD pid, GString &user);
		static bool GetDomain(DWORD pid, GString &domain);

		/*Comments InternalName ProductName CompanyName	LegalCopyright ProductVersion FileDescription LegalTrademarks PrivateBuild FileVersion OriginalFilename SpecialBuild*/
		static bool GetDescription(DWORD pid, GString &out, const GString &desc = _G("ProductName"));
		static bool GetMemoryUsage(DWORD pid, GPROCESS_MEMORY_COUNTERS_EX &pmc);
		static bool GetPath(DWORD pid, GString &path);
		static HICON GetAppIcon(DWORD pid, int type = GICON_SMALL);
		static HICON GetAppIcon(const GString &path, int type = GICON_SMALL);
		static DWORD GetExitcode(DWORD pid = 0);
		static DWORD GetNumProcesses();
		static DWORD GetNumThreads(DWORD pid = -1);
		static DWORD GetNumHandles(DWORD pid);
		static DWORD64 GetPeb32(DWORD pid);
		static DWORD64 GetPeb64(DWORD pid);
		static bool GetCommandLine(DWORD pid, GString &cmdline);
		static bool RunningAsAdmin(DWORD pid);

		static bool ReadMemory(DWORD pid, DWORD64 address, PVOID buffer, DWORD64 size);
		static bool WriteMemory(DWORD pid, DWORD64 address, PVOID buffer, DWORD64 size);
		static bool Terminate(DWORD pid, int exitcode = -1);
		static bool Suspend(DWORD pid);
		static bool Resume(DWORD pid);
		static bool Exit(int exitcode = -1);

		static bool SetDebugPrivilege(DWORD pid = -1);
		static bool RestartAsAdmin(DWORD pid = -1);

		static bool IsWow64(DWORD pid);

		static bool IsValid(DWORD pid);
	private:
	};

	class ProcessEnum {
	public:
		bool Add(Process *p);
		ProcessList Added();
		ProcessList Removed();
		ProcessList Updated();
		bool HasUpdated(const ProcessList &updated);
		ProcessList &List();
		void Clear();
	private:
		ProcessList list;
	};

	class ProcessCPU {
	public:
		ProcessCPU();
		~ProcessCPU();

		void Init(DWORD pid);
		bool Initialized();

		float GetUsage();

		void Cleanup();
	private:
		HANDLE self;
		DWORD process_id;
		int num_processors;
		ULARGE_INTEGER last_cpu;
		ULARGE_INTEGER last_kernel_cpu;
		ULARGE_INTEGER last_user_cpu;
		float usage;
		bool initialized;
		HighResolutionTimer timer;
	};

	extern DWORD GetAccountSid(DWORD pid);

}

#undef QueryFullProcessImageName
#ifdef _UNICODE
#define QueryFullProcessImageName QueryFullProcessImageNameW
#else
#define QueryFullProcessImageName QueryFullProcessImageNameA
#endif

#undef GetModuleFileNameEx
#ifdef _UNICODE
#define GetModuleFileNameEx GetModuleFileNameExW
#else
#define GetModuleFileNameEx GetModuleFileNameExA
#endif

#undef GetFileVersionInfoSize
#ifdef _UNICODE
#define GetFileVersionInfoSize GetFileVersionInfoSizeW
#else
#define GetFileVersionInfoSize GetFileVersionInfoSizeA
#endif

#undef GetFileVersionInfo
#ifdef _UNICODE
#define GetFileVersionInfo GetFileVersionInfoW
#else
#define GetFileVersionInfo GetFileVersionInfoA
#endif

#undef VerQueryValue
#ifdef _UNICODE
#define VerQueryValue VerQueryValueW
#else
#define VerQueryValue VerQueryValueA
#endif

#undef LookupAccountSid
#ifdef _UNICODE
#define LookupAccountSid LookupAccountSidW
#else
#define LookupAccountSid LookupAccountSidA
#endif

#undef GetSystemDirectory
#ifdef _UNICODE
#define GetSystemDirectory GetSystemDirectoryW
#else
#define GetSystemDirectory GetSystemDirectoryA
#endif

#undef ShellExecuteEx
#ifdef _UNICODE
#define ShellExecuteEx ShellExecuteExW
#else
#define ShellExecuteEx ShellExecuteExA
#endif

#endif