#include "Thread.h"
#include "Process.h"
#include "NT.h"

namespace GF {

	Thread::Thread(NT::PSYSTEM_THREADS st)
	{
		memcpy(&systemThreads, st, sizeof(NT::SYSTEM_THREADS));

		handle = NULL;

		/*HANDLE handle = OpenThread(THREAD_QUERY_INFORMATION, false, tid);
		if (!handle)
			return;

		NT::THREAD_BASIC_INFORMATION tbi = { 0 };
		NT::NTSTATUS status = NT::ZwQueryInformationThread(handle, NT::ThreadBasicInformation, &tbi, sizeof(tbi), NULL);

		if (status == 0) {
			teb_address = reinterpret_cast<DWORD_PTR>(tbi.TebBaseAddress);
			exit_status = tbi.ExitStatus;
			affinity_mask = tbi.AffinityMask;
		}

		CloseHandle(handle);*/
	}

	DWORD Thread::ThreadId()
	{
		return DWORD(systemThreads.ClientId.ThreadId);
	}

	DWORD Thread::ProcessId()
	{
		return DWORD(systemThreads.ClientId.ProcessId);
	}

	bool Thread::Context(CONTEXT &context, bool set)
	{
		return ThreadManager::Context(ThreadId(), context, set);
	}

	bool Thread::ContextWow64(WOW64_CONTEXT &context, bool set)
	{
		return ThreadManager::ContextWow64(ThreadId(), context, set);
	}

	bool Thread::Terminate(int exitcode)
	{
		return ThreadManager::Terminate(ThreadId(), exitcode);
	}

	bool Thread::Suspend()
	{
		return ThreadManager::Suspend(ThreadId());
	}

	bool Thread::Resume()
	{
		return ThreadManager::Resume(ThreadId());
	}

	HANDLE Thread::Open(DWORD dwDesiredAccess)
	{
		if (handle)
			CloseHandle(handle);

		handle = OpenThread(dwDesiredAccess, FALSE, ProcessId());

		return handle;
	}

	void Thread::Close()
	{
		if (handle)
			CloseHandle(handle);

		handle = NULL;
	}

	NT::PSYSTEM_THREADS Thread::Info()
	{
		return &systemThreads;
	}


	DWORD64 Thread::StartAddress()
	{
//		if (!systemThreads.StartAddress)
//#ifdef _AMD64_
//			systemThreads.StartAddress = ThreadManager::GetStartAddress(ThreadId());
//#else
//			systemThreads.StartAddress = DWORD(ThreadManager::GetStartAddress(ThreadId()));
//#endif

		return ThreadManager::GetStartAddress(ThreadId());
		//return systemThreads.StartAddress;
	}

	DWORD64 Thread::Teb32()
	{
		if (ProcessManager::IsWow64(ProcessId()))
			return Teb64() + 0x2000;

		return NULL;
	}

	DWORD64 Thread::Teb64()
	{
		HANDLE handle = OpenThread(THREAD_QUERY_LIMITED_INFORMATION, false, ThreadId());
		if (!handle)
			return NULL;
#ifdef _AMD64_
		NT::THREAD_BASIC_INFORMATION tbi = { 0 };
#else
		NT::THREAD_BASIC_INFORMATION_WOW64 tbi = { 0 };
#endif
		int ret = NT::ZwQueryInformationThread(handle, NT::ThreadBasicInformation, &tbi, sizeof(tbi), NULL);
		if (ret != 0) {
			CloseHandle(handle);
			return NULL;
		}

#ifndef _AMD64_
		tbi.TebBaseAddress -= 0x2000;
#endif

		CloseHandle(handle);

		return DWORD64(tbi.TebBaseAddress);
	}

	DWORD Thread::ExitStatus()
	{
		return NULL;
	}

	DWORD64 Thread::AffinityMask()
	{
		return NULL;
	}

	DWORD Thread::Priority()
	{
		return DWORD(systemThreads.Priority);
	}

	DWORD Thread::BasePriority()
	{
		return DWORD(systemThreads.BasePriority);
	}

	float Thread::UsedCPU()
	{
		return 0;
	}

	ThreadList ThreadManager::Enumerate(DWORD pid)
	{
		ThreadList buf;

		/*HANDLE snap = CreateToolhelp32Snapshot(TH32CS_SNAPTHREAD, NULL);
		if (snap == INVALID_HANDLE_VALUE)
		return buf;

		THREADENTRY32 te32 = { 0 };
		te32.dwSize = sizeof(te32);

		if (!Thread32First(snap, &te32)) {
		CloseHandle(snap);
		return buf;
		}
		do {
		if (te32.th32OwnerProcessID != pid && pid != -1)
		continue;

		buf.push_back(new Thread(te32.th32ThreadID, te32.th32OwnerProcessID));
		} while (Thread32Next(snap, &te32));

		CloseHandle(snap);*/

		DWORD size = 0x20000;
		PVOID buffer = malloc(size);
		DWORD dwLength = 0;

		while (NT::ZwQuerySystemInformation(NT::SystemProcessInformation, buffer, size, &dwLength) == STATUS_INFO_LENGTH_MISMATCH)
			buffer = realloc(buffer, size *= 2);

		NT::PSYSTEM_PROCESSES sp = reinterpret_cast<NT::PSYSTEM_PROCESSES>(buffer);

		do {
			sp = reinterpret_cast<NT::PSYSTEM_PROCESSES>(ULONG_PTR(sp) + sp->NextEntryDelta);

			if (pid != sp->ProcessId && pid != -1)
				continue;

			NT::PSYSTEM_THREADS st = (NT::PSYSTEM_THREADS)&sp->Threads[0];
#ifdef _AMD64_
			st++;
#endif
			for (register DWORD i = 0; i < sp->ThreadCount; i++) {
				buf.push_back(new Thread(st));
				st++;
			}

		} while (sp->NextEntryDelta);

		free(buffer);

		return buf;
	}

	int ThreadManager::FreeList(ThreadList &ptl)
	{
		int num_destroyed = 0;

		for (ThreadListIterator it = ptl.begin(); it != ptl.end(); it++, num_destroyed++)
			delete *it;

		ptl.clear();

		return num_destroyed;
	}

	Thread *ThreadManager::GetByThreadId(DWORD tid, ThreadList *ptl, bool copy)
	{
		if (tid == 0)
			return NULL;

		ThreadList list = ptl ? *ptl : Enumerate();
		Thread *pt = nullptr;

		for (ThreadListIterator it = list.begin(); it != list.end(); it++) {
			if (tid != (*it)->ThreadId())
				continue;

			if (copy)
				pt = new Thread((*it)->Info());
			else
				pt = *it;
		}

		if (!ptl)
			FreeList(list);

		return pt;
	}

	DWORD ThreadManager::GetProcessId(DWORD tid)
	{
		HANDLE handle = OpenThread(THREAD_QUERY_LIMITED_INFORMATION, false, tid);
		if (!handle)
			return NULL;

		DWORD process_id = GetProcessIdOfThread(handle);

		CloseHandle(handle);

		return process_id;
	}

	DWORD ThreadManager::GetProcessId(HANDLE handle)
	{
		return GetProcessIdOfThread(handle);
	}

	bool ThreadManager::Context(DWORD tid, CONTEXT &context, bool set)
	{
		HANDLE handle = OpenThread(THREAD_GET_CONTEXT, FALSE, tid);
		if (!handle)
			return false;

		Suspend(tid);

		BOOL ret = set ?
			SetThreadContext(handle, &context) :
			GetThreadContext(handle, &context);

		Resume(tid);

		CloseHandle(handle);

		return ret != 0;
	}

	bool ThreadManager::ContextWow64(DWORD tid, WOW64_CONTEXT &context, bool set)
	{
		HANDLE handle = OpenThread(THREAD_GET_CONTEXT | THREAD_QUERY_INFORMATION, FALSE, tid);
		if (!handle)
			return false;

		Suspend(tid);

		BOOL ret = set ?
			Wow64SetThreadContext(handle, &context) :
			Wow64GetThreadContext(handle, &context);

		Resume(tid);

		CloseHandle(handle);

		return ret != 0;
	}

	DWORD64 ThreadManager::GetStartAddress(DWORD tid)
	{
		DWORD64 start_address = 0;

		HANDLE handle = OpenThread(THREAD_QUERY_INFORMATION, false, tid);
		if (!handle)
			return NULL;

		NT::NTSTATUS status = NT::ZwQueryInformationThread(handle, NT::ThreadQuerySetWin32StartAddress, &start_address, sizeof(DWORD_PTR), NULL);
		if (status != 0)
			start_address = NULL;

		CloseHandle(handle);

		return start_address;
	}

	DWORD ThreadManager::GetExitcode(DWORD tid)
	{
		HANDLE handle = OpenThread(THREAD_QUERY_LIMITED_INFORMATION, FALSE, tid);
		if (!handle)
			return NULL;

		DWORD exitcode = 0;
		GetExitCodeThread(handle, &exitcode);
		CloseHandle(handle);

		return exitcode;
	}

	DWORD ThreadManager::GetNumThreads()
	{
		DWORD count = 0;
		DWORD dwLength = 0;
		ULONG sizeBuffer = 0x20000;
		PVOID buffer = malloc(sizeBuffer);

		while (NT::ZwQuerySystemInformation(NT::SystemProcessInformation, buffer, sizeBuffer, &dwLength) == STATUS_INFO_LENGTH_MISMATCH)
			buffer = realloc(buffer, sizeBuffer *= 2);

		NT::PSYSTEM_PROCESSES sp = reinterpret_cast<NT::PSYSTEM_PROCESSES>(buffer);

		do {
			sp = reinterpret_cast<NT::PSYSTEM_PROCESSES>(ULONG_PTR(sp) + sp->NextEntryDelta);
			count += sp->ThreadCount;
		} while (sp->NextEntryDelta);

		free(buffer);

		return count;
	}

	bool ThreadManager::Terminate(DWORD tid, int exitcode)
	{
		HANDLE handle = OpenThread(THREAD_TERMINATE | SYNCHRONIZE, FALSE, tid);
		if (!handle)
			return false;

		BOOL ret = TerminateThread(handle, exitcode == -1 ? GetExitcode(tid) : exitcode);
		WaitForSingleObject(handle, 1000);
		CloseHandle(handle);

		return ret != 0;
	}

	bool ThreadManager::Suspend(DWORD tid)
	{
		HANDLE handle = OpenThread(THREAD_SUSPEND_RESUME, FALSE, tid);
		if (!handle)
			return false;

		DWORD count = SuspendThread(handle);

		CloseHandle(handle);

		return count != (DWORD)-1;
	}

	bool ThreadManager::Resume(DWORD tid)
	{
		HANDLE handle = OpenThread(THREAD_SUSPEND_RESUME, FALSE, tid);
		if (!handle)
			return false;

		DWORD count = ResumeThread(handle);

		CloseHandle(handle);

		return count != (DWORD)-1;
	}

	HANDLE ThreadManager::CreateThread(LPTHREAD_START_ROUTINE routine)
	{
		HANDLE handle = ::CreateThread(0, 0, routine, 0, 0, 0);
		//CloseHandle(handle);

		return handle;
	}

	bool ThreadManager::IsValid(DWORD tid)
	{
		if (tid == NULL)
			return false;

		//TODO: improve functionality
		HANDLE handle = OpenThread(THREAD_QUERY_LIMITED_INFORMATION, FALSE, tid);
		if (!handle)
			return false;

		CloseHandle(handle);

		return true;
	}

	ThreadEnum::ThreadEnum(DWORD process_id)
	{
		pid = process_id;
	}

	bool ThreadEnum::Add(Thread *t)
	{
		for (ThreadListIterator it = list.begin(); it != list.end(); it++)
			if (t->ThreadId() == (*it)->ThreadId())
				return false;

		if (!ThreadManager::IsValid(t->ThreadId()))
			return false;

		list.push_back(new Thread(t->Info()));

		return true;
	}

	ThreadList ThreadEnum::Added()
	{
		ThreadList buf = ThreadManager::Enumerate();
		ThreadList added;

		for (ThreadListIterator it = buf.begin(); it != buf.end(); it++)
			if (Add(*it))
				added.push_back(new Thread((*it)->Info()));

		ThreadManager::FreeList(buf);

		return added;
	}

	ThreadList ThreadEnum::Removed()
	{
		ThreadList removed;

		for (ThreadListIterator it = list.begin(); it != list.end(); it++) {

			if (!ThreadManager::IsValid((*it)->ThreadId())) {
				removed.push_back(new Thread((*it)->Info()));
				delete *it;
				it = list.erase(it);
			}
		}

		return removed;
	}

	ThreadList ThreadEnum::Updated()
	{
		ThreadList added = Added();
		ThreadList removed = Removed();
		ThreadList updated;

		for (ThreadListIterator it = added.begin(); it != added.end(); it++)
			updated.push_back(new Thread((*it)->Info()));

		for (ThreadListIterator it = removed.begin(); it != removed.end(); it++)
			updated.push_back(new Thread((*it)->Info()));

		ThreadManager::FreeList(added);
		ThreadManager::FreeList(removed);

		return updated;
	}

	bool ThreadEnum::HasUpdated(const ThreadList &updated)
	{
		return updated.size() > 0;
	}

	ThreadList &ThreadEnum::List()
	{
		return list;
	}

	void ThreadEnum::Clear()
	{
		ThreadManager::FreeList(list);
	}

	DWORD ThreadEnum::Pid()
	{
		return pid;
	}

}