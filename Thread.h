#ifndef __GTHREAD__
#define __GTHREAD__

#include <list>
#include <windows.h>
#include <tlhelp32.h>
#include "NT.h"

namespace GF {

	class Thread {
	public:
		Thread(NT::PSYSTEM_THREADS st);

		DWORD ThreadId();
		DWORD ProcessId();

		bool Context(CONTEXT &context, bool set = false);
		bool ContextWow64(WOW64_CONTEXT &context, bool set = false);

		bool Terminate(int exitcode = -1);
		bool Suspend();
		bool Resume();
		HANDLE Open(DWORD dwDesiredAccess = THREAD_ALL_ACCESS);
		void Close();

		NT::PSYSTEM_THREADS Info();

		DWORD64 StartAddress();
		DWORD64 Teb32();
		DWORD64 Teb64();
		DWORD ExitStatus();
		DWORD64 AffinityMask();
		DWORD Priority();
		DWORD BasePriority();
		float UsedCPU();
	private:
		HANDLE handle;
		NT::SYSTEM_THREADS systemThreads;
	};

	typedef std::list<Thread*> ThreadList;
	typedef std::list<Thread*>::iterator ThreadListIterator;

	class ThreadManager {
	public:

		static ThreadList Enumerate(DWORD pid = -1);
		static int FreeList(ThreadList &ptl);

		static Thread *GetByThreadId(DWORD tid, ThreadList *ptl = NULL, bool copy = true);

		static DWORD GetProcessId(DWORD tid);
		static DWORD GetProcessId(HANDLE handle);
		static bool Context(DWORD tid, CONTEXT &context, bool set = false);
		static bool ContextWow64(DWORD tid, WOW64_CONTEXT &context, bool set = false);
		static DWORD64 GetStartAddress(DWORD tid);
		static DWORD GetExitcode(DWORD tid);
		DWORD ThreadManager::GetNumThreads();

		static bool Terminate(DWORD tid, int exitcode = -1);
		static bool Suspend(DWORD tid);
		static bool Resume(DWORD tid);

		static HANDLE CreateThread(LPTHREAD_START_ROUTINE routine);

		static bool IsValid(DWORD tid);
	private:
	};

	class ThreadEnum {
	public:
		ThreadEnum(DWORD process_id = 0);
		bool Add(Thread *t);
		ThreadList Added();
		ThreadList Removed();
		ThreadList Updated();
		bool HasUpdated(const ThreadList &updated);
		ThreadList &List();
		void Clear();
		DWORD Pid();
	private:
		ThreadList list;
		DWORD pid;
	};

}

#endif