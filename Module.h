#ifndef __GMODULE__
#define __GMODULE__

#include <list>
#include <windows.h>
#include <tlhelp32.h>
#include "GString.h"

namespace GF {

	class Module {
	public:
		Module(MODULEENTRY32 me32);

		DWORD ProcessId();
		DWORD64 Address();
		DWORD64 Size();
		HMODULE Module_();
		GString Name();
		GString Path();
	private:
		DWORD pid;
		HMODULE module;
		GString name;
		GString path;
		DWORD64 address;
		DWORD64 size;
	};

	typedef std::list<Module> ModuleList;
	typedef std::list<Module>::iterator ModuleListIterator;

	class ModuleManager {
	public:
		static ModuleList Enumerate(DWORD process_id);

		static Module *GetByName(GString module_name, DWORD process_id);
		static Module *GetByName(GString module_name, ModuleList *ml);

		static void GetModuleNameFromPeb32(DWORD pid, DWORD64 peb, DWORD64 startaddress, GString &out);
		static void GetModuleNameFromPeb64(DWORD pid, DWORD64 peb, DWORD64 startaddress, GString &out);

		static bool IsValid(DWORD64 module_address);
		static bool IsValid(GString module_name);
	private:
	};

}

#endif