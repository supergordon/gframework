#ifndef __GSERVICE__
#define __GSERVICE__

#include <list>
#include <windows.h>
#include "GString.h"

namespace GF {

	class Service {
	public:

		Service(const GString &service_name, const GString &display_name = GEmptyString, SERVICE_STATUS_PROCESS *p_ssp = NULL);

		const GString &Name();
		const GString &DisplayName();
		const GString &Path();
		const GString &LoadOrderGroup();
		const GString &Dependencies();
		const GString &StartName();
		GString TypeName();
		GString StartTypeName();
		GString ErrorControlName();
		GString CurrentStateName();
		SERVICE_STATUS_PROCESS *Status();
		DWORD Type();
		DWORD StartType();
		DWORD ErrorControl();
		DWORD CurrentState();
		DWORD TagId();
		DWORD ProcessId();
		DWORD Exitcode();
		DWORD ControlsAccepted();
		bool RunsInSystemProcess();
		bool Start();
		bool Stop();
		bool Unregister();
	private:
		GString name;
		GString display;
		GString path;
		GString loadordergroup;
		GString dependencies;
		GString start_name;

		SERVICE_STATUS_PROCESS ssp;
		DWORD start_type;
		DWORD error_control;
		DWORD tag_id;
	};

	typedef std::list<Service*> ServiceList;
	typedef std::list<Service*>::iterator ServiceListIterator;

	class ServiceManager {
	public:
		static ServiceList Enumerate();
		static int FreeList(ServiceList &sl);

		static Service *GetByName(const GString &service_name, ServiceList *sl = NULL, bool copy = true);

		static SC_HANDLE GetSCManager();

		static bool StartService_(const GString &service_name);
		static bool StopService_(const GString &service_name);
		static bool RegisterService_(const GString &service_name, const GString &file_path);
		static bool UnregisterService_(const GString &service_name);
		static bool LoadService(const GString &service_name, const GString &file_path);
		static bool UnloadService(const GString &service_name);

		static bool IsValid(const GString &service_name);
	private:
	};

	class ServiceEnum {
	public:
		bool Add(Service *p);
		ServiceList Added();
		ServiceList Removed();
		ServiceList Updated();
		bool HasUpdated(const ServiceList &updated);
		ServiceList &List();
		void Clear();
	private:
		ServiceList list;
	};

}

#undef OpenService
#ifdef _UNICODE
#define OpenService OpenServiceW
#else
#define OpenService OpenServiceA
#endif

#undef CreateService
#ifdef _UNICODE
#define CreateService CreateServiceW
#else
#define CreateService CreateServiceA
#endif

#undef EnumServicesStatusEx
#ifdef _UNICODE
#define EnumServicesStatusEx EnumServicesStatusExW
#else
#define EnumServicesStatusEx EnumServicesStatusExA
#endif

#undef CreateFile
#ifdef _UNICODE
#define CreateFile CreateFileW
#else
#define CreateFile CreateFileA
#endif

#undef QueryServiceConfig
#ifdef _UNICODE
#define QueryServiceConfig QueryServiceConfigW
#else
#define QueryServiceConfig QueryServiceConfigA
#endif

#undef GetWindowsDirectory
#ifdef _UNICODE
#define GetWindowsDirectory GetWindowsDirectoryW
#else
#define GetWindowsDirectory GetWindowsDirectoryA
#endif

#endif